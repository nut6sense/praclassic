<?php

/**
 * This is the model class for table "news".
 *
 * The followings are the available columns in table 'news':
 * @property integer $newsid
 * @property string $user_id
 * @property string $newsname
 * @property string $newssubdetail
 * @property string $newsdetail
 * @property string $newspic
 */
class News extends CActiveRecord {
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return News the static model class
	 */
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()	{
		return 'news';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, newsname, newssubdetail, newsdetail', 'required'),
			array('user_id', 'length', 'max'=>11),
			array('newsname', 'length', 'max'=>50),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('newsid, user_id, newsname, newssubdetail, newsdetail, newspic', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations() {
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()	{
		return array(
			'newsid' => 'Newsid',
			'user_id' => 'User',
			'newsname' => 'ชื่อ',
			'newssubdetail' => 'รายละเอียดส่วนย่อย',
			'newsdetail' => 'รายละเอียด',
			'newspic' => 'รูปภาพ',
		);
	}


	public function tblNewsData()	{
		$tblCategory = null;
		$newsQuery = self::Model()->findAllByAttributes(array('user_id' => 'Admin'), array('order'=>'newsid'));
		foreach ($newsQuery as $newsKey => $newsValue) {
			$tblCategory .= CHtml::openTag('tr');
			$tblCategory .= CHtml::openTag('td');
      $tblCategory .= CHtml::checkBox('NEWS_DELETE['.$newsValue['newsid'].']',false,array('value'=>$newsValue['newsid']));
      $tblCategory .= CHtml::closeTag('td');
      $tblCategory .= CHtml::openTag('td');
      $tblCategory .= ($newsKey+1);
      $tblCategory .= CHtml::closeTag('td');
      $tblCategory .= CHtml::openTag('td');
      $tblCategory .= $newsValue['newsname'];
      $tblCategory .= CHtml::closeTag('td');
      $tblCategory .= CHtml::openTag('td');
      $tblCategory .= CHtml::link('แก้ไข', '#', array('id' => 'btn-edit', 'name'=>'editNews', 'value_id'=>$newsValue['newsid']));
      $tblCategory .= CHtml::closeTag('td');
      $tblCategory .= CHtml::closeTag('tr');
		}
		return $tblCategory;
	}

	public function getDataNews($page=null){
		$data = null;
		if ($page == 'index') {
			$AllNews = News::model()->findAll(array('order'=>'newsid DESC','limit'=>4));
			$head = '<h1>ข่าวประชาสัมพันธ์</h1>';
		}else{
			$AllNews = News::model()->findAll(array('order'=>'newsid DESC'));
			$head = '<h4>ข่าวประชาสัมพันธ์</h4>';
		}

		$settingModel = Setting::model()->findByAttributes(array('setting_id'=>1));
		$param = json_decode($settingModel->setting_parameter);

		$layoutNews = $param->layoutNews;

		if (count($AllNews)!=0) {

			$data .= CHtml::openTag('div',array('class'=>'row-fluid headerForm sizeContent'));
	    $data .= CHtml::openTag('div',array('class'=>'span12'));
			$data .= $head;
	    $data .= CHtml::closeTag('div');
	    $data .= CHtml::closeTag('div');
		}

		switch ($layoutNews) {
			case '1':
				foreach ($AllNews as $key => $value) {

		      $img = CHtml::image(Yii::app()->request->baseUrl.'/img/newsPic/'.$value['newspic'], null, array('style'=>'width:100px;','class'=>'img-polaroid')); 

					$data .= CHtml::openTag('div',array('class'=>'row-fluid news-topic'));
					$data .= CHtml::openTag('div',array('class'=>'span2'));
	      	$data .= CHtml::link($img, Yii::app()->request->baseUrl.'/news/newsDetail/'.$value['newsid']);
			    $data .= CHtml::closeTag('div');
			    $data .= CHtml::openTag('div',array('class'=>'span10'));
	      	$data .= CHtml::link('<h4>'.$value['newsname'].'</h4>', Yii::app()->request->baseUrl.'/news/newsDetail/'.$value['newsid']);
					$data .= $value['newssubdetail'];
			    $data .= CHtml::closeTag('div');
			    $data .= CHtml::closeTag('div');

		  	}
				break;

			case '2':
				$i = 1;
				$end = count($AllNews);
				foreach ($AllNews as $key => $value) {
					
		      $img = CHtml::image(Yii::app()->request->baseUrl.'/img/newsPic/'.$value['newspic'], null, array('style'=>'width:100px;','class'=>'img-polaroid')); 
		      if (($i%2)==1) {
						$data .= CHtml::openTag('div',array('class'=>'row-fluid news-topic'));
		      }
					$data .= CHtml::openTag('div',array('class'=>'span2'));
	      	$data .= CHtml::link($img, Yii::app()->request->baseUrl.'/news/newsDetail/'.$value['newsid']);
			    $data .= CHtml::closeTag('div');
			    $data .= CHtml::openTag('div',array('class'=>'span4'));
	      	$data .= CHtml::link('<h4>'.$value['newsname'].'</h4>', Yii::app()->request->baseUrl.'/news/newsDetail/'.$value['newsid']);
					$data .= $value['newssubdetail'];
			    $data .= CHtml::closeTag('div');

			    if (($i%2)==0 || $i==$end) {
			    	$data .= CHtml::closeTag('div');
		      }

		      $i++;
		  	}
				break;
		}

		if ($page == 'index') {
			if (count($AllNews)!=0) {
				$text = CHtml::openTag('i',array('class'=>'icon-chevron-left icon-white','style'=>'margin: 0;'));
		    $text .= CHtml::closeTag('i');
		    $text .= 'ชมทั้งหมด';
		    $text .= CHtml::openTag('i',array('class'=>'icon-chevron-right icon-white'));
		    $text .= CHtml::closeTag('i');

				$data .= CHtml::openTag('div',array('class'=>'row-fluid news-topic'));
				$data .= CHtml::openTag('div',array('class'=>'span12','style'=>'text-align: right;'));
	    	$data .= CHtml::link($text, Yii::app()->request->baseUrl.'/site/news',array('style'=>'font-weight: bold;text-decoration: underline;'));
		    $data .= CHtml::closeTag('div');
		    $data .= CHtml::closeTag('div');
				$data .= '<hr>';
			}
		}else{
			$data .= '<hr>';
		}
	    return $data;
	}

}
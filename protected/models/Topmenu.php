<?php

/**
 * This is the model class for table "topmenu".
 *
 * The followings are the available columns in table 'topmenu':
 * @property integer $topmenu_id
 * @property string $topmenu_topic
 * @property string $topmenu_link
 * @property integer $topmenu_number
 */
class Topmenu extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Topmenu the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'topmenu';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('topmenu_topic, topmenu_link, topmenu_number', 'required'),
			array('topmenu_number', 'numerical', 'integerOnly'=>true),
			array('topmenu_topic', 'length', 'max'=>50),
			array('topmenu_link', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('topmenu_id, topmenu_topic, topmenu_link, topmenu_number', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'topmenu_id' => 'Topmenu',
			'topmenu_topic' => 'Topmenu Topic',
			'topmenu_link' => 'Topmenu Link',
			'topmenu_number' => 'Topmenu Number',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('topmenu_id',$this->topmenu_id);
		$criteria->compare('topmenu_topic',$this->topmenu_topic,true);
		$criteria->compare('topmenu_link',$this->topmenu_link,true);
		$criteria->compare('topmenu_number',$this->topmenu_number);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function getTopMenu(){
  	$topMenu = Topmenu::model()->findAll(array('order'=>'topmenu_number ASC'));
  	$table = null;

  	$dataNumber = array();
  	foreach ($topMenu as $key => $value) {
  		$dataNumber[$value['topmenu_number']] = $value['topmenu_number'];
  	}

  	foreach ($topMenu as $key => $value) {
  		$table .= CHtml::openTag('tr');
	  	$table .= CHtml::openTag('td');
	  	$table .= CHtml::dropDownList('number['.$value['topmenu_number'].']', $value['topmenu_number'], $dataNumber, array('class'=>'span12', 'no-menu' => $value['topmenu_number']));
	  	$table .= CHtml::closeTag('td');
	  	$table .= CHtml::openTag('td');
	  	$table .= $value['topmenu_topic'];
	  	$table .= CHtml::closeTag('td');
	  	$table .= CHtml::closeTag('tr');
  	}
  	
  	return $table;
	}
}
<?php

/**
 * This is the model class for table "premium".
 *
 * The followings are the available columns in table 'premium':
 * @property integer $premiumid
 * @property string $premiumname
 * @property string $premiumdetail
 * @property string $premiumpic
 * @property string $premiumdate
 * @property integer $category_id
 * @property integer $view
 * @property integer $default
 */
class Premium extends CActiveRecord {
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Premium the static model class
	 */
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()	{
		return 'premium';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('premiumname, premiumdetail, premiumpic, premiumdate, category_id', 'required'),
			array('category_id, view, default', 'numerical', 'integerOnly'=>true),
			array('premiumname', 'length', 'max'=>50),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('premiumid, premiumname, premiumdetail, premiumpic, premiumdate, category_id, view, default', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()	{
		return array(
			'premiumid' => 'Premiumid',
			'premiumname' => 'ชื่อพระ',
			'premiumdetail' => 'รายละเอียด',
			'premiumpic' => 'Premiumpic',
			'premiumdate' => 'Premiumdate',
			'category_id' => 'หมวดหมู่',
			'view' => 'view',
			'default' => 'ตั้งเป็นหน้าปก Premium',
			//
			'title_premiumname' => 'ชื่อพระ',
			'title_premiumdetail' => 'รายละเอียด',
			'title_premiumdate' => 'เมื่อวันที่',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search() {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('premiumid',$this->premiumid);
		$criteria->compare('premiumname',$this->premiumname,true);
		$criteria->compare('premiumdetail',$this->premiumdetail,true);
		$criteria->compare('premiumpic',$this->premiumpic,true);
		$criteria->compare('premiumdate',$this->premiumdate,true);
		$criteria->compare('category_id',$this->category_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function tblDataPremium()	{
		$tblDetail = null;
		$dataQuery = Yii::app()->db->createCommand('SELECT * FROM premium ORDER BY premiumid ASC')->query();
		foreach ($dataQuery as $key => $value) {
			$tblDetail .= CHtml::openTag('tr');
			$tblDetail .= CHtml::openTag('td');
      $tblDetail .= CHtml::checkBox('PRM_DELETE['.$value['premiumid'].']',false,array('value'=>$value['premiumid']));
      $tblDetail .= CHtml::closeTag('td');
      $tblDetail .= CHtml::openTag('td');
      $tblDetail .= ($key+1);
      $tblDetail .= CHtml::closeTag('td');
      $tblDetail .= CHtml::openTag('td');
      $tblDetail .= CHtml::link($value['premiumname'], '#', array('id' => 'btn-edit', 'name'=>'editPremium', 'value_id'=>$value['premiumid']));
      $tblDetail .= CHtml::closeTag('td');
      $tblDetail .= CHtml::openTag('td');
      $tblDetail .= $value['view'];
      $tblDetail .= CHtml::closeTag('td');
      $tblDetail .= CHtml::openTag('td');
      $tblDetail .= $value['premiumdate'];
      $tblDetail .= CHtml::closeTag('td');
      $tblDetail .= CHtml::openTag('td');
      // $tblDetail .= CHtml::link('แก้ไข', '#', array('id' => 'btn-edit', 'name'=>'editPremium', 'value_id'=>$value['premiumid']));
      $tblDetail .= CHtml::closeTag('td');
      $tblDetail .= CHtml::closeTag('tr');
		}
		return $tblDetail;
	}

	public function showPremium() {
		$loop = 1;
		$dialog_content = null;
		$premiumQuery = Yii::app()->db->createCommand('SELECT * FROM premium ORDER BY premiumid ASC')->query();
		foreach ($premiumQuery as $key => $value) {
			$findCategory = Category::model()->findByAttributes(array('category_id'=>$value['category_id']));

			$decodePicture = json_decode($value['premiumpic'], true);
	    $picPath = Yii::app()->request->baseUrl.'/img/premiumPic/'.$decodePicture[1]['picName'];
	    $img = CHtml::image($picPath, null, array('id'=>$value['premiumid'], 'class'=>'picProduct')).'<br>'; 

	    if(($loop-1)%5==0 || $loop==1) {
				$dialog_content .= CHtml::openTag('div', array('class'=>'row-fluid', 'style'=>'margin-bottom:30px;'));
	    }

		  $dialog_content .= CHtml::openTag('div', array('class'=>'span2 picProductBox margin2'));
		  $dialog_content .= CHtml::openTag('div', array('class'=>'picProductSize','style'=>'margin: 6px 3px;'));
		  $dialog_content .= CHtml::link($img, Yii::app()->request->baseUrl.'/site/premiumDescription/'.$value['premiumid']);
		  $dialog_content .= CHtml::closeTag('div');

      $dialog_content .= CHtml::openTag('div',array('class'=>'fixBox'));
		  $dialog_content .= $value['premiumname'];
		  $dialog_content .= CHtml::closeTag('div');

		  $dialog_content .= CHtml::openTag('div',array('class'=>'fixBox'));
		  $dialog_content .= '&nbsp';
		  $dialog_content .= CHtml::closeTag('div');

			$dialog_content .= CHtml::openTag('div',array('class'=>'fixBox'));
		  $dialog_content .= 'ประเภท : '.$findCategory->category_name;
		  $dialog_content .= CHtml::closeTag('div');
		  $dialog_content .= CHtml::closeTag('div');

		  if($loop%5==0 || $loop==(sizeof($premiumQuery)))  {
		  	$dialog_content .= CHtml::closeTag('div');
		  }

		  $loop++;
		}
		
		if(sizeof($premiumQuery)<5) {
			$dialog_content .= CHtml::closeTag('div');
		}
		
		return $dialog_content;
	}

	public function activePremium() {
		$findPremium = self::model()->findByAttributes(array('default'=>1));
		if(sizeof($findPremium)) {
			$decodePicture = json_decode($findPremium['premiumpic'], true);
			$picPath = Yii::app()->request->baseUrl.'/img/premiumPic/'.$decodePicture[1]['picName'];
			$img = CHtml::image($picPath, null, array('class'=>'img-polaroid')); 
		}else{
			$picPath = Yii::app()->request->baseUrl.'/img/none.jpg';
			$img = CHtml::image($picPath, null, array('class'=>'img-polaroid')); 
		}

		return CHtml::link($img, Yii::app()->request->baseUrl.'/site/premium');
	}

}
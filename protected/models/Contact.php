<?php

/**
 * This is the model class for table "contact".
 *
 * The followings are the available columns in table 'contact':
 * @property integer $cid
 * @property string $type
 * @property string $name
 * @property string $message
 * @property string $email
 * @property string $telephone
 * @property string $created
 * @property string $ipaddress
 * @property string $subject
 * @property string $status
 * @property string $param
 */
class Contact extends CActiveRecord
{
	public $picture;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Contact the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'contact';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('type, name, subject, message, email, telephone', 'required'),
			array('type, email, telephone, ipaddress', 'length', 'max'=>50),
			array('name', 'length', 'max'=>100),
			array('param', 'safe'),

			array('email', 'email', 'message'=>'รูปแบบอีเมล์ไม่ถูกต้อง'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('cid, type, name, subject, message, email, telephone, created, ipaddress, status, param', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'cid' => 'Cid',
			'type' => 'ประเภทการติดต่อ',
			'subject' => 'หัวข้อการแจ้ง',
			'name' => 'ชื่อ-นามสกุล',
			'message' => 'ข้อความ',
			'email' => 'อีเมล',
			'telephone' => 'เบอร์โทรศัพท์',
			'created' => 'Created',
			'ipaddress' => 'Ipaddress',
			'status' => 'status',
			'param'=>'param',
			//
			'picture'=>'รูปภาพ',
			'from'=>'ข้อความจาก',
			'title_email'=>'อีเมล',
			'title_subject'=>'หัวข้อ',
			'title_message'=>'ข้อความ',
			'send_to_email'=>'ตอบกลับถึง',
			'send_from_email'=>'ตอบกลับจาก',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('cid',$this->cid);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('subject',$this->subject,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('message',$this->message,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('telephone',$this->telephone,true);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('ipaddress',$this->ipaddress,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	public function getContactType() {
		$contactType = array(
			// '1'=>'สอบถามข้อมูล',
			// '2'=>'ติดต่อซื้อ',
			// '3'=>'ติดต่อขาย',
			'4'=>'แจ้งปัญหา หรือร้องเรียน',
			'5'=>'ติชมแนะนำเว็บไซต์',
		);

		return array(null => 'โปรดเลือกหัวข้อการติดต่อ') + $contactType;
	}

	public function getAlertMessage($type){
		$messageQuery = Contact::model()->findAllByAttributes(array('type'=>$type,'status'=>0));
		return count($messageQuery);
	}

	public function tblMessage($type)	{
		$messageQuery = Contact::model()->findAllByAttributes(array('type'=>$type,'status'=>0));
		$tblDetail = null;
		foreach ($messageQuery as $Key => $Value) {
			
			$created = explode(' ', $Value['created']);
			$time = $created[1];
			$created = explode('-', $created[0]);
			$created = $created[2].'/'.$created[1].'/'.$created[0].' ('.$time.')';

			$tblDetail .= CHtml::openTag('tr');

			$tblDetail .= CHtml::openTag('td',array('style'=>'width: 30px'));
      $tblDetail .= CHtml::checkBox('MESSAGE_DELETE['.$Value['cid'].']',false,array('value'=>$Value['cid']));
      $tblDetail .= CHtml::closeTag('td');

      $tblDetail .= CHtml::openTag('td');
      $tblDetail .= CHtml::link($Value['subject'], Yii::app()->createUrl('contact/contactMessage/'.$Value['cid']), array('id' => 'btn-edit'));
      $tblDetail .= CHtml::closeTag('td');

      $tblDetail .= CHtml::openTag('td');
      $tblDetail .= $Value['name'];
      $tblDetail .= CHtml::closeTag('td');

      $tblDetail .= CHtml::openTag('td',array('style'=>'width: 200px'));
      $tblDetail .= $created;
      $tblDetail .= CHtml::closeTag('td');

      $tblDetail .= CHtml::closeTag('tr');
		}
		return $tblDetail;
	}

	public function showMessage($form = nill, $model = null)	{

		// $dialog_id = 'showMessage';
	 //  $dialog_heaher = 'ข้อมูลติชมแนะนำเว็บ';

	 //  $inputOption = array('class'=>'span7');
	 //  $htmlOptions = array('class'=>'span3', 'style'=>'margin-left:65px;');

	 //  $dialog_content = CHtml::openTag('div', array('class'=>'container-fluid'));

	 //  $dialog_content .= CHtml::openTag('div', array('class'=>'row-fluid'));
	 //  $dialog_content .= CHtml::openTag('div', $htmlOptions);
	 //  $dialog_content .= $form->labelEx($model, 'subject');
	 //  $dialog_content .= CHtml::closeTag('div');
	 //  $dialog_content .= CHtml::openTag('div');
	 //  $dialog_content .= $form->textField($model, 'subject', array('readonly'=>true)+$inputOption);
	 //  $dialog_content .= CHtml::closeTag('div');
	 //  $dialog_content .= CHtml::closeTag('div');

	 //  $dialog_content .= CHtml::openTag('div', array('class'=>'row-fluid'));
	 //  $dialog_content .= CHtml::openTag('div', $htmlOptions);
	 //  $dialog_content .= $form->labelEx($model, 'message');
	 //  $dialog_content .= CHtml::closeTag('div');
	 //  $dialog_content .= CHtml::openTag('div');
	 //  $dialog_content .= $form->textArea($model, 'message', array('readonly'=>true)+$inputOption);
	 //  $dialog_content .= CHtml::closeTag('div');
	 //  $dialog_content .= CHtml::closeTag('div');

	 //  $dialog_content .= CHtml::openTag('div', array('class'=>'row-fluid'));
	 //  $dialog_content .= CHtml::openTag('div', $htmlOptions);
	 //  $dialog_content .= $form->labelEx($model, 'name');
	 //  $dialog_content .= CHtml::closeTag('div');
	 //  $dialog_content .= CHtml::openTag('div');
	 //  $dialog_content .= $form->textField($model, 'name', array('readonly'=>true)+$inputOption);
	 //  $dialog_content .= CHtml::closeTag('div');
	 //  $dialog_content .= CHtml::closeTag('div');

	 //  $dialog_content .= CHtml::openTag('div', array('class'=>'row-fluid'));
	 //  $dialog_content .= CHtml::openTag('div', $htmlOptions);
	 //  $dialog_content .= $form->labelEx($model, 'telephone');
	 //  $dialog_content .= CHtml::closeTag('div');
	 //  $dialog_content .= CHtml::openTag('div');
	 //  $dialog_content .= $form->textField($model, 'telephone', array('readonly'=>true)+$inputOption);
	 //  $dialog_content .= CHtml::closeTag('div');
	 //  $dialog_content .= CHtml::closeTag('div');

	 //  $dialog_content .= CHtml::openTag('div', array('class'=>'row-fluid'));
	 //  $dialog_content .= CHtml::openTag('div', $htmlOptions);
	 //  $dialog_content .= $form->labelEx($model, 'email');
	 //  $dialog_content .= CHtml::closeTag('div');
	 //  $dialog_content .= CHtml::openTag('div');
	 //  $dialog_content .= $form->textField($model, 'email', array('readonly'=>true)+$inputOption);
	 //  $dialog_content .= CHtml::closeTag('div');
	 //  $dialog_content .= CHtml::closeTag('div');

	 //  $dialog_content .= CHtml::closeTag('div');

	  // Dialog::alertGetMessage($dialog_id, $dialog_heaher, $dialog_content);
	}

	public function tblPrivateMassage($user_id = null)	{
		$tblDetail = null;
		$contactQuery = Contact::model()->findAll(array('order'=>'cid'));
		$i = 1;
		foreach ($contactQuery as $key => $value) {
			$decode = json_decode($value->param, true);
			if($decode['user_id']==$user_id) {
				$tblDetail .= CHtml::openTag('tr');
				$tblDetail .= CHtml::openTag('td');
	      $tblDetail .= CHtml::checkBox('PM_DELETE['.$value->cid.']',false,array('value'=>$value->cid));
	      $tblDetail .= CHtml::closeTag('td');
	      $tblDetail .= CHtml::openTag('td');
	      $tblDetail .= $i;
	      $tblDetail .= CHtml::closeTag('td');
	      $tblDetail .= CHtml::openTag('td');
	      $tblDetail .= $value->created;
	      $tblDetail .= CHtml::closeTag('td');
	      $tblDetail .= CHtml::openTag('td');
	      //
	      // $tblDetail .= CHtml::link($value->subject, "#dialogPrivateMassage", array('id' => 'btn-edit', 'name'=>'dialogPrivateMassage', 'privateMassageId'=>$value['cid'], 'role'=>'button', 'data-toggle'=>'modal'));
	      $tblDetail .= CHtml::link($value->subject, Yii::app()->createUrl('shop/resendPrivateMessage/'.$user_id.'/'.$value['cid']));
	      //
	      $tblDetail .= CHtml::closeTag('td');
	      $tblDetail .= CHtml::openTag('td');
	      $tblDetail .= $value->name;
	      $tblDetail .= CHtml::closeTag('td');
	      $tblDetail .= CHtml::closeTag('tr');
	      $i++;
			}
		}
		return $tblDetail;
	}

	public function getPrivateMassageDetail($PrivateMassageId = null)	{
		$itemsDetail = array();
		$contactQuery = self::model()->findByAttributes(array('cid'=>$PrivateMassageId));
		foreach ($contactQuery as $key => $value) {
			$itemsDetail[$key] = $value;
		}
		$decode = json_decode($itemsDetail['param'], true);
		$itemsDetail['picture'] = isset($decode['picture'])?$decode['picture']:null;
		$itemsDetail['user_id'] = $decode['user_id'];
		$userQuery = Users::model()->findByAttributes(array('user_id'=>$itemsDetail['user_id']));
		$itemsDetail['shop_email'] = $userQuery->email;
		
		return $itemsDetail;
	}

}
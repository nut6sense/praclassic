<?php

/**
 * This is the model class for table "products".
 *
 * The followings are the available columns in table 'products':
 * @property integer $productid
 * @property string $user_id
 * @property string $productname
 * @property integer $productprice
 * @property string $productdetail
 * @property string $productpic
 * @property integer $productstatus
 * @property integer $productnoted
 * @property integer $category_id
 * @property integer $productdate
 * @property integer $approve
 * @property integer $view
 */
class Products extends CActiveRecord {

	public $productName2;
	public $page;
	public $product_1_pic;
	public $product_2_pic;
	public $product_3_pic;
	public $product_4_pic;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Products the static model class
	 */
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()	{
		return 'products';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, productname, category_id', 'required'),
			array('productstatus, productprice, productnoted, category_id, view', 'numerical', 'integerOnly'=>true),
			array('user_id, productname', 'length', 'max'=>50),
			array('productdetail, productpic', 'safe'),
			array('productdate', 'date', 'format'=>'yyyy-MM-dd'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('productid, user_id, productname, productprice, productdetail, productpic, productstatus, productnoted, productdate, category_id, approve, view', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()	{
		return array(
			'productid' => 'Productid',
			'user_id' => 'User',
			'productname' => 'ชื่อพระ',
			'productprice' => 'ราคา',
			'productdetail' => 'รายละเอียด',
			'productpic' => 'Productpic',
			'productstatus' => 'สถานะ',
			'productnoted' => 'Productnoted',
			'category_id' => 'Category',
			'productdate' => 'เมื่อวันที่',
			'approve'=>'approve',
			'view'=>'view',
			//
			'title_productname' => 'ชื่อพระ',
			'productName2' => 'productName2',
			'page' => 'page',
			'view' => 'จำนวนการเข้าดู',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search() {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('productid',$this->productid);
		$criteria->compare('user_id',$this->user_id,true);
		$criteria->compare('productname',$this->productname,true);
		$criteria->compare('productprice',$this->productprice);
		$criteria->compare('productdetail',$this->productdetail,true);
		$criteria->compare('productpic',$this->productpic,true);
		$criteria->compare('productstatus',$this->productstatus);
		$criteria->compare('productnoted',$this->productnoted);
		$criteria->compare('category_id',$this->category_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function getProductStatus($value = null) {
		$status = array(
			'1'=>'มาใหม่',
			'2'=>'โชว์พระ',
			'3'=>'ปล่อยแล้ว',
			'4'=>'ยังอยู่',
			'5'=>'โทรถาม',
			'6'=>'ระบุราคา',
			'7'=>'ไม่แสดง',
			);

		if (!is_null($value)) {
			$status = $status[$value];
		}

    return $status;
	}

	public function tblDataProduct()	{
		$tblDetail = null;
		$dataQuery = Yii::app()->db->createCommand('SELECT * FROM products LEFT JOIN category ON(products.category_id=category.category_id) WHERE user_id = '."'".Yii::app()->input->get('param1')."' ".' ORDER BY productid ASC')->query();
		foreach ($dataQuery as $key => $value) {
			$tblDetail .= CHtml::openTag('tr');
			$tblDetail .= CHtml::openTag('td');
      $tblDetail .= CHtml::checkBox('PRD_DELETE['.$value['productid'].']',false,array('value'=>$value['productid']));
      $tblDetail .= CHtml::closeTag('td');
      $tblDetail .= CHtml::openTag('td');
      $tblDetail .= ($key+1);
      $tblDetail .= CHtml::closeTag('td');
      $tblDetail .= CHtml::openTag('td');

      $proName = explode('&&', $value['productname']);
      $proName = implode($proName, ' ');
      $tblDetail .= CHtml::link($proName, Yii::app()->createUrl('products/editProduct/'.Yii::app()->input->get('param1').'/'.$value['productid']), array('id' => 'btn-edit'));

      $tblDetail .= CHtml::closeTag('td');
      $tblDetail .= CHtml::openTag('td');
      $tblDetail .= $value['productprice'];
      $tblDetail .= CHtml::closeTag('td');
      $tblDetail .= CHtml::openTag('td');
      $tblDetail .= $value['view'];
      $tblDetail .= CHtml::closeTag('td');
      $tblDetail .= CHtml::openTag('td');
      $tblDetail .= FormatToDate::formatDate($value['productdate']);
      $tblDetail .= CHtml::closeTag('td');
      $tblDetail .= CHtml::openTag('td');
      $tblDetail .= Products::model()->getProductStatus($value['productstatus']);
      $tblDetail .= CHtml::closeTag('td');
      // $tblDetail .= CHtml::openTag('td');
      // $tblDetail .= CHtml::link('แก้ไข', Yii::app()->createUrl('products/editProduct/'.Yii::app()->input->get('param1').'/'.$value['productid']), array('id' => 'btn-edit'));
      // $tblDetail .= CHtml::closeTag('td');
      $tblDetail .= CHtml::closeTag('tr');
		}
		return $tblDetail;
	}

	public function getProdcutDetail($post = null)	{
		if(sizeof($post)!=0) {
			$itemsDetail = new self;
			$dataQuery = self::model()->findByAttributes(array('productid' => $post));
			foreach ($dataQuery as $key => $value) {
				$itemsDetail->$key = $value;
			}
			$proName = explode('&&', $itemsDetail->productname);
			$itemsDetail->productname = $proName[0];
			$itemsDetail->productName2 = isset($proName[1])?$proName[1]:null;
			$productpic = json_decode($itemsDetail->productpic, true);
			$itemsDetail->product_1_pic = $productpic[1]['picName'];
			$itemsDetail->product_2_pic = $productpic[2]['picName'];
			$itemsDetail->product_3_pic = $productpic[3]['picName'];
			$itemsDetail->product_4_pic = $productpic[4]['picName'];
		}else{
			$itemsDetail = new self;
		}

		return $itemsDetail;
	}

	public function tblPopDataProduct($user_id = null)	{
		if(!is_null($user_id)) {
			$where = 'WHERE productstatus!=7 and user_id = "'.$user_id.'"';
		}else{
			$where = 'WHERE productstatus!=7';
		}
		$tblDetail = null;
		$dataQuery = Yii::app()->db->createCommand('SELECT * FROM products '.$where.' ORDER BY productid ASC')->query();
		foreach ($dataQuery as $key => $value) {
			$tblDetail .= CHtml::openTag('tr');
			$tblDetail .= CHtml::openTag('td');
      $tblDetail .= CHtml::checkBox('PRD_POPULAR['.$value['productid'].']',($value['productnoted']==0?false:true),array('value'=>$value['productid']));
      $tblDetail .= CHtml::closeTag('td');
      $tblDetail .= CHtml::openTag('td');
      $tblDetail .= ($key+1);
      $tblDetail .= CHtml::closeTag('td');
      $tblDetail .= CHtml::openTag('td');
      $tblDetail .= str_replace('&&', ' ', $value['productname']);
      $tblDetail .= CHtml::closeTag('td');
      $tblDetail .= CHtml::closeTag('tr');
		}
		return $tblDetail;
	}

	public function tblApproveProduct()	{
		$tblDetail = null;
		$dataQuery = Yii::app()->db->createCommand('SELECT * FROM products LEFT JOIN category ON(products.category_id=category.category_id) WHERE approve = 0 ORDER BY productid ASC')->query();
		foreach ($dataQuery as $key => $value) {
			$explode = explode('&&', $value['productname']);

			$tblDetail .= CHtml::openTag('tr');
			$tblDetail .= CHtml::openTag('td');
      $tblDetail .= CHtml::checkBox('PRD_DELETE['.$value['productid'].']',false,array('value'=>$value['productid']));
      $tblDetail .= CHtml::closeTag('td');
      $tblDetail .= CHtml::openTag('td');
      $tblDetail .= ($key+1);
      $tblDetail .= CHtml::closeTag('td');
      $tblDetail .= CHtml::openTag('td');
      $tblDetail .= CHtml::link($explode[0].' '.$explode[1], Yii::app()->createUrl('products/referencesProduct/'.$value['productid']), array('id' => 'btn-edit'));
      $tblDetail .= CHtml::closeTag('td');
      $tblDetail .= CHtml::openTag('td');
      $tblDetail .= $value['productprice'];
      $tblDetail .= CHtml::closeTag('td');
      $tblDetail .= CHtml::openTag('td');
      $tblDetail .= 'view';
      $tblDetail .= CHtml::closeTag('td');
      $tblDetail .= CHtml::openTag('td');
      $tblDetail .= FormatToDate::formatDate($value['productdate']);
      $tblDetail .= CHtml::closeTag('td');
      $tblDetail .= CHtml::openTag('td');
      $tblDetail .= Products::model()->getProductStatus($value['productstatus']);
      $tblDetail .= CHtml::closeTag('td');
      $tblDetail .= CHtml::openTag('td');
      $tblDetail .= CHtml::link('ยืนยัน', "approveProduct", array('id' => 'approve-product', 'productid'=>$value['productid']));
      $tblDetail .= CHtml::closeTag('td');
      $tblDetail .= CHtml::closeTag('tr');
		}
		return $tblDetail;
	}

	public function getAlertProduct()	{
		$products = Products::model()->findAllByAttributes(array('approve'=>0));
		return count($products);
	}

	public function popularProduct($user_id = null) {
		$loop = 0;
		$statusValue = self::getProductStatus();
		$dialog_content = null;
		$where = 'AND products.user_id = "'.$user_id.'"';
		$productQuery = Yii::app()->db->createCommand('SELECT * FROM products INNER JOIN users ON(products.user_id=users.user_id) WHERE products.productnoted=1 and products.productstatus!=7 '.$where.' ORDER BY productid ASC')->query();
		
		$dataPop = array();
		foreach ($productQuery as $key => $value) {
			array_push($dataPop, $value);
		}

		$coutPop = count($dataPop);

		$getData = $dataPop;
		$dataPop = array();
		$dataPop[0] = isset($getData[3])?$getData[3]:null;
		$dataPop[1] = isset($getData[1])?$getData[1]:null;
		$dataPop[2] = isset($getData[0])?$getData[0]:null;
		$dataPop[3] = isset($getData[2])?$getData[2]:null;
		$dataPop[4] = isset($getData[4])?$getData[4]:null;

		$dialog_content .= CHtml::openTag('div', array('class'=>'row-fluid','id'=>'productPop'));

		for ($i=0; $i < 5; $i++) { 
		// foreach ($productQuery as $key => $value) {
			// if($loop%6==0) {
			// 	$dialog_content .= CHtml::openTag('div', array('class'=>'row-fluid'));
	  //   }
			
			if (isset($dataPop[$i]['name'])) {

				$decodePicture = json_decode($dataPop[$i]['productpic'], true);
		    $picPath = Yii::app()->request->baseUrl.'/img/productThumb/'.$decodePicture[1]['picName'];
		    $img = CHtml::image($picPath, null, array('id'=>$dataPop[$i]['user_id'], 'class'=>'picProduct')).'<br>'; 

		    if($dataPop[$i]['productstatus']==6) {
		    	$price = $dataPop[$i]['productprice'];
		    }else{
		    	$price = $statusValue[$dataPop[$i]['productstatus']];
		    }

			  $dialog_content .= CHtml::openTag('div', array('class'=>'span2 picProductBox margin2'));

		  	$dialog_content .= CHtml::openTag('div', array('class'=>'picProductSize','style'=>'margin: 6px 3px;'));
			  $dialog_content .= CHtml::link($img, Yii::app()->request->baseUrl.'/site/productDescription/'.$dataPop[$i]['productid']);
			  $dialog_content .= CHtml::closeTag('div');

      	$proName = explode('&&', $dataPop[$i]['productname']);

		  	$dialog_content .= CHtml::openTag('div',array('class'=>'fixBox'));
			  $dialog_content .= $proName[0];
			  $dialog_content .= CHtml::closeTag('div');

			  $dialog_content .= CHtml::openTag('div',array('class'=>'fixBox'));
			  $dialog_content .= isset($proName[1])?$proName[1]:'&nbsp;';
			  $dialog_content .= CHtml::closeTag('div');

			  $dialog_content .= CHtml::openTag('div');
			  $dialog_content .= '฿ '.$price;
			  $dialog_content .= CHtml::closeTag('div');

			  // $dialog_content .= CHtml::openTag('div');
			  // $dialog_content .= $dataPop[$i]['name'];
			  // $dialog_content .= CHtml::closeTag('div');

			  $dialog_content .= CHtml::closeTag('div');

			  

			}else{
				if (($coutPop%2)!=0) {
					$dialog_content .= CHtml::openTag('div', array('class'=>'span2 picProductBox margin2', 'style'=>'background-color: initial;'));
			  	$dialog_content .= CHtml::closeTag('div');
				}
			}
			// if($loop%5==0 && $loop==(sizeof($productQuery)-1)) {
		 //  	$dialog_content .= CHtml::closeTag('div');
		 //  }

			// $loop++;
		}

		$dialog_content .= CHtml::closeTag('div');

		// if(sizeof($productQuery)<6) {
		// 	$dialog_content .= CHtml::closeTag('div');
		// }
		
		return $dialog_content;
	}

	public function allProduct($user_id = null, $post = null) {
		$where = null;
		$page = 1;
		$order = 'DESC';

		if(!is_null($user_id)) {
			$order = 'ASC';
			$where = 'products.user_id = "'.$user_id.'"';
		}elseif(!is_null($post)) {
			$whereArray = array();
			if(!empty($post['Products']['productname'])) {
				$strlen = mb_strlen($post['Products']['productname'], 'UTF-8');
				if ($strlen==1) {
					$postProductName = 'productname LIKE "'.$post['Products']['productname'].'%"';
				}else{
					$postProductName = 'productname LIKE "%'.$post['Products']['productname'].'%"';
				}
				array_push($whereArray, $postProductName);
			}
			if(!empty($post['Category']['category_name'])) {
				$postCategory = 'category_id = '.$post['Category']['category_name'];
				array_push($whereArray, $postCategory);
			}

			$implode = implode(' AND ', $whereArray);
			if(!empty($implode)) {
				$where = $implode;
			}

			if(!empty($post['Products']['page'])) {
				$page = $post['Products']['page'];
			}

		}

		$where = !is_null($where)?' and '.$where:null;
		$statusValue = self::getProductStatus();
		$loop = 1;
		$dialog_content = null;

		$limitLength = 150;
		$limitStart = ($page==1)?0:(($page-1)*$limitLength);
		$limitStop = $limitLength;

		$productQueryAll = Yii::app()->db->createCommand('SELECT * FROM products INNER JOIN users ON(products.user_id=users.user_id) INNER JOIN shops ON(shops.user_id=users.user_id) where products.approve = 1 and productstatus != 7 '.$where.' ORDER BY productid '.$order)->query();
		$productQuery = Yii::app()->db->createCommand('SELECT * FROM products INNER JOIN users ON(products.user_id=users.user_id) INNER JOIN shops ON(shops.user_id=users.user_id) where products.approve = 1 and productstatus != 7 '.$where.' ORDER BY productid '.$order.' limit '.$limitStart.','.$limitStop)->query();
		foreach ($productQuery as $key => $value) {
			$decodePicture = json_decode($value['productpic'], true);
	    $picPath = Yii::app()->request->baseUrl.'/img/productThumb/'.$decodePicture[1]['picName'];
	    $img = CHtml::image($picPath, null, array('id'=>$value['user_id'], 'class'=>'picProduct')).'<br>'; 

	    if(($loop-1)%6==0 || $loop==1) {
				$dialog_content .= CHtml::openTag('div', array('class'=>'row-fluid', 'style'=>'margin-bottom:30px;'));
	    }

	    switch ($value['productstatus']) {
	    	case 2:
	    		$price = $statusValue[$value['productstatus']];
	    		$statusColor = array('style'=>'color:#0099FF;');
	    		break;

	    	case 3:
	    		$price = $statusValue[$value['productstatus']];
	    		$statusColor = array('style'=>'color:#FF0000;');
	    		break;

    		case 5:
	    		$price = $statusValue[$value['productstatus']];
	    		$statusColor = array('style'=>'color:#2BD125;');
	    		break;

	    	case 6:
	    		$price = $value['productprice'];
	    		$statusColor = array('style'=>'color:#2BD125;');
	    		break;
	    	
	    	default:
	    		$price = $statusValue[$value['productstatus']];
	    		$statusColor = array();
	    		break;
	    }

		  $dialog_content .= CHtml::openTag('div', array('class'=>'span2 picProductBox margin2-6'));
		  $dialog_content .= CHtml::openTag('div', array('class'=>'picProductSize','onclick'=>"window.location='".Yii::app()->request->baseUrl."/site/productDescription/".$value['productid']."'"));
		  $dialog_content .= $img;
		  $dialog_content .= CHtml::closeTag('div');

      $proName = explode('&&', $value['productname']);
      $name1 = (!is_null($user_id)?($key+1).'. ':null).$proName[0];
      $name2 = isset($proName[1])?$proName[1]:'&nbsp;';
      $dialog_content .= CHtml::openTag('div',array('class'=>'fixBox'));
		  $dialog_content .= !empty($name1)?$name1:'&nbsp;';
		  $dialog_content .= CHtml::closeTag('div');

			$dialog_content .= CHtml::openTag('div',array('class'=>'fixBox'));
		  $dialog_content .= !empty($name2)?$name2:'&nbsp;';
		  $dialog_content .= CHtml::closeTag('div');

		  $dialog_content .= CHtml::openTag('div', $statusColor);
		  $dialog_content .= '฿ '.$price;
		  $dialog_content .= CHtml::closeTag('div');

		  if(is_null($user_id)) {
		  	$dialog_content .= CHtml::openTag('div');
		  	$shopShortName = explode('&&', $value['name']);
		  	$dialog_content .= $shopShortName[0];
		  	$dialog_content .= CHtml::closeTag('div');
			}
		  $dialog_content .= CHtml::closeTag('div');

		  if($loop%6==0 || $loop==sizeof($productQuery))  {
		  	$dialog_content .= CHtml::closeTag('div');
		  }

		  $loop++;
		}

		if(sizeof($productQuery)<5) {
			$dialog_content .= CHtml::closeTag('div');
		}

		$dialog_content .= CHtml::openTag('div', array('class'=>'row-fluid'));
		$dialog_content .= CHtml::openTag('div', array('class'=>'span12','style'=>'text-align: center;'));

		$dialog_content .= Paging::getPaging(count($productQueryAll),$limitLength,$page);

		$dialog_content .= CHtml::closeTag('div');
		$dialog_content .= CHtml::closeTag('div');

		
		return $dialog_content;
	}

	public function allProductIndex() {

		$statusValue = self::getProductStatus();
		$loop = 1;
		$dialog_content = null;
		$limitStart = 0;
		$limitStop = $limitStart+150;
		$productQuery = Yii::app()->db->createCommand('SELECT * FROM products INNER JOIN users ON(products.user_id=users.user_id) INNER JOIN shops ON(shops.user_id=users.user_id) where products.approve = 1 and productstatus != 7 ORDER BY productid DESC limit '.$limitStart.','.$limitStop)->query();
		foreach ($productQuery as $key => $value) {
			$decodePicture = json_decode($value['productpic'], true);
	    $picPath = Yii::app()->request->baseUrl.'/img/productThumb/'.$decodePicture[1]['picName'];
	    $img = CHtml::image($picPath, null, array('id'=>$value['user_id'], 'class'=>'picProduct')).'<br>'; 

	    if(($loop-1)%5==0 || $loop==1) {
				$dialog_content .= CHtml::openTag('div', array('class'=>'row-fluid', 'style'=>'margin-bottom:30px;'));
	    }

	    switch ($value['productstatus']) {
	    	case 2:
	    		$price = $statusValue[$value['productstatus']];
	    		$statusColor = array('style'=>'color:#0099FF;');
	    		break;

	    	case 3:
	    		$price = $statusValue[$value['productstatus']];
	    		$statusColor = array('style'=>'color:#FF0000;');
	    		break;

    		case 5:
	    		$price = $statusValue[$value['productstatus']];
	    		$statusColor = array('style'=>'color:#2BD125;');
	    		break;

	    	case 6:
	    		$price = $value['productprice'];
	    		$statusColor = array('style'=>'color:#2BD125;');
	    		break;
	    	
	    	default:
	    		$price = $statusValue[$value['productstatus']];
	    		$statusColor = array();
	    		break;
	    }

		  $dialog_content .= CHtml::openTag('div', array('class'=>'span2 picProductBox margin2'));
		  $dialog_content .= CHtml::openTag('div', array('class'=>'picProductSize','onclick'=>"window.location='".Yii::app()->request->baseUrl."/site/productDescription/".$value['productid']."'"));
		  $dialog_content .= $img;
		  $dialog_content .= CHtml::closeTag('div');

      $proName = explode('&&', $value['productname']);
      $name1 = $proName[0];
      $name2 = isset($proName[1])?$proName[1]:'&nbsp;';

      $dialog_content .= CHtml::openTag('div',array('class'=>'fixBox'));
		  $dialog_content .= !empty($name1)?$name1:'&nbsp;';
		  $dialog_content .= CHtml::closeTag('div');

			$dialog_content .= CHtml::openTag('div',array('class'=>'fixBox'));
		  $dialog_content .= !empty($name2)?$name2:'&nbsp;';
		  $dialog_content .= CHtml::closeTag('div');

		  $dialog_content .= CHtml::openTag('div', $statusColor);
		  $dialog_content .= '฿ '.$price;
		  $dialog_content .= CHtml::closeTag('div');
		  $dialog_content .= CHtml::openTag('div');
		  $shopShortName = explode('&&', $value['name']);
		  $dialog_content .= $shopShortName[0];
		  $dialog_content .= CHtml::closeTag('div');
		  $dialog_content .= CHtml::closeTag('div');

		  if($loop%5==0 || $loop==sizeof($productQuery))  {
		  	$dialog_content .= CHtml::closeTag('div');
		  }

		  $loop++;
		}

		if(sizeof($productQuery)<5) {
			$dialog_content .= CHtml::closeTag('div');
		}

		$dialog_content .= CHtml::openTag('div', array('class'=>'row-fluid'));
		$dialog_content .= CHtml::openTag('div', array('class'=>'span12','style'=>'text-align: right;'));

		$text = CHtml::openTag('i',array('class'=>'icon-chevron-left icon-white','style'=>'margin: 0;'));
    $text .= CHtml::closeTag('i');
    $text .= 'ชมทั้งหมด';
    $text .= CHtml::openTag('i',array('class'=>'icon-chevron-right icon-white'));
    $text .= CHtml::closeTag('i');
  	$dialog_content .= CHtml::link($text, Yii::app()->request->baseUrl.'/site/shop_list',array('style'=>'font-weight: bold;text-decoration: underline;'));

		$dialog_content .= CHtml::closeTag('div');
		$dialog_content .= CHtml::closeTag('div');

		
		return $dialog_content;
	}

	public function getAlertBlockProduct()	{
		$products = Products::model()->findAllByAttributes(array('approve'=>3));
		return count($products);
	}

	public function tblDataProductBlock($shopId = null)	{
		$seachByShopId = !is_null($shopId)?" and user_id = '".$shopId."' ":null;
		$tblDetail = null;
		$dataQuery = Yii::app()->db->createCommand('SELECT * FROM products LEFT JOIN category ON(products.category_id=category.category_id) WHERE approve = 3 '.$seachByShopId.'ORDER BY productid ASC')->query();
		foreach ($dataQuery as $key => $value) {
			$explode = explode('&&', $value['productname']);

			$tblDetail .= CHtml::openTag('tr');
      $tblDetail .= CHtml::openTag('td');
      $tblDetail .= ($key+1);
      $tblDetail .= CHtml::closeTag('td');
      $tblDetail .= CHtml::openTag('td');
      $tblDetail .= CHtml::link($explode[0].' '.$explode[1], Yii::app()->createUrl('site/productDescription/'.$value['productid']), array('id' => 'btn-edit'));
      $tblDetail .= CHtml::closeTag('td');
      $tblDetail .= CHtml::openTag('td');
      $tblDetail .= $value['productprice'];
      $tblDetail .= CHtml::closeTag('td');
      $tblDetail .= CHtml::openTag('td');
      $tblDetail .= FormatToDate::formatDate($value['productdate']);
      $tblDetail .= CHtml::closeTag('td');
      $tblDetail .= CHtml::openTag('td');
      $tblDetail .= Products::model()->getProductStatus($value['productstatus']);
      $tblDetail .= CHtml::closeTag('td');
      $tblDetail .= CHtml::openTag('td');
      $tblDetail .= CHtml::link('ปลดล๊อค', "#", array('id' => 'unban-product', 'productid'=>$value['productid'], 'shopid'=>$shopId));
      $tblDetail .= CHtml::closeTag('td');
      $tblDetail .= CHtml::closeTag('tr');
		}
		return $tblDetail;
	}
	
}
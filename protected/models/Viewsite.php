<?php

/**
 * This is the model class for table "viewsite".
 *
 * The followings are the available columns in table 'viewsite':
 * @property string $ip
 * @property string $date
 * @property string $time
 * @property integer $unit
 */
class Viewsite extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Viewsite the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'viewsite';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ip, date, time, unit', 'required'),
			array('unit', 'numerical', 'integerOnly'=>true),
			array('ip', 'length', 'max'=>50),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('ip, date, time, unit', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ip' => 'Ip',
			'date' => 'Date',
			'time' => 'Time',
			'unit' => 'Unit',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ip',$this->ip,true);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('time',$this->time,true);
		$criteria->compare('unit',$this->unit);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function updateView(){

		if (is_null(Yii::app()->user->getState('online'))) {

			Yii::app()->user->setState('online', 'online');

			$viewModel = Viewsite::model()->findByAttributes(array('ip'=>$_SERVER['REMOTE_ADDR'],'date'=>date('Y-m-d')));

			if (count($viewModel)==0) {
				$viewModel = new Viewsite;
				$viewModel->ip = $_SERVER['REMOTE_ADDR'];
				$viewModel->date = date('Y-m-d');
				$viewModel->time = date('H:i:s');
				$viewModel->unit = 1;
			}else{
				$viewModel->time = date('H:i:s');
				$viewModel->unit = $viewModel->unit + 1;
			}

			$viewModel->save();

			$dateNow = date('Y-m-d');
			$statisticsViewModel = StatisticsView::model()->findByAttributes(array('id'=>1));

			if (is_null($statisticsViewModel)) {
				$statisticsViewModel = new StatisticsView;
				$statisticsViewModel->total = 1;
				$statisticsViewModel->today = 1;
				$statisticsViewModel->date = $dateNow;
				$statisticsViewModel->save();
			}else{

				if ($statisticsViewModel->date != $dateNow) {
					$countToday = 1;
					$dateToday = $dateNow;
				}else{
					$countToday = $statisticsViewModel->today +1;
					$dateToday = $statisticsViewModel->date;
				}

				$statisticsViewModel->total = $statisticsViewModel->total +1;
				$statisticsViewModel->today = $countToday;
				$statisticsViewModel->date = $dateToday;
				$statisticsViewModel->save();

			}

		}
		
	}

}
<?php

/**
 * This is the model class for table "category".
 *
 * The followings are the available columns in table 'category':
 * @property integer $category_id
 * @property string $category_name
 */
class Category extends CActiveRecord {
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Category the static model class
	 */
	public static function model($className=__CLASS__)	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()	{
		return 'category';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('category_name', 'required'),
			array('category_name', 'length', 'max'=>50),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('category_id, category_name', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()	{
		return array(
			'category_id' => 'หมวดหมู่',
			'category_name' => 'ขื่อหมวดหมู่',
			//
			'category_add' => 'เพิ่มหมวดหมู่',
			'category_edit' => 'แก้ไขหมวดหมู่',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('category_id',$this->category_id);
		$criteria->compare('category_name',$this->category_name,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function tblCategoryData()	{
		$tblCategory = null;
		$catQuery = self::Model()->findAll(array('order'=>'category_id'));
		foreach ($catQuery as $catKey => $catValue) {
			$tblCategory .= CHtml::openTag('tr');
			$tblCategory .= CHtml::openTag('td');
      $tblCategory .= CHtml::checkBox('CAT_DELETE['.$catValue['category_id'].']',false,array('value'=>$catValue['category_id']));
      $tblCategory .= CHtml::closeTag('td');
      $tblCategory .= CHtml::openTag('td');
      $tblCategory .= $catValue['category_id'];
      $tblCategory .= CHtml::closeTag('td');
      $tblCategory .= CHtml::openTag('td');
      $tblCategory .= CHtml::link($catValue['category_name'], Yii::app()->createUrl('category/dataCategory/'.$catValue['category_id']), array('id' => 'btn-edit', 'name'=>'editCategory', 'value_id'=>$catValue['category_id'], 'value_name'=>$catValue['category_name'], 'role'=>'button', 'data-toggle'=>'modal'));
      $tblCategory .= CHtml::closeTag('td');
      $tblCategory .= CHtml::openTag('td');
      $tblCategory .= 'Amount';
      $tblCategory .= CHtml::closeTag('td');
      // $tblCategory .= CHtml::openTag('td');
      // $tblCategory .= CHtml::link('แก้ไข', Yii::app()->createUrl('category/dataCategory/'.$catValue['category_id']), array('id' => 'btn-edit', 'name'=>'editCategory', 'value_id'=>$catValue['category_id'], 'value_name'=>$catValue['category_name'], 'role'=>'button', 'data-toggle'=>'modal'));
      // $tblCategory .= CHtml::closeTag('td');
      $tblCategory .= CHtml::closeTag('tr');
		}
		return $tblCategory;
	}

	public function getCategoryData()	{
		$itemsDetail = array(null=>'-- กรุณาเลือกข้อมูล --');
		$catQuery = self::Model()->findAll(array('order'=>'category_id'));
		foreach ($catQuery as $key => $value) {
			$itemsDetail = $itemsDetail + array($value['category_id'] => $value['category_name']);
		}
		return $itemsDetail;
	}

	public function getCategoryDetail($category_id = null)	{
		$result = self::model()->findByAttributes(array('category_id'=>$category_id));
		return $result;
	}

}
<?php

/**
 * This is the model class for table "setting".
 *
 * The followings are the available columns in table 'setting':
 * @property integer $setting_id
 * @property string $setting_parameter
 */
class Setting extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Setting the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'setting';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('setting_parameter', 'required'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('setting_id, setting_parameter', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'setting_id' => 'Setting',
			'setting_parameter' => 'Setting Parameter',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('setting_id',$this->setting_id);
		$criteria->compare('setting_parameter',$this->setting_parameter,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function getTheme(){
		$theme = array(
			'1' => 'Brown (Default)',
			'2' => 'Red',
			);
		$settingModel = Setting::model()->findByAttributes(array('setting_id'=>1));
		$param = json_decode($settingModel->setting_parameter);

		$data = CHtml::dropDownList('theme', $param->theme, $theme, array('class'=>'span12', 'theme-id'=>$param->theme));

		return $data;
	}

	public function getThemePage(){
		$theme = array(
			'1' => 'brown.css',
			'2' => 'red.css',
			);
		$settingModel = Setting::model()->findByAttributes(array('setting_id'=>1));
		$param = json_decode($settingModel->setting_parameter);

		$data = $theme[$param->theme];

		return $data;
	}

	public function getValueLayoutNews(){
		$settingModel = Setting::model()->findByAttributes(array('setting_id'=>1));
		$param = json_decode($settingModel->setting_parameter);

		$data = $param->layoutNews;

		return $data;
	}

	public function getBgImage(){
		$settingModel = Setting::model()->findByAttributes(array('setting_id'=>1));
		$param = json_decode($settingModel->setting_parameter);
		
		$data = isset($param->background)?'background-image: url('.Yii::app()->request->baseUrl.'/img/'.$param->background.');':null;

		return $data;
	}

	public function getColorBox1(){
		$settingModel = Setting::model()->findByAttributes(array('setting_id'=>1));
		$param = json_decode($settingModel->setting_parameter);
		
		$data = isset($param->colorBox1)?'background-color: '.$param->colorBox1:null;

		return $data;
	}

	public function getColorBox2(){
		$settingModel = Setting::model()->findByAttributes(array('setting_id'=>1));
		$param = json_decode($settingModel->setting_parameter);
		
		$data = isset($param->colorBox2)?'background-color: '.$param->colorBox2:null;

		return $data;
	}

	public function getColorBox3(){
		$settingModel = Setting::model()->findByAttributes(array('setting_id'=>1));
		$param = json_decode($settingModel->setting_parameter);
		
		$data = isset($param->colorBox3)?'background-color: '.$param->colorBox3:null;

		return $data;
	}

	public function getColorCode1(){
		$settingModel = Setting::model()->findByAttributes(array('setting_id'=>1));
		$param = json_decode($settingModel->setting_parameter);
		
		$data = isset($param->colorBox1)?$param->colorBox1:null;

		return $data;
	}

	public function getColorCode2(){
		$settingModel = Setting::model()->findByAttributes(array('setting_id'=>1));
		$param = json_decode($settingModel->setting_parameter);
		
		$data = isset($param->colorBox2)?$param->colorBox2:null;

		return $data;
	}

	public function getColorCode3(){
		$settingModel = Setting::model()->findByAttributes(array('setting_id'=>1));
		$param = json_decode($settingModel->setting_parameter);
		
		$data = isset($param->colorBox3)?$param->colorBox3:null;

		return $data;
	}

	public function getFontColorHeader(){
		$settingModel = Setting::model()->findByAttributes(array('setting_id'=>1));
		$param = json_decode($settingModel->setting_parameter);
		
		$data = isset($param->fontColorHeader)?'color: '.$param->fontColorHeader:null;

		return $data;
	}

	public function getFontColorContent(){
		$settingModel = Setting::model()->findByAttributes(array('setting_id'=>1));
		$param = json_decode($settingModel->setting_parameter);
		
		$data = isset($param->fontColorContent)?'color: '.$param->fontColorContent:null;

		return $data;
	}

	public function getFontColorHeaderCode(){
		$settingModel = Setting::model()->findByAttributes(array('setting_id'=>1));
		$param = json_decode($settingModel->setting_parameter);
		
		$data = isset($param->fontColorHeader)?$param->fontColorHeader:null;

		return $data;
	}

	public function getFontColorContentCode(){
		$settingModel = Setting::model()->findByAttributes(array('setting_id'=>1));
		$param = json_decode($settingModel->setting_parameter);
		
		$data = isset($param->fontColorContent)?$param->fontColorContent:null;

		return $data;
	}

	public function getThemeColorCode(){
		$settingModel = Setting::model()->findByAttributes(array('setting_id'=>1));
		$param = json_decode($settingModel->setting_parameter);
		
		$data = isset($param->themeColorCode)?$param->themeColorCode:null;
		$data = is_null($data)?'Default':$data;
		return $data;
	}

	public function getThemeColorBox(){
		$settingModel = Setting::model()->findByAttributes(array('setting_id'=>1));
		$param = json_decode($settingModel->setting_parameter);
		$data = null;
		$colorCode = isset($param->themeColor)?$param->themeColor:null;
		if (!is_null($colorCode)) {
			$data = 'background: linear-gradient(to bottom, rgb('.$colorCode.') 0%, rgba('.$colorCode.', 0.5) 50%, rgba('.$colorCode.', 0.4) 52%, rgba('.$colorCode.', 0.2) 100%);';
		}

		return $data;
	}

	public function getThemeColorFooterBox(){
		$settingModel = Setting::model()->findByAttributes(array('setting_id'=>1));
		$param = json_decode($settingModel->setting_parameter);
		$data = null;
		$colorCode = isset($param->themeColor)?$param->themeColor:null;
		if (!is_null($colorCode)) {
			$data = 'background: linear-gradient(to bottom, rgb('.$colorCode.') 0%, rgba('.$colorCode.',1) 0%)';
		}else{
			$data = 'background: linear-gradient(to bottom, rgb(99, 14, 14) 0%, rgba(99, 14, 14,1) 0%)';
		}

		return $data;
	}

}
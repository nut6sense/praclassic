<?php

/**
 * This is the model class for table "gallery".
 *
 * The followings are the available columns in table 'gallery':
 * @property integer $galleryid
 * @property string $galleryname
 * @property string $gallerydetail
 * @property string $gallerypic
 * @property string $gallerydate
 * @property integer $category_id
 * @property integer $view
 */
class Gallery extends CActiveRecord {
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Gallery the static model class
	 */
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()	{
		return 'gallery';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('galleryname, gallerydetail, gallerypic, gallerydate, category_id', 'required'),
			array('category_id, view', 'numerical', 'integerOnly'=>true),
			array('galleryname', 'length', 'max'=>50),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('galleryid, galleryname, gallerydetail, gallerypic, gallerydate, category_id, view', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()	{
		return array(
			'galleryid' => 'Galleryid',
			'galleryname' => 'ชื่อพระ',
			'gallerydetail' => 'รายละเอียด',
			'gallerypic' => 'Gallerypic',
			'gallerydate' => 'Gallerydate',
			'category_id' => 'หมวดหมู่',
			'view' => 'จำนวนผู้เข้าชม',
			//
			'title_galleryname' => 'ชื่อพระ',
			'title_gallerydetail' => 'รายละเอียด',
			'title_gallerydate' => 'เมื่อวันที่',
			'title_category_id' => 'หมวดหมู่',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search() {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('galleryid',$this->galleryid);
		$criteria->compare('galleryname',$this->galleryname,true);
		$criteria->compare('gallerydetail',$this->gallerydetail,true);
		$criteria->compare('gallerypic',$this->gallerypic,true);
		$criteria->compare('gallerydate',$this->gallerydate,true);
		$criteria->compare('category_id',$this->category_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function tblDataGallery()	{
		$tblDetail = null;
		$dataQuery = Yii::app()->db->createCommand('SELECT * FROM gallery ORDER BY galleryid ASC')->query();
		foreach ($dataQuery as $key => $value) {
			$tblDetail .= CHtml::openTag('tr');
			$tblDetail .= CHtml::openTag('td');
      $tblDetail .= CHtml::checkBox('GLY_DELETE['.$value['galleryid'].']',false,array('value'=>$value['galleryid']));
      $tblDetail .= CHtml::closeTag('td');
      $tblDetail .= CHtml::openTag('td');
      $tblDetail .= ($key+1);
      $tblDetail .= CHtml::closeTag('td');
      $tblDetail .= CHtml::openTag('td');
      $tblDetail .= CHtml::link($value['galleryname'], '#', array('id' => 'btn-edit', 'name'=>'editGallery', 'value_id'=>$value['galleryid']));
      $tblDetail .= CHtml::closeTag('td');
      $tblDetail .= CHtml::openTag('td');
      $tblDetail .= $value['view'];
      $tblDetail .= CHtml::closeTag('td');
      $tblDetail .= CHtml::openTag('td');
      $tblDetail .= $value['gallerydate'];
      $tblDetail .= CHtml::closeTag('td');
      // $tblDetail .= CHtml::openTag('td');
      // $tblDetail .= CHtml::link('แก้ไข', '#', array('id' => 'btn-edit', 'name'=>'editGallery', 'value_id'=>$value['galleryid']));
      // $tblDetail .= CHtml::closeTag('td');
      $tblDetail .= CHtml::closeTag('tr');
		}
		return $tblDetail;
	}

	public function showGallery($idCategory = null) {
		$where = null;
		if(!is_null($idCategory) && !empty($idCategory)) {
			$where = ' WHERE category_id ='.$idCategory.' ';
		}
		$loop = 1;
		$dialog_content = null;
		$galleryQuery = Yii::app()->db->createCommand('SELECT * FROM gallery '.$where.' ORDER BY galleryid ASC')->query();
		foreach ($galleryQuery as $key => $value) {
			$findCategory = Category::model()->findByAttributes(array('category_id'=>$value['category_id']));

			$decodePicture = json_decode($value['gallerypic'], true);
	    $picPath = Yii::app()->request->baseUrl.'/img/galleryPic/'.$decodePicture[1]['picName'];
	    $img = CHtml::image($picPath, null, array('id'=>$value['galleryid'], 'class'=>'picProduct')); 

	    if(($loop-1)%5==0 || $loop==1) {
				$dialog_content .= CHtml::openTag('div', array('class'=>'row-fluid','style'=>'margin-bottom:30px;'));
	    }
	    
		  $dialog_content .= CHtml::openTag('div', array('class'=>'span2 picProductBox margin2'));
		  $dialog_content .= CHtml::openTag('div', array('class'=>'picProductSize','style'=>'margin: 6px 3px;'));
		  $dialog_content .= CHtml::link($img, Yii::app()->request->baseUrl.'/site/galleryDescription/'.$value['galleryid']);
		  $dialog_content .= CHtml::closeTag('div');

      $dialog_content .= CHtml::openTag('div',array('class'=>'fixBox'));
		  $dialog_content .= $value['galleryname'];
		  $dialog_content .= CHtml::closeTag('div');

		  $dialog_content .= CHtml::openTag('div',array('class'=>'fixBox'));
		  $dialog_content .= '&nbsp';
		  $dialog_content .= CHtml::closeTag('div');

			$dialog_content .= CHtml::openTag('div',array('class'=>'fixBox'));
		  $dialog_content .= 'ประเภท : '.$findCategory->category_name;
		  $dialog_content .= CHtml::closeTag('div');
		  $dialog_content .= CHtml::closeTag('div');

		  if($loop%5==0 || $loop==(sizeof($galleryQuery)))  {
		  	$dialog_content .= CHtml::closeTag('div');
		  }

		  $loop++;
		}
		
		if(sizeof($galleryQuery)<5) {
			$dialog_content .= CHtml::closeTag('div');
		}
		
		return $dialog_content;
	}
	
}
<?php

/**
 * This is the model class for table "shops".
 *
 * The followings are the available columns in table 'shops':
 * @property integer $shopid
 * @property string $user_id
 * @property string $name
 * @property string $detail
 * @property string $rule
 * @property string $map
 * @property string $cover
 * @property string $referrance
 * @property string $referrance_phonenumber
 * @property string $sample_pic
 * @property string $shop_pic
 * @property string $note
 * @property string $view
 */
class Shops extends CActiveRecord
{
	public $code_input;
	public $code_hidden;
	public $shotName;
	public $fullName;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Shops the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'shops';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		
		
		return array(
			array('user_id, name, shotName, fullName', 'required'),
			array('user_id', 'length', 'max'=>11),
			array('name, referrance', 'length', 'max'=>50),
			array('cover, shop_pic', 'length', 'max'=>100),
			array('referrance_phonenumber', 'length', 'max'=>13),
			array('detail, rule, map', 'safe'),
			array('view', 'numerical', 'integerOnly'=>true),
			
			array('shopid, user_id, name, detail, rule, map, cover, shop_pic, referrance, referrance_phonenumber,sample_pic,note,view', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		
		
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'shopid' => 'Shopid',
			'user_id' => 'User',
			'name' => 'ชื่อร้านค้า : ',
			'detail' => 'รายละเอียดร้านค้า : ',
			'rule' => 'เงื่อนไขการรับประกัน : ',
			'map' => 'ที่ตั้งร้านค้า : ',
			'cover' => 'Cover',
			'referrance' => 'ผู้รับรองการเปิดร้านค้า : ',
			'referrance_phonenumber' => 'โทรศัพท์ผู้รับรอง : ',
			'note' => 'หมายเหตุ',
			
			'sample_pic' => '',
			'shop_pic' => 'รูปเจ้าของร้าน',
			'title_shopid' => 'รหัสร้านค้า',
			'title_name' => 'ชื่อร้านค้า',
			'title_detail' => 'รายละเอียดร้านค้า',
			'title_rule' => 'เงื่อนไขการรับประกัน',
			'title_map' => 'ที่ตั้งร้านค้า',
			'title_referrance' => 'ผู้รับรองการเปิดร้านค้า',
			'title_referrance_phonenumber' => 'โทรศัพท์ผู้รับรอง',
			
			'code_input' => '',
			'code_hidden' => '',
			'shotName' => 'ชื่อร้านค้าแบบย่อ',
			'fullName' => 'ชื่อร้านค้า',
			//
			'title_shotName' => 'ชื่อร้านค้าแบบย่อ',
			'title_fullName' => 'ชื่อร้านค้า',
			'view' => 'ผู้เข้าชม',
			);
	}

	public function emailList()
	{
		return array(
			'0' => 'panpila@gmail.com',
			'1' => 'heartnet13th@hotmail.com',
			'2' => 'thaiwebexpert@hotmail.com',
			'3' => 'j-wasan@hotmail.com',
		);
	}

	public function tblNewShopsData()	{
		$tblDetail = null;
		$userQuery = Yii::app()->db->createCommand('SELECT *, users.name AS firstname, shops.name AS shopname FROM users INNER JOIN payment ON(users.user_id=payment.user_id) INNER JOIN shops on(users.user_id=shops.user_id) WHERE users.permission=0 AND users.active=1 ORDER BY id ASC')->query();
		foreach ($userQuery as $uKey => $userValue) {
			$tblDetail .= CHtml::openTag('tr');
			$tblDetail .= CHtml::openTag('td');
      $tblDetail .= CHtml::checkBox('SHOP_DELETE['.$userValue['user_id'].']',false,array('value'=>$userValue['user_id']));
      $tblDetail .= CHtml::closeTag('td');
      $tblDetail .= CHtml::openTag('td');
      $tblDetail .= ($uKey+1);
      $tblDetail .= CHtml::closeTag('td');
      $tblDetail .= CHtml::openTag('td');

      $shopShotName = explode('&&', $userValue['shopname']);
      $shopShotName = !empty($shopShotName[1])?$shopShotName[1]:$shopShotName[0];
      $imgEmail = CHtml::image(Yii::app()->request->baseUrl.'/img/icon_email.png', null, array('style'=>'width:25px;'));

      $tblDetail .= CHtml::link($shopShotName, Yii::app()->createUrl('shop/shopDetail/'.$userValue['user_id'].'/newShop'), array('id' => 'btn-edit', 'name'=>'shopDetail'));
      $tblDetail .= CHtml::closeTag('td');
      $tblDetail .= CHtml::openTag('td');
      //
      $tblDetail .= CHtml::link($imgEmail, Yii::app()->createUrl('shop/sendShopEmail/'.$userValue['user_id'].'/newShop'), array('name'=>'document'));
      //
      $tblDetail .= CHtml::closeTag('td');
      $tblDetail .= CHtml::openTag('td');
      $tblDetail .= $userValue['phonenumber'];
      $tblDetail .= CHtml::closeTag('td');
      $tblDetail .= CHtml::openTag('td');
      $tblDetail .= $userValue['pay_type'].' เดือน';
      $tblDetail .= CHtml::closeTag('td');
      $tblDetail .= CHtml::openTag('td');
      $tblDetail .= CHtml::link('ยืนยัน', Yii::app()->createUrl('shop/paymentDetail/'.$userValue['user_id'].'/newShop'), array('id' => 'btn-edit', 'name'=>'confirmShop'));
      $tblDetail .= CHtml::closeTag('td');
      $tblDetail .= CHtml::closeTag('tr');
		}
		return $tblDetail;
	}

	public function tblAllShopsData()	{
    $page = !is_null(Yii::app()->input->get('param1'))?Yii::app()->input->get('param1'):1;
		$limitLength = 50;
		$limitStart = ($page==1)?0:(($page-1)*$limitLength);

		$tblDetail = null;
		$shopQuery = 'SELECT *,users.name AS firstname, shops.name AS shopname FROM users INNER JOIN shops ON(users.user_id=shops.user_id) WHERE users.permission<>0 ORDER BY sequence ASC limit '.$limitStart.','.$limitLength;
		$userQuery = Yii::app()->db->createCommand($shopQuery)->query();
		foreach ($userQuery as $uKey => $userValue) {
			$expireQuery = Payment::model()->findByAttributes(array('user_id' => $userValue['user_id'], 'pay_status' => '1'));
			if(isset($expireQuery->date_end)) {
				$expireColor = '';
				$exDateEnd = explode('-', $expireQuery->date_end);
				$amountOfDay = ' ('.Shops::findAmountOfDay($expireQuery->date_end).' วัน)';
			}else{
				$expireColor = '#f00';
				$expireText = 'หมดอายุ';
			}

			if (!empty($userValue['referrance'])) {
				$color = '#08B30F';
				$referrance = 'มีผู้รับรอง';
			}else{
				$color = '#f00';
				$referrance = 'ไม่มีผู้รับรอง';
			}

			if($userValue['active']=='1') {
				$statusColor = '#08B30F';
				$status = 'เปิด';
			}else{
				$statusColor = '#f00';
				$status = 'ปิด';
			}

			if(!empty($userValue['note'])) {
				$picture_name = 'have_note.png';
			}else{
				$picture_name = 'dont_have_note.png';
			}

	    $img = CHtml::image(Yii::app()->request->baseUrl.'/img/'.$picture_name, null, array('style'=>'width:25px;'));
	    $imgEmail = CHtml::image(Yii::app()->request->baseUrl.'/img/icon_email.png', null, array('style'=>'width:25px;'));
	    $imgDocument = CHtml::image(Yii::app()->request->baseUrl.'/img/icon_id_card.png', null, array('style'=>'width:25px;'));

			$tblDetail .= CHtml::openTag('tr');
			$tblDetail .= CHtml::openTag('td');
      $tblDetail .= CHtml::checkBox('SHOP_DELETE['.$userValue['user_id'].']',false,array('value'=>$userValue['user_id']));
      $tblDetail .= CHtml::closeTag('td');
      $tblDetail .= CHtml::openTag('td');
      $tblDetail .= $userValue['sequence'];
      $tblDetail .= CHtml::closeTag('td');
      $tblDetail .= CHtml::openTag('td');

      $shopShotName = explode('&&', $userValue['shopname']);
      $shopShotName = !empty($shopShotName[1])?$shopShotName[1]:$shopShotName[0];

      $tblDetail .= CHtml::link($shopShotName, Yii::app()->createUrl('shop/shopDetail/'.$userValue['user_id'].'/allShop'), array('id' => 'btn-edit', 'name'=>'editShop'));
      $tblDetail .= CHtml::closeTag('td');
      $tblDetail .= CHtml::openTag('td');
      $tblDetail .= CHtml::link('ล็อกอิน', Yii::app()->createUrl('shop/dataShop/'.$userValue['user_id']), array('id'=>'btn-login', 'name'=>'loginShop'));
      $tblDetail .= CHtml::closeTag('td');
      $tblDetail .= CHtml::openTag('td', array('style'=>'text-align:center;'));
      //
      $tblDetail .= CHtml::link($imgEmail, Yii::app()->createUrl('shop/sendShopEmail/'.$userValue['user_id'].'/allShop'), array('name'=>'document'));
      //
      $tblDetail .= CHtml::closeTag('td');
      $tblDetail .= CHtml::openTag('td');
      $tblDetail .= CHtml::link('สถิติ', Yii::app()->createUrl('shop/paymentHistory/'.$userValue['user_id'].'/allShop'), array('id' => 'btn-edit', 'name'=>'historyShop'));
      $tblDetail .= CHtml::closeTag('td');
      $tblDetail .= CHtml::openTag('td', array('style'=>'color: '.$expireColor));
      $tblDetail .= isset($expireQuery->pay_type)?$expireQuery->pay_type.' เดือน':$expireText;
      $tblDetail .= CHtml::closeTag('td');
      $tblDetail .= CHtml::openTag('td', array('style'=>'color: '.$expireColor));
      $tblDetail .= isset($expireQuery->date_end)?$exDateEnd[2].'/'.$exDateEnd[1].'/'.$exDateEnd[0].$amountOfDay:$expireText;
      $tblDetail .= CHtml::closeTag('td');
      $tblDetail .= CHtml::openTag('td', array('style'=>'text-align:center;'));
      //
      $tblDetail .= CHtml::link($imgDocument, Yii::app()->createUrl('shop/document/'.$userValue['user_id']), array('name'=>'document'));
      //
      $tblDetail .= CHtml::closeTag('td');
      $tblDetail .= CHtml::openTag('td', array('style'=>'text-align:center;'));
      $tblDetail .= CHtml::link($img, Yii::app()->createUrl('shop/noteDetail/'.$userValue['user_id'].'/allShop'), array('id' => 'btn-edit', 'name'=>'noteShop'));
      $tblDetail .= CHtml::closeTag('td');
      $tblDetail .= CHtml::openTag('td', array('style'=>'color: '.$statusColor));
      $tblDetail .= $status;
      $tblDetail .= CHtml::closeTag('td');
      $tblDetail .= CHtml::openTag('td',array('style'=>'color: '.$color));
      $tblDetail .= $referrance;
      $tblDetail .= CHtml::closeTag('td');
      $tblDetail .= CHtml::closeTag('tr');
		}
		return $tblDetail;
	}

	public function tblBanShopsData($status = null)	{
		$tblDetail = null;
		if($status==1) {
			$fromPage = 'banShop';
		}elseif($status==2){
			$fromPage = 'banMonthShop';
		}else{
			$fromPage = 'banOtherShop';
		}
		$imgEmail = CHtml::image(Yii::app()->request->baseUrl.'/img/icon_email.png', null, array('style'=>'width:25px;'));
		
		$shopQuery = 'SELECT *,users.name AS firstname, shops.name AS shopname FROM users INNER JOIN payment ON(users.user_id=payment.user_id) INNER JOIN shops ON(users.user_id=shops.user_id) INNER JOIN block ON(users.user_id=block.user_id) WHERE users.permission<>0 AND users.active=0 AND payment.pay_status=1 AND block.status='.$status.' ORDER BY id ASC';
		$userQuery = Yii::app()->db->createCommand($shopQuery)->query();
		foreach ($userQuery as $uKey => $userValue) {
			$explode_shopname = explode('&&', $userValue['shopname']);
			$exDateEnd = explode('-', $userValue['date_end']);

			$tblDetail .= CHtml::openTag('tr');
			$tblDetail .= CHtml::openTag('td');
      $tblDetail .= CHtml::checkBox('SHOP_DELETE['.$userValue['user_id'].']',false,array('value'=>$userValue['user_id']));
      $tblDetail .= CHtml::closeTag('td');
      $tblDetail .= CHtml::openTag('td');
      $tblDetail .= $userValue['sequence'];
      $tblDetail .= CHtml::closeTag('td');
      $tblDetail .= CHtml::openTag('td');
      $tblDetail .= CHtml::link($explode_shopname[1], Yii::app()->createUrl('shop/shopDetail/'.$userValue['user_id'].'/'.$fromPage), array('id' => 'btn-edit', 'name'=>'editShop'));
      $tblDetail .= CHtml::closeTag('td');
      $tblDetail .= CHtml::openTag('td');
      $tblDetail .= CHtml::link('ล็อกอิน', Yii::app()->createUrl('shop/dataShop/'.$userValue['user_id']), array('id'=>'btn-login', 'name'=>'loginShop'));
      $tblDetail .= CHtml::closeTag('td');
      $tblDetail .= CHtml::openTag('td');
      //
      $tblDetail .= CHtml::link($imgEmail, Yii::app()->createUrl('shop/sendShopEmail/'.$userValue['user_id'].'/'.$fromPage), array('name'=>'document'));
      //
      $tblDetail .= CHtml::closeTag('td');
      $tblDetail .= CHtml::openTag('td');
      $tblDetail .= ($userValue['status']=='1'?'7 วัน':($userValue['status']=='2'?'1 เดือน':'แบบระบุวัน'));
      $tblDetail .= CHtml::closeTag('td');
      $tblDetail .= CHtml::openTag('td');
      $tblDetail .= $userValue['date_start'];
      $tblDetail .= CHtml::closeTag('td');
      $tblDetail .= CHtml::openTag('td');
      $tblDetail .= $userValue['date_end'];
      $tblDetail .= CHtml::closeTag('td');
      $tblDetail .= CHtml::openTag('td', array('style'=>'color: #f00'));
      $tblDetail .= CHtml::link('ยกเลิก', Yii::app()->createUrl('shop/cancelBLock/'.$userValue['user_id'].'/'.$fromPage), array('confirm'=>'ต้องการยกเลิกการระงับการใช้งานร้านค้าที่เลือกใช้หรือไม่ ?'));
      $tblDetail .= CHtml::closeTag('td');
      $tblDetail .= CHtml::closeTag('tr');
		}
		return $tblDetail;
	}

	public function tblExpireShopsData()	{
		$tblDetail = null;
		$shopQuery = 'SELECT *,users.name AS firstname, shops.name AS shopname FROM users INNER JOIN shops ON(users.user_id=shops.user_id) WHERE users.permission<>0 AND users.active=2 ORDER BY id ASC';
		$userQuery = Yii::app()->db->createCommand($shopQuery)->query();
		foreach ($userQuery as $uKey => $userValue) {
			$expireQuery = Payment::model()->findByAttributes(array('user_id' => $userValue['user_id']), array('order'=>'pay_id desc'));

			if(!empty($userValue['note'])) {
				$picture_name = 'have_note.png';
			}else{
				$picture_name = 'dont_have_note.png';
			}

			$explode_shopname = explode('&&', $userValue['shopname']);
			$imgEmail = CHtml::image(Yii::app()->request->baseUrl.'/img/icon_email.png', null, array('style'=>'width:25px;'));
	    $img = CHtml::image(Yii::app()->request->baseUrl.'/img/'.$picture_name, null, array('style'=>'width:25px;')); 

			$tblDetail .= CHtml::openTag('tr');
			$tblDetail .= CHtml::openTag('td');
      $tblDetail .= CHtml::checkBox('SHOP_DELETE['.$userValue['user_id'].']',false,array('value'=>$userValue['user_id']));
      $tblDetail .= CHtml::closeTag('td');
      $tblDetail .= CHtml::openTag('td');
      $tblDetail .= $userValue['sequence'];
      $tblDetail .= CHtml::closeTag('td');
      $tblDetail .= CHtml::openTag('td');
      $tblDetail .= CHtml::link($explode_shopname[1], Yii::app()->createUrl('shop/shopDetail/'.$userValue['user_id'].'/expireShop'), array('id' => 'btn-edit', 'name'=>'editShop'));
      $tblDetail .= CHtml::closeTag('td');
      $tblDetail .= CHtml::openTag('td');
      $tblDetail .= CHtml::link('ล็อกอิน', Yii::app()->createUrl('shop/dataShop/'.$userValue['user_id']), array('id'=>'btn-login', 'name'=>'loginShop'));
      $tblDetail .= CHtml::closeTag('td');
      $tblDetail .= CHtml::openTag('td');
      //
      $tblDetail .= CHtml::link($imgEmail, Yii::app()->createUrl('shop/sendShopEmail/'.$userValue['user_id'].'/expireShop'), array('name'=>'document'));
      //
      $tblDetail .= CHtml::closeTag('td');
      $tblDetail .= CHtml::openTag('td');
      $tblDetail .= CHtml::link('ต่ออายุ', Yii::app()->createUrl('shop/paymentHistory/'.$userValue['user_id'].'/expireShop'), array('id' => 'btn-edit', 'name'=>'historyShop'));
      $tblDetail .= CHtml::closeTag('td');
      $tblDetail .= CHtml::openTag('td');
      $tblDetail .= 'การจ่ายเงิน';
      $tblDetail .= CHtml::closeTag('td');
      $tblDetail .= CHtml::openTag('td');
      $tblDetail .= $expireQuery->date_end;
      $tblDetail .= CHtml::closeTag('td');
      $tblDetail .= CHtml::openTag('td');
      $tblDetail .= CHtml::link($img, Yii::app()->createUrl('shop/noteDetail/'.$userValue['user_id'].'/expireShop'), array('id' => 'btn-edit', 'name'=>'noteShop'));
      $tblDetail .= CHtml::closeTag('td');
      $tblDetail .= CHtml::closeTag('tr');
		}
		return $tblDetail;
	}

	public function getShopDetail($user_id = nill)	{
		$shopQuery = Yii::app()->db->createCommand('SELECT *, users.name AS firstname, shops.name AS shopname FROM users INNER JOIN banks ON(users.user_id=banks.user_id) INNER JOIN shops on(users.user_id=shops.user_id) WHERE users.user_id='.$user_id)->query();
		foreach ($shopQuery as $sKey => $sValue) {
			$shopDetail = $sValue;
		}
		$shopDetail['password'] = null;

		$shopAllNames = explode('&&', $shopDetail['shopname']);
		$shopDetail['shotName'] = isset($shopAllNames[0])?$shopAllNames[0]:null;
		$shopDetail['fullName'] = isset($shopAllNames[1])?$shopAllNames[1]:null;

		$sampleArray = json_decode($shopDetail['sample_pic'], true);
		// $shopDetail['sample_1_pic'] = $sampleArray[1]['picName'];
		// $shopDetail['sample_1_detail'] = $sampleArray[1]['detail'];
		// $shopDetail['sample_2_pic'] = $sampleArray[2]['picName'];
		// $shopDetail['sample_2_detail'] = $sampleArray[2]['detail'];
		// $shopDetail['sample_3_pic'] = $sampleArray[3]['picName'];
		// $shopDetail['sample_3_detail'] = $sampleArray[3]['detail'];
		// $shopDetail['sample_4_pic'] = $sampleArray[4]['picName'];
		// $shopDetail['sample_4_detail'] = $sampleArray[4]['detail'];
		$shopDetail['sample'] = $sampleArray;

		return $shopDetail;
	}

	public function tblMailMember()	{
		$tblDetail = null;
		$query = self::emailList();
		foreach ($query as $key => $value) {
			$tblDetail .= CHtml::openTag('tr');
      $tblDetail .= CHtml::openTag('td');
      $tblDetail .= ($key+1);
      $tblDetail .= CHtml::closeTag('td');
      $tblDetail .= CHtml::openTag('td');
      $tblDetail .= $value;
      $tblDetail .= CHtml::closeTag('td');
      $tblDetail .= CHtml::closeTag('tr');
		}
		return $tblDetail;
	}

	public function randomStr(){

		// $str2ran = 'abcdefghijklmnopqrstuvwxyz0123456789'; 
		$str2ran = '0123456789'; 
		$str_result = "";  
		while(strlen($str_result)<6){  
			$str_result .= substr($str2ran,(rand()%strlen($str2ran)),1); 
		}

		return $str_result;
	}

	public function getAlertNewShop()	{
		$newShop = Users::model()->findAllByAttributes(array('permission'=>0));
		return count($newShop);
	}

	public function getCountBanShop($status){
		$shopQuery = 'SELECT *,users.name AS firstname, shops.name AS shopname FROM users INNER JOIN payment ON(users.user_id=payment.user_id) INNER JOIN shops ON(users.user_id=shops.user_id) INNER JOIN block ON(users.user_id=block.user_id) WHERE users.permission<>0 AND users.active=0 AND payment.pay_status=1 AND block.status='.$status;
		$userQuery = Yii::app()->db->createCommand($shopQuery)->query();
		return count($userQuery);
	}

	public function getCountShop($active = null)	{
		if(!is_null($active)) {
			$expire = array('permission'=>1, 'active'=>$active);
		}else{
			$expire = array('permission'=>1);
		}
		$countShop = Users::model()->findAllByAttributes($expire);
		return count($countShop);
	}

	public function logoShop(){
	  $findShop = Shops::model()->findByAttributes(array('user_id' => Yii::app()->input->get('param1')));
	  if (!is_null($findShop->cover)) {
    	$logo = CHtml::image(Yii::app()->request->baseUrl.'/img/logo/'.$findShop->cover, null, array('class'=>'logoShop')); 
	  }else{
    	$logo = CHtml::image(Yii::app()->request->baseUrl.'/img/banner.png', null, array('class'=>'logoShop')); 
	  }
		return $logo;
	}

	public function standardShop($post = null) {
		$loop = 1;
		$dialog_content = null;
		$page = 1;
		$where = null;

		if (!is_null($post)) {
			if(!empty($post['Products']['page'])) {
				$page = $post['Products']['page'];
			}
		}

		if(!empty($post['Shops']['name'])) {
			$where = 'AND SUBSTR(shops.name, 1, (INSTR(shops.name, \'&&\')-1)) like \'%'.$post['Shops']['name'].'%\'';
		}

		$limitLength = 150;
		$limitStart = ($page==1)?0:(($page-1)*$limitLength);
		$limitStop = $limitLength;

		$shopQueryAll = Yii::app()->db->createCommand('SELECT shops.shopid, shops.name AS shopname, users.user_id, shops.shop_pic, users.sequence FROM users INNER JOIN payment ON(users.user_id=payment.user_id) INNER JOIN shops ON(users.user_id=shops.user_id) WHERE users.permission<>0 AND users.active=1 AND payment.pay_status=1 '.$where.' ORDER BY users.sequence ASC')->query();
		$shopQuery = Yii::app()->db->createCommand('SELECT shops.shopid, shops.name AS shopname, users.user_id, shops.shop_pic, users.sequence FROM users INNER JOIN payment ON(users.user_id=payment.user_id) INNER JOIN shops ON(users.user_id=shops.user_id) WHERE users.permission<>0 AND users.active=1 AND payment.pay_status=1 '.$where.' ORDER BY users.sequence ASC limit '.$limitStart.','.$limitStop)->query();
		foreach ($shopQuery as $key => $value) {
	    $picPath = !empty($value['shop_pic'])?Yii::app()->request->baseUrl.'/img/shopThumb/'.$value['shop_pic']:Yii::app()->request->baseUrl.'/img/shopPic/none.jpg';
	    $img = CHtml::image($picPath, null, array('id'=>$value['user_id'], 'class'=>'picProduct')).'<br>'; 

	    if(($loop-1)%6==0 || $loop==1) {
				$dialog_content .= CHtml::openTag('div', array('class'=>'row-fluid','style'=>'margin-bottom:30px;'));
	    }
	    
		  $dialog_content .= CHtml::openTag('div', array('class'=>'span2 picProductBox margin2-6'));
		  $dialog_content .= CHtml::openTag('div', array('class'=>'picProductSize','style'=>'margin: 6px 3px;'));
		  $dialog_content .= CHtml::link($img, Yii::app()->request->baseUrl.'/site/ShopDetail/'.$value['user_id']);
		  $dialog_content .= CHtml::closeTag('div');
		  $dialog_content .= CHtml::openTag('div',array('class'=>'fixBox'));
		  
		  $shopName = explode('&&', $value['shopname']);
		  $shopName = isset($shopName[0])?$shopName[0]:null;
		  $dialog_content .= $shopName;
		  $dialog_content .= CHtml::closeTag('div');
		  $dialog_content .= CHtml::openTag('div');
		  $dialog_content .= 'ID : '.$value['sequence'];
		  $dialog_content .= CHtml::closeTag('div');
		  $dialog_content .= CHtml::closeTag('div');

		  if($loop%6==0 || $loop==(sizeof($shopQuery))) {
		  	$dialog_content .= CHtml::closeTag('div');
		  }

		  $loop++;
		}

		if(sizeof($shopQuery)<5) {
			$dialog_content .= CHtml::closeTag('div');
		}

		$dialog_content .= CHtml::openTag('div', array('class'=>'row-fluid'));
		$dialog_content .= CHtml::openTag('div', array('class'=>'span12','style'=>'text-align: center;'));

		$dialog_content .= Paging::getPaging(count($shopQueryAll),$limitLength,$page);

		$dialog_content .= CHtml::closeTag('div');
		$dialog_content .= CHtml::closeTag('div');
		
		return $dialog_content;
	}

	public function getNoteDetail($user_id = nill)	{
		$shopQuery = Shops::model()->findByAttributes(array('user_id' => $user_id));
		$itemsDetail['note'] = $shopQuery->note;
		return $itemsDetail;
	}

	public function getShopIdByName($name){
		$chkShopName = Yii::app()->db->createCommand("SELECT * FROM users INNER JOIN shops ON (users.user_id=shops.user_id) where users.username='".$name."' OR shops.name like '%&&".$name."'")->query();
		if(sizeof($chkShopName)) {
			foreach($chkShopName as $key => $value) {
			 	$response['shop_id'] = $value['shopid'];
			 	$response['result'] = true;
			} 
		}else{
			$response['result'] = false;
		}

		return $response;
	}

	public function leftAllShop() {
		$data = null;
		$shopQuery = Yii::app()->db->createCommand('SELECT shops.shopid, shops.name AS shopname, users.user_id, shops.shop_pic, users.sequence FROM users INNER JOIN payment ON(users.user_id=payment.user_id) INNER JOIN shops ON(users.user_id=shops.user_id) WHERE users.permission<>0 AND users.active=1 AND payment.pay_status=1 ORDER BY rand() limit 44')->query();
		foreach ($shopQuery as $key => $value) {
	    $picPath = !empty($value['shop_pic'])?Yii::app()->request->baseUrl.'/img/shopThumb/'.$value['shop_pic']:Yii::app()->request->baseUrl.'/img/shopPic/none.jpg';
	    $img = CHtml::image($picPath, null, array('id'=>$value['user_id'], 'class'=>'picProductBand' ,'style'=>'width: 100%;')); 

	   
		  $data .= CHtml::openTag('div', array('class'=>'picProductSize', 'style'=>'width: 95.5%;'));
		  $data .= CHtml::link($img, Yii::app()->request->baseUrl.'/site/ShopDetail/'.$value['user_id']);
		  $data .= CHtml::closeTag('div');
		}
		
		return $data;
	}

	public function findAmountOfDay($date_end) {
		$endExplode = explode('-', $date_end);
		$endDay = $endExplode[2];
		$endMonth = $endExplode[1];
		$endYear = $endExplode[0];
		$endDate = mktime(0, 0, 0, $endMonth, $endDay, $endYear);

		$nowDay = date('d');
		$nowMonth = date('m');
		$nowYear = date('Y');
		$nowDate = mktime(0, 0, 0, $nowMonth, $nowDay, $nowYear);

		$DateNum = ceil(($endDate - $nowDate)/86400);

		return $DateNum;
	}

}
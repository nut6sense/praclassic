<?php

/**
 * This is the model class for table "reference".
 *
 * The followings are the available columns in table 'reference':
 * @property integer $reference_id
 * @property string $user_id
 * @property string $reference_name
 * @property string $reference_phonenumber
 * @property string $reference_date
 */
class Reference extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Reference the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'reference';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, reference_name', 'required'),
			array('user_id, reference_name, reference_phonenumber', 'length', 'max'=>50),
			array('reference_date', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('reference_id, user_id, reference_name, reference_phonenumber, reference_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'reference_id' => 'Reference',
			'user_id' => 'User',
			'reference_name' => 'Reference Name',
			'reference_phonenumber' => 'Reference Phonenumber',
			'reference_date' => 'Reference Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('reference_id',$this->reference_id);
		$criteria->compare('user_id',$this->user_id,true);
		$criteria->compare('reference_name',$this->reference_name,true);
		$criteria->compare('reference_phonenumber',$this->reference_phonenumber,true);
		$criteria->compare('reference_date',$this->reference_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
<?php

/**
 * This is the model class for table "block".
 *
 * The followings are the available columns in table 'block':
 * @property integer $blockid
 * @property string $user_id
 * @property string $status
 * @property string $date_start
 * @property string $date_end
 */
class Block extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Block the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'block';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id', 'required'),
			array('user_id', 'length', 'max'=>11),
			array('date_start, date_end', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('blockid, user_id, status, date_start, date_end', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'blockid' => 'Blockid',
			'user_id' => 'User',
			'status' => 'Status',
			'date_start' => 'Date Start',
			'date_end' => 'Date End',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('blockid',$this->blockid);
		$criteria->compare('user_id',$this->user_id,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('date_start',$this->date_start,true);
		$criteria->compare('date_end',$this->date_end,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function blockUser($model_user = null)	{
		if($model_user->block_user=='1') {
			$day = 7;
			$month = 0;
			$smsBanDescription = $day.' วัน';
		}elseif($model_user->block_user=='2') {
			$day = 30;
			$month = 0;
			$smsBanDescription = '1 เดือน';
		}elseif($model_user->block_user=='3') {
			$day = $model_user->block_day;
			$month = 0;
			$smsBanDescription = $day.' วัน';
		}
		$blockEnd = date("Y-m-d", mktime(date("H")+0, date("i")+0, date("s")+0, date("m")+$month, date("d")+$day, date("Y")+0));

		$command = Yii::app()->db->createCommand();
		$blockModel = new self;
		$blockModel->blockid = null;
    $blockModel->user_id = $model_user->user_id;
    $blockModel->status = $model_user->block_user;
    $blockModel->date_start = date('Y-m-d');
    $blockModel->date_end = $blockEnd;
    
    if($blockModel->validate()) {
    	$blockModel->save();

    	Users::model()->updateAll(array('active'=>0), 'user_id = "'.$model_user->user_id.'"');

    	$msisdn = $model_user->phonenumber;
			$msisdn = preg_replace('#[^0-9]#u', '', $msisdn);
			$messageDetail = 'ร้านค้าของท่านถูกระงับการใช้งานเป็นระยะเวลา '.$smsBanDescription;
			SMS::SendSMS($msisdn, $messageDetail);
    }
	}

	public function checkBlockShop()	{
		$blockData = Yii::app()->db->createCommand('SELECT * FROM block')->query();
		foreach ($blockData as $key => $value) {
			$nowDate = date('Y-m-d');
			if($value['date_end']==$nowDate) {
				Users::renewSequence($value['user_id']);
				Users::model()->updateAll(array('active'=>1), 'user_id = "'.$value['user_id'].'"');
				self::model()->deleteAll('user_id = :user_id' , array('user_id' => $value['user_id']));
			}
		}
	}

}
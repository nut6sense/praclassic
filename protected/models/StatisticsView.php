<?php

/**
 * This is the model class for table "statistics_view".
 *
 * The followings are the available columns in table 'statistics_view':
 * @property integer $id
 * @property integer $total
 * @property integer $today
 */
class StatisticsView extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return StatisticsView the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'statistics_view';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('total, today', 'required'),
			array('total, today', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, total, today', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'total' => 'Total',
			'today' => 'Today',
			'date' => 'date',
		);
	}
	
	public function getCountView(){
		$StatisticsView = StatisticsView::model()->findByAttributes(array('id'=>1));

		$data = array(
			'total' => !is_null($StatisticsView)?$StatisticsView->total:1,
			'today' => !is_null($StatisticsView)?$StatisticsView->today:1,
			);
		return $data;
	}
}
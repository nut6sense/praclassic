<?php

/**
 * This is the model class for table "payconfirm".
 *
 * The followings are the available columns in table 'payconfirm':
 * @property integer $payconfirm_id
 * @property integer $shop_id
 * @property integer $shop_name
 * @property integer $payconfirm_pic
 * @property integer $payconfirm_type
 * @property integer $bank_name
 * @property integer $amount
 * @property string $dateTime
 * @property string $payconfirm_phone
 * @property string $note
 */
class Payconfirm extends CActiveRecord
{
	public $date;
	public $time;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Payconfirm the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'payconfirm';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('shop_id, shop_name, payconfirm_pic, payconfirm_type, payconfirm_phone', 'required'),
			array('bank_name, amount', 'numerical', 'integerOnly'=>true),
			array('payconfirm_phone', 'length', 'max'=>50),
			array('note', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('payconfirm_id, shop_name, bank_name, amount, dateTime, payconfirm_phone, note', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'payconfirm_id' => 'Payconfirm',
			'shop_id' => 'shop_id',
			'shop_name' => 'ชื่อผู้ใช้ หรือ ชื่อร้าน',
			'payconfirm_pic'=>'หลักฐานการชำระเงิน',
			'payconfirm_type'=>'สมาชิกแบบราย',
			'bank_name' => 'ธนาคาร',
			'amount' => 'จำนวนเงิน',
			'dateTime' => 'Date Time',
			'payconfirm_phone' => 'เบอร์โทรติดต่อ',
			'note' => 'หมายเหตุ',
			//
			'date' => 'วันที่',
			'time' => 'เวลา',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('payconfirm_id',$this->payconfirm_id);
		$criteria->compare('shop_id',$this->shop_id);
		$criteria->compare('bank_name',$this->bank_name);
		$criteria->compare('amount',$this->amount);
		$criteria->compare('dateTime',$this->dateTime,true);
		$criteria->compare('payconfirm_phone',$this->payconfirm_phone,true);
		$criteria->compare('note',$this->note,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function tblPayConfirmData(){
		$tblDetail = null;

		$payConQuery = Payconfirm::model()->findAll();

		$num = 1;
		foreach ($payConQuery as $key => $value) {
			$tblDetail .= CHtml::openTag('tr');

			$tblDetail .= CHtml::openTag('td');
      $tblDetail .= CHtml::checkBox('PAY_DELETE['.$value['payconfirm_id'].']',false,array('value'=>$value['payconfirm_id']));
      $tblDetail .= CHtml::closeTag('td');

      $tblDetail .= CHtml::openTag('td');
      $tblDetail .= ($num);
      $tblDetail .= CHtml::closeTag('td');

      $shopName = Shops::model()->findByAttributes(array('shopid'=>$value['shop_id']));
      $shopFullName = explode('&&', $shopName['name']);
      $shopFullName = isset($shopFullName[1])?$shopFullName[1]:$shopFullName[0];

      $tblDetail .= CHtml::openTag('td');
      $tblDetail .= $shopFullName;
      $tblDetail .= CHtml::closeTag('td');

      $bankName = Banks::model()->getBank();

      $tblDetail .= CHtml::openTag('td');
      $tblDetail .= $bankName[$value['bank_name']];
      $tblDetail .= CHtml::closeTag('td');

      $tblDetail .= CHtml::openTag('td');
      $tblDetail .= $value['amount'];
      $tblDetail .= CHtml::closeTag('td');

      $dateTime = explode(' ', $value['dateTime']);

      $tblDetail .= CHtml::openTag('td');
      $tblDetail .= FormatToDate::FormatDate($dateTime[0]);
      $tblDetail .= CHtml::closeTag('td');

      $tblDetail .= CHtml::openTag('td');
      $tblDetail .= FormatToDate::FormatTime($dateTime[1]);
      $tblDetail .= CHtml::closeTag('td');

      $tblDetail .= CHtml::openTag('td');
      $tblDetail .= $value['note'];
      $tblDetail .= CHtml::closeTag('td');

      $imgIcon = Yii::app()->request->baseUrl.'/img/have_note.png';
      $imgSrc = Yii::app()->request->baseUrl.'/img/payConPic/'.$value['payconfirm_pic'];

      $tblDetail .= CHtml::openTag('td');
      $tblDetail .= Highslide::IdPicHighslide($imgSrc, $imgIcon);
      $tblDetail .= CHtml::closeTag('td');

      $tblDetail .= CHtml::closeTag('tr');

      $num++;
		}
		return $tblDetail;
	}

	public function tblPayConfirmDataShop($shopid = null){
		$tblDetail = null;

		$payConQuery = Payconfirm::model()->findAllByAttributes(array('shop_id'=>$shopid));

		$num = 1;
		foreach ($payConQuery as $key => $value) {
			$tblDetail .= CHtml::openTag('tr');

      $tblDetail .= CHtml::openTag('td');
      $tblDetail .= ($num);
      $tblDetail .= CHtml::closeTag('td');

      $shopName = Shops::model()->findByAttributes(array('shopid'=>$value['shop_id']));
      $shopFullName = explode('&&', $shopName['name']);
      $shopFullName = $shopFullName[1];

      $tblDetail .= CHtml::openTag('td');
      $tblDetail .= $shopFullName;
      $tblDetail .= CHtml::closeTag('td');

      $bankName = Banks::model()->getBank();

      $tblDetail .= CHtml::openTag('td');
      $tblDetail .= $bankName[$value['bank_name']];
      $tblDetail .= CHtml::closeTag('td');

      $tblDetail .= CHtml::openTag('td');
      $tblDetail .= $value['amount'];
      $tblDetail .= CHtml::closeTag('td');

      $dateTime = explode(' ', $value['dateTime']);

      $tblDetail .= CHtml::openTag('td');
      $tblDetail .= FormatToDate::FormatDate($dateTime[0]);
      $tblDetail .= CHtml::closeTag('td');

      $tblDetail .= CHtml::openTag('td');
      $tblDetail .= FormatToDate::FormatTime($dateTime[1]);
      $tblDetail .= CHtml::closeTag('td');

      $tblDetail .= CHtml::openTag('td');
      $tblDetail .= $value['note'];
      $tblDetail .= CHtml::closeTag('td');

      $imgIcon = Yii::app()->request->baseUrl.'/img/have_note.png';
      $imgSrc = Yii::app()->request->baseUrl.'/img/payConPic/'.$value['payconfirm_pic'];

      $tblDetail .= CHtml::openTag('td');
      $tblDetail .= Highslide::IdPicHighslide($imgSrc, $imgIcon);
      $tblDetail .= CHtml::closeTag('td');

      $tblDetail .= CHtml::closeTag('tr');

      $num++;
		}
		return $tblDetail;
	}

}
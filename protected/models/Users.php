<?php

/**
 * This is the model class for table "users".
 *
 * The followings are the available columns in table 'users':
 * @property integer $id
 * @property string $user_id
 * @property string $username
 * @property string $password
 * @property string $name
 * @property string $surname
 * @property string $email
 * @property string $phonenumber
 * @property string $iden_id
 * @property string $iden_picture
 * @property integer $permission
 * @property integer $active
 */
class Users extends CActiveRecord
{
  public $confirmPassword;
  public $block_user;
  public $block_day;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Users the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'users';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, username, password, name, surname, phonenumber, email, iden_id', 'required'),
			array('permission, active', 'numerical', 'integerOnly'=>true),
			array('user_id', 'length', 'max'=>11),
			array('username', 'length', 'max'=>12),
			array('password', 'length', 'max'=>32),
			array('name', 'length', 'max'=>50),
			array('surname', 'length', 'max'=>50),

			array('email', 'length', 'max'=>50),
			array('email', 'email', 'message'=>'รูปแบบอีเมล์ไม่ถูกต้อง'),

			array('phonenumber', 'length', 'max'=>13),
			array('iden_id', 'length', 'max'=>17),
			array('iden_picture', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, user_id, username, password, name, surname, email, phonenumber, iden_id, iden_picture, permission, active', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'username' => 'Username : ',
			'password' => 'Password : ',
			'name' => 'ชื่อ : ',
			'surname' => 'นามสกุล : ',
			'email' => 'อีเมล์ : ',
			'phonenumber' => 'เบอร์โทรศัพท์ : ',
			'iden_id' => 'เลขที่บัตรประชาชน : ',
			'iden_picture' => 'ไฟล์รูปบัตรประชาชน : ',
			'permission' => 'Permission',
			'active' => 'สถานะ',
			//
			'confirmPassword' => 'ยืนยัน Password : *',
			'title_username' => 'Username',
			'title_password' => 'Password',
			'title_confirmPassword' => 'ยืนยัน Password',
			'title_name' => 'ชื่อ',
			'title_surname' => 'นามสกุล',
			'title_email' => 'อีเมล์',
			'title_phonenumber' => 'เบอร์โทรศัพท์',
			'title_iden_id' => 'เลขที่บัตรประชาชน',
			'tile_iden_picture' => 'รูปบัตรประชาชน',
			//
			'block_user' => 'ระงับการใช้งาน',
			'block_day' => 'ระบุวัน',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id,true);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('phonenumber',$this->phonenumber,true);
		$criteria->compare('iden_id',$this->iden_id,true);
		$criteria->compare('iden_picture',$this->iden_picture,true);
		$criteria->compare('permission',$this->permission);
		$criteria->compare('active',$this->active);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function blockList()
	{
		$banList = array(
			'1' => '7 วัน',
			'2' => '1 เดือน',
			'3' => 'อื่น ๆ',
		);

		return array(null=>'โปรดเลือกการระงับการใช้งาน')+$banList;
	}

	public function checkPermission($chkPer){
		if (isset(Yii::app()->user->permission)) {
			if(Yii::app()->user->permission!=3){
				if(Yii::app()->user->permission!=$chkPer) {
					Yii::app()->getController()->redirect(Yii::app()->request->baseUrl);
				}
			}
    }
	}
	
	public function blankSequence() {
		$result = array(null=>'โปรดเลือกลำดับร้านค้า');
		$sequenceQuery = Yii::app()->db->createCommand("SELECT * FROM users order by sequence DESC Limit 1")->query();
		foreach ($sequenceQuery as $key => $value) {
			$maxSequence = $value['sequence'];
		}
		for($i=1;$i<=$maxSequence;$i++) {
			$chkSequence = self::model()->findByAttributes(array('active'=>'1', 'sequence'=>$i));
			if(sizeof($chkSequence)==0) {
				$result+=array($i=>$i);
			}
		}
		return $result;
	}

	public function checkIdenId($iden_id = null){
		$explode_iden = explode('-', $iden_id);
		
		$group_1=$explode_iden[0]; 
		$group_2=$explode_iden[1]; 
		$group_3=$explode_iden[2]; 
		$group_4=$explode_iden[3]; 
		$group_5=$explode_iden[4]; 
		
		$num1=$group_1; 
		$num2=substr($group_2,0,1); 
		$num3=substr($group_2,1,1); 
		$num4=substr($group_2,2,1); 
		$num5=substr($group_2,3,1); 
		$num6=substr($group_3,0,1); 
		$num7=substr($group_3,1,1); 
		$num8=substr($group_3,2,1); 
		$num9=substr($group_3,3,1); 
		$num10=substr($group_3,4,1);
		$num11=substr($group_4,0,1);
		$num12=substr($group_4,1,1);
		$num13=$group_5;
		
		$cal_num1=$num1*13;
		$cal_num2=$num2*12;
		$cal_num3=$num3*11;
		$cal_num4=$num4*10;
		$cal_num5=$num5*9;
		$cal_num6=$num6*8;
		$cal_num7=$num7*7;
		$cal_num8=$num8*6;
		$cal_num9=$num9*5;
		$cal_num10=$num10*4;
		$cal_num11=$num11*3;
		$cal_num12=$num12*2;
		
		$cal_sum=$cal_num1+$cal_num2+$cal_num3+$cal_num4+$cal_num5+$cal_num6+$cal_num7+$cal_num8+$cal_num9+$cal_num10+$cal_num11+$cal_num12;

		$cal_mod=$cal_sum%11;

		$cal_2=11-$cal_mod;

		$countStr = strlen($cal_2);

		if($countStr!=1) {
			$cal_2 = substr($cal_2, -1);
		}

		if ($group_1<>"" || $group_1<>"" || $group_1<>"" || $group_1<>"" || $group_1<>"" ) {
			if ($cal_2==$num13) {
				return true;
			} else {
				return false;
			}
		}
	}

	public function renewSequence($user_id = null){
		$userQuery = self::model()->findByAttributes(array('user_id' => $user_id));
		$chkSequenceQuery = self::model()->findByAttributes(array('sequence'=>$userQuery->sequence, 'active'=>1));
		if(sizeof($chkSequenceQuery)!=0) {
			$findHightSequenceQuery = Yii::app()->db->createCommand("SELECT * FROM users order by sequence DESC Limit 1")->query();
			foreach ($findHightSequenceQuery as $key => $value) {
				$oldSequence = $value['sequence'];
			}
			$newSequence = $oldSequence+1;
			$userQuery->sequence = $newSequence;
			$userQuery->update();
		}
		return true;
	}

}
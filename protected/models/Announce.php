<?php

/**
 * This is the model class for table "announce".
 *
 * The followings are the available columns in table 'announce':
 * @property integer $announceid
 * @property string $user_id
 * @property string $announcename
 * @property string $announcesubdetail
 * @property string $announcedetail
 */
class Announce extends CActiveRecord {
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Announce the static model class
	 */
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()	{
		return 'announce';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, announcename, announcesubdetail, announcedetail', 'required'),
			array('user_id', 'length', 'max'=>11),
			array('announcename', 'length', 'max'=>50),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('announceid, user_id, announcename, announcesubdetail, announcedetail', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()	{
		return array(
			'announceid' => 'Announceid',
			'user_id' => 'User',
			'announcename' => 'ชื่อ',
			'announcesubdetail' => 'รายละเอียดส่วนย่อย',
			'announcedetail' => 'รายละเอียด',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search() {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('announceid',$this->announceid);
		$criteria->compare('user_id',$this->user_id,true);
		$criteria->compare('announcename',$this->announcename,true);
		$criteria->compare('announcesubdetail',$this->announcesubdetail,true);
		$criteria->compare('announcedetail',$this->announcedetail,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function tblAnnounceData()	{
		$tblDetail = null;
		$announceQuery = self::Model()->findAllByAttributes(array('user_id' => 'Admin'), array('order'=>'announceid'));
		foreach ($announceQuery as $announceKey => $announceValue) {
			$tblDetail .= CHtml::openTag('tr');
			$tblDetail .= CHtml::openTag('td');
      $tblDetail .= CHtml::checkBox('ANNOUNCE_DELETE['.$announceValue['announceid'].']',false,array('value'=>$announceValue['announceid']));
      $tblDetail .= CHtml::closeTag('td');
      $tblDetail .= CHtml::openTag('td');
      $tblDetail .= ($announceKey+1);
      $tblDetail .= CHtml::closeTag('td');
      $tblDetail .= CHtml::openTag('td');
      $tblDetail .= $announceValue['announcename'];
      $tblDetail .= CHtml::closeTag('td');
      $tblDetail .= CHtml::openTag('td');
      $tblDetail .= CHtml::link('แก้ไข', '#', array('id' => 'btn-edit', 'name'=>'editAnnounce', 'value_id'=>$announceValue['announceid']));
      $tblDetail .= CHtml::closeTag('td');
      $tblDetail .= CHtml::closeTag('tr');
		}
		return $tblDetail;
	}

	public function getAnnounceData($page=null){

		$findAnnounce = Announce::model()->findByAttributes(array('announceid'=>1));
		$data = null;
		if (!is_null($findAnnounce)) {

			if ($page=='index') {
				$head = '<h1>'.$findAnnounce['announcename'].'</h1>';
			}else{
				$head = '<h4>'.$findAnnounce['announcename'].'</h4>';
			}
		
			$data .= CHtml::openTag('div',array('class'=>'fields row-fluid headerForm sizeContent'));
			$data .= CHtml::openTag('div',array('class'=>'span12'));
			$data .= $head;
	    $data .= CHtml::closeTag('div');
	    $data .= CHtml::closeTag('div');

	    $data .= CHtml::openTag('div',array('class'=>'row-fluid headerForm sizeContent'));
			$data .= CHtml::openTag('div',array('class'=>'span12'));
			$data .= $findAnnounce['announcedetail'];
	    $data .= CHtml::closeTag('div');
	    $data .= CHtml::closeTag('div');
	    $data .= '<hr>';
		}

    return $data;
	}

}
<?php

/**
 * This is the model class for table "shopsmassage".
 *
 * The followings are the available columns in table 'shopsmassage':
 * @property integer $shopsmassageid
 * @property string $user_id
 * @property string $shopsmassagedetail
 * @property string $shopsmassagetype
 */
class Shopsmassage extends CActiveRecord {
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Shopsmassage the static model class
	 */
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()	{
		return 'shopsmassage';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, shopsmassagedetail, shopsmassagetype', 'required'),
			array('user_id, shopsmassagetype', 'length', 'max'=>11),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('shopsmassageid, user_id, shopsmassagedetail, shopsmassagetype', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()	{
		return array(
			'shopsmassageid' => 'Shopsmassageid',
			'user_id' => 'User',
			'shopsmassagedetail' => 'Shopsmassagedetail',
			'shopsmassagetype' => 'Shopsmassagetype',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search() {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('shopsmassageid',$this->shopsmassageid);
		$criteria->compare('user_id',$this->user_id,true);
		$criteria->compare('shopsmassagedetail',$this->shopsmassagedetail,true);
		$criteria->compare('shopsmassagetype',$this->shopsmassagetype,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function getMassage($massageType = null, $user_id) {
		$massage = null;
		$dataQuery = Shopsmassage::model()->findByAttributes(array('shopsmassagetype' => $massageType, 'user_id'=>$user_id));
		if(sizeof($dataQuery)!=0) {
			$massage = $dataQuery->shopsmassagedetail;
		}

		return $massage;
	}
	
}
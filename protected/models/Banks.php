<?php

/**
 * This is the model class for table "banks".
 *
 * The followings are the available columns in table 'banks':
 * @property integer $bank_id
 * @property string $user_id
 * @property integer $bank_name
 * @property integer $bank_type
 * @property integer $bank_Branch
 * @property string $account_name
 * @property string $account_number
 */
class Banks extends CActiveRecord {
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Banks the static model class
	 */
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()	{
		return 'banks';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, bank_name, bank_type, account_name, account_number', 'required'),
			array('bank_name, bank_type', 'numerical', 'integerOnly'=>true),
			array('user_id, account_name, account_number', 'length', 'max'=>50),
			array('bank_Branch', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('bank_id, user_id, bank_name, bank_type, account_name, account_number', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels() {
		return array(
			'bank_id' => 'Bank',
			'user_id' => 'User',
			'bank_name' => 'ธนาคาร',
			'bank_type' => 'ประเภทบัญชี',
			'bank_Branch' => 'สาขา',
			'account_name' => 'ชื่อบัญชี',
			'account_number' => 'เลขที่บัญชี',
			//
			'title_bank_name' => 'ธนาคาร',
			'title_bank_type' => 'ประเภทบัญชี',
			'title_account_name' => 'ชื่อบัญชี',
			'title_account_number' => 'เลขที่บัญชี',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search() {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('bank_id',$this->bank_id);
		$criteria->compare('user_id',$this->user_id,true);
		$criteria->compare('bank_name',$this->bank_name);
		$criteria->compare('bank_type',$this->bank_type);
		$criteria->compare('account_name',$this->account_name,true);
		$criteria->compare('account_number',$this->account_number,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function getBank() {
		$bankName = array(
			'1'=>'ธ.กรุงเทพ',
			'2'=>'ธ.ทหารไทย',
			'3'=>'ธ.กรุงศรีอยุธยา',
			'4'=>'ธ.กสิกรไทย',
			'5'=>'ธ.ดีบีเอสไทยทนุ',
			'6'=>'ธ.ไทยพาณิชย์',
			'7'=>'ธ.ธนชาต',
			'8'=>'ธ.นครหลวงไทย ',
			'9'=>'ธ.ยูโอบีรัตนสิน',
			'10'=>'ธ.สแตนดาร์ชาร์ตอร์นครธน',
			'11'=>'ธ.เอเชีย',
			'12'=>'ธ.กรุงไทย',
			);

    return array(null=>'โปรดเลือกธนาคาร')+$bankName;
	}

	public function getBankType() {
		$bankType = array(
			'1'=>'สะสมทรัพย์',
			'2'=>'กระแสรายวัน',
			);

		return array(null=>'โปรดเลือกประเภทบัญชี ')+$bankType;
	}
}
<?php

/**
 * This is the model class for table "massage".
 *
 * The followings are the available columns in table 'massage':
 * @property integer $massageid
 * @property string $massagename
 * @property string $massagetype
 * @property string $user_id
 */
class Massage extends CActiveRecord {
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Massage the static model class
	 */
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()	{
		return 'massage';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('massagename, massagetype', 'required'),
			array('massagetype', 'length', 'max'=>50),
			array('user_id', 'length', 'max'=>11),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('massageid, massagename, massagetype', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()	{
		return array(
			'massageid' => 'Massageid',
			'massagename' => 'Massagename',
			'massagetype' => 'Massagetype',
			'user_id' => 'Userid',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search() {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('massageid',$this->massageid);
		$criteria->compare('massagename',$this->massagename,true);
		$criteria->compare('massagetype',$this->massagetype,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function getMassage($massageType = null) {
		$massage = null;
		$massageQuery = Massage::model()->findByAttributes(array('massagetype' => $massageType));
		if(sizeof($massageQuery)!=0) {
			$massage = $massageQuery->massagename;
		}

		return $massage;
	}

	public function getDisiplineData(){
    
		$findMessage = Massage::model()->findByAttributes(array('massageid' => 2));;
		$data = null;
		if (!is_null($findMessage)) {
	    $data .= CHtml::openTag('div',array('class'=>'row-fluid headerForm sizeContent'));
			$data .= CHtml::openTag('div',array('class'=>'span12'));
			$data .= $findMessage['massagename'];
	    $data .= CHtml::closeTag('div');
	    $data .= CHtml::closeTag('div');
	    $data .= '<hr>';
		}

    return $data;
	}

}
<?php

/**
 * This is the model class for table "payment".
 *
 * The followings are the available columns in table 'payment':
 * @property integer $pay_id
 * @property string $user_id
 * @property string $pay_type
 * @property string $date_begin
 * @property string $date_end
 * @property string $date_pay
 * @property string $pay_amount
 * @property string $pay_status
 */
class Payment extends CActiveRecord {
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Payment the static model class
	 */
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()	{
		return 'payment';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, pay_type', 'required'),
			array('user_id, pay_type', 'length', 'max'=>50),
			array('pay_amount, pay_status', 'numerical', 'integerOnly'=>true),
			array('date_begin, date_end, date_pay', 'date', 'format'=>'yyyy-MM-dd'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('pay_id, user_id, pay_type, pay_amount, pay_status, date_begin, date_end, date_pay', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()	{
		return array(
			'pay_id' => 'Pay',
			'user_id' => 'User',
			'pay_type' => 'สมาชิกแบบราย : ',
			'date_begin' => 'วันเริ่มต้น',
			'date_end' => 'วันหมดอายุ',
			'date_pay' => 'วันที่จ่าย',
			'pay_amount' => 'เป็นจำนวนเงิน',
			'pay_status' => 'payStatus',
			//
			'title_pay_type' => 'สมาชิกแบบราย',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search() {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('pay_id',$this->pay_id);
		$criteria->compare('user_id',$this->user_id,true);
		$criteria->compare('pay_type',$this->pay_type,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function getPaymentType() {
		$type = array(
			'12'=>'12 เดือน',
			'24'=>'24 เดือน',
			'36'=>'36 เดือน',
			);

		return $type;
	}

	public function getPaymentDetail($user_id = null) {
		$itemsDetail = array();
		$payQuery = Payment::model()->findByAttributes(array('user_id' => $user_id));
		foreach ($payQuery as $key => $value) {
			$itemsDetail[$key] = $value;
		}
		$explodeDateBegin = explode('-', $itemsDetail['date_begin']);
		$itemsDetail['date_begin'] = $explodeDateBegin[2].'/'.$explodeDateBegin[1].'/'.$explodeDateBegin[0];

		return $itemsDetail;
	}

	public function getPaymentHistory($user_id = nill)	{
		$paymentArray = array();
		$paymentQuery = self::model()->findAllByAttributes(array('user_id' => $user_id));
		foreach ($paymentQuery as $key => $value) {
			array_push($paymentArray, $value);
		}
		return $paymentArray;
	}

	public function checkExpireShop()	{
		$paymentData = Yii::app()->db->createCommand('SELECT * FROM users INNER JOIN payment ON(users.user_id=payment.user_id) WHERE payment.pay_status = 1')->query();
		foreach ($paymentData as $key => $value) {
			$explodeEndDate = explode("-", $value['date_end']);
			$minusEndDate = date('Y-m-d', mktime(0, 0, 0, $explodeEndDate[1], $explodeEndDate[2]-14, $explodeEndDate[0]));
			$nowDate = date('Y-m-d');

			if($minusEndDate==$nowDate) {
				$msisdn = $value['phonenumber'];
				$msisdn = preg_replace('#[^0-9]#u', '', $msisdn);
				$messageDetail = 'ร้านค้าของท่านกำลังจะหมดอายุภายในวันที่ '.$explodeEndDate[2].'/'.$explodeEndDate[1].'/'.$explodeEndDate[0];
				SMS::SendSMS($msisdn, $messageDetail);
			}elseif($value['date_end']==$nowDate) {
				Users::renewSequence($value['user_id']);
				Users::model()->updateAll(array('active'=>2), 'user_id = "'.$value['user_id'].'"');
				Payment::model()->updateAll(array('pay_status'=>2), 'user_id = "'.$value['user_id'].'"');
			}
		}
	}

	public function tblAllPaymentData()	{
		$tblData = null;
		$paymentData = Yii::app()->db->createCommand('SELECT *,shops.name AS shopname, users.name AS firstName, users.surname AS lastName FROM users INNER JOIN shops ON(users.user_id=shops.user_id) INNER JOIN payment ON(users.user_id=payment.user_id) ORDER BY pay_id ASC')->query();
		foreach ($paymentData as $key => $value) {
			$explode_shopname = explode('&&', $value['shopname']);
			$tblData .= CHtml::openTag('tr');
			$tblData .= CHtml::openTag('td');
      $tblData .= $explode_shopname[1];
      $tblData .= CHtml::closeTag('td');
      $tblData .= CHtml::openTag('td');
      $tblData .= $value['firstName'].' '.$value['lastName'];
      $tblData .= CHtml::closeTag('td');
      $tblData .= CHtml::openTag('td');
      $tblData .= $value['date_pay'];
      $tblData .= CHtml::closeTag('td');
      $tblData .= CHtml::openTag('td', array('style'=>'text-align:right;'));
      $tblData .= number_format($value['pay_amount'], 2, '.', ',');
      $tblData .= CHtml::closeTag('td');
      $tblData .= CHtml::openTag('td');
		}
		return $tblData;
	}

	public function tblPaymentData($user_id = null)	{
		$tblData = null;
		$paymentData = self::model()->findAllByAttributes(array('user_id' => $user_id), array('order'=>'pay_id'));
		foreach ($paymentData as $key => $value) {
			$tblData .= CHtml::openTag('tr');
			$tblData .= CHtml::openTag('td');
      $tblData .= $value['date_begin'];
      $tblData .= CHtml::closeTag('td');
      $tblData .= CHtml::openTag('td');
      $tblData .= $value['date_end'];
      $tblData .= CHtml::closeTag('td');
      $tblData .= CHtml::openTag('td');
      $tblData .= $value['date_pay'];
      $tblData .= CHtml::closeTag('td');
      $tblData .= CHtml::openTag('td');
      $tblData .= number_format($value['pay_amount'], 2, '.', ',');
      $tblData .= CHtml::closeTag('td');
      $tblData .= CHtml::openTag('td');
		}
		return $tblData;
	}

}
<?php
  Yii::app()->counter->refresh();
  Viewsite::model()->updateView();
  $getCountView = StatisticsView::getCountView();
?>
<div id="block-statistics" class="statistics clearfix">
  <h2>สถิติเว็บไซต์</h2>
  <div class="row-fluid lists">
    <div class="span7"><i class="icon-info-sign icon-white"></i>ร้านค้า</div>
    <div class="span5 value"><?php echo Statistics::getCountShop(); ?></div>
  </div>
  <div class="row-fluid lists">
    <div class="span7"><i class="icon-info-sign icon-white"></i>สินค้า</div>
    <div class="span5 value"><?php echo Statistics::getCountProduct(); ?></div>
  </div>
  <div class="row-fluid lists">
    <div class="span7"><i class="icon-info-sign icon-white"></i>ผู้ชมรวม</div>
    <div class="span5 value"><?php echo number_format(Yii::app()->counter->getTotal()); ?></div>
    <!-- <div class="span5 value"><?php echo number_format($getCountView['total']); ?></div> -->
  </div>
  <div class="row-fluid lists">
    <div class="span7"><i class="icon-info-sign icon-white"></i>ผู้ชมวันนี้</div>
    <div class="span5 value"><?php echo number_format(Yii::app()->counter->getToday()); ?></div>
    <!-- <div class="span5 value"><?php echo number_format($getCountView['today']); ?></div> -->
  </div>
  <div class="row-fluid lists">
    <div class="span7"><i class="icon-info-sign icon-white"></i>ผู้ชมขณะนี้</div>
    <div class="span5 value"><?php echo number_format(Yii::app()->counter->getOnline()); ?></div>
    <!-- <div class="span5 value"><?php echo Statistics::getCountViewNow(); ?></div> -->
  </div>
</div>

<!-- online: <?php echo Yii::app()->counter->getOnline(); ?><br />
today: <?php echo Yii::app()->counter->getToday(); ?><br />
yesterday: <?php echo Yii::app()->counter->getYesterday(); ?><br />
total: <?php echo Yii::app()->counter->getTotal(); ?><br />
maximum: <?php echo Yii::app()->counter->getMaximal(); ?><br />
date for maximum: <?php echo date('d.m.Y', Yii::app()->counter->getMaximalTime()); ?> -->
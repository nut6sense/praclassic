<?php

  $menuAdmin = array(
    'manageShop' => array('text' => 'ระบบจัดการร้านค้า'),
    );

  $menuAdmin['manageShop']['child'] = array(
    'dataShop' => array('label' => 'ข้อมูลร้านค้า','link' => '/shop/dataShop/'.Yii::app()->input->get('param1')),
    'dataProduct' => array('label' => 'ข้อมูลสินค้า','link' => '/products/dataProduct/'.Yii::app()->input->get('param1')),
    'popularProduct' => array('label' => 'พระเด่น','link' => '/products/popularProduct/'.Yii::app()->input->get('param1')),
    'announceShop' => array('label' => 'ประกาศร้านค้า','link' => '/shop/announceShop/'.Yii::app()->input->get('param1')),
    'newsShop' => array('label' => 'ข่าวประชาสัมพันธ์','link' => '/shop/newsShop/'.Yii::app()->input->get('param1')),
    'privateMessage' => array('label' => 'ข้อความส่วนตัว','link' => ''),
    'profileShop' => array('label' => 'แก้ไขข้อมูลร้านค้า','link' => '/shop/profileShop/'.Yii::app()->input->get('param1')),
    'logoShop' => array('label' => 'เปลี่ยน Logo ร้าน','link' => ''),
    'gotoShop' => array('label' => 'ไปร้านค้า','link' => ''),
    'logout' => array('label' => 'ออกจากระบบ','link' => '/site/logout'),
    );
?>
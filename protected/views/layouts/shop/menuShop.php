<?php
  require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . '_menuShopSetting.php'; 

  echo CHtml::openTag('ul', array('class' => 'membermenu links'));

  foreach ($menuAdmin as $kMenu => $vMenu) {
    if(isset($menuAdmin[$kMenu]['child'])){
      foreach ($menuAdmin[$kMenu]['child'] as $keyChild => $valueChild) {
        echo CHtml::openTag('li', array('id-menu' => $keyChild));
        if($valueChild['link']==''){
          $linkURL = '#';
        }else{
          $linkURL = Yii::app()->request->baseUrl . $valueChild['link'];
        }
        echo CHtml::link($valueChild['label'], $linkURL);
        echo CHtml::closeTag('li');
      }
    }
  }
  echo CHtml::closeTag('ul');
  Yii::app()->clientScript->registerScript('activeMenu',"$('[id-menu]').removeAttr('class','active');$('#leftMenuAdmin').find('[id-menu=\"".Yii::app()->controller->action->id."\"]').attr('class','active');");
?>

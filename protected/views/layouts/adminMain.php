<!DOCTYPE html>
<html lang="th">
  <head>
    <meta charset="utf-8">
    <meta name="language" content="th" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    
    <!-- SEO -->
    <meta name="keyword" content="">
    <meta name="description" content="">
    <title>พระคลาสสิก แหล่งรวมพระเครื่อง พระคลาสสิก พระดังต่างๆ จากทั่วประเทศไทย | พระคลาสสิก.com หรือ (<?php echo CHtml::encode($this->pageTitle); ?>)</title>
    <!--[if lt IE 9]> HTML5Shiv
        <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <!-- Bootstrap -->
    <?php $this->renderPartial('/layouts/plugin/bootstrap'); ?>
    <!-- Highslide -->
    <?php $this->renderPartial('/layouts/plugin/highslide'); ?>

    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />

    <!-- Override -->
    <?php $this->renderPartial('/layouts/plugin/template'); ?>

    <!-- UserCounter -->
    <?php $this->renderPartial('/layouts/plugin/userCounter'); ?>

    <script type="text/javascript">
      window.baseUrl = '<?php echo Yii::app()->request->baseUrl; ?>';
    </script>
  </head>
  <?php

    Users::model()->checkPermission(3);

    $attributes = null;
    $uri = explode('/', Yii::app()->request->url);
    foreach ($uri as $value) {
      if($value != "praclassic" && $value != "site" && !empty($value)) {
        $attributes .= $value." ";
      }
    }
  ?>
  <body class="<?php print $attributes; ?>">
    <div class="container" id="container-main">
      <div class="row-fluid">
        <!-- Banner -->
        <div class="span12 banner"><img src="<?php echo Yii::app()->request->baseUrl; ?>/img/banner.png" alt="banner" /></div>
        <!-- Navigation -->
        <div class="row-fluid navigation">
          <div class="span12 navigation">
            <div id="block-navigation" class="clearfix">
              <div class="navbar">
                <div class="navbar-inner">
                  <div class="container" style="background-color: #000;">
<!--                     <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                    </a>
                    <a class="brand hidden-desktop">พระคลาสสิก - PraClassic</a>
                    <div class="nav-collapse collapse"> -->
                      <?php $this->renderPartial('/layouts/topMenu'); ?>
                    <!-- </div> -->
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- Content -->
        <div class="row-fluid content">
          <!-- Sideleft -->
          <div class="span3" style="width: 17%;">

            <div class="span12 sideleft colorBox1">
              <!-- Block: Statistics -->
              <?php $this->renderpartial('/layouts/statistics'); ?>
            </div>

            <div class="span12 sideleft colorBox2" style="margin: 0px 0px 20px 0px;">
            
              <!-- Block: Adminmenu -->
              <div id="block-adminmenu" class="block adminmenu clearfix">
                <h2>เมนูผู้ดูแลระบบ</h2>
                <?php $this->renderPartial('/layouts/admin/menuAdmin'); ?>
              </div>
            </div>
          </div>
          <!-- Maincontent -->
          <div class="span9 sideleft colorBox3 main" style="width: 80.38%;">
            <?php echo $content; ?>
          </div>
        </div>
        <!-- Footer -->
        <div class="row-fluid footer" style="background-color: #000;">
          <div class="span12 visible-desktop">
            <?php
            $this->widget('zii.widgets.CMenu', array(
              'htmlOptions' => array('class' => 'navigation links'),
              'items' => array(
                array('label' => 'หน้าแรก', 'url'=>array('/site/index')),
                array('label' => 'รายการพระเครื่อง', 'url'=>array('/site/shop_list')),
                array('label' => 'ร้านพระมาตรฐาน', 'url'=>array('/site/shop_standard')),
                array('label' => 'ข่าวประชาสัมพันธ์', 'url'=>array('/site/news')),
                array('label' => 'ระเบียบการใช้งาน', 'url'=>array('/site/discipline')),
                array('label' => 'ติดต่อเรา', 'url'=>array('/site/contactus')),
              ),
            ));
            ?>
          </div>
          <div class="row-fluid">
            <div class="span12">© สงวนลิขสิทธิ์ตามกฏหมายเกี่ยวกับทุกบทความและความคิดเห็นบนเว็บไซต์นี้</div>
          </div>
        </div>
      </div>
    </div>
    <link rel="stylesheet/less" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/plugin/bootstrap/less/datepicker.less">
    <?php
      Dialog::alertSuccess();
      Dialog::alertError();
      //-- AllScriptFile
      $this->renderPartial('/layouts/plugin/allScript');
      
    ?>
  </body>
</html>
<?php
  require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . '_menuAdminSetting.php'; 

  echo CHtml::openTag('ul', array('class' => 'nav nav-list'));

  foreach ($menuAdmin as $kMenu => $vMenu) {

    echo CHtml::openTag('li', array('class' => 'nav-header'));
    echo $vMenu['text'];
    echo CHtml::closeTag('li');

    if(isset($menuAdmin[$kMenu]['child'])){

      foreach ($menuAdmin[$kMenu]['child'] as $keyChild => $valueChild) {
        echo CHtml::openTag('li', array('id-menu' => $keyChild));
        if($valueChild['link']==''){
          $linkURL = '#';
        }else{
          $linkURL = Yii::app()->request->baseUrl . $valueChild['link'];
        }

        $alert = null;
        if (isset($valueChild['alert'])&&$valueChild['alert']!=0) {
          $alert = ' <span class="alertCount">'.$valueChild['alert'].'</span>';
        }

        if (isset($valueChild['count'])&&$valueChild['count']!=0) {
          $alert = ' <span class="countBox">'.$valueChild['count'].'</span>';
        }

        echo CHtml::link($valueChild['label'].$alert, $linkURL);
        echo CHtml::closeTag('li');
      }

    }
    
  }

  echo CHtml::closeTag('ul');

  //
  //Script activeMenu
  Yii::app()->clientScript->registerScript('activeMenu',"$('[id-menu]').removeAttr('class','active');$('#leftMenuAdmin').find('[id-menu=\"".Yii::app()->controller->action->id."\"]').attr('class','active');");
?>

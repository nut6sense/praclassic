<?php

	$menuAdmin = array(
    'manageSystem' => array('text' => 'ระบบจัดการ'),
    'member' =>  array('text' => 'สมาชิกทั่วไป'),
    'shop' =>  array('text' => 'ร้านค้ามาตรฐาน'),
    'blockShop' =>  array('text' => 'ร้านค้าถูกระงับ'),
    'blockProduct' =>  array('text' => 'สินค้าที่ถูกล๊อค'),
    'productReference' =>  array('text' => 'สินค้าที่ไม่มีการรับรอง'),
    'contact' =>  array('text' => 'รายการติดต่อสอบถาม'),
    'payConfirm' =>  array('text' => 'การชำระเงิน'),
    'category' =>  array('text' => 'หมวดหมู่พระ'),
    'news' =>  array('text' => 'ระบบข่าว'),
    'mailSystem' =>  array('text' => 'ระบบจดหมายข่าว'),
    'setting' =>  array('text' => 'ตั้งค่าเว็บไซต์'),
    'other' =>  array('text' => 'อื่นๆ'),
    );

  $menuAdmin['manageSystem']['child'] = array(
    'ShopMassage' => array('label' => 'ข้อความถึงร้านค้าทุกร้าน','link' => '/Massage/ShopMassage'),
    'RuleMassage' => array('label' => 'ระเบียบการใช้งาน','link' => '/Massage/RuleMassage'),
    'GalleryMassage' => array('label' => 'ข้อความ GALLERY','link' => '/Massage/GalleryMassage'),
    'PremiumMassage' => array('label' => 'ข้อความ PREMIUM','link' => '/Massage/PremiumMassage'),

    );

  $menuAdmin['member']['child'] = array(
    'allMember' => array('label' => 'สมาชิกทั้งหมด','link' => ''),

    );

  $menuAdmin['shop']['child'] = array(
    'newShop' => array('label' => 'ร้านค้าใหม่','link' => '/shop/newShop', 'alert'=>Shops::model()->getAlertNewShop()),
    'allShop' => array('label' => 'ร้านค้าทั้งหมด','link' => '/shop/allShop', 'count'=>Shops::model()->getCountShop()),
    'expireShop' => array('label' => 'ร้านค้าที่หมดอายุ','link' => '/shop/expireShop', 'count'=>Shops::model()->getCountShop(2)),

    );

  $menuAdmin['blockShop']['child'] = array(
    'banShop' => array('label' => 'ร้านค้าถูกระงับ (7 วัน)','link' => '/shop/banShop', 'count'=>Shops::model()->getCountBanShop(1)),
    'banMonthShop' => array('label' => 'ร้านค้าถูกระงับ (1 เดือน)','link' => '/shop/banMonthShop', 'count'=>Shops::model()->getCountBanShop(2)),
    'banOtherShop' => array('label' => 'ร้านค้าถูกระงับ (ระบุวัน)','link' => '/shop/banOtherShop', 'count'=>Shops::model()->getCountBanShop(3)),
    );

  $menuAdmin['blockProduct']['child'] = array(
    'banProduct' => array('label' => 'สินค้าที่ถูกล๊อคทั้งหมด','link' => '/products/banProduct', 'count'=>Products::model()->getAlertBlockProduct(1)),
    );

  $menuAdmin['productReference']['child'] = array(
    'productReferences' => array('label' => 'สินค้าไม่มีการรับรอง','link' => '/products/productReferences', 'alert'=>Products::model()->getAlertProduct()),
  );

  $menuAdmin['contact']['child'] = array(
    'message' => array('label' => 'ติชมแนะนำเว็บ','link' => '/contact/message', 'alert'=>Contact::model()->getAlertMessage(5)),
    'issue' => array('label' => 'แจ้งปัญหาร้องเรียน','link' => '/contact/issue', 'alert'=>Contact::model()->getAlertMessage(4)),
    'mentor' => array('label' => 'ที่ปรึกษาด้านพระเครื่อง','link' => '/contact/mentor', 'alert'=>Contact::model()->getAlertMessage(1)),

    );

  $menuAdmin['payConfirm']['child'] = array(
    'payConfirm' => array('label' => 'ยืนยันการชำระเงิน','link' => '/admin/ShowPayConfirm'),

    );

  $menuAdmin['category']['child'] = array(
    'category' => array('label' => 'เพิ่ม / แก้ไข หมวดหมู่พระ','link' => '/category/category'),

    );

  $menuAdmin['news']['child'] = array(
    'announce' => array('label' => 'เพิ่ม / แก้ไข ประกาศ','link' => '/announce/dataAnnounce'),
    'news' => array('label' => 'เพิ่ม / แก้ไข ข่าวประชาสัมพันธ์','link' => '/news/news'),

    );

  $menuAdmin['mailSystem']['child'] = array(
    'sendMessage' => array('label' => 'ส่งจดหมายข่าว','link' => '/admin/sendEmail'),
    'mailMember' => array('label' => 'รายชื่อ อีเมล์','link' => '/shop/mailMember'),

    );

  $menuAdmin['setting']['child'] = array(
    'settingWebsite' => array('label' => 'กำหนดรูปแบบเว็บไซต์','link' => '/admin/settingWebsite'),
    'settingMain' => array('label' => 'กำหนดค่าหลัก','link' => '/admin/settingMain'),

    );

   $menuAdmin['other']['child'] = array(
    'profile' => array('label' => 'แก้ไขข้อมูลส่วนตัว','link' => '/admin/editProfile'),
    'allPayment' => array('label' => 'ประวัติการชำระเงิน','link' => '/shop/allPayment'),
    'sendSms' => array('label' => 'การส่ง SMS','link' => '/admin/sendSms'),
    'gallery' => array('label' => 'GALLERY','link' => '/gallery/gallery'),
    'premium' => array('label' => 'PREMIUM','link' => '/premium/premium'),
    'logout' => array('label' => 'ออกจากระบบ','link' => '/site/logout'),
    'website' => array('label' => 'ดูหน้าแรกเว็บไซต์','link' => '/'),
    );

?>
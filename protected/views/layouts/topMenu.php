<?php
  $topMenu = Topmenu::model()->findAll(array('order'=>'topmenu_number ASC'));
  $items = array();
  foreach ($topMenu as $key => $value) {
    array_push($items, array('label' => $value['topmenu_topic'], 'url'=>array($value['topmenu_link'])));
  }

  $this->widget('zii.widgets.CMenu', array(
    'htmlOptions'=>array('class'=>'navigation links topMenuLink'),
    'items' => $items,
  ));
?>
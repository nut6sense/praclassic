<?php /* @var $this Controller */ ?>
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8">
	<meta name="language" content="th" />

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />

  <!-- Layouts and overrides by Tan -->
  <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/layout.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/override.css" />

	<!-- ของใหม่ -->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="language" content="en" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">

	<link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/plugin/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/custom.css">

    <script type="text/javascript">
      window.baseUrl = '<?php echo Yii::app()->request->baseUrl; ?>';
    </script>
    <!-- ///////// -->

	<title>พระคลาสสิก แหล่งรวมพระเครื่อง พระคลาสสิก พระดังต่างๆ จากทั่วประเทศไทย | พระคลาสสิก.com หรือ (<?php echo CHtml::encode($this->pageTitle); ?>)</title>
</head>

<body>
  <section id="container">
    <header class="clearfix">
      <div class="banner"><img src="http://folio.tzv.me/praclassic/img/img001_header.jpg" alt="banner" /></div>
      <nav class="clearfix">
        <?php
        $this->widget('zii.widgets.CMenu',array(
          'items' => array(
            array('label' => 'หน้าแรก', 'url'=>array('/site/index')),
            array('label' => 'รายการพระเครื่อง', 'url'=>array('/site/shop-list')),
            array('label' => 'ร้านพระมาตรฐาน', 'url'=>array('/site/shop-standard')),
            array('label' => 'ข่าวประชาสัมพันธ์', 'url'=>array('/site/news')),
            array('label' => 'ระเบียบการใช้งาน', 'url'=>array('/site/discipline')),
            array('label' => 'ติชมแนะนำเว็บไซต์', 'url'=>array('/site/comment')),
            array('label' => 'แจ้งปัญหาร้องเรียน', 'url'=>array('/site/report')),
          ),
        ));
        ?>
      </nav>
    </header>
    <article class="contentArea clearfix">
      <div class="leftSide">
        <?php
        if(empty(Yii::app()->user->userKey)){

          $this->renderpartial('/layouts/login');

        }else{
        ?>
        <div id="member_logging">
          <?php
            echo "<pre>";
            print_r(Yii::app()->user->userKey);
            echo "</pre>";
            echo CHtml::link('Logout',Yii::app()->request->baseUrl."/site/logout");
          ?>
        </div>
        <?php
        }
        ?>
      </div>
      <div class="rightSide"><?php echo $content; ?></div>
    </article>
    <footer id="footer" class="clearfix">
      <nav>
      <?php
        $this->widget('zii.widgets.CMenu', array(
          'items' => array(
            array('label' => 'หน้าแรก', 'url'=>array('/site/index')),
            array('label' => 'รายการพระเครื่อง', 'url'=>array('/site/shop-list')),
            array('label' => 'ร้านพระมาตรฐาน', 'url'=>array('/site/shop-standard')),
            array('label' => 'ข่าวประชาสัมพันธ์', 'url'=>array('/site/news')),
            array('label' => 'ระเบียบการใช้งาน', 'url'=>array('/site/discipline')),
            array('label' => 'ติชมแนะนำเว็บไซต์', 'url'=>array('/site/comment')),
            array('label' => 'แจ้งปัญหาร้องเรียน', 'url'=>array('/site/report')),
            array('label' => 'ผู้ดูแลระบบ', 'url'=>array('/admin')),
          ),
        ));
      ?>
      </nav>
      <div class="description">Copyright©2013 www.parclassic.com, www.พระคลาสสิก.com All Rights Reserved</div>
    </footer>
  </section>
  <?php
    Dialog::alertSuccess();
    Dialog::alertError();
  ?>

  <?php

    Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/plugin/ckeditor/ckeditor.js');
    Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/plugin/ckeditor/samples/sample.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/js/jquery-1.9.1.js', CClientScript::POS_END);
    // Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/js/jquery-ui-1.8.23.custom.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/plugin/bootstrap/js/bootstrap.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/js/jquery.form.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/js/scripts.js', CClientScript::POS_END);
  ?>
</body>
</html>

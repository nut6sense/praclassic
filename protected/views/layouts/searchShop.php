<?php
  $inputOption = array('class'=>'span12');
  $form = $this->beginWidget('CActiveForm', array(
    'id' => 'search-shop-form',
    'action' => Yii::app()->request->baseUrl . '/site/shop_standard',
  ));
  $model_shops = new Shops;
?>
<div class="items" style="margin-top:10px;">
  <div class="item-field">
    <?php echo $form->textField($model_shops, 'name', array('placeholder'=>'ค้นหาร้านพระ','class'=>'span')); ?>
  </div>
</div>
<div class="actions" style="text-align: center;">
  <?php echo CHtml::submitButton('ค้นหา', array('class' => 'btn btn-primary span7')); ?>
</div>
<?php
  $this->endWidget();
?>
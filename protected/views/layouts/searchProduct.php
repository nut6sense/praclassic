<?php
  $inputOption = array('class'=>'span12');
  $form = $this->beginWidget('CActiveForm', array(
    'id' => 'search-product-form',
    'action' => Yii::app()->request->baseUrl . '/site/shop_list',
  ));

  $model_products = new Products;
  $model_category = new Category;
?>
<div class="items">
  <label>ค้นหาพระเครื่อง</label>
  <div class="item-field">
    <?php echo $form->textField($model_products, 'productname', array('placeholder'=>'กรอกคำค้นหา','class'=>'span')); ?>
  </div>
  <label>เลือกหมวดค้นหา</label>
  <div class="item-field">
    <?php echo $form->DropDownList($model_category, 'category_name', Category::getCategoryData(),array('class'=>'span')); ?>
  </div>
</div>
<div class="actions" style="text-align: center;">
  <?php echo CHtml::submitButton('ค้นหา', array('id' => 'btn-search', 'class' => 'btn btn-primary span7')); ?>
</div>
<?php
  $this->endWidget();
  Dialog::alertMessage();  
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="language" content="en" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    
    <!-- SEO -->
    <meta name="keyword" content="">
    <meta name="description" content="">
    <title>พระคลาสสิก แหล่งรวมพระเครื่อง พระคลาสสิก พระดังต่างๆ จากทั่วประเทศไทย | พระคลาสสิก.com หรือ (<?php echo CHtml::encode($this->pageTitle); ?>)</title>
    <!--[if lt IE 9]> HTML5Shiv
        <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <!-- Bootstrap -->
    <?php $this->renderPartial('/layouts/plugin/bootstrap'); ?>
    <!-- Highslide -->
    <?php $this->renderPartial('/layouts/plugin/highslide'); ?>

    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />

    <!-- Override -->
    <?php $this->renderPartial('/layouts/plugin/template'); ?>

    <!-- UserCounter -->
    <?php $this->renderPartial('/layouts/plugin/userCounter'); ?>

    <script type="text/javascript">
      window.baseUrl = '<?php echo Yii::app()->request->baseUrl; ?>';
    </script>

    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
  </head>

  <body>

    <?php echo $content; ?>

  <?php
    Dialog::alertSuccess();
    Dialog::alertError();
    
    //-- AllScriptFile
      $this->renderPartial('/layouts/plugin/allScript');
      
  ?>

  </body>
</html>
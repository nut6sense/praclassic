<?php
  $inputOption = array('class'=>'span12');
  $form = $this->beginWidget('CActiveForm', array(
    'id' => 'login-form',
    'action' => Yii::app()->request->baseUrl . '/shop/login',
  ));
  $model = new LoginForm;
?>
<div class="items">
  <label for="field_username"><?php echo Users::model()->getAttributeLabel('username'); ?></label>
  <div class="item-field">
    <?php echo $form->textField($model, 'username', $inputOption); ?>
  </div>
  <label for="field_password"><?php echo Users::model()->getAttributeLabel('password'); ?></label>
  <div class="item-field">
    <?php echo $form->passwordField($model, 'password', $inputOption); ?>
  </div>
</div>
<div class="actions">
  <?php echo CHtml::submitbutton('เข้าสู่ระบบ', array('class'=>'btn btn-primary')); ?>
  <?php echo CHtml::link('ลืมรหัสผ่าน',Yii::app()->request->baseUrl.'/shop/forgetPassword'); ?>
  <?php //echo CHtml::link('สมัครสมาชิก',Yii::app()->request->baseUrl.'/shop/register').' | '.CHtml::link('ลืมรหัสผ่าน',Yii::app()->request->baseUrl.'/shop/forgetPassword'); ?>
</div>
<?php
  $this->endWidget();
  Dialog::alertMessage();  
?>
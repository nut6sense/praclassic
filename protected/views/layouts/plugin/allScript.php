<?php

	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/plugin/jscolor/jscolor.js');

	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/plugin/ckeditor/ckeditor.js');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/plugin/ckeditor/samples/sample.js', CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/plugin/loupe/jquery.loupe.js', CClientScript::POS_END);
	// Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/js/jquery-1.9.1.js', CClientScript::POS_END);
	// Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/js/jquery-ui-1.8.23.custom.min.js', CClientScript::POS_END);
	// Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/plugin/bootstrap/js/bootstrap.js', CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/js/jquery.form.js', CClientScript::POS_END);
	Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/js/scripts.js', CClientScript::POS_END);

?>
<style type="text/css">

	body{
		<?php echo Setting::getBgImage(); ?>
		<?php echo Setting::getFontColorContent(); ?>
	}

	.sideleft.colorBox1{
		<?php echo Setting::getColorBox1(); ?>
	}

	.sideleft.colorBox2{
		<?php echo Setting::getColorBox2(); ?>
	}

	.sideleft.colorBox3{
		<?php echo Setting::getColorBox3(); ?>
	}

	.headPop{
		text-align: center;
		height: 60px;
		background: url('../../img/customCss/lineHead.png')center;
		/*background-repeat: round;*/
		background-repeat:no-repeat;
		background-position:center;
	}

	.footPop{
		height: 70px;
		margin-top: 20px;
		background: url('../../img/customCss/lineFoot.png');
		/*background-repeat: round;*/
		background-repeat:no-repeat;
		background-position:center;
	}

	div.main h1 {
		<?php echo Setting::getFontColorHeader(); ?>
	}

	div.sideleft h2 {
		<?php echo Setting::getFontColorHeader(); ?>
	}

	.nav-header {
		<?php echo Setting::getFontColorHeader(); ?>
	}

	a:hover, a:focus {
		color: rgba(255, 255, 255, 0.5);
	}

	.block ul.links li a:hover, .block ul.links li.active a {
		background-color: rgba(255, 255, 255, 0.3);
		color: #EEE;
		text-decoration: none;
		border-radius: 4px;
	}

	.block ul.links li a {
		<?php echo Setting::getFontColorContent(); ?>
	}

	.nav>li>a:hover, .nav>li>a:focus {
		text-decoration: none;
		background-color: rgba(255, 255, 255, 0.3);
	}

	

	div.navbar .container { 
		<?php echo Setting::getThemeColorBox(); ?>
	}

	div.footer {
		<?php echo Setting::getThemeColorFooterBox(); ?>
	}
	

</style>
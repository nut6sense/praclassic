<script>
  CKEDITOR.replace( 'editor1', {

    extraPlugins: 'htmlwriter',

    contentsCss: 'body {color:#000; background-color#:FFF;}',

    docType: '<!DOCTYPE HTML>',

    allowedContent:
      'h1 h2 h3 p pre[align]; ' +
      'blockquote code kbd samp var del ins cite q b i u strike ul ol li hr table tbody tr td th caption; ' +
      'img[!src,alt,align,width,height]; font[!face]; font[!family]; font[!color]; font[!size]; font{!background-color}; a[!href]; a[!name]',

    coreStyles_bold: { element: 'b' },
    coreStyles_italic: { element: 'i' },
    coreStyles_underline: { element: 'u' },
    coreStyles_strike: { element: 'strike' },

    font_style: {
      element: 'font',
      attributes: { 'face': '#(family)' }
    },

    fontSize_sizes: 'xx-small/1;x-small/2;small/3;medium/4;large/5;x-large/6;xx-large/7',
    fontSize_style: {
      element: 'font',
      attributes: { 'size': '#(size)' }
    },

    colorButton_foreStyle: {
      element: 'font',
      attributes: { 'color': '#(color)' }
    },

    colorButton_backStyle: {
      element: 'font',
      styles: { 'background-color': '#(color)' }
    },

    stylesSet: [
      { name: 'Computer Code', element: 'code' },
      { name: 'Keyboard Phrase', element: 'kbd' },
      { name: 'Sample Text', element: 'samp' },
      { name: 'Variable', element: 'var' },
      { name: 'Deleted Text', element: 'del' },
      { name: 'Inserted Text', element: 'ins' },
      { name: 'Cited Work', element: 'cite' },
      { name: 'Inline Quotation', element: 'q' }
    ],

    on: {
      pluginsLoaded: configureTransformations,
      loaded: configureHtmlWriter
    }
  });

  function configureTransformations( evt ) {
    var editor = evt.editor;

    editor.dataProcessor.htmlFilter.addRules( {
      attributes: {
        style: function( value, element ) {
          return CKEDITOR.tools.convertRgbToHex( value );
        }
      }
    } );

    function alignToAttribute( element ) {
      if ( element.styles[ 'text-align' ] ) {
        element.attributes.align = element.styles[ 'text-align' ];
        delete element.styles[ 'text-align' ];
      }
    }
    editor.filter.addTransformations( [
      [ { element: 'p', right: alignToAttribute } ],
      [ { element: 'h1',  right: alignToAttribute } ],
      [ { element: 'h2',  right: alignToAttribute } ],
      [ { element: 'h3',  right: alignToAttribute } ],
      [ { element: 'pre', right: alignToAttribute } ]
    ] );
  }

  function configureHtmlWriter( evt ) {
    var editor = evt.editor,
      dataProcessor = editor.dataProcessor;

    dataProcessor.writer.selfClosingEnd = '>';

    var dtd = CKEDITOR.dtd;
    for ( var e in CKEDITOR.tools.extend( {}, dtd.$nonBodyContent, dtd.$block, dtd.$listItem, dtd.$tableContent ) ) {
      dataProcessor.writer.setRules( e, {
        indent: true,
        breakBeforeOpen: true,
        breakAfterOpen: false,
        breakBeforeClose: !dtd[ e ][ '#' ],
        breakAfterClose: true
      });
    }
  }
</script>
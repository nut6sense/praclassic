<?php
  $form = $this->beginWidget('CActiveForm', array(
    'id' => 'setting-web-form',
    'action' => Yii::app()->request->baseUrl . '/admin/submitSettingWebSite',
    'htmlOptions' => array('class' => 'form-horizontal', 'enctype' => 'multipart/form-data')
  ));
  $textAlign = 'style="text-align:center;"';

  $theme = Setting::model()->findByAttributes(array('setting_id'=>1));
  $setting = json_decode($theme->setting_parameter);

  if (isset($setting->colorBox1)) {
    $colorBox1 = $setting->colorBox1;
  }else{
    $colorBox1 = '#ddd';
  }

  if (isset($setting->colorBox2)) {
    $colorBox2 = $setting->colorBox2;
  }else{
    $colorBox2 = '#ddd';
  }

  if (isset($setting->colorBox3)) {
    $colorBox3 = $setting->colorBox3;
  }else{
    $colorBox3 = '#ddd';
  }

?>

<div class="container-fluid">
  <div class="row-fluid">
    <div class="span12"><h1>กำหนดรูปแบบเว็บไซต์</h1></div>
  </div>

  <div class="row-fluid">
    <div class="span12"><h4>กำหนดสีของเว็บไซต์</h4></div>
  </div>
  <div class="row-fluid" style="margin-bottom: 20px;">
    <div class="span3">
      เลือกสีของเว็บไซต์
    </div>
    <div class="span9">
      <?php //echo Setting::model()->getTheme(); ?>
      <input type="text" name="themeColor" id="themeColor" class="color {adjust:false}" value="<?php echo Setting::getThemeColorCode(); ?>">
    </div>
  </div>
  <div class="row-fluid" style="margin-bottom: 20px;">
    <div class="span3">
    </div>
    <div class="span9">
      <?php 
        echo CHtml::button('Update', array('class'=>'btn btn-primary', 'id'=>'btn-themeColorUpdate'));
        echo CHtml::button('Default', array('class'=>'btn btn-inverse','style'=>'margin-left: 10px;', 'id'=>'btn-themeColorDefault'));
      ?>
    </div>
  </div>

  <div class="row-fluid">
    <div class="span12"><h4>กำหนดพื้นหลังของเว็บไซต์</h4></div>
  </div>
  <div class="row-fluid" style="margin-bottom: 20px;">
    <div class="span3">
      เลือกรูปภาพ
    </div>
    <div class="span9">
      <?php 
        echo CHtml::fileField('background_image', null)."<br>"; 
        echo CHtml::button('Update', array('class'=>'btn btn-primary', 'id'=>'btn-bgUpdate'));
        echo CHtml::button('Default', array('class'=>'btn btn-inverse','style'=>'margin-left: 10px;', 'id'=>'btn-bgDefault'));
      ?>
    </div>
  </div>

  <div class="row-fluid">
    <div class="span12"><h4>กำหนดลำดับเมนูด้านบน (Top Menu)</h4></div>
  </div>
  <div class="row-fluid">
    <div class="span12">
      <table class="table table-bordered table-default">
        <thead>
          <tr>
            <th class="span3">ลำดับ</th>
            <th>รายการเมนู</th>
          </tr>
        </thead>
        <tbody>
          <?php echo Topmenu::model()->getTopMenu(); ?>
        </tbody>
      </table>
    </div>
  </div>

  <div class="row-fluid">
    <div class="span12"><h4>รูปแบบข่าวประชาสัมพันธ์</h4></div>
  </div>
  <div class="row-fluid" style="margin-bottom: 20px;">
    <div class="span3">
      เลือกรูปแบบแสดงผล
    </div>
    <div class="span9">
      <div class="row-fluid" style="margin-bottom: 5px;">
        <span class="span1">
          <?php echo CHtml::radioButton('layoutNews', false, array('value'=>1)); ?>
        </span>
        <span class="span3 layout-table">รูปภาพ</span>
        <span class="span7 layout-table">รายละเอียด</span>
      </div>
    </div>
  </div>

  <div class="row-fluid" style="margin-bottom: 20px;">
    <div class="span3">
    </div>
    <div class="span9">
      <div class="row-fluid" style="margin-bottom: 5px;">
        <span class="span1">
          <?php echo CHtml::radioButton('layoutNews', false, array('value'=>2)); ?>
        </span>
        <span class="span2 layout-table">รูปภาพ</span>
        <span class="span3 layout-table">รายละเอียด</span>
        <span class="span2 layout-table">รูปภาพ</span>
        <span class="span3 layout-table">รายละเอียด</span>
      </div>
    </div>
  </div>

  <div class="row-fluid">
    <div class="span12"><h4>กำหนดสี 3 ส่วน</h4></div>
  </div>
  <div class="row-fluid" style="margin-bottom: 20px;">
    <div class="span3">
      เลือกส่วนที่ต้องการกำหนดสี
    </div>
    <div class="span9">
      <div class="row-fluid" style="margin-bottom: 5px;">
        <div class="span2">สถิติเว็บไซต์</div>
        <div class="span10">
          <input type="text" name="colorBox1" id="colorBox1" class="color {adjust:false}" value="<?php echo Setting::getColorCode1(); ?>">
        </div>
      </div>
      <div class="row-fluid" style="margin-bottom: 5px;">
        <div class="span2">เมนูด้านซ้าย</div>
        <div class="span10">
          <input type="text" name="colorBox2" id="colorBox2" class="color {adjust:false}" value="<?php echo Setting::getColorCode2(); ?>">
        </div>
      </div>
      <div class="row-fluid" style="margin-bottom: 5px;">
        <div class="span2">เนื้อหาเว็บไซต์</div>
        <div class="span10">
          <input type="text" name="colorBox3" id="colorBox3" class="color {adjust:false}" value="<?php echo Setting::getColorCode3(); ?>">
        </div>
      </div>
    </div>
  </div>

  <div class="row-fluid" style="margin-bottom: 20px;">
    <div class="span3">
    </div>
    <div class="span9">
      <div class="row-fluid" style="margin-bottom: 5px;">
        <div class="span2"></div>
        <div class="span10">
          <?php 
            echo CHtml::button('Update', array('class'=>'btn btn-primary', 'id'=>'btn-colorUpdate'));
            echo CHtml::button('Default', array('class'=>'btn btn-inverse','style'=>'margin-left: 10px;', 'id'=>'btn-colorDefault'));
          ?>
        </div>
      </div>
    </div>
  </div>

  <div class="row-fluid">
    <div class="span12"><h4>กำหนดสีตัวอักษร</h4></div>
  </div>
  <div class="row-fluid" style="margin-bottom: 20px;">
    <div class="span3">
      เลือกส่วนที่ต้องการกำหนดสี
    </div>
    <div class="span9">
      <div class="row-fluid" style="margin-bottom: 5px;">
        <div class="span2">สีอักษรส่วนหัว</div>
        <div class="span10">
          <input type="text" name="fontColorHeader" id="fontColorHeader" class="color {adjust:false}" value="<?php echo Setting::getFontColorHeaderCode(); ?>">
        </div>
      </div>
      <div class="row-fluid" style="margin-bottom: 5px;">
        <div class="span2">สีอักษรข้อความ</div>
        <div class="span10">
          <input type="text" name="fontColorContent" id="fontColorContent" class="color {adjust:false}" value="<?php echo Setting::getFontColorContentCode(); ?>">
        </div>
      </div>
    </div>
  </div>

  <div class="row-fluid" style="margin-bottom: 20px;">
    <div class="span3">
    </div>
    <div class="span9">
      <div class="row-fluid" style="margin-bottom: 5px;">
        <div class="span2"></div>
        <div class="span10">
          <?php 
            echo CHtml::button('Update', array('class'=>'btn btn-primary', 'id'=>'btn-fontColorUpdate'));
            echo CHtml::button('Default', array('class'=>'btn btn-inverse','style'=>'margin-left: 10px;', 'id'=>'btn-fontColorDefault'));
          ?>
        </div>
      </div>
    </div>
  </div>

</div>
	
<?php
  $checkLayoutNews = Setting::model()->getValueLayoutNews();
  Yii::app()->clientScript->registerScript('layoutNews', "
    $('#setting-web-form').find('[name=\"layoutNews\"][value=\"".$checkLayoutNews."\"]').attr('checked',true)
    ");
	$this->endWidget();
?>
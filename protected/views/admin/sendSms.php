<div id="category_page">
  <div class="tab-content">
    <div class="tab-pane active">

    <?php
      $form = $this->beginWidget('CActiveForm', array(
          'id' => 'shop-massage-form',
          'action' => Yii::app()->request->baseUrl . '/admin/sending',
          'htmlOptions' => array('class' => 'form-horizontal')
              ));
    ?>

    <div class="container-fluid">
      <div class="row-fluid headerForm sizeContent">
        <div class="span12">
          <h1>การส่ง SMS</h1>
        </div>
      </div>
      <div style="margin-top:20px;">

        <div class="row-fluid">
          <div class="span3">
            <label>หมายเลขที่ต้องการส่ง</label>
          </div>
          <div class="span9">
            <?php echo CHtml::textField('phone', null,array('class'=>'span9','placeholder'=>'08XXXXXXXX')); ?>
          </div>
        </div>
        <div class="row-fluid" style="margin-top:20px;">
          <div class="span3">
            <label>ข้อความ</label>
          </div>
          <div class="span9">
            <?php echo CHtml::textArea('detail', null,array('class'=>'span9','placeholder'=>'กรอกข้อความที่ต้องการส่ง')); ?>
          </div>
        </div>
        <div class="row-fluid" style="margin-top:20px;">
          <div class="span12">
            <?php echo CHtml::submitButton('ส่งข้อความ', array('class' => 'btn btn-info','style'=>'width: 90px')); ?>
            <?php echo CHtml::resetButton('เริ่มใหม่', array('class' => 'btn','style'=>'width: 90px')); ?>
          </div>
        </div>

      </div>
    </div>

    <?php
      $this->endWidget();
    ?>

    <?php
      Dialog::alertMessage();
    ?>

    </div>
  </div>
</div>
<?php
	$inputOption = array('class'=>'span3');
	$form = $this->beginWidget('CActiveForm', array(
    'id' => 'login-form',
    'action' => Yii::app()->request->baseUrl . '/admin/login',
    // 'htmlOptions' => array('class' => 'form-horizontal' , 'target' => 'upload_target', 'enctype' => 'multipart/form-data'),
        ));

	$model = new LoginForm;
?>
<div class="navbar loginLaout ">
	<div class="container headerLoginAdmin">
		<div>ลงชื่อเข้าสู่ระบบ</div>
	</div>
	<div class="span5 bodyLoginAdmin" style="color: #000;">
		<div class="row">
			<div class="span90">
				<label>ชื่อผู้ใช้ : </label>
			</div>
			<div class="span3">
				<?php echo $form->textField($model,'username', $inputOption); ?>
			</div>
		</div>

		<div class="row">
			<div class="span90">
				<label>รหัสผ่าน : </label>
			</div>
			<div class="span3">
				<?php echo $form->passwordField($model,'password', $inputOption); ?>
			</div>
		</div>

		<div class="row">
			<div class="span90">
			</div>
			<div class="span3">
        <?php echo CHtml::submitbutton('เข้าสู่ระบบ', array('class'=>'btn btn-primary')); ?>
        <?php echo CHtml::button('ออกจากระบบ', array('class'=>'btn btn-info','onclick'=>"window.location='".Yii::app()->request->baseUrl."/site/logout'")); ?>
			</div>
		</div>

	</div>
</div>
<?php
	$this->endWidget();
	Dialog::alertMessage();
	
?>
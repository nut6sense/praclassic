<div id="category_page">
  <div class="tab-content">
    <div class="tab-pane active">

    <?php
      $form = $this->beginWidget('CActiveForm', array(
          'id' => 'email-massage-form',
          'action' => Yii::app()->request->baseUrl . '/admin/sendingMail',
          'htmlOptions' => array('class' => 'form-horizontal')
              ));
    ?>

    <div class="container-fluid">
      <div class="row-fluid headerForm sizeContent">
        <div class="span12">
          <h1>ส่งจดหมายข่าว</h1>
        </div>
      </div>
      <div style="margin-top:20px;">

        <div class="row-fluid">
          <div class="span3">
            <label>หัวเรื่อง</label>
          </div>
          <div class="span9">
            <?php echo CHtml::textField('subject', null,array('class'=>'span9','placeholder'=>'หัวเรื่อง')); ?>
          </div>
        </div>
        <div class="row-fluid" style="margin-top:20px;">
          <div class="span3">
            <label>อีเมล์ที่ต้องการส่ง</label>
          </div>
          <div class="span9">
            <?php echo CHtml::textField('email', null,array('class'=>'span9','placeholder'=>'Email@xxxx.com')); ?>
          </div>
        </div>
        <div class="row-fluid" style="margin-top:20px;">
          <div class="span3">
            <label>ข้อความ</label>
          </div>
          <div class="span9">
            <?php echo CHtml::textArea('message', null,array('class'=>'span9','placeholder'=>'กรอกข้อความที่ต้องการส่ง')); ?>
          </div>
        </div>
        <div class="row-fluid" style="margin-top:20px;">
          <div class="span12">
            <?php echo CHtml::submitButton('ส่งจดหมาย', array('class' => 'btn btn-info','style'=>'width: 90px')); ?>
            <?php echo CHtml::resetButton('เริ่มใหม่', array('class' => 'btn','style'=>'width: 90px')); ?>
          </div>
        </div>

      </div>
    </div>

    <?php
      $this->endWidget();
    ?>

    <?php
      Dialog::alertMessage();
    ?>

    </div>
  </div>
</div>
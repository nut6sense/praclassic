<?php
  $form = $this->beginWidget('CActiveForm', array(
      'id' => 'profile-shop-form',
      'action' => Yii::app()->request->baseUrl . '/admin/saveProfile',
      'htmlOptions' => array('enctype' => 'multipart/form-data')
          ));

  $LabelInputOption = array('class'=>'span4');
  $inputOption = array('class'=>'span8');
?>

<div class="container-fluid">
  <div class="row-fluid">
    <div class="span12"><h1>แก้ไขข้อมูลร้านค้า</h1></div>
  </div>
  <div class="row-fluid">
    <div class="span12">
      <?php echo $form->labelEx($model_user, 'title_username', $LabelInputOption); ?>
      <?php echo $form->textField($model_user, 'username', $inputOption + array('readonly'=>true)); ?>
    </div>
  </div>
  <div class="row-fluid">
    <div class="span12">
      <?php echo $form->labelEx($model_user, 'title_password', $LabelInputOption); ?>
      <?php echo $form->PasswordField($model_user, 'password', $inputOption); ?>
    </div>
  </div>
  <div class="row-fluid">
    <div class="span12">
      <?php echo $form->labelEx($model_user, 'title_confirmPassword', $LabelInputOption); ?>
      <?php echo $form->PasswordField($model_user, 'confirmPassword', $inputOption); ?>
    </div>
  </div>
  <hr/>
  <div class="row-fluid">
    <div class="span12">
      <?php echo $form->labelEx($model_user, 'title_name', $LabelInputOption); ?>
      <?php echo $form->textField($model_user, 'name', $inputOption); ?>
    </div>
  </div>
  <div class="row-fluid">
    <div class="span12">
      <?php echo $form->labelEx($model_user, 'title_surname', $LabelInputOption); ?>
      <?php echo $form->textField($model_user, 'surname', $inputOption); ?>
    </div>
  </div>
  <div class="row-fluid">
    <div class="span12">
      <?php echo $form->labelEx($model_user, 'title_iden_id', $LabelInputOption); ?>
      <?php echo $form->textField($model_user, 'iden_id', array('onkeyup'=>'autoFormat(this, "iden")', 'onKeyPress'=>'chkNumber()')+$inputOption); ?>
    </div>
  </div>
  <div class="row-fluid">
    <div class="span12">
      <?php echo $form->labelEx($model_user, 'title_phonenumber', $LabelInputOption); ?>
      <?php echo $form->textField($model_user, 'phonenumber', array('onkeyup'=>'autoFormat(this, "telephone")', 'onKeyPress'=>'chkNumber()')+$inputOption); ?>
    </div>
  </div>
  <div class="row-fluid">
    <div class="span12">
      <?php echo $form->labelEx($model_user, 'title_email', $LabelInputOption); ?>
      <?php echo $form->textField($model_user, 'email', $inputOption); ?>
    </div>
  </div>
  <div class="row-fluid">
    <div class="span12 row-fluid field-submit">
      <?php echo $form->hiddenField($model_user, 'user_id'); ?>
      <?php echo CHtml::submitButton('แก้ไขข้อมูล', array('id' => 'btn-add', 'class' => 'btn btn-info')); ?>
    </div>
  </div>
</div>
<?php
  $this->endWidget();
  Dialog::alertMessage();
?>
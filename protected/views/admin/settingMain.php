<?php
  $form = $this->beginWidget('CActiveForm', array(
    'id' => 'setting-web-form',
    'action' => Yii::app()->request->baseUrl . '/admin/submitSettingMain',
    'htmlOptions' => array('class' => 'form-horizontal', 'enctype' => 'multipart/form-data')
  ));
  $textAlign = 'style="text-align:center;"';

  $settingMain = Setting::model()->findByAttributes(array('setting_id'=>1));
  $setting = json_decode($settingMain->setting_parameter);

  $smsUser = null;
  $smsPass = null;
  $smsType = null;
  $emailMain = null;

  if (isset($setting->smsUser)) {
    $smsUser = $setting->smsUser;
  }

  if (isset($setting->smsPass)) {
    $smsPass = $setting->smsPass;
  }

  if (isset($setting->smsType)) {
    $smsType = $setting->smsType;
  }

  if (isset($setting->emailMain)) {
    $emailMain = $setting->emailMain;
  }

?>

<div class="container-fluid">
  <div class="row-fluid">
    <div class="span12"><h1>กำหนดค่าหลัก</h1></div>
  </div>

  <div class="row-fluid">
    <div class="span12"><h4>ตั้งค่าการใช้งาน SMS</h4></div>
  </div>
  <div class="row-fluid" style="margin-bottom: 20px;">
    <div class="span3">
      Username : 
    </div>
    <div class="span9">
      <input type="text" name="sms_username" id="sms_username" value="<?php echo $smsUser; ?>">
    </div>
  </div>
  <div class="row-fluid" style="margin-bottom: 20px;">
    <div class="span3">
      Password : 
    </div>
    <div class="span9">
      <input type="password" name="sms_password" id="sms_password" value="<?php echo $smsPass; ?>">
    </div>
  </div>
  <div class="row-fluid" style="margin-bottom: 20px;">
    <div class="span3">
      SMS Type : 
    </div>
    <div class="span9">
      <select name="sms_type" id="sms_type">
        <option value="standard" <?php echo ($smsType=='standard')?'selected':null; ?>>Standard</option>
        <option value="premium" <?php echo ($smsType=='premium')?'selected':null; ?>>Premium</option>
      </select>
    </div>
  </div>

  <div class="row-fluid" style="margin-bottom: 20px;">
    <div class="span3">
    </div>
    <div class="span9">
      <?php 
        echo CHtml::button('Update', array('class'=>'btn btn-primary', 'id'=>'btn-smsUpdate'));
        echo CHtml::button('Default', array('class'=>'btn btn-inverse','style'=>'margin-left: 10px;', 'id'=>'btn-smsUpdateDefault'));
      ?>
    </div>
  </div>

  <div class="row-fluid">
    <div class="span12"><h4>ตั้งค่า E-Mail</h4></div>
  </div>
  <div class="row-fluid" style="margin-bottom: 20px;">
    <div class="span3">
      E-Mail :
    </div>
    <div class="span9">
      <input type="text" name="setting_email" id="setting_email" value="<?php echo $emailMain; ?>">
    </div>
  </div>
  <div class="row-fluid" style="margin-bottom: 20px;">
    <div class="span3">
    </div>
    <div class="span9">
      <?php 
        echo CHtml::button('Update', array('class'=>'btn btn-primary', 'id'=>'btn-emailUpdate'));
        echo CHtml::button('Default', array('class'=>'btn btn-inverse','style'=>'margin-left: 10px;', 'id'=>'btn-emailDefault'));
      ?>
    </div>
  </div>

</div>
	
<?php
	$this->endWidget();
?>
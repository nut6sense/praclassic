<div id="category_page">
  <div class="tab-content">
    <div class="tab-pane active">
    <?php
      $form = $this->beginWidget('CActiveForm', array(
          'id' => 'premium-massage-form',
          'action' => Yii::app()->request->baseUrl . '/Massage/addMassage',
          'htmlOptions' => array('class' => 'form-horizontal')
              ));
    ?>
    <div class="container-fluid">
      <div class="row-fluid headerForm sizeContent">
        <div class="span12"><h1>ข้อความ Premium ถึงสมาชิกร้านค้าทุกร้าน</h1></div>
      </div>
      <div class="row-fluid">
        <div class="span12 sizeContent ckBox">
          <?php echo CHtml::textArea('premium-massage', Massage::getMassage('PremiumMassage'), array('cols'=>'80', 'id'=>'editor1', 'name'=>'editor1', 'rows'=>'10')); ?>
        </div>
      </div>
      <div class="row-fluid field-submit">
        <div class="span12">
          <?php echo CHtml::hiddenField('massageType', 'PremiumMassage'); ?>
          <?php echo CHtml::submitButton('เพิ่มข้อความ', array('id' => 'btn-add', 'class' => 'btn btn-info')); ?>
        </div>
      </div>
    </div>
    <?php
      $this->renderPartial('//layouts/plugin/scriptWebTextEditor');
      $this->endWidget();
      Dialog::alertMessage();
    ?>
    </div>
  </div>
</div>
<?php
  $form = $this->beginWidget('CActiveForm', array(
    'id' => 'shop-detail-form',
    // 'action' => Yii::app()->request->baseUrl . '/shop/delShop',
    'htmlOptions' => array('class' => 'form-horizontal')
  ));

  $user_id = Yii::app()->input->get('param1');
  $textAlign = 'style="text-align:center;"';
  $marginTopData = 'style="margin-top:5px;"';
?>

<div class="container-fluid bodyForm">
  <div class="row-fluid">
    <div class="span12"><h1>ข้อมูลร้านค้า</h1></div>
  </div>
  <div class="row-fluid">
    <div class="span4 datashop-picture">
      <div class="row-fluid">
        <div class="span" <?php echo $textAlign; ?> >
          <div class="picShop">
            <?php 
              $picPath = !empty($dataShop['shop_pic'])?Yii::app()->request->baseUrl.'/img/shopPic/'.$dataShop['shop_pic']:Yii::app()->request->baseUrl.'/img/shopPic/none.jpg';
              echo CHtml::image($picPath, null, array('id'=>'Shops_shop_pic'));
            ?>
          </div>
        </div>
      </div>
      <br/>
      <div class="row-fluid">
        <div class="span" <?php echo $textAlign; ?> >
          <?php echo CHtml::link('ส่งข้อความถึงร้านค้า', Yii::app()->request->baseUrl.'/site/privateMassage/'.$user_id, array('id' => 'btn-add', 'class' => 'btn btn-info btnLink')); ?>
        </div>
      </div>
    </div>
    <div class="span8 datashop-detail">
    	<div class="row-fluid">
        <div class="span3">
          <?php echo $form->labelEx($shopsModel, 'title_shopid'); ?>
        </div>
        <div class="span9" <?php echo $marginTopData; ?> >
          <?php echo $dataShop['sequence']; ?>
          <?php //echo $dataShop['shopid']; ?>
        </div>
      </div>

      <div class="row-fluid">
        <div class="span3">
          <?php echo $form->labelEx($shopsModel, 'title_name'); ?>
        </div>
        <div class="span9" <?php echo $marginTopData; ?>>
          <?php echo $dataShop['shopname']; ?>
        </div>
      </div>

      <div class="row-fluid">
        <div class="span3">
          <label>โดย</label>
        </div>
        <div class="span9" <?php echo $marginTopData; ?>>
          <?php echo $dataShop['firstname']; ?>
        </div>
      </div>

      <div class="row-fluid">
        <div class="span3">
          <?php echo $form->labelEx($shopsModel, 'title_map'); ?>
        </div>
        <div class="span9" <?php echo $marginTopData; ?>>
          <?php echo $dataShop['map']; ?>
        </div>
      </div>

      <div class="row-fluid">
        <div class="span3">
          <?php echo $form->labelEx($usersModel, 'title_phonenumber'); ?>
        </div>
        <div class="span9" <?php echo $marginTopData; ?>>
          <?php echo $dataShop['phonenumber']; ?>
        </div>
      </div>

      <div class="row-fluid">
        <div class="span3">
          <label style="font-weight:bold;">การชำระเงิน</label>
        </div>
        <div class="span9" <?php echo $marginTopData; ?>>
          <?php echo $banksModel->getAttributeLabel('title_account_number').' : '.$dataShop['account_number']; ?>
        </div>
      </div>

      <div class="row-fluid">
        <div class="span3"></div>
        <div class="span9" <?php echo $marginTopData; ?>>
          <?php 
            $bankList = Banks::getBank();
            echo $banksModel->getAttributeLabel('title_bank_name').' : '.$bankList[$dataShop['bank_name']];
          ?>
        </div>
      </div>

      <div class="row-fluid">
        <div class="span3"></div>
        <div class="span9" <?php echo $marginTopData; ?>>
          <?php 
            echo $banksModel->getAttributeLabel('bank_Branch').' : '.$dataShop['bank_Branch'];
          ?>
        </div>
      </div>

     <div class="row-fluid">
        <div class="span3"></div>
        <div class="span9" <?php echo $marginTopData; ?>>
          <?php 
            $bankTypeList = Banks::getBankType();
            echo $banksModel->getAttributeLabel('title_bank_type').' : '.$bankTypeList[$dataShop['bank_type']];
          ?>
        </div>
      </div>

      <div class="row-fluid">
        <div class="span3">
          <?php echo $form->labelEx($shopsModel, 'view'); ?>
        </div>
        <div class="span9" <?php echo $marginTopData; ?>>
          <?php echo $dataShop['view']; ?>
        </div>
      </div>

      <div class="row-fluid">
        <div class="span3">
          <label>จำนวนวันคงเหลือ</label>
        </div>
        <div class="span9" <?php echo $marginTopData; ?>>
          <?php echo $dataShop['dayBalance'].' วัน'; ?>
        </div>
      </div>

    </div>
  </div>
  <hr/>
  <div class="row-fluid">
    <div class="span12 headPop"><h1>พระเด่น</h1></div>
  </div>
  <?php
  	echo Products::popularProduct($user_id);
  ?>
  <div class="row-fluid">
    <div class="span12 footPop"></div>
  </div>
  <div class="row-fluid">
    <div class="span12"><h1>พระทั้งหมด</h1></div>
  </div>
  <?php
  	echo Products::allProduct($user_id);
  ?>
</div>
	
<?php
	$this->endWidget();
  Dialog::alertMessage();
?>
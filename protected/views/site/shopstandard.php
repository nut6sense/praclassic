<?php
  $form = $this->beginWidget('CActiveForm', array(
      'id' => 'all-shop-form',
      'action' => '#',
      // 'action' => Yii::app()->request->baseUrl . '/shop/delShop'
          ));
  $model_shops = new Shops;
  $model_products = new Products;
?>

<!-- <div class="container-fluid">
  <div class="row-fluid headerForm sizeContent">
    <div class="span12">
      <h1>ร้านค้ามาตราฐาน</h1>
    </div>
  </div>
  <div class="row-fluid bodyForm">
  	<?php
      echo $form->hiddenField($model_products, 'page', array('value'=>isset($_POST['Products']['page'])?$_POST['Products']['page']:null,));
  		echo Shops::standardShop($_POST); 
  	?>
  </div>
</div> -->

<div id="shoplist" class="container-fluid">
  <div class="row-fluid headerForm sizeContent">
    <div class="span12">
      <h1>ร้านค้ามาตราฐาน</h1>
    </div>
  </div>

  <div class="span12 bodyForm" style="margin-left:0px;">

    <div class="row-fluid">
      <div class="span6">
        <?php echo $form->textField($model_shops, 'name', array('placeholder'=>'กรอกคำค้นหา','class'=>'span','value'=>isset($_POST['Shops']['name'])?$_POST['Shops']['name']:null,)); ?>
      </div>
      <div class="span3">
        <?php echo CHtml::submitButton('ค้นหา', array('id' => 'btn-search', 'class' => 'btn btn-primary')); ?>
      </div>
    </div>

    <div class="row-fluid">
      <div class="span12">
        <?php
          $firstChar = array("ก","ข","ฃ","ค","ฅ","ฆ","ง","จ","ฉ","ช","ซ","ฌ","ญ","ฎ","ฎ","ฐ","ฑ","ฒ","ณ","ด","ต","ถ","ท","ธ","น","บ","ป","ผ","ฝ","พ","ฟ","ภ","ม","ย","ร","ล","ว","ศ","ษ","ส","ห","ฬ","อ","ฮ");
          $engChar = range('A', 'Z');

          echo "<ul class=\"firstChar\">";
          foreach($firstChar as $value => $char) {
            $value++;
            echo "<li name=\"letter-search\" class=\"char-$value\"><a class=\"btn\">$char</a></li>";
          }
          echo "</ul>";

          echo "<ul class=\"firstChar\">";
          foreach($engChar as $value => $char) {
            $value++;
            echo "<li name=\"letter-search\" class=\"char-$value\"><a class=\"btn\">$char</a></li>";
          }
          echo "</ul>";
        ?>
      </div>
    </div>
    <br/>
    <div class="row-fluid results items">
      <div class="span12">
        <?php
          echo $form->hiddenField($model_products, 'page', array('value'=>isset($_POST['Products']['page'])?$_POST['Products']['page']:null,));
          echo Shops::standardShop($_POST); 
        ?>
      </div>
    </div>

  </div>
</div>
<?php
  $this->endWidget();
?>
<?php
/* @var $this SiteController */
/* @var $model ContactForm */
/* @var $form CActiveForm */

$this->pageTitle=Yii::app()->name . ' - Contact Us';
$this->breadcrumbs=array(
	'Contact',
);

$submit = array('class' => 'btn');
$span12 = array('class' => 'span12');
?>

<h1>ติดต่อเรา</h1>

<?php if(Yii::app()->user->hasFlash('contact')): ?>

<div class="flash-success">
	<?php echo Yii::app()->user->getFlash('contact'); ?>
</div>

<?php else: ?>

<p>
หากคุณต้องการสอบถามปัญหาใดๆ ในการใช้งานเว็บไซต์ ต้องการที่จะติชมเว็บไซต์ ท่านสามารถข้อมูลลงในแบบฟอร์มด้านล่างนี้
</p>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'contact-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

	<p class="note">กรุณากรอกข้อมูลทุกช่องที่มีสัญลักษณ์ <span class="required">*</span></p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row-fluid">
		<?php echo $form->labelEx($model, 'name', $span12); ?>
		<?php echo $form->textField($model, 'name', $span12); ?>
	</div>

	<div class="row-fluid">
		<?php echo $form->labelEx($model,'email', $span12); ?>
		<?php echo $form->textField($model,'email', $span12); ?>
	</div>

	<div class="row-fluid">
		<?php echo $form->labelEx($model,'subject', $span12); ?>
		<?php echo $form->textField($model,'subject', $span12, array('size'=>60,'maxlength'=>128)); ?>
	</div>

	<div class="row-fluid">
		<?php echo $form->labelEx($model,'body', $span12); ?>
		<?php echo $form->textArea($model,'body', $span12, array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row-fluid">
		<?php echo $form->labelEx($model,'telephone', $span12); ?>
		<?php echo $form->textField($model,'telephone', $span12); ?>
	</div>

	<?php if(CCaptcha::checkRequirements()): ?>
	<div class="row-fluid">
		<?php echo $form->labelEx($model,'verifyCode'); ?>
		<div>
		<?php $this->widget('CCaptcha'); ?>
		<?php echo $form->textField($model,'verifyCode', $span12); ?>
		</div>
		<div class="hint">กรุณากรอกตัวอักษรที่เห็นด้านบน (ตัวพิมพ์เล็กใหญ่มีค่าเท่ากันคือเหมือนกัน)</div>
		<?php echo $form->error($model,'verifyCode'); ?>
	</div>
	<?php endif; ?>

	<div class="row-fluid buttons">
		<?php echo CHtml::submitButton('ส่งข้อมูล', $submit); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<?php endif; ?>
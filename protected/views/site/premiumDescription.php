<?php
  $form = $this->beginWidget('CActiveForm', array(
    'id' => 'premium-description-form',
    // 'action' => Yii::app()->request->baseUrl . '/shop/delShop',
    'htmlOptions' => array('class' => 'form-horizontal')
  ));

  $premiumModel = new Premium;

  $thai_month_arr=array(0=>'', 1=>'มกราคม', 2=>'กุมภาพันธ์', 3=>'มีนาคม', 4=>'เมษายน', 5=>'พฤษภาคม', 6=>'มิถุนายน', 7=>'กรกฎาคม', 8=>'สิงหาคม', 9=>'กันยายน', 10=>'ตุลาคม', 11=>'พฤศจิกายน', 12=>'ธันวาคม');
  $textAlign = array('style'=>'text-align:right;');
  $picAlign = array('class'=>'span', 'style'=>'text-align:center');
?>

<div class="container-fluid">
  <div class="row-fluid">
    <div class="span12" style="text-align:center;">
      <h3>
        <?php echo 'ชื่อพระ : '.$dataPremium['premiumname']; ?>
      </h3>
    </div>
  </div>

  <div class="row-fluid">
    <div class="span12" style="text-align:center;">
      <?php
        $itemsDetail = array();
        $decodePicture = json_decode($dataPremium['premiumpic'], true);
        foreach ($decodePicture as $key => $value) {
          if(!is_null($value['picName'])) {
            array_push($itemsDetail, $value);
          }
        }
        foreach ($itemsDetail as $key => $value) {
          if($key==0 || $key==2) {
            echo CHtml::openTag('div', array('class'=>'row-fluid', 'style'=>'margin-bottom:20px;'));
          }

          if(sizeof($itemsDetail)==1){
            $picAlign = array('class'=>'span12', 'style'=>'text-align:center');
            $picSize = array('style'=>'margin:0 auto;');
          }elseif(sizeof($itemsDetail)!=1) {
            if($key==0 || $key==2) {
              $picAlign = array('class'=>'span6', 'style'=>'text-align:right');
              $picSize = array('style'=>'float: right;');
            }else{
              $picAlign = array('class'=>'span6', 'style'=>'text-align:left');
              $picSize = array('style'=>'float: left;');
            }
          }

          echo CHtml::openTag('div', $picAlign);
          echo CHtml::openTag('div', array('class'=>'picProductDetail')+$picSize);
          $picPath = Yii::app()->request->baseUrl.'/img/premiumPic/'.$value['picName'];
          echo CHtml::image($picPath, null, array('id'=>'product_pic', 'class'=>'','style'=>'height:250px;'));
          echo CHtml::closeTag('div');
          echo CHtml::closeTag('div');

          if($key==1 || $key==sizeof($itemsDetail)-1) {
            echo CHtml::closeTag('div');
          }
        }
      ?>
    </div>
  </div>

  <div class="row-fluid">
    <div class="span2">
      <?php echo $form->labelEx($premiumModel, 'title_premiumname', $textAlign); ?>
    </div>
    <div class="span10">
      <label><?php echo $dataPremium['premiumname']; ?></label>
    </div>
  </div>

  <div class="row-fluid">
    <div class="span2">
      <?php echo $form->labelEx($premiumModel, 'title_premiumdetail', $textAlign); ?>
    </div>
    <div class="span10">
      <label><?php echo $dataPremium['premiumdetail']; ?></label>
    </div>
  </div>

  <div class="row-fluid">
    <div class="span2">
      <?php echo $form->labelEx($premiumModel, 'title_premiumdate', $textAlign); ?>
    </div>
    <div class="span10">
      <label>
        <?php 
          $explodeDate = explode('-', $dataPremium['premiumdate']);
          echo $explodeDate[2].' '.$thai_month_arr[intval($explodeDate[1])].' '.$explodeDate[0]; 
        ?>
      </label>
    </div>
  </div>

  <div class="row-fluid field-submit">
    <div class="span12">
      <?php echo CHtml::link('ย้อนกลับ', '../premium', array('id' => 'btn-add', 'class' => 'btn btn-success btnLink')); ?>&nbsp;
    </div>
  </div>
</div>
  
<?php
  $this->endWidget();
?>
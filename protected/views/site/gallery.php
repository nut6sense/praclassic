<?php
  $form = $this->beginWidget('CActiveForm', array(
      'id' => 'gallery-form',
      'action' => '#'
          ));

  if(isset($_POST['Gallery']['category_id'])) {
    $categorySelect = $_POST['Gallery']['category_id'];
  }else{
    $categorySelect = null;
  }

  $model_gallery = new Gallery;

  $inputOption = array('class'=>'span');
?>

<div class="container-fluid">
  <div class="row-fluid headerForm sizeContent">
    <div class="span12">
      <h1>แนะนำรายละเอียดเกี่ยวกับหน้าเว็บ</h1>
    </div>
  </div>

  <div class="row-fluid bodyForm">
    <div class="row-fluid">
      <div>
        <?php echo $galleryText; ?>
      </div>
    </div>

    <div class="row-fluid">
      <div>
        <h1>Gallery</h1>
      </div>
    </div>

    <div class="row-fluid">
      <div class="span1">
        <?php echo $form->labelEx($model_gallery, 'title_category_id'); ?>
      </div>
      <div class="span5">
        <?php echo $form->DropDownList($model_gallery, 'category_id', Category::getCategoryData(), $inputOption); ?>
      </div>
      <div class="span3">
        <?php echo CHtml::submitButton('ค้นหา', array('id' => 'btn-add', 'class' => 'btn btn-info')); ?>&nbsp;
      </div>
    </div>

    <div class="row-fluid">
      <div>
        <?php
          echo Gallery::showGallery($categorySelect); 
        ?>
      </div>
    </div>
  </div>
</div>

<?php
  $this->endWidget();
?>
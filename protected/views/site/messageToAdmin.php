<?php
  $form = $this->beginWidget('CActiveForm', array(
      'id' => 'contact-us-form',
      'action' => Yii::app()->request->baseUrl . '/site/AddContact',
      'htmlOptions' => array('class' => 'form-horizontal','enctype' => 'multipart/form-data')
          ));

  $model_contact = new Contact;

  $span_label = array('class' => 'span3');
  $span_field = array('class' => 'span9');
  $span_div = array('class' => 'span12');
  $submit = array('class' => 'btn');
?>
<h1>ที่ปรึกษาด้านพระเครื่อง</h1>
<div class="container-fluid">
  <div class="row-fluid">
    <div class="span12">หากคุณต้องการสอบถามปัญหาใดๆ ในการใช้งานเว็บไซต์ ต้องการที่จะติชมเว็บไซต์ ท่านสามารถข้อมูลลงในแบบฟอร์มด้านล่างนี้</div>
  </div>
  <br/>

  <?php echo $form->HiddenField($model_contact, 'type', array('value'=>1)); ?>

  <div class="row-fluid field field-subject">
    <?php echo $form->labelEx($model_contact, 'subject', $span_label); ?>
    <?php echo $form->textField($model_contact,'subject', $span_field); ?>
  </div>
  <div class="row-fluid field field-message">
    <?php echo $form->labelEx($model_contact, 'message', $span_label); ?>
    <?php echo $form->TextArea($model_contact,'message', $span_field); ?>
  </div>
  <div class="row-fluid field field-name">
    <?php echo $form->labelEx($model_contact, 'name', $span_label); ?>
    <?php echo $form->textField($model_contact,'name', $span_field); ?>
  </div>
  <div class="row-fluid field field-telephone">
    <?php echo $form->labelEx($model_contact, 'telephone', $span_label); ?>
    <?php echo $form->textField($model_contact,'telephone', array('onkeyup'=>'autoFormat(this, "telephone")', 'onKeyPress'=>'chkNumber()')+$span_field); ?>
  </div>
  <div class="row-fluid field field-email">
    <?php echo $form->labelEx($model_contact, 'email', $span_label); ?>
    <?php echo $form->textField($model_contact,'email', $span_field); ?>
  </div>
  <div class="row-fluid field field-picture">
    <?php echo $form->labelEx($model_contact, 'picture', $span_label); ?>
    <?php echo $form->FileField($model_contact, 'picture', $span_field); ?>
  </div>
  <div class="row-fluid submit">
    <div class="span3">&nbsp;</div>
    <div class="span9" style="margin:0"><?php echo CHtml::submitButton('ส่งข้อมูล', array('id' => 'btn-add', 'class' => 'btn btn-primary')); ?></div>
  </div>
</div>

<?php
  $this->endWidget();
  Dialog::alertMessage();
?>
<?php
  $form = $this->beginWidget('CActiveForm', array(
    'id' => 'product-discription-form',
    // 'action' => Yii::app()->request->baseUrl . '/shop/delShop',
    'htmlOptions' => array('class' => 'form-horizontal')
  ));

  $optionLeft = 'span3';
  $optionReight = 'span9';

  $statusValue = Products::getProductStatus();

  $textAlign = array('style'=>'text-align:right;');
  $picAlign = array('class'=>'span', 'style'=>'text-align:center');

  switch ($dataProduct['productstatus']) {
  case 2:
    $price = $statusValue[$dataProduct['productstatus']];
    $statusColor = 'style="color:#0099FF;"';
    break;

  case 3:
    $price = $statusValue[$dataProduct['productstatus']];
    $statusColor = 'style="color:#FF0000;"';
    break;

  case 5:
    $price = $statusValue[$dataProduct['productstatus']];
    $statusColor = 'style="color:#2BD125;"';
    break;

  case 6:
    $price = $dataProduct['productprice'];
    $statusColor = 'style="color:#2BD125;"';
    break;
  
  default:
    $price = $statusValue[$dataProduct['productstatus']];
    $statusColor = null;
    break;
  }
?>

<div class="container-fluid">
  <div class="row-fluid">
    <div class="span12"><h1>รายละเอียดสินค้า</h1></div>
  </div>

  <div class="row-fluid">
    <div class="span12" style="text-align:center;">
      <h3>
        <?php echo $productModel->getAttributeLabel('title_productname').' : '.$dataProduct['productname']; ?>
      </h3>
    </div>
  </div>

  <div class="row-fluid">
    <div class="span12" style="text-align:center;">
      <?php
        $itemsDetail = array();
        $decodePicture = json_decode($dataProduct['productpic'], true);
        foreach ($decodePicture as $key => $value) {
          if(!is_null($value['picName'])) {
            array_push($itemsDetail, $value);
          }
        }
        foreach ($itemsDetail as $key => $value) {
          if($key==0 || $key==2) {
            echo CHtml::openTag('div', array('class'=>'row-fluid', 'style'=>'margin-bottom:20px;'));
          }

          if(sizeof($itemsDetail)==1){
            $picAlign = array('class'=>'span12', 'style'=>'text-align:center');
            $picSize = array('style'=>'margin:0 auto;');
          }elseif(sizeof($itemsDetail)==3){
            if($key==0) {
              $picAlign = array('class'=>'span6', 'style'=>'text-align:right');
              $picSize = array('style'=>'float: right;');
            }elseif($key==2){
              $picAlign = array('class'=>'span12', 'style'=>'text-align:center');
              $picSize = array('style'=>'margin:0 auto;');
            }else{
              $picAlign = array('class'=>'span6', 'style'=>'text-align:left');
              $picSize = array('style'=>'float: left;');
            }
          }else{
            if($key==0 || $key==2) {
              $picAlign = array('class'=>'span6', 'style'=>'text-align:right');
              $picSize = array('style'=>'float: right;');
            }else{
              $picAlign = array('class'=>'span6', 'style'=>'text-align:left');
              $picSize = array('style'=>'float: left;');
            }
          }

          echo CHtml::openTag('div', $picAlign);
          // echo CHtml::openTag('div', array('class'=>'picProductDetail')+$picSize);
          echo CHtml::openTag('div', array('class'=>'')+$picSize);
          $picPath = Yii::app()->request->baseUrl.'/img/productPic/'.$value['picName'];
          echo Highslide::PicProduct($picPath,null,250);
          echo CHtml::closeTag('div');
          echo CHtml::closeTag('div');

          if($key==1 || $key==sizeof($itemsDetail)-1) {
            echo CHtml::closeTag('div');
          }
        }
      ?>
    </div>
  </div>
  <div class="row-fluid">
    <div class="descriptionBox">

      <div class="row-fluid">
        <div class="<?php echo $optionLeft; ?>">
          <?php echo $form->labelEx($productModel, 'title_productname', $textAlign); ?>
        </div>
        <div class="<?php echo $optionReight; ?>">
          <?php echo $dataProduct['productname']; ?>
        </div>
      </div>

      <div class="row-fluid">
        <div class="<?php echo $optionLeft; ?>">
          <?php echo $form->labelEx($productModel, 'productprice', $textAlign); ?>
        </div>
        <div class="<?php echo $optionReight; ?>" <?php echo $statusColor; ?> >
          <?php 
            echo '฿ '.$price; 
          ?>
        </div>
      </div>

      <div class="row-fluid">
        <div class="<?php echo $optionLeft; ?>">
          <?php echo $form->labelEx($productModel, 'productdetail', $textAlign); ?>
        </div>
        <div class="<?php echo $optionReight; ?>">
          <?php echo $dataProduct['productdetail']; ?>
        </div>
      </div>

      <div class="row-fluid">
        <div class="<?php echo $optionLeft; ?>">
          <?php echo $form->labelEx($shopsModel, 'title_name', $textAlign); ?>
        </div>
        <div class="<?php echo $optionReight; ?>">
          <?php 
            $shopName = explode('&&', $dataProduct['shopname']); 
            // echo $shopName[1]; 
            echo CHtml::link($shopName[1], Yii::app()->request->baseUrl.'/site/ShopDetail/'.$dataProduct['user_id'],array('class'=>'disShopName'));
          ?>
        </div>
      </div>

      <div class="row-fluid">
        <div class="<?php echo $optionLeft; ?>">
          <?php echo $form->labelEx($shopsModel, 'title_shopid', $textAlign); ?>
        </div>
        <div class="<?php echo $optionReight; ?>">
          <?php echo $dataProduct['sequence']; ?>
        </div>
      </div>

      <div class="row-fluid">
        <div class="<?php echo $optionLeft; ?>">
          <?php echo $form->labelEx($usersModel, 'title_phonenumber', $textAlign); ?>
        </div>
        <div class="<?php echo $optionReight; ?>">
          <?php echo $dataProduct['phonenumber']; ?>
        </div>
      </div>

      <div class="row-fluid">
        <div class="<?php echo $optionLeft; ?>">
          <?php echo $form->labelEx($productModel, 'productdate', $textAlign); ?>
        </div>
        <div class="<?php echo $optionReight; ?>">
          <?php echo $dataProduct['productdate']; ?>
        </div>
      </div>

      <div class="row-fluid">
        <div class="<?php echo $optionLeft; ?>">
          <?php echo $form->labelEx($productModel, 'view', $textAlign); ?>
        </div>
        <div class="<?php echo $optionReight; ?>">
          <?php echo $dataProduct['view']; ?>
        </div>
      </div>

    </div>
  </div>

  <div class="row-fluid">
    <div class="span12" style="text-align: center;">
      <?php 
        if (isset(yii::app()->user->permission) && yii::app()->user->permission == 3) {
          $getProduct = Products::model()->findByAttributes(array('productid'=>$dataProduct['productid']));
          if ($getProduct->approve == 3) {
            echo CHtml::button('ย้อนกลับ',array('class'=>'btn btn-success','onclick'=>'window.location="'.Yii::app()->request->baseUrl.'/products/banProduct"')); 
          }else{
            echo CHtml::button('ล๊อคพระ',array('id'=>'blockProduct', 'class'=>'btn btn-danger','prd-id'=>$dataProduct['productid'])); 
          }
        }
      ?>
    </div>
  </div>

</div>
  
<?php
  $this->endWidget();
?>
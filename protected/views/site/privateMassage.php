<?php
  $form = $this->beginWidget('CActiveForm', array(
      'id' => 'private-massage-form',
      'action' => Yii::app()->request->baseUrl . '/site/addPrivateMassage',
      'htmlOptions' => array('enctype' => 'multipart/form-data'),
          ));

  $user_id = Yii::app()->input->get('param1');

  $model_contact = new Contact;
  $model_users = new Users;

  $span_label = array('class' => 'span3');
  $span_field = array('class' => 'span9');
  $span_div = array('class' => 'span12');
  $submit = array('class' => 'btn');
?>

  <h1>ส่งข้อความถึงร้านค้า</h1>

  <?php echo $form->HiddenField($model_contact, 'type', array('value'=>0)); ?>

<div class="container-fluid">
<!--   <div class="row-fluid field field-type">
    <?php echo $form->labelEx($model_contact, 'type', $span_label); ?>
    <?php echo $form->DropDownList($model_contact, 'type', Contact::getContactType(), $span_field); ?>
  </div> -->
  <div class="row-fluid field field-subject">
    <?php echo $form->labelEx($model_contact, 'subject', $span_label); ?>
    <?php echo $form->textField($model_contact,'subject', $span_field); ?>
  </div>
  <div class="row-fluid field field-message">
    <?php echo $form->labelEx($model_contact, 'message', $span_label); ?>
    <?php echo $form->TextArea($model_contact,'message', $span_field); ?>
  </div>
  <div class="row-fluid field field-name">
    <?php echo $form->labelEx($model_contact, 'name', $span_label); ?>
    <?php echo $form->textField($model_contact,'name', $span_field); ?>
  </div>
  <div class="row-fluid field field-telephone">
    <?php echo $form->labelEx($model_contact, 'telephone', $span_label); ?>
    <?php echo $form->textField($model_contact,'telephone', array('onkeyup'=>'autoFormat(this, "telephone")')+$span_field); ?>
  </div>
  <div class="row-fluid field field-email">
    <?php echo $form->labelEx($model_contact, 'email', $span_label); ?>
    <?php echo $form->textField($model_contact,'email', $span_field); ?>
  </div>
  <div class="row-fluid field field-picture">
    <?php echo $form->labelEx($model_contact, 'picture', $span_label); ?>
    <?php echo $form->FileField($model_contact, 'picture', $span_field); ?>
  </div>
  <div class="row-fluid submit">
    <div class="span3">
    	<?php echo $form->hiddenField($model_users,'user_id', array('value'=>$user_id)); ?>
    </div>
    <div class="span9" style="margin:0"><?php echo CHtml::submitButton('ส่งข้อมูล', array('id' => 'btn-add', 'class' => 'btn btn-primary')); ?></div>
  </div>
</div>

<?php
  $this->endWidget();
  Dialog::alertMessage();
?>
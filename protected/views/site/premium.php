<?php
  $form = $this->beginWidget('CActiveForm', array(
      'id' => 'premium-form',
      // 'action' => '#'
          ));
?>

<div class="container-fluid">
  <div class="row-fluid headerForm sizeContent">
    <div class="span12">
      <h1>แนะนำรายละเอียดเกี่ยวกับหน้าเว็บ</h1>
    </div>
  </div>

  <div class="row-fluid bodyForm">
    <div class="row-fluid">
      <div>
        <?php echo $PremiumText; ?>
      </div>
    </div>

    <div class="row-fluid">
      <div>
        <h1>20 อันดับ โชว์พระเครื่อง</h1>
      </div>
    </div>

    <div class="row-fluid">
      <div>
        <?php
          echo Premium::showPremium(); 
        ?>
      </div>
    </div>
  </div>
</div>

<?php
  $this->endWidget();
?>
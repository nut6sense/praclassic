<?php
  $form = $this->beginWidget('CActiveForm', array(
    'id' => 'shop-list-form',
    // 'action' => Yii::app()->request->baseUrl . '/news/saveNews',
    'action' => '#',
    ));

  $model_products = new Products;
  $model_category = new Category;

  $inputOption = array('class'=>'span8');
?>

<div id="shoplist" class="container-fluid">
  <div class="row-fluid headerForm sizeContent">
    <div class="span12">
      <h1>รายการพระเครื่อง</h1>
    </div>
  </div>

  <div class="span12 bodyForm" style="margin-left:0px;">

    <div class="row-fluid">
      <div class="span3">
        <?php echo $form->textField($model_products, 'productname', array('placeholder'=>'กรอกคำค้นหา','class'=>'span','value'=>isset($_POST['Products']['productname'])?$_POST['Products']['productname']:null,)); ?>
        <?php echo $form->hiddenField($model_products, 'page', array('value'=>isset($_POST['Products']['page'])?$_POST['Products']['page']:null,)); ?>
      </div>
      <div class="span3">
        <?php echo $form->DropDownList($model_category, 'category_name', Category::getCategoryData(),array('class'=>'span','options' => array((isset($_POST['Category']['category_name'])?$_POST['Category']['category_name']:null)=>array('selected'=>true)))); ?>
      </div>
      <div class="span3">
        <?php echo CHtml::submitButton('ค้นหา', array('id' => 'btn-search', 'class' => 'btn btn-primary')); ?>
      </div>
    </div>

    <div class="row-fluid">
      <div class="span12">
        <?php
          $firstChar = array("ก","ข","ฃ","ค","ฅ","ฆ","ง","จ","ฉ","ช","ซ","ฌ","ญ","ฎ","ฎ","ฐ","ฑ","ฒ","ณ","ด","ต","ถ","ท","ธ","น","บ","ป","ผ","ฝ","พ","ฟ","ภ","ม","ย","ร","ล","ว","ศ","ษ","ส","ห","ฬ","อ","ฮ");
          $engChar = range('A', 'Z');

          echo "<ul class=\"firstChar\">";
          foreach($firstChar as $value => $char) {
            $value++;
            echo "<li name=\"letter-search\" class=\"char-$value\"><a class=\"btn\">$char</a></li>";
          }
          echo "</ul>";

          echo "<ul class=\"firstChar\">";
          foreach($engChar as $value => $char) {
            $value++;
            echo "<li name=\"letter-search\" class=\"char-$value\"><a class=\"btn\">$char</a></li>";
          }
          echo "</ul>";
				?>
      </div>
    </div>
    <br/>
    <div class="row-fluid results items">
      <div class="span12">
        <?php
					echo Products::allProduct(null, $_POST);
				?>
      </div>
    </div>

  </div>
</div>
<?php
  $this->endWidget();
?>
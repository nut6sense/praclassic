<?php
  $form = $this->beginWidget('CActiveForm', array(
      'id' => 'contact-us-form',
      'action' => Yii::app()->request->baseUrl . '/site/AddPayConfirm',
      'htmlOptions' => array('class' => 'form-horizontal','enctype' => 'multipart/form-data')
          ));

  $model_contact = new Contact;
  $model_payConfirm = new Payconfirm;

  $span_label = array('class' => 'span3');
  $span_field = array('class' => 'span9');
  $span_div = array('class' => 'span12');
  $submit = array('class' => 'btn');
?>
<h1>ยืนยันการชำระเงิน</h1>
<div class="container-fluid">
  <div class="row-fluid">
    <div class="span12">กรุณากรอกข้อมูล รายละเอียดการจ่ายเงิน</div>
  </div>
  <br/>

  <div class="row-fluid field">
    <?php echo $form->labelEx($model_payConfirm, 'shop_name', $span_label); ?>
    <?php echo $form->textField($model_payConfirm,'shop_name', $span_field); ?>
  </div>

  <div class="row-fluid field">
    <?php echo $form->labelEx($model_payConfirm, 'payconfirm_type', $span_label); ?>
    <?php echo $form->DropDownList($model_payConfirm, 'payconfirm_type', Payment::getPaymentType(), $span_field); ?>
  </div>

  <div class="row-fluid field">
    <?php echo $form->labelEx($model_payConfirm, 'payconfirm_phone', $span_label); ?>
    <?php echo $form->textField($model_payConfirm,'payconfirm_phone', array('onkeyup'=>'autoFormat(this, "telephone")', 'onKeyPress'=>'chkNumber()')+$span_field); ?>
  </div>

  <div class="row-fluid field">
    <?php echo $form->labelEx($model_payConfirm, 'payconfirm_pic', $span_label); ?>
    <?php echo $form->FileField($model_payConfirm, 'payconfirm_pic', $span_field); ?>
  </div>

  <div class="row-fluid field">
    <?php echo $form->labelEx($model_payConfirm, 'bank_name', $span_label); ?>
    <?php echo $form->DropDownList($model_payConfirm, 'bank_name', Banks::getBank(), $span_field); ?>
  </div>

  <div class="row-fluid field">
    <?php echo $form->labelEx($model_payConfirm, 'amount', $span_label); ?>
    <?php echo $form->textField($model_payConfirm,'amount', $span_field+array('placeholder'=>'ระบุเป็นตัวเลขเท่านั้น')); ?>
  </div>

  <div class="row-fluid field">
    <?php echo $form->labelEx($model_payConfirm, 'date', $span_label); ?>
    <?php echo $form->textField($model_payConfirm,'date', array('class'=>'span9 datepicker','value'=>date('d/m/Y'))); ?>
  </div>

  <div class="row-fluid field">
    <?php echo $form->labelEx($model_payConfirm, 'time', $span_label); ?>
    <?php echo $form->textField($model_payConfirm,'time', $span_field+array('value'=>date('H.i น.'))); ?>
  </div>

  <div class="row-fluid field">
    <?php echo $form->labelEx($model_payConfirm, 'note', $span_label); ?>
    <?php echo $form->TextArea($model_payConfirm,'note', $span_field); ?>
  </div>
  
  <div class="row-fluid submit">
    <div class="span3">&nbsp;</div>
    <div class="span9" style="margin:0"><?php echo CHtml::submitButton('ส่งข้อมูล', array('id' => 'btn-add', 'class' => 'btn btn-primary')); ?></div>
  </div>
</div>

<?php
  $this->endWidget();
  Dialog::alertMessage();
?>
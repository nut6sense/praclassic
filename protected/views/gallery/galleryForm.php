<?php
  $form = $this->beginWidget('CActiveForm', array(
      'id' => 'data-gallery-form',
      'action' => Yii::app()->request->baseUrl . '/gallery/submitGallery',
      'htmlOptions' => array('enctype' => 'multipart/form-data')
          ));

  $inputOption = array('class'=>'span8');

  if(!is_null($model_gallery->gallerypic)) {
    $glyPic = json_decode($model_gallery->gallerypic, true);
  }
?>

<div class="container-fluid">
  <div class="row-fluid headerForm sizeContent">
    <div class="span12">
      <h1>เพิ่ม Gallery ใหม่</h1>
    </div>
  </div>

  <!-- <div class="span12 bodyForm" style="margin-left:0px;height:1000px;width:900px;overflow:auto;"> -->

    <div class="row-fluid">
      <div class="span3">
        <?php echo $form->labelEx($model_gallery, 'category_id'); ?>
      </div>
      <div class="span9">
        <?php echo $form->DropDownList($model_gallery, 'category_id', Category::getCategoryData(), $inputOption); ?>
      </div>
    </div>

    <div class="row-fluid">
      <div class="span3">
        <?php echo $form->labelEx($model_gallery, 'galleryname'); ?>
      </div>
      <div class="span9">
        <?php echo $form->textField($model_gallery, 'galleryname', $inputOption); ?>
      </div>
    </div>

    <div class="row-fluid">
      <div class="span3">
        <?php echo $form->labelEx($model_gallery, 'gallerydetail'); ?>
      </div>
      <div class="span11 sizeContent ckBox">
        <?php 
          if(!empty($model_gallery->gallerydetail)) {
            $gallerydetail = $model_gallery->gallerydetail;
            $btnText = 'แก้ไข Gallery';
          }else{
            $gallerydetail = null;
            $btnText = 'เพิ่ม Gallery';
          }
          echo CHtml::textArea('gallerydetail', $gallerydetail, array('cols'=>'80', 'id'=>'editor1', 'name'=>'editor1', 'rows'=>'10')); 
        ?>
      </div>
    </div>

    <div class="row-fluid">
      <div class="span3">
        <label>รูป Gallery ที่ 1</label>
      </div>
      <div class="span9">
        <?php 
          $picPath1 = !empty($glyPic[1]['picName'])?Yii::app()->request->baseUrl.'/img/galleryPic/'.$glyPic[1]['picName']:null;
          echo !is_null($picPath1)?Highslide::PicProduct($picPath1,null,250).'<br />':null;
          // echo CHtml::image($picPath1, null, array('id'=>'Gallery_prd_1_pic'));  
        ?>
        <?php echo $form->FileField($model_gallery, 'gallerypic[1][picName]', $inputOption); ?>
      </div>
    </div>

    <div class="row-fluid">
      <div class="span3">
        <label>รูป Gallery ที่ 2</label>
      </div>
      <div class="span9">
        <?php
          $picPath2 = !empty($glyPic[2]['picName'])?Yii::app()->request->baseUrl.'/img/galleryPic/'.$glyPic[2]['picName']:null; 
          echo !is_null($picPath2)?Highslide::PicProduct($picPath2,null,250).'<br />':null;
          // echo CHtml::image($picPath2, null, array('id'=>'Gallery_prd_2_pic'));  
        ?>
        <?php echo $form->FileField($model_gallery, 'gallerypic[2][picName]', $inputOption); ?>
      </div>
    </div>

    <div class="row-fluid">
      <div class="span3">
        <label>รูป Gallery ที่ 3</label>
      </div>
      <div class="span9">
        <?php 
          $picPath3 = !empty($glyPic[3]['picName'])?Yii::app()->request->baseUrl.'/img/galleryPic/'.$glyPic[3]['picName']:null; 
          echo !is_null($picPath3)?Highslide::PicProduct($picPath3,null,250).'<br />':null;
          // echo CHtml::image($picPath3, null, array('id'=>'Gallery_prd_3_pic'));  
        ?>
        <?php echo $form->FileField($model_gallery, 'gallerypic[3][picName]', $inputOption); ?>
      </div>
    </div>

    <div class="row-fluid">
      <div class="span3">
        <label>รูป Gallery ที่ 4</label>
      </div>
      <div class="span9">
        <?php 
          $picPath4 = !empty($glyPic[4]['picName'])?Yii::app()->request->baseUrl.'/img/galleryPic/'.$glyPic[4]['picName']:null; 
          echo !is_null($picPath4)?Highslide::PicProduct($picPath4,null,250).'<br />':null;
          // echo CHtml::image($picPath4, null, array('id'=>'Gallery_prd_4_pic'));  
        ?>
        <?php echo $form->FileField($model_gallery, 'gallerypic[4][picName]', $inputOption); ?>
      </div>
    </div>

    <div class="row-fluid field-submit">
      <div class="span12">
        <?php echo $form->hiddenField($model_gallery, 'galleryid'); ?>
        <?php echo CHtml::link('ย้อนกลับ', 'gallery', array('id' => 'btn-add', 'class' => 'btn btn-success btnLink')); ?>&nbsp;
        <?php echo CHtml::submitButton($btnText, array('id' => 'btn-add', 'class' => 'btn btn-info')); ?>&nbsp;
      </div>
    </div>

    <?php
      $this->renderPartial('//layouts/plugin/scriptWebTextEditor');
    ?>

  <!-- </div> -->
</div>
<?php
  $this->endWidget();
?>
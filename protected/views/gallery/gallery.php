<?php
  $form = $this->beginWidget('CActiveForm', array(
      'id' => 'gallery-form',
      'action' => Yii::app()->request->baseUrl . '/gallery/delGallery'
          ));
?>

<div class="container-fluid">
  <div class="row-fluid headerForm sizeContent">
    <div class="span12">
      <h1>Gallery</h1>
    </div>
  </div>

  <div class="row-fluid bodyForm">
    <div class="span12">
      <table class="table" id="gallery-detail-table">
        <thead>
          <tr>
            <th>#</th>
            <th>ลำดับ</th>
            <th>รายการสินค้า</th>
            <th>ผู้ชม</th>
            <th>วันที่</th>
            <!-- <th>แก้ไข</th> -->
          </tr>
        </thead>
        <tbody>
          <?php 
            echo Gallery::tblDataGallery(); 
          ?>
        </tbody>
      </table>
    </div>
  </div>

  <div class="row-fluid field-submit">
    <div class="span12">
      <?php echo CHtml::link('เพิ่ม Gallery', 'galleryForm', array('id' => 'btn-add', 'class' => 'btn btn-info btnLink')); ?>&nbsp;
      <?php echo CHtml::submitButton('ลบ Gallery เลือก', array('id' => 'btn-delete', 'class' => 'btn btn-danger',  'confirm'=>'ต้องการลบข้อมูล Gallery ที่เลือกใช้หรือไม่ ?')); ?>&nbsp;
    </div>
  </div>
</div>

<?php
  $this->endWidget();
?>

<?php
  Dialog::alertMessage();
?>
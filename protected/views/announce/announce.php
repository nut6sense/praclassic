<?php
  $form = $this->beginWidget('CActiveForm', array(
      'id' => 'announce-form',
      'action' => Yii::app()->request->baseUrl . '/announce/delAnnounce'
          ));

  $model_announce = new Announce;
?>

<div class="container-fluid">
  <div class="row-fluid headerForm sizeContent">
    <div class="span12">
      <h1>ประกาศ</h1>
    </div>
  </div>

  <div class="row-fluid bodyForm">
    <div class="span12">
      <table class="table" id="news-detail-table">
        <thead>
          <tr>
            <th>#</th>
            <th>ลำดับ</th>
            <th>ชื่อ</th>
            <th>แก้ไข</th>
          </tr>
        </thead>
        <tbody>
          <?php 
            echo Announce::tblAnnounceData(); 
          ?>
        </tbody>
      </table>
    </div>
  </div>

  <div class="row-fluid field-submit">
    <div class="span12">
    	<?php echo CHtml::link('เพิ่มประกาศ', 'dataAnnounce', array('id' => 'btn-add', 'class' => 'btn btn-info btnLink')); ?>&nbsp;
      <?php echo CHtml::submitButton('ลบประกาศที่เลือก', array('id' => 'btn-delete', 'class' => 'btn btn-danger',  'confirm'=>'ต้องการลบข้อมูลประกาศที่เลือกใช้หรือไม่ ?')); ?>&nbsp;
    </div>
  </div>
</div>

<?php
  $this->endWidget();
?>

<?php
  Dialog::alertMessage();
?>
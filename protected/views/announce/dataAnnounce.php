<?php
  $form = $this->beginWidget('CActiveForm', array(
      'id' => 'data-announce-form',
      'action' => Yii::app()->request->baseUrl . '/announce/saveAnnounce'
          ));

  $inputOption = array('class'=>'span8');
?>

<div class="container-fluid">
  <div class="row-fluid headerForm sizeContent">
    <div class="span12">
      <h1>รายละเอียดประกาศ</h1>
    </div>
  </div>

  <div class="span12 bodyForm" style="margin-left:0px;">

    <div class="row-fluid">
      <div class="span3">
        <?php echo $form->labelEx($model_announce, 'announcename'); ?>
      </div>
      <div class="span9">
        <?php echo $form->textField($model_announce, 'announcename', $inputOption); ?>
      </div>
    </div>

    <div class="row-fluid">
      <div class="span3">
        <?php echo $form->labelEx($model_announce, 'announcesubdetail'); ?>
      </div>
      <div class="span9">
        <?php echo $form->textArea($model_announce, 'announcesubdetail', $inputOption); ?>
      </div>
    </div>

    <div class="row-fluid">
      <div class="span3">
        <?php echo $form->labelEx($model_announce, 'announcedetail'); ?>
      </div>
      <div class="span11 sizeContent ckBox">
        <?php 
          if(!empty($model_announce->announcedetail)) {
            $announcedetail = $model_announce->announcedetail;
            $btnText = 'แก้ไขประกาศ';
          }else{
            $announcedetail = null;
            $btnText = 'เพิ่มประกาศ';
          }
          echo CHtml::textArea('announcedetail', $announcedetail, array('cols'=>'80', 'id'=>'editor1', 'name'=>'editor1', 'rows'=>'10')); 
        ?>
      </div>
    </div>

    <div class="row-fluid field-submit">
    <div class="span12">
      <?php echo $form->hiddenField($model_announce, 'announceid'); ?>
      <?php //echo CHtml::link('ย้อนกลับ', 'announce', array('id' => 'btn-add', 'class' => 'btn btn-success btnLink')); ?>&nbsp;
      <?php echo CHtml::submitButton($btnText, array('id' => 'btn-add', 'class' => 'btn btn-info')); ?>&nbsp;
    </div>
  </div>

    <?php
      $this->renderPartial('//layouts/plugin/scriptWebTextEditor');
    ?>

  </div>
</div>
<?php
  $this->endWidget();
?>
<?php
  $form = $this->beginWidget('CActiveForm', array(
      'id' => 'data-premium-form',
      'action' => Yii::app()->request->baseUrl . '/premium/submitPremium',
      'htmlOptions' => array('enctype' => 'multipart/form-data')
          ));

  $inputOption = array('class'=>'span8');

  if(!is_null($model_premium->premiumpic)) {
    $prmPic = json_decode($model_premium->premiumpic, true);
  }
?>

<div class="container-fluid">
  <div class="row-fluid headerForm sizeContent">
    <div class="span12">
      <h1>เพิ่ม Premium ใหม่</h1>
    </div>
  </div>

  <!-- <div class="span12-fluid bodyForm" style="margin-left:0px;height:1000px;width:900px;overflow:auto;"> -->

    <div class="row-fluid">
      <div class="span3">
        <?php echo $form->labelEx($model_premium, 'category_id'); ?>
      </div>
      <div class="span9">
        <?php echo $form->DropDownList($model_premium, 'category_id', Category::getCategoryData(), $inputOption); ?>
      </div>
    </div>

    <div class="row-fluid">
      <div class="span3">
        <?php echo $form->labelEx($model_premium, 'premiumname'); ?>
      </div>
      <div class="span9">
        <?php echo $form->textField($model_premium, 'premiumname', $inputOption); ?>
      </div>
    </div>

    <div class="row-fluid">
      <div class="span3"></div>
      <div class="span9">
        <label class="checkbox">
          <?php echo $form->checkBox($model_premium, 'default'); ?>
          ตั้งเป็นหน้าปก Premium
        <label>
      </div>
    </div>

    <div class="row-fluid">
      <div class="span3">
        <?php echo $form->labelEx($model_premium, 'premiumdetail'); ?>
      </div>
      <div class="span11 sizeContent ckBox">
        <?php 
          if(!empty($model_premium->premiumdetail)) {
            $premiumdetail = $model_premium->premiumdetail;
            $btnText = 'แก้ไข Premium';
          }else{
            $premiumdetail = null;
            $btnText = 'เพิ่ม Premium';
          }
          echo CHtml::textArea('premiumdetail', $premiumdetail, array('cols'=>'80', 'id'=>'editor1', 'name'=>'editor1', 'rows'=>'10')); 
        ?>
      </div>
    </div>

    <div class="row-fluid">
      <div class="span3">
        <label>รูป Premium ที่ 1</label>
      </div>
      <div class="span9">
        <?php 
          $picPath1 = !empty($prmPic[1]['picName'])?Yii::app()->request->baseUrl.'/img/premiumPic/'.$prmPic[1]['picName']:null;
          echo !is_null($picPath1)?Highslide::PicProduct($picPath1,null,250).'<br />':null;
          // echo CHtml::image($picPath1, null, array('id'=>'Premium_prd_1_pic'));  
        ?>
        <?php echo $form->FileField($model_premium, 'premiumpic[1][picName]', $inputOption); ?>
      </div>
    </div>

    <div class="row-fluid">
      <div class="span3">
        <label>รูป Premium ที่ 2</label>
      </div>
      <div class="span9">
        <?php
          $picPath2 = !empty($prmPic[2]['picName'])?Yii::app()->request->baseUrl.'/img/premiumPic/'.$prmPic[2]['picName']:null; 
          echo !is_null($picPath2)?Highslide::PicProduct($picPath2,null,250).'<br />':null;
          // echo CHtml::image($picPath2, null, array('id'=>'Premium_prd_2_pic'));  
        ?>
        <?php echo $form->FileField($model_premium, 'premiumpic[2][picName]', $inputOption); ?>
      </div>
    </div>

    <div class="row-fluid">
      <div class="span3">
        <label>รูป Premium ที่ 3</label>
      </div>
      <div class="span9">
        <?php 
          $picPath3 = !empty($prmPic[3]['picName'])?Yii::app()->request->baseUrl.'/img/premiumPic/'.$prmPic[3]['picName']:null; 
          echo !is_null($picPath3)?Highslide::PicProduct($picPath3,null,250).'<br />':null;
          // echo CHtml::image($picPath3, null, array('id'=>'Premium_prd_3_pic'));  
        ?>
        <?php echo $form->FileField($model_premium, 'premiumpic[3][picName]', $inputOption); ?>
      </div>
    </div>

    <div class="row-fluid">
      <div class="span3">
        <label>รูป Premium ที่ 4</label>
      </div>
      <div class="span9">
        <?php 
          $picPath4 = !empty($prmPic[4]['picName'])?Yii::app()->request->baseUrl.'/img/premiumPic/'.$prmPic[4]['picName']:null; 
          echo !is_null($picPath4)?Highslide::PicProduct($picPath4,null,250).'<br />':null;
          // echo CHtml::image($picPath4, null, array('id'=>'Premium_prd_4_pic'));  
        ?>
        <?php echo $form->FileField($model_premium, 'premiumpic[4][picName]', $inputOption); ?>
      </div>
    </div>

    <div class="row-fluid field-submit">
      <div class="span12">
        <?php echo $form->hiddenField($model_premium, 'premiumid'); ?>
        <?php echo CHtml::link('ย้อนกลับ', 'premium', array('id' => 'btn-add', 'class' => 'btn btn-success btnLink')); ?>&nbsp;
        <?php echo CHtml::submitButton($btnText, array('id' => 'btn-add', 'class' => 'btn btn-info')); ?>&nbsp;
      </div>
    </div>

    <?php
      $this->renderPartial('//layouts/plugin/scriptWebTextEditor');
    ?>

  <!-- </div> -->
</div>
<?php
  $this->endWidget();
?>
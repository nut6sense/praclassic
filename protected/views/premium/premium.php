<?php
  $form = $this->beginWidget('CActiveForm', array(
      'id' => 'premium-form',
      'action' => Yii::app()->request->baseUrl . '/premium/delPremium',
      'htmlOptions' => array('class' => 'form-horizontal')
          ));
?>

<div class="container-fluid">
  <div class="row-fluid headerForm sizeContent">
    <div class="span12">
      <h1>Premium</h1>
    </div>
  </div>

  <div class="row-fluid bodyForm">
    <div class="span12">
      <table class="table" id="premium-detail-table">
        <thead>
          <tr>
            <th>#</th>
            <th>ลำดับ</th>
            <th>รายการสินค้า</th>
            <th>ผู้ชม</th>
            <th>วันที่</th>
            <!-- <th>แก้ไข</th> -->
          </tr>
        </thead>
        <tbody>
          <?php 
            echo Premium::tblDataPremium(); 
          ?>
        </tbody>
      </table>
    </div>
  </div>

  <div class="row-fluid field-submit">
    <div class="span12">
      <?php echo CHtml::link('เพิ่ม Premium', 'premiumForm', array('id' => 'btn-add', 'class' => 'btn btn-info btnLink')); ?>&nbsp;
      <?php echo CHtml::submitButton('ลบ Premium เลือก', array('id' => 'btn-delete', 'class' => 'btn btn-danger',  'confirm'=>'ต้องการลบข้อมูล Premium ที่เลือกใช้หรือไม่ ?')); ?>&nbsp;
    </div>
  </div>
</div>

<?php
  $this->endWidget();
?>

<?php
  Dialog::alertMessage();
?>
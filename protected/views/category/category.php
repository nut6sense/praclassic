<?php
  $form = $this->beginWidget('CActiveForm', array(
      'id' => 'category-form',
      'action' => Yii::app()->request->baseUrl . '/category/delCategory',
      'htmlOptions' => array('class' => 'form-horizontal')
          ));

  $model_category = new Category;
?>

<div class="container-fluid">
  <div class="row-fluid headerForm sizeContent">
    <div class="span12">
      <h1>หมวดหมู่พระ</h1>
    </div>
  </div>

  <div class="row-fluid bodyForm">
    <div class="span12">
      <table class="table" id="category-detail-table">
        <thead>
          <tr>
            <th>#</th>
            <th>รหัส</th>
            <th>หมวดหมู่</th>
            <th>จำนวน</th>
            <!-- <th>แก้ไข</th> -->
          </tr>
        </thead>
        <tbody>
          <?php echo Category::tblCategoryData(); ?>
        </tbody>
      </table>
    </div>
  </div>

  <div class="row-fluid field-submit">
    <div class="span12">
      <?php echo CHtml::link('เพิ่มหมวดหมู่พระ', Yii::app()->createUrl('category/dataCategory'), array('id' => 'btn-add', 'class' => 'btn btn-info btnLink', 'role'=>'button', 'data-toggle'=>'modal')); ?>&nbsp;
      <?php echo CHtml::submitButton('ลบหมวดหมู่พระที่เลือก', array('id' => 'btn-delete', 'class' => 'btn btn-danger',  'confirm'=>'ต้องการลบข้อมูลหมวดหมู่พระที่เลือกใช้หรือไม่ ?')); ?>&nbsp;
    </div>
  </div>
</div>

<?php
  $this->endWidget();

  Dialog::alertMessage();
?>
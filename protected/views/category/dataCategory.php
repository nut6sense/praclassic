<?php
  $form = $this->beginWidget('CActiveForm', array(
      'id' => 'data-category-form',
      'action' => Yii::app()->request->baseUrl . '/category/addCategory',
          ));

  $model_category = new Category;

  if(is_null(Yii::app()->input->get('param1'))) {
    $back = 'category';
  }else{
    $back = '../category';
  }
?>

<div class="container-fluid">
  <div class="row-fluid">
    <div class="span12"><h1>เพิ่มหมวดหมู่พระ</h1></div>
  </div>

  <div class="row-fluid">
    <div class="span3">
      <?php echo $form->labelEx($model_category, 'category_name'); ?>
    </div>
    <div class="span4">
      <?php echo $form->textField($model_category, 'category_name', array('value'=>$dataCategory['category_name'])); ?>
    </div>
  </div>

  <div class="row-fluid field-submit">
    <div class="span12">
      <?php echo $form->hiddenField($model_category, 'category_id', array('value'=>$dataCategory['category_id']));; ?>
      <?php echo CHtml::link('ย้อนกลับ', $back, array('id' => 'btn-add', 'class' => 'btn btn-success btnLink')); ?>&nbsp;
      <?php echo CHtml::submitButton('บันทึกข้อมูล', array('id' => 'btn-add', 'class' => 'btn btn-info')); ?>
    </div>
  </div>
</div>

<?php
  $this->endWidget();
?>
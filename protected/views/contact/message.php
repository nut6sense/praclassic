<?php
  $form = $this->beginWidget('CActiveForm', array(
      'id' => 'contact-message-form',
      'action' => Yii::app()->request->baseUrl . '/contact/delMessage',
      'htmlOptions' => array('class' => 'form-horizontal')
          ));

  $model = new Contact;
?>

<div class="container-fluid">
  <div class="row-fluid headerForm sizeContent">
    <div class="span12">
      <h1>ติชมแนะนำเว็บ</h1>
    </div>
  </div>
  <div class="row-fluid bodyForm">
    <div class="span12">
      <table class="table" id="category-detail-table">
        <thead>
          <tr>
            <th>#</th>
            <th>หัวข้อ</th>
            <th>ผู้ส่ง</th>
            <th>วันที่</th>
            <!-- <th>แก้ไข</th> -->
          </tr>
        </thead>
        <tbody>
          <?php echo Contact::tblMessage(5); ?>
        </tbody>
      </table>
    </div>
  </div>
  <div class="row-fluid field-submit">
    <div class="span12">
      <?php echo CHtml::submitButton('ลบรายการที่เลือก', array('id' => 'btn-delete', 'class' => 'btn btn-danger',  'confirm'=>'ต้องการลบข้อมูลที่เลือกใช้หรือไม่ ?')); ?>
    </div>
  </div>
</div>

<?php
  $this->endWidget();

  echo Contact::showMessage($form, $model);
  Dialog::alertMessage();

?>
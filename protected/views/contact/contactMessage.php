<?php
  $form = $this->beginWidget('CActiveForm', array(
      'id' => 'data-category-form',
      // 'action' => Yii::app()->request->baseUrl . '/category/addCategory',
          ));

  $model = new Contact;

  if(isset($dataParam['picture']) && !empty($dataParam['picture'])) {
  	$path = Yii::app()->request->baseUrl . '/img/contactPic/'.$dataParam['picture'];
  }else{
  	$path = null;
  }

?>

<div class="container-fluid">
  <div class="row-fluid">
    <div class="span12"><h1><?php echo $topic[0]; ?></h1></div>
  </div>

  <div class="row-fluid">
    <div class="span3">
      <?php echo $form->labelEx($model, 'subject'); ?>
    </div>
    <div class="span8">
      <?php echo $form->textField($model, 'subject', array('value'=>$dataContact->subject, 'readonly'=>true, 'class'=>'span')); ?>
    </div>
  </div>

  <div class="row-fluid">
    <div class="span3">
      <?php echo $form->labelEx($model, 'message'); ?>
    </div>
    <div class="span8">
      <?php echo $form->textArea($model, 'message', array('value'=>$dataContact->message, 'readonly'=>true, 'class'=>'span')); ?>
    </div>
  </div>

  <div class="row-fluid">
    <div class="span3">
      <?php echo $form->labelEx($model, 'name'); ?>
    </div>
    <div class="span8">
      <?php echo $form->textField($model, 'name', array('value'=>$dataContact->name, 'readonly'=>true, 'class'=>'span')); ?>
    </div>
  </div>

  <div class="row-fluid">
    <div class="span3">
      <?php echo $form->labelEx($model, 'telephone'); ?>
    </div>
    <div class="span8">
      <?php echo $form->textField($model, 'telephone', array('value'=>$dataContact->telephone, 'readonly'=>true, 'class'=>'span')); ?>
    </div>
  </div>

  <div class="row-fluid">
    <div class="span3">
      <?php echo $form->labelEx($model, 'email'); ?>
    </div>
    <div class="span8">
      <?php echo $form->textField($model, 'email', array('value'=>$dataContact->email, 'readonly'=>true, 'class'=>'span')); ?>
    </div>
  </div>

  <div class="row-fluid">
    <div class="span3">
      <?php echo $form->labelEx($model, 'picture'); ?>
    </div>
    <div class="span8">
      <img src="<?php echo $path; ?>"/>
    </div>
  </div>

  <div class="row-fluid field-submit">
    <div class="span12">
      <?php echo CHtml::link('ย้อนกลับ', '../'.$topic[1], array('id' => 'btn-add', 'class' => 'btn btn-success btnLink')); ?>&nbsp;
    </div>
  </div>
</div>

<?php
  $this->endWidget();
?>
<?php
  $form = $this->beginWidget('CActiveForm', array(
      'id' => 'ban-products-form',
      'action' => Yii::app()->request->baseUrl . '/products/delProducts',
      'htmlOptions' => array('class' => 'form-horizontal')
          ));
?>

<div class="container-fluid">
  <div class="row-fluid headerForm sizeContent">
    <div class="span12">
      <h1>สินค้าที่ถูกล๊อค</h1>
    </div>
  </div>
  <div class="row-fluid bodyForm">
    <div class="span12">
      <table class="table" id="category-detail-table">
        <thead>
          <tr>
            <th>ลำดับ</th>
            <th>รายการสินค้า</th>
            <th>ราคาขาย</th>
            <th>วันที่</th>
            <th>สถานะ</th>
            <th>ปลดล๊อค</th>
          </tr>
        </thead>
        <tbody>
          <?php 
            echo Products::tblDataProductBlock(); 
          ?>
        </tbody>
      </table>
    </div>
  </div>
</div>

<?php
  $this->endWidget();

  Dialog::alertMessage();
?>
<?php
  $form = $this->beginWidget('CActiveForm', array(
      'id' => 'popular-form',
      'action' => Yii::app()->request->baseUrl . '/products/editProducts',
      'htmlOptions' => array('class' => 'form-horizontal')
          ));
  $user_id = Yii::app()->input->get('param1');
?>

<div class="container-fluid">
  <div class="row-fluid">
    <div class="span12"><h1>พระเด่น</h1></div>
  </div>
  <div class="row-fluid">
    <div class="span12">
      <table class="table" id="category-detail-table">
        <thead>
          <tr>
            <th>#</th>
            <th>ลำดับ</th>
            <th>รายการสินค้า</th>
          </tr>
        </thead>
        <tbody>
          <?php 
            echo Products::tblPopDataProduct($user_id); 
          ?>
        </tbody>
      </table>
    </div>
  </div>
  <div class="row-fluid">
    <div class="span12 row-fluid field-submit">
      <?php echo CHtml::hiddenField('user_id', $user_id); ?>
      <?php echo CHtml::submitButton('แก้ไขพระเด่น', array('id' => 'btn-edit', 'class' => 'btn btn-info',  'confirm'=>'ต้องการแก้ไขข้อมูลพระเด่นที่เลือกใช้หรือไม่ ?')); ?>
    </div>
  </div>
</div>

<?php
  $this->endWidget();
?>

<?php
  Dialog::alertMessage();
?>
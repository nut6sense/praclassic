<?php
  $form = $this->beginWidget('CActiveForm', array(
      'id' => 'products-form',
      'action' => Yii::app()->request->baseUrl . '/products/delProducts',
      'htmlOptions' => array('class' => 'form-horizontal')
          ));
?>

<div class="container-fluid">
  <div class="row-fluid">
    <div class="span12"><h1>ข้อมูลสินค้า</h1></div>
  </div>
  <div class="row-fluid">
    <div class="span12">
      <table class="table" id="category-detail-table">
        <thead>
          <tr>
            <th>#</th>
            <th>ลำดับ</th>
            <th>รายการสินค้า</th>
            <th>ราคาขาย</th>
            <th>ผู้ชม</th>
            <th>วันที่</th>
            <th>สถานะ</th>
            <!-- <th>แก้ไข</th> -->
          </tr>
        </thead>
        <tbody>
          <?php 
            echo Products::tblDataProduct(); 
          ?>
        </tbody>
      </table>
    </div>
    <div class="row-fluid field-submit">
      <div class="span12">
        <?php echo CHtml::hiddenField('user_id', Yii::app()->input->get('param1')); ?>
        <?php //echo CHtml::link('เพิ่มพระใหม่', Yii::app()->createUrl('products/editProduct/'.Yii::app()->input->get('param1')), array('id' => 'btn-productForm', 'class' => 'btn btn-info linkDialog')); ?>&nbsp;
        <?php echo CHtml::submitButton('ลบพระที่เลือก', array('id' => 'btn-delete', 'class' => 'btn btn-danger',  'confirm'=>'ต้องการลบข้อมูลสินค้าที่เลือกใช้หรือไม่ ?')); ?>
      </div>
    </div>
  </div>
</div>

<?php
  $this->endWidget();

  // Products::AddProductDialogCotent($form);

  Dialog::alertMessage();
?>
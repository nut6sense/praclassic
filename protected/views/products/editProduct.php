<?php
  $form = $this->beginWidget('CActiveForm', array(
    'id' => 'edit-product-form',
    'action' => Yii::app()->request->baseUrl . '/products/addProduct',
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
  ));

  $model_user = new Users;
  $model_products = new Products;
  $model_category = new Category;

  $user_id = Yii::app()->input->get('param1');
  $findAllProduct = Products::model()->findAllByAttributes(array('user_id'=>$user_id));
  
  $alertBlock = 'style="display: none;"';
  $btnDisabled = array('disabled'=>false);
  if(sizeof($findAllProduct) > 150) {
    $alertBlock = null;
    $btnDisabled = array('disabled'=>true);
  }

  $inputOption = array('class'=>'span');
  $headOption = 'class="span3"';
  $subOption = 'class="span9"';
?>

<div class="container-fluid">
  <div class="row-fluid">
    <div class="span12"><h1>รายละเอียดสินค้า</h1></div>
  </div>

  <div class="row-fluid" <?php echo $alertBlock; ?> >
    <div class="alert alert-error">
      <strong>ไม่สามารถเพิ่มพระใหม่ได้ </strong>เนื่องจากจำนวนพระภายในร้านของคุณเกิน 150 องค์
    </div>
  </div>

  <div class="row-fluid">
    <div <?php echo $headOption; ?> >
      <?php echo $form->labelEx($model_category, 'category_id'); ?>
    </div>
    <div <?php echo $subOption; ?> >
      <?php echo $form->DropDownList($model_category, 'category_id', Category::getCategoryData(), array('options'=>array($dataProduct->category_id=>array('selected'=>true)))+$inputOption); ?>
    </div>
  </div>

  <div class="row-fluid">
    <div <?php echo $headOption; ?> >
      <?php echo $form->labelEx($model_products, 'productname'); ?>
    </div>
    <div <?php echo $subOption; ?> >
      <?php echo $form->textField($model_products, 'productname', $inputOption+array('maxlength'=>25,'placeholder'=>'กรอกตัวอักษรได้ไม่เกิน 25 ตัว', 'value'=>$dataProduct->productname)); ?>
    </div>
  </div>

  <div class="row-fluid">
    <div <?php echo $headOption; ?> >
    </div>
    <div <?php echo $subOption; ?> >
      <?php echo $form->textField($model_products, 'productName2', $inputOption+array('maxlength'=>25,'placeholder'=>'กรอกตัวอักษรได้ไม่เกิน 25 ตัว', 'value'=>$dataProduct->productName2)); ?>
    </div>
  </div>

  <div class="row-fluid">
    <div <?php echo $headOption; ?> >
      <?php echo $form->labelEx($model_products, 'productstatus'); ?>
    </div>
    <div <?php echo $subOption; ?> >
      <?php echo $form->DropDownList($model_products, 'productstatus', Products::getProductStatus(), array('options'=>array($dataProduct->productstatus=>array('selected'=>true)))+$inputOption); ?>
    </div>
  </div>

  <div class="row-fluid">
    <div <?php echo $headOption; ?> >
      <?php echo $form->labelEx($model_products, 'productprice'); ?>
    </div>
    <div <?php echo $subOption; ?> >
      <?php echo $form->textField($model_products, 'productprice', $inputOption+array('readonly'=>true, 'value'=>$dataProduct->productprice)); ?>
    </div>
  </div>

  <div class="row-fluid">
    <div <?php echo $headOption; ?> >
      <?php echo $form->labelEx($model_products, 'productdetail'); ?>
    </div>
    <div <?php echo $subOption; ?> >
      <?php echo $form->textArea($model_products, 'productdetail', $inputOption+array('value'=>$dataProduct->productdetail)); ?>
    </div>
  </div>

  <div class="row-fluid">
    <div <?php echo $headOption; ?> >
      <label>รูปภาพที่ 1</label>
    </div>
    <div <?php echo $subOption; ?> >
      <?php 
        if(isset($dataProduct->product_1_pic) && !is_null($dataProduct->product_1_pic)){
          $picPath = Yii::app()->baseUrl.'/img/productPic/'.$dataProduct->product_1_pic;
          echo Highslide::PicProduct($picPath,null,250).'<br />';
        }

        if($formPage!='references') {
          echo $form->FileField($model_products, 'productpic[1][picName]', $inputOption); 
        }
      ?>
    </div>
  </div>

  <div class="row-fluid">
    <div <?php echo $headOption; ?> >
      <label>รูปภาพที่ 2</label>
    </div>
    <div <?php echo $subOption; ?> >
      <?php 
        if(isset($dataProduct->product_2_pic) && !is_null($dataProduct->product_2_pic)){
          $picPath = Yii::app()->baseUrl.'/img/productPic/'.$dataProduct->product_2_pic;
          echo Highslide::PicProduct($picPath,null,250).'<br />';
        }
        
        if($formPage!='references') {
          echo $form->FileField($model_products, 'productpic[2][picName]', $inputOption); 
        }
      ?>
    </div>
  </div>

  <div class="row-fluid">
    <div <?php echo $headOption; ?> >
      <label>รูปภาพที่ 3</label>
    </div>
    <div <?php echo $subOption; ?> >
      <?php 
        if(isset($dataProduct->product_3_pic) && !is_null($dataProduct->product_3_pic)){
          $picPath = Yii::app()->baseUrl.'/img/productPic/'.$dataProduct->product_3_pic;
          echo Highslide::PicProduct($picPath,null,250).'<br />';
        }
        
        if($formPage!='references') {
          echo $form->FileField($model_products, 'productpic[3][picName]', $inputOption); 
        }
      ?>
    </div>
  </div>

  <div class="row-fluid">
    <div <?php echo $headOption; ?> >
      <label>รูปภาพที่ 4</label>
    </div>
    <div <?php echo $subOption; ?> >
      <?php 
        if(isset($dataProduct->product_4_pic) && !is_null($dataProduct->product_4_pic)){
          $picPath = Yii::app()->baseUrl.'/img/productPic/'.$dataProduct->product_4_pic;
          echo Highslide::PicProduct($picPath,null,250).'<br />';
        }
        
        if($formPage!='references') {
          echo $form->FileField($model_products, 'productpic[4][picName]', $inputOption); 
        }
      ?>
    </div>
  </div>

  <div class="row-fluid field-submit">
    <div class="span12">
      <?php echo CHtml::hiddenField('user_id', Yii::app()->input->get('param1')); ?>
      <?php echo  $form->hiddenField($model_products, 'productid', $inputOption+array('value'=>$dataProduct->productid)); ?>
      <?php
        if($formPage!='references') {
          if(is_null($dataProduct->productid)) {
            $backBtn = '../dataProduct/'.$user_id;
          }else{
            $backBtn = '../../dataProduct/'.$user_id;
          }
        }else{
          $backBtn = '../productReferences';
        }
        echo CHtml::link('ย้อนกลับ', $backBtn, array('id' => 'btn-add', 'class' => 'btn btn-success btnLink')); 
      ?>&nbsp;
      <?php 
        if($formPage!='references') {
          echo CHtml::submitButton('บันทึกข้อมูล', array('id' => 'btn-add', 'class' => 'btn btn-info') + $btnDisabled); 
        }
      ?>
    </div>
  </div>
</div>
<?php
	$this->endWidget();

  Dialog::alertMessage();
?>
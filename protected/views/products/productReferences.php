<?php
  $form = $this->beginWidget('CActiveForm', array(
      'id' => 'products-form',
      'action' => Yii::app()->request->baseUrl . '/products/delProducts',
      'htmlOptions' => array('class' => 'form-horizontal')
          ));
?>

<div class="container-fluid">
  <div class="row-fluid headerForm sizeContent">
    <div class="span12">
      <h1>สินค้าที่ไม่มีการรับรอง</h1>
    </div>
  </div>
  <div class="row-fluid bodyForm">
    <div class="span12">
      <table class="table" id="category-detail-table">
        <thead>
          <tr>
            <th>#</th>
            <th>ลำดับ</th>
            <th>รายการสินค้า</th>
            <th>ราคาขาย</th>
            <th>ผู้ชม</th>
            <th>วันที่</th>
            <th>สถานะ</th>
            <th>ยืนยัน</th>
          </tr>
        </thead>
        <tbody>
          <?php 
            echo Products::tblApproveProduct(); 
          ?>
        </tbody>
      </table>
    </div>
  </div>
  <div class="row-fluid field-submit">
    <div class="span12">
      <?php echo CHtml::hiddenField('shopType', 'allShop'); ?>
      <?php echo CHtml::submitButton('ลบร้านค้าที่เลือก', array('id' => 'btn-delete', 'class' => 'btn btn-danger',  'confirm'=>'ต้องการลบข้อมูลร้านค้าที่เลือกใช้หรือไม่ ?')); ?>&nbsp;
    </div>
  </div>
</div>

<?php
  $this->endWidget();

  Dialog::alertMessage();
?>
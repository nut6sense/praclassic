<?php
  $form = $this->beginWidget('CActiveForm', array(
    'id' => 'data-news-form',
    'action' => Yii::app()->request->baseUrl . '/news/saveNews',
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
    ));

  $inputOption = array('class'=>'span8');
?>

<div class="container-fluid">
  <div class="row-fluid headerForm sizeContent">
    <div class="span12">
      <h1>เพิ่มรายละเอียดข่าวประชาสัมพันธ์</h1>
    </div>
  </div>

  <div class="span12 bodyForm" style="margin-left:0px;">

    <div class="row-fluid">
      <div class="span3">
        <?php echo $form->labelEx($model_news, 'newsname'); ?>
      </div>
      <div class="span9">
        <?php echo $form->textField($model_news, 'newsname', $inputOption); ?>
      </div>
    </div>

    <div class="row-fluid">
      <div class="span3">
        <?php echo $form->labelEx($model_news, 'newssubdetail'); ?>
      </div>
      <div class="span9">
        <?php echo $form->textArea($model_news, 'newssubdetail', $inputOption); ?>
      </div>
    </div>

    <div class="row-fluid">
      <div class="span3">
        <?php echo $form->labelEx($model_news, 'newspic'); ?>
      </div>
      <div class="span9">
          <?php echo $form->FileField($model_news, 'newspic', $inputOption);?>
          <?php
            if (!is_null($model_news['newspic'])) {
              $img = CHtml::image(Yii::app()->request->baseUrl.'/img/newsPic/'.$model_news['newspic'], null, array('style'=>'width:100px;')); 
              echo '<br>'.$img;
            }
          ?>
      </div>
    </div>

    <div class="row-fluid">
      <div class="span3">
        <?php echo $form->labelEx($model_news, 'newsdetail'); ?>
      </div>
      <div class="span11 sizeContent ckBox">
        <?php 
          if(!empty($model_news->newsdetail)) {
            $newsdetail = $model_news->newsdetail;
            $btnText = 'แก้ไขข่าวประชาสัมพันธ์';
          }else{
            $newsdetail = null;
            $btnText = 'เพิ่มข่าวประชาสัมพันธ์';
          }
          echo CHtml::textArea('newsdetail', $newsdetail, array('cols'=>'80', 'id'=>'editor1', 'name'=>'editor1', 'rows'=>'10')); 
        ?>
      </div>
    </div>

    <div class="row-fluid field-submit">
      <div class="span12">
        <?php echo $form->hiddenField($model_news, 'newsid'); ?>
        <?php echo CHtml::link('ย้อนกลับ', 'news', array('id' => 'btn-add', 'class' => 'btn btn-success btnLink')); ?>&nbsp;
        <?php echo CHtml::submitButton($btnText, array('id' => 'btn-add', 'class' => 'btn btn-info')); ?>&nbsp;
      </div>
    </div>

    <?php
      $this->renderPartial('//layouts/plugin/scriptWebTextEditor');
    ?>

  </div>
</div>
<?php
  $this->endWidget();
?>
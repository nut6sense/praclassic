<?php
  $form = $this->beginWidget('CActiveForm', array(
      'id' => 'news-form',
      'action' => Yii::app()->request->baseUrl . '/news/delNews'
          ));

  $model_news = new News;
?>

<div class="container-fluid">
  <div class="row-fluid headerForm sizeContent">
    <div class="span12">
      <h1>ข่าวประชาสัมพันธ์</h1>
    </div>
  </div>

  <div class="row-fluid bodyForm">
    <div class="span12">
      <table class="table" id="news-detail-table">
        <thead>
          <tr>
            <th style="width: 25px">#</th>
            <th style="width: 25px">ลำดับ</th>
            <th>ชื่อข่าวประชาสัมพันธ์</th>
            <th style="width: 50px">แก้ไข</th>
          </tr>
        </thead>
        <tbody>
          <?php 
            echo News::tblNewsData(); 
          ?>
        </tbody>
      </table>
    </div>
  </div>

  <div class="row-fluid field-submit">
    <div class="span12">
    	<?php echo CHtml::link('เพิ่มข่าวประชาสัมพันธ์', 'dataNews', array('id' => 'btn-add', 'class' => 'btn btn-info btnLink')); ?>&nbsp;
      <?php echo CHtml::submitButton('ลบข่าวประชาสัมพันธ์ที่เลือก', array('id' => 'btn-delete', 'class' => 'btn btn-danger',  'confirm'=>'ต้องการลบข้อมูลข่าวประชาสัมพันธ์ที่เลือกใช้หรือไม่ ?')); ?>&nbsp;
    </div>
  </div>
</div>

<?php
  $this->endWidget();
?>

<?php
  Dialog::alertMessage();
?>
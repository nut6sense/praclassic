<?php
  $form = $this->beginWidget('CActiveForm', array(
    'id' => 'data-shop-form',
    // 'action' => Yii::app()->request->baseUrl . '/shop/delShop',
    'htmlOptions' => array('class' => 'form-horizontal')
  ));

  $divOption = 'text-align:center;';
?>

<div class="container-fluid">

  <div class="row-fluid">
    <div class="span12"><h1>ข่าวประชาสัมพันธ์</h1></div>
  </div>

  <div class="row-fluid">
    <div class="span12" style="<?php echo $divOption; ?>">
      <h4><?php echo $newsModel['newsname']; ?></h4>
    </div>
  </div>

  <div class="row-fluid">
    <div class="span12" style="<?php echo $divOption; ?>">
      <?php 
        echo CHtml::image(Yii::app()->request->baseUrl.'/img/newsPic/'.$newsModel['newspic'], null, array('class'=>'newsPic'));
      ?>
    </div>
  </div>

  <div class="row-fluid">
    <div class="span12" style="<?php echo $divOption; ?>">
      <?php echo $newsModel['newsdetail']; ?>
    </div>
  </div>

  
</div>
	
<?php
	$this->endWidget();
?>
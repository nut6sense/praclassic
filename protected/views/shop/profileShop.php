<?php
  $form = $this->beginWidget('CActiveForm', array(
      'id' => 'profile-shop-form',
      'action' => Yii::app()->request->baseUrl . '/shop/saveProfileShop',
      'htmlOptions' => array('enctype' => 'multipart/form-data')
          ));

  $LabelInputOption = array('class'=>'span4');
  $inputOption = array('class'=>'span8');
?>

<div class="container-fluid">
  <div class="row-fluid">
    <div class="span12"><h1>แก้ไขข้อมูลร้านค้า</h1></div>
  </div>
  <div class="row-fluid">
    <div class="span12">
      <?php echo $form->labelEx($model_user, 'title_username', $LabelInputOption); ?>
      <?php echo $form->textField($model_user, 'username', $inputOption + array('readonly'=>true)); ?>
    </div>
  </div>
  <div class="row-fluid">
    <div class="span12">
      <?php echo $form->labelEx($model_user, 'title_password', $LabelInputOption); ?>
      <?php echo $form->PasswordField($model_user, 'password', $inputOption); ?>
    </div>
  </div>
  <div class="row-fluid">
    <div class="span12">
      <?php echo $form->labelEx($model_user, 'title_confirmPassword', $LabelInputOption); ?>
      <?php echo $form->PasswordField($model_user, 'confirmPassword', $inputOption); ?>
    </div>
  </div>
  <hr/>
  <div class="row-fluid">
    <div class="span12">
      <?php echo $form->labelEx($model_user, 'title_name', $LabelInputOption); ?>
      <?php echo $form->textField($model_user, 'name', $inputOption+array('readonly'=>true)); ?>
    </div>
  </div>
  <div class="row-fluid">
    <div class="span12">
      <?php echo $form->labelEx($model_user, 'title_surname', $LabelInputOption); ?>
      <?php echo $form->textField($model_user, 'surname', $inputOption+array('readonly'=>true)); ?>
    </div>
  </div>
  <div class="row-fluid">
    <div class="span12">
      <?php echo $form->labelEx($model_user, 'title_iden_id', $LabelInputOption); ?>
      <?php echo $form->textField($model_user, 'iden_id', $inputOption+array('readonly'=>true)); ?>
    </div>
  </div>
  <div class="row-fluid">
    <div class="span12">
      <?php echo $form->labelEx($model_user, 'title_phonenumber', $LabelInputOption); ?>
      <?php echo $form->textField($model_user, 'phonenumber', array('onkeyup'=>'autoFormat(this, "telephone")', 'onKeyPress'=>'chkNumber()')+$inputOption); ?>
    </div>
  </div>
  <div class="row-fluid">
    <div class="span12">
      <?php echo $form->labelEx($model_user, 'title_email', $LabelInputOption); ?>
      <?php echo $form->textField($model_user, 'email', $inputOption); ?>
    </div>
  </div>
  <div class="row-fluid">
    <div class="span12">
      <?php echo $form->labelEx($model_shop, 'shotName', $LabelInputOption); ?>
      <?php echo $form->textField($model_shop, 'shotName', $inputOption+array('maxlength'=>25,'placeholder'=>'กรอกตัวอักษรได้ไม่เกิน 25 ตัว')); ?>
    </div>
  </div>
  <div class="row-fluid">
    <div class="span12">
      <?php echo $form->labelEx($model_shop, 'fullName', $LabelInputOption); ?>
      <?php echo $form->textField($model_shop, 'fullName', $inputOption); ?>
    </div>
  </div>
  <div class="row-fluid">
    <div class="span12">
      <?php echo $form->labelEx($model_shop, 'title_map', $LabelInputOption); ?>
      <?php echo $form->textArea($model_shop, 'map', $inputOption); ?>
    </div>
  </div>
  <div class="row-fluid">
    <div class="span12">
      <?php echo $form->labelEx($model_shop, 'title_detail', $LabelInputOption); ?>
      <?php echo $form->textArea($model_shop, 'detail', $inputOption); ?>
    </div>
  </div>
  <div class="row-fluid">
    <div class="span12">
      <?php echo $form->labelEx($model_shop, 'title_rule', $LabelInputOption); ?>
      <?php echo $form->textArea($model_shop, 'rule', $inputOption); ?>
    </div>
  </div>
  <div class="row-fluid">
    <div class="span12">
      <?php echo $form->labelEx($model_shop, 'title_referrance', $LabelInputOption); ?>
      <?php echo $form->textField($model_shop, 'referrance', $inputOption+array('readonly'=>true)); ?>
    </div>
  </div>
  <div class="row-fluid">
    <div class="span12">
      <?php echo $form->labelEx($model_shop, 'title_referrance_phonenumber', $LabelInputOption); ?>
      <?php echo $form->textField($model_shop, 'referrance_phonenumber', $inputOption+array('readonly'=>true)); ?>
    </div>
  </div>
  <div class="row-fluid">
    <div class="span12">
      <?php echo $form->labelEx($model_bank, 'title_bank_name', $LabelInputOption); ?>
      <?php echo $form->DropDownList($model_bank, 'bank_name', Banks::getBank(), $inputOption); ?>
    </div>
  </div>
  <div class="row-fluid">
    <div class="span12">
      <?php echo $form->labelEx($model_bank, 'title_bank_type', $LabelInputOption); ?>
      <?php echo $form->DropDownList($model_bank, 'bank_type', Banks::getBankType(), $inputOption); ?>
    </div>
  </div>
  <div class="row-fluid">
    <div class="span12">
      <?php echo $form->labelEx($model_bank, 'bank_Branch', $LabelInputOption); ?>
      <?php echo $form->textField($model_bank, 'bank_Branch', $inputOption); ?>
    </div>
  </div>
  <div class="row-fluid">
    <div class="span12">
      <?php echo $form->labelEx($model_bank, 'title_account_name', $LabelInputOption); ?>
      <?php echo $form->textField($model_bank, 'account_name', $inputOption); ?>
    </div>
  </div>
  <div class="row-fluid">
    <div class="span12">
      <?php echo $form->labelEx($model_bank, 'title_account_number', $LabelInputOption); ?>
      <?php echo $form->textField($model_bank, 'account_number', array('onkeyup'=>'autoFormat(this, "book_bank")', 'onKeyPress'=>'chkNumber()', 'maxlength'=>10)+$inputOption); ?>
    </div>
  </div>
  <div class="row-fluid">
    <div class="span3">
      <?php 
        $picPath = !empty($model_shop->shop_pic)?Yii::app()->request->baseUrl.'/img/shopPic/'.$model_shop->shop_pic:Yii::app()->request->baseUrl.'/img/shopPic/none.jpg';
        echo CHtml::image($picPath, null, array('id'=>'Shops_shop_pic', 'style'=>'width:220px;')); 
      ?>
    </div>
    <div class="span9">
      <?php echo $form->FileField($model_shop, 'shop_pic', $inputOption); ?>
    </div>
  </div>
  <div class="row-fluid">
    <div class="span12 row-fluid field-submit">
      <?php echo $form->hiddenField($model_user, 'user_id'); ?>
      <?php echo CHtml::submitButton('แก้ไขข้อมูล', array('id' => 'btn-add', 'class' => 'btn btn-info')); ?>
    </div>
  </div>
</div>
<?php
  $this->endWidget();
  Dialog::alertMessage();
?>
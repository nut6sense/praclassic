<?php
  $form = $this->beginWidget('CActiveForm', array(
      'id' => 'all-shop-form',
      'action' => Yii::app()->request->baseUrl . '/shop/delShop',
      'htmlOptions' => array('class' => 'form-horizontal')
          ));
?>

<div class="container-fluid">
  <div class="row-fluid headerForm sizeContent">
    <div class="span12">
      <h1>รายชื่ออีเมล์</h1>
    </div>
  </div>

  <div class="row-fluid bodyForm">
    <div class="span12">
      <table class="table" id="category-detail-table">
        <thead>
          <tr>
            <th>ลำดับ</th>
            <th>ชื่ออีเมล์</th>
          </tr>
        </thead>
        <tbody>
          <?php 
            echo Shops::tblMailMember(); 
          ?>
        </tbody>
      </table>
    </div>
  </div>

  <div class="row-fluid field-submit">
    <div class="span12">
      <?php //echo CHtml::hiddenField('shopType', 'allShop'); ?>
      <?php //echo CHtml::submitButton('ลบร้านค้าที่เลือก', array('id' => 'btn-delete', 'class' => 'btn btn-danger',  'confirm'=>'ต้องการลบข้อมูลร้านค้าที่เลือกใช้หรือไม่ ?')); ?>&nbsp;
    </div>
  </div>
</div>

<?php
  $this->endWidget();
?>

<?php
  Dialog::alertMessage();
?>
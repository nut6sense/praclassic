<?php
  $form = $this->beginWidget('CActiveForm', array(
    'id' => 'shop-detail-form',
    'action' => Yii::app()->request->baseUrl . '/shop/editShop',
  ));
  $model_user = new Users;
  $model_shop = new Shops;
  $model_bank = new Banks;

  $pictureMarin = 'style="margin-bottom:5px;"';
  $inputOption = array('class'=>'span6');
  $htmlOptions = 'class="span3", style="margin-left:65px;"';
?>
<div class="container-fluid">
  <div class="row-fluid">
    <div class="span12"><h1>แก้ไขข้อมูลร้านค้า</h1></div>
  </div>

  <div class="row-fluid">
    <div <?php echo $htmlOptions; ?> >
      <?php echo $form->labelEx($model_user, 'title_username'); ?>
    </div>
    <div>
      <?php echo $form->textField($model_user, 'username', array('value'=>$dataShop['username'])+$inputOption); ?>
    </div>
  </div> 

  <div class="row-fluid">
    <div <?php echo $htmlOptions; ?> >
      <?php echo $form->labelEx($model_user, 'title_password'); ?>
    </div>
    <div>
      <?php echo $form->PasswordField($model_user, 'password', $inputOption); ?>
    </div>
  </div>

  <div class="row-fluid">
    <div class="span">
      <label style="font-weight:bold;">ข้อมูลประวัติส่วนตัว</label>
    </div>
  </div>

  <div class="row-fluid">
    <div <?php echo $htmlOptions; ?> >
      <?php echo $form->labelEx($model_user, 'title_name'); ?>
    </div>
    <div>
      <?php echo $form->textField($model_user, 'name', array('value'=>$dataShop['firstname'])+$inputOption); ?>
    </div>
  </div>

  <div class="row-fluid">
    <div <?php echo $htmlOptions; ?> >
      <?php echo $form->labelEx($model_user, 'title_surname'); ?>
    </div>
    <div>
      <?php echo $form->textField($model_user, 'surname', array('value'=>$dataShop['surname'])+$inputOption); ?>
    </div>
  </div>

  <div class="row-fluid">
    <div <?php echo $htmlOptions; ?> >
      <?php echo $form->labelEx($model_user, 'title_phonenumber'); ?>
    </div>
    <div>
      <?php echo $form->textField($model_user, 'phonenumber', array('value'=>$dataShop['phonenumber'], 'onkeyup'=>'autoFormat(this, "telephone")', 'onKeyPress'=>'chkNumber()')+$inputOption); ?>
    </div>
  </div>

  <div class="row-fluid">
    <div <?php echo $htmlOptions; ?> >
      <?php echo $form->labelEx($model_user, 'title_email'); ?>
    </div>
    <div>
      <?php echo $form->textField($model_user, 'email', array('value'=>$dataShop['email'])+$inputOption); ?>
    </div>
  </div>

  <div class="row-fluid">
    <div <?php echo $htmlOptions; ?> >
      <?php echo $form->labelEx($model_user, 'title_iden_id'); ?>
    </div>
    <div>
      <?php echo $form->textField($model_user, 'iden_id', array('value'=>$dataShop['iden_id'], 'onkeyup'=>'autoFormat(this, "iden")', 'onKeyPress'=>'chkNumber()')+$inputOption); ?>
    </div>
  </div>

  <div class="row-fluid">
    <div <?php echo $htmlOptions; ?> >
      <?php echo $form->labelEx($model_user, 'tile_iden_picture'); ?>
    </div>
    <div>
      <?php echo Highslide::PicHighslide(Yii::app()->baseUrl.'/img/iden/'.$dataShop['iden_picture'].'.jpg',220); ?>
    </div>
  </div>

  <div class="row-fluid">
    <div class="span">
      <label style="font-weight:bold;">รายละเอียดร้านค้า</label>
    </div>
  </div>

  <div class="row-fluid">
    <div <?php echo $htmlOptions; ?> >
      <?php echo $form->labelEx($model_shop, 'title_shotName'); ?>
    </div>
    <div>
      <?php echo $form->textField($model_shop, 'shotName', array('value'=>$dataShop['shotName'], 'maxlength'=>25, 'placeholder'=>"กรอกตัวอักษรได้ไม่เกิน 25 ตัว")+$inputOption); ?>
    </div>
  </div>

  <div class="row-fluid">
    <div <?php echo $htmlOptions; ?> >
      <?php echo $form->labelEx($model_shop, 'title_fullName'); ?>
    </div>
    <div>
      <?php echo $form->textField($model_shop, 'fullName', array('value'=>$dataShop['fullName'])+$inputOption); ?>
    </div>
  </div>

  <div class="row-fluid">
    <div <?php echo $htmlOptions; ?> >
      <?php echo $form->labelEx($model_shop, 'title_detail'); ?>
    </div>
    <div>
      <?php echo $form->TextArea($model_shop, 'detail', array('value'=>$dataShop['detail'])+$inputOption); ?>
    </div>
  </div>

  <div class="row-fluid">
    <div <?php echo $htmlOptions; ?> >
      <?php echo $form->labelEx($model_shop, 'title_referrance'); ?>
    </div>
    <div>
      <?php echo $form->textField($model_shop, 'referrance', array('value'=>$dataShop['referrance'])+$inputOption); ?>
    </div>
  </div>

  <div class="row-fluid">
    <div <?php echo $htmlOptions; ?> >
      <?php echo $form->labelEx($model_shop, 'title_referrance_phonenumber'); ?>
    </div>
    <div>
      <?php echo $form->textField($model_shop, 'referrance_phonenumber', array('value'=>$dataShop['referrance_phonenumber'], 'onkeyup'=>'autoFormat(this, "telephone")', 'onKeyPress'=>'chkNumber()')+$inputOption); ?>
    </div>
  </div>

  <div class="row-fluid">
    <div class="span">
      <label style="font-weight:bold;">ข้อมูลบัญชีธนาคาร</label>
    </div>
  </div>

  <div class="row-fluid">
    <div <?php echo $htmlOptions; ?> >
      <?php echo $form->labelEx($model_bank, 'title_bank_name'); ?>
    </div>
    <div>
      <?php echo $form->DropDownList($model_bank, 'bank_name', Banks::getBank(), array('options'=>array($dataShop['bank_name']=>array('selected'=>true)))+$inputOption); ?>
    </div>
  </div>

  <div class="row-fluid">
    <div <?php echo $htmlOptions; ?> >
      <?php echo $form->labelEx($model_bank, 'title_bank_type'); ?>
    </div>
    <div>
      <?php echo $form->DropDownList($model_bank, 'bank_type', Banks::getBankType(), array('options'=>array($dataShop['bank_type']=>array('selected'=>true)))+$inputOption); ?>
    </div>
  </div>

  <div class="row-fluid">
    <div <?php echo $htmlOptions; ?> >
      <?php echo $form->labelEx($model_bank, 'bank_Branch'); ?>
    </div>
    <div>
      <?php echo $form->textField($model_bank, 'bank_Branch', array('value'=>$dataShop['bank_Branch'])+$inputOption); ?>
    </div>
  </div>

  <div class="row-fluid">
    <div <?php echo $htmlOptions; ?> >
      <?php echo $form->labelEx($model_bank, 'title_account_name'); ?>
    </div>
    <div>
      <?php echo $form->textField($model_bank, 'account_name', array('value'=>$dataShop['account_name'])+$inputOption); ?>
    </div>
  </div>

  <div class="row-fluid">
    <div <?php echo $htmlOptions; ?> >
      <?php echo $form->labelEx($model_bank, 'title_account_number'); ?>
    </div>
    <div>
      <?php echo $form->textField($model_bank, 'account_number', array('onkeyup'=>'autoFormat(this, "book_bank")', 'onKeyPress'=>'chkNumber()', 'maxlength'=>10, 'value'=>$dataShop['account_number'])+$inputOption); ?>
    </div>
  </div>

  <div class="row-fluid">
    <div class="span">
      <label style="font-weight:bold;">รูปเพื่อให้ชมรมพิจารณา</label>
    </div>
  </div>

  <div class="row-fluid" <?php echo $pictureMarin; ?> >
    <div <?php echo $htmlOptions; ?> >
      <label>รูปที่ 1 ด้านหน้า</label>
    </div>
    <div>
      <?php 
        echo !is_null($dataShop['sample'][1]['picName']['front'])?Highslide::PicHighslide(Yii::app()->baseUrl.'/img/samplePic/'.$dataShop['sample'][1]['picName']['front'],220):'ไม่มีรูปด้านหน้า'; 
      ?>
    </div>
  </div>

  <div class="row-fluid" <?php echo $pictureMarin; ?> >
    <div <?php echo $htmlOptions; ?> >
      <label>รูปที่ 1 ด้านหลัง</label>
    </div>
    <div>
      <?php 
        echo !is_null($dataShop['sample'][1]['picName']['back'])?Highslide::PicHighslide(Yii::app()->baseUrl.'/img/samplePic/'.$dataShop['sample'][1]['picName']['back'],220):'ไม่มีรูปด้านหลัง'; 
      ?>
    </div>
  </div>

  <div class="row-fluid" <?php echo $pictureMarin; ?> >
    <div <?php echo $htmlOptions; ?> >
      <label>รูปที่ 1 ฐาน</label>
    </div>
    <div>
      <?php 
        echo !is_null($dataShop['sample'][1]['picName']['base'])?Highslide::PicHighslide(Yii::app()->baseUrl.'/img/samplePic/'.$dataShop['sample'][1]['picName']['base'],220):'ไม่มีรูปด้านฐาน'; 
      ?>
    </div>
  </div>

  <div class="row-fluid">
    <div <?php echo $htmlOptions; ?> >
      <label>รายละเอียดรูปที่ 1</label>
    </div>
    <div>
      <label><?php echo $dataShop['sample'][1]['detail']; ?></label>
    </div>
  </div>

  <div class="row-fluid" <?php echo $pictureMarin; ?> >
    <div <?php echo $htmlOptions; ?> >
      <label>รูปที่ 2 ด้านหน้า</label>
    </div>
    <div>
      <?php 
        echo !is_null($dataShop['sample'][2]['picName']['front'])?Highslide::PicHighslide(Yii::app()->baseUrl.'/img/samplePic/'.$dataShop['sample'][2]['picName']['front'],220):'ไม่มีรูปด้านหน้า'; 
      ?>
    </div>
  </div>

  <div class="row-fluid" <?php echo $pictureMarin; ?> >
    <div <?php echo $htmlOptions; ?> >
      <label>รูปที่ 2 ด้านหลัง</label>
    </div>
    <div>
      <?php 
        echo !is_null($dataShop['sample'][2]['picName']['back'])?Highslide::PicHighslide(Yii::app()->baseUrl.'/img/samplePic/'.$dataShop['sample'][2]['picName']['back'],220):'ไม่มีรูปด้านหลัง'; 
      ?>
    </div>
  </div>

  <div class="row-fluid" <?php echo $pictureMarin; ?> >
    <div <?php echo $htmlOptions; ?> >
      <label>รูปที่ 2 ฐาน</label>
    </div>
    <div>
      <?php 
        echo !is_null($dataShop['sample'][2]['picName']['base'])?Highslide::PicHighslide(Yii::app()->baseUrl.'/img/samplePic/'.$dataShop['sample'][2]['picName']['base'],220):'ไม่มีรูปด้านฐาน'; 
      ?>
    </div>
  </div>

  <div class="row-fluid">
    <div <?php echo $htmlOptions; ?> >
      <label>รายละเอียดรูปที่ 2</label>
    </div>
    <div>
      <label><?php echo $dataShop['sample'][2]['detail']; ?></label>
    </div>
  </div>

  <div class="row-fluid" <?php echo $pictureMarin; ?> >
    <div <?php echo $htmlOptions; ?> >
      <label>รูปที่ 3 ด้านหน้า</label>
    </div>
    <div>
      <?php 
        echo !is_null($dataShop['sample'][3]['picName']['front'])?Highslide::PicHighslide(Yii::app()->baseUrl.'/img/samplePic/'.$dataShop['sample'][3]['picName']['front'],220):'ไม่มีรูปด้านหน้า'; 
      ?>
    </div>
  </div>

  <div class="row-fluid" <?php echo $pictureMarin; ?> >
    <div <?php echo $htmlOptions; ?> >
      <label>รูปที่ 3 ด้านหลัง</label>
    </div>
    <div>
      <?php 
        echo !is_null($dataShop['sample'][3]['picName']['back'])?Highslide::PicHighslide(Yii::app()->baseUrl.'/img/samplePic/'.$dataShop['sample'][3]['picName']['back'],220):'ไม่มีรูปด้านหลัง'; 
      ?>
    </div>
  </div>

  <div class="row-fluid" <?php echo $pictureMarin; ?> >
    <div <?php echo $htmlOptions; ?> >
      <label>รูปที่ 3 ฐาน</label>
    </div>
    <div>
      <?php 
        echo !is_null($dataShop['sample'][3]['picName']['base'])?Highslide::PicHighslide(Yii::app()->baseUrl.'/img/samplePic/'.$dataShop['sample'][3]['picName']['base'],220):'ไม่มีรูปด้านฐาน'; 
      ?>
    </div>
  </div>

  <div class="row-fluid">
    <div <?php echo $htmlOptions; ?> >
      <label>รายละเอียดรูปที่ 3</label>
    </div>
    <div>
      <label><?php echo $dataShop['sample'][3]['detail']; ?></label>
    </div>
  </div>

  <div class="row-fluid" <?php echo $pictureMarin; ?> >
    <div <?php echo $htmlOptions; ?> >
      <label>รูปที่ 4 ด้านหน้า</label>
    </div>
    <div>
      <?php 
        echo !is_null($dataShop['sample'][4]['picName']['front'])?Highslide::PicHighslide(Yii::app()->baseUrl.'/img/samplePic/'.$dataShop['sample'][4]['picName']['front'],220):'ไม่มีรูปด้านหน้า'; 
      ?>
    </div>
  </div>

  <div class="row-fluid" <?php echo $pictureMarin; ?> >
    <div <?php echo $htmlOptions; ?> >
      <label>รูปที่ 4 ด้านหลัง</label>
    </div>
    <div>
      <?php 
        echo !is_null($dataShop['sample'][4]['picName']['back'])?Highslide::PicHighslide(Yii::app()->baseUrl.'/img/samplePic/'.$dataShop['sample'][4]['picName']['back'],220):'ไม่มีรูปด้านหลัง'; 
      ?>
    </div>
  </div>

  <div class="row-fluid" <?php echo $pictureMarin; ?> >
    <div <?php echo $htmlOptions; ?> >
      <label>รูปที่ 4 ฐาน</label>
    </div>
    <div>
      <?php 
        echo !is_null($dataShop['sample'][4]['picName']['base'])?Highslide::PicHighslide(Yii::app()->baseUrl.'/img/samplePic/'.$dataShop['sample'][4]['picName']['base'],220):'ไม่มีรูปด้านฐาน'; 
      ?>
    </div>
  </div>

  <div class="row-fluid">
    <div <?php echo $htmlOptions; ?> >
      <label>รายละเอียดรูปที่ 4</label>
    </div>
    <div>
      <label><?php echo $dataShop['sample'][4]['detail']; ?></label>
    </div>
  </div>

  <div class="row-fluid" <?php echo $pictureMarin; ?> >
    <div <?php echo $htmlOptions; ?> >
      <label>รูปที่ 5 ด้านหน้า</label>
    </div>
    <div>
      <?php 
        echo !is_null($dataShop['sample'][5]['picName']['front'])?Highslide::PicHighslide(Yii::app()->baseUrl.'/img/samplePic/'.$dataShop['sample'][5]['picName']['front'],220):'ไม่มีรูปด้านหน้า'; 
      ?>
    </div>
  </div>

  <div class="row-fluid" <?php echo $pictureMarin; ?> >
    <div <?php echo $htmlOptions; ?> >
      <label>รูปที่ 5 ด้านหลัง</label>
    </div>
    <div>
      <?php 
        echo !is_null($dataShop['sample'][5]['picName']['back'])?Highslide::PicHighslide(Yii::app()->baseUrl.'/img/samplePic/'.$dataShop['sample'][5]['picName']['back'],220):'ไม่มีรูปด้านหลัง'; 
      ?>
    </div>
  </div>

  <div class="row-fluid" <?php echo $pictureMarin; ?> >
    <div <?php echo $htmlOptions; ?> >
      <label>รูปที่ 5 ฐาน</label>
    </div>
    <div>
      <?php 
        echo !is_null($dataShop['sample'][5]['picName']['base'])?Highslide::PicHighslide(Yii::app()->baseUrl.'/img/samplePic/'.$dataShop['sample'][5]['picName']['base'],220):'ไม่มีรูปด้านฐาน'; 
      ?>
    </div>
  </div>

  <div class="row-fluid">
    <div <?php echo $htmlOptions; ?> >
      <label>รายละเอียดรูปที่ 5</label>
    </div>
    <div>
      <label><?php echo $dataShop['sample'][5]['detail']; ?></label>
    </div>
  </div>

  <div class="row-fluid" <?php echo $pictureMarin; ?> >
    <div <?php echo $htmlOptions; ?> >
      <label>รูปที่ 6 ด้านหน้า</label>
    </div>
    <div>
      <?php 
        echo !is_null($dataShop['sample'][6]['picName']['front'])?Highslide::PicHighslide(Yii::app()->baseUrl.'/img/samplePic/'.$dataShop['sample'][6]['picName']['front'],220):'ไม่มีรูปด้านหน้า'; 
      ?>
    </div>
  </div>

  <div class="row-fluid" <?php echo $pictureMarin; ?> >
    <div <?php echo $htmlOptions; ?> >
      <label>รูปที่ 6 ด้านหลัง</label>
    </div>
    <div>
      <?php 
        echo !is_null($dataShop['sample'][6]['picName']['back'])?Highslide::PicHighslide(Yii::app()->baseUrl.'/img/samplePic/'.$dataShop['sample'][6]['picName']['back'],220):'ไม่มีรูปด้านหลัง'; 
      ?>
    </div>
  </div>

  <div class="row-fluid" <?php echo $pictureMarin; ?> >
    <div <?php echo $htmlOptions; ?> >
      <label>รูปที่ 6 ฐาน</label>
    </div>
    <div>
      <?php 
        echo !is_null($dataShop['sample'][6]['picName']['base'])?Highslide::PicHighslide(Yii::app()->baseUrl.'/img/samplePic/'.$dataShop['sample'][6]['picName']['base'],220):'ไม่มีรูปด้านฐาน'; 
      ?>
    </div>
  </div>

  <div class="row-fluid">
    <div <?php echo $htmlOptions; ?> >
      <label>รายละเอียดรูปที่ 6</label>
    </div>
    <div>
      <label><?php echo $dataShop['sample'][6]['detail']; ?></label>
    </div>
  </div>
<?php
  if($formPage!='newShop') {
?>
  <div class="row-fluid">
    <div class="span">
      <label style="font-weight:bold;">ข้อมูลการระงับการใช้งาน</label>
    </div>
  </div>

  <div class="row-fluid">
    <div <?php echo $htmlOptions; ?> >
      <?php echo $form->labelEx($model_user, 'active'); ?>
    </div>
    <div>
      <label><?php echo ($dataShop['active']=='1'?'ปกติ':($dataShop['active']=='2'?'หมดอายุ':'ถูกระงับการใช้งาน')); ?></label>
    </div>
  </div>

  <div id="block-detail" class="row-fluid">

    <div class="row-fluid">
      <div <?php echo $htmlOptions; ?> >
        <?php echo $form->labelEx($model_user, 'block_user'); ?>
      </div>
      <div>
        <?php echo $form->DropDownList($model_user, 'block_user', Users::blockList(), $inputOption); ?>
      </div>
    </div>

    <div class="row-fluid">
      <div <?php echo $htmlOptions; ?> >
        <?php echo $form->labelEx($model_user, 'block_day'); ?>
      </div>
      <div>
        <?php echo $form->textField($model_user, 'block_day', $inputOption+array('readonly'=>true)); ?>
      </div>
    </div>

  </div>
<?php
  }
  if($formPage=='allShop') {
  // if($formPage=='hide') {
?>
  <div class="row-fluid">
    <div class="span">
      <label style="font-weight:bold;">ข้อมูลรหัสร้านค้า</label>
    </div>
  </div>

  <div class="row-fluid">
    <div <?php echo $htmlOptions; ?> >
      <label>รหัสร้านค้าปัจจุบัน</label>
    </div>
    <div>
      <label><?php echo $dataShop['sequence']; ?></label>
    </div>
  </div>

  <div class="row-fluid">
    <div <?php echo $htmlOptions; ?> >
      <label>รหัสร้านค้าที่ว่างอยู่</label>
    </div>
    <div>
      <?php echo $form->DropDownList($model_user, 'sequence', Users::blankSequence(), $inputOption); ?>
    </div>
  </div>
<?php
  }
?>
  <div class="row-fluid field-submit">
    <div class="span12">
      <?php echo $form->hiddenField($model_user, 'user_id', array('value'=>Yii::app()->input->get('param1'))); ?>
      <?php echo CHtml::hiddenField('formPage', $formPage); ?>
      <?php echo CHtml::link('ย้อนกลับ', '../../'.$formPage, array('id' => 'btn-add', 'class' => 'btn btn-success btnLink')); ?>&nbsp;
      <?php echo CHtml::submitButton('บันทึกข้อมูล', array('id' => 'btn-add', 'class' => 'btn btn-info')); ?>
    </div>
  </div>

</div>
	
<?php
	$this->endWidget();

  Dialog::alertMessage();
?>
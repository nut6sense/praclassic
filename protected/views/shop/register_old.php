<?php
  $form = $this->beginWidget('CActiveForm', array(
    'id' => 'regis-form',
    'action' => Yii::app()->request->baseUrl . '/shop/addUser',
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
        ));

  $userModel = new Users;
  $shopModel = new Shops;
  $bankModel = new Banks;
  $PaymentModel = new Payment;

  $inputOption = array('class'=>'span3');
  $DetailOption = array('class'=>'span3','style'=>'height: 100px;','placeholder'=>'รายละเอียด');


?>
  <div class="headerForm">
    <div>ใบสมัครสมาชิก <sub>( กรุณาทำตามขั้นตอนการสมัครและกรอกข้อมูลให้ครบทุกช่องตรงตามความเป็นจริง )<sub></div>
  </div>
  <div class="span12 bodyForm">

    <div class="row">
      <h4>ข้อมูลผู้ใช้งาน</h4>
    </div>
    <div class="row">
      <div class="span2">
        <?php echo $form->labelEx($userModel, 'username', $inputOption); ?>
      </div>
      <div class="span3">
        <?php echo $form->textField($userModel, 'username', $inputOption); ?>
      </div>
    </div>

    <div class="row">
      <div class="span2">
        <?php echo $form->labelEx($userModel, 'password', $inputOption); ?>
      </div>
      <div class="span3">
        <?php echo $form->PasswordField($userModel, 'password', $inputOption); ?>
      </div>
    </div>

    <div class="row">
      <div class="span2">
        <?php echo $form->labelEx($userModel, 'confirmPassword', $inputOption); ?>
      </div>
      <div class="span3">
        <?php echo $form->PasswordField($userModel, 'confirmPassword', $inputOption); ?>
      </div>
    </div>

    <div class="row">
      <h4>ข้อมูลประวัติส่วนตัว</h4>
    </div>
    <div class="row">
      <div class="span2">
        <?php echo $form->labelEx($userModel, 'name', $inputOption); ?>
      </div>
      <div class="span3">
        <?php echo $form->textField($userModel, 'name', $inputOption); ?>
      </div>
      <div class="span2">
        <?php echo $form->labelEx($userModel, 'surname', $inputOption); ?>
      </div>
      <div class="span3">
        <?php echo $form->textField($userModel, 'surname', $inputOption); ?>
      </div>
    </div>

    <div class="row">
      <div class="span2">
        <?php echo $form->labelEx($userModel, 'phonenumber', $inputOption); ?>
      </div>
      <div class="span3">
        <?php echo $form->textField($userModel, 'phonenumber', $inputOption); ?>
      </div>
      <div class="span2">
        <?php echo $form->labelEx($userModel, 'email', $inputOption); ?>
      </div>
      <div class="span3">
        <?php echo $form->textField($userModel, 'email', $inputOption); ?>
      </div>
    </div>

    <div class="row">
      <div class="span2">
        <?php echo $form->labelEx($userModel, 'iden_id', $inputOption); ?>
      </div>
      <div class="span3">
        <?php echo $form->textField($userModel, 'iden_id', $inputOption); ?>
      </div>
      <div class="span2">
        <?php echo $form->labelEx($userModel, 'iden_picture', $inputOption); ?>
      </div>
      <div class="span3">
        <?php echo $form->FileField($userModel, 'iden_picture', $inputOption); ?>
      </div>
    </div>

    <div class="row">
      <h4>รายละเอียดร้านค้า</h4>
    </div>
    <div class="row">
      <div class="span2">
        <?php echo $form->labelEx($shopModel, 'name', $inputOption); ?>
      </div>
      <div class="span3">
        <?php echo $form->textField($shopModel, 'name', $inputOption); ?>
      </div>
    </div>

    <div class="row">
      <div class="span2">
        <?php echo $form->labelEx($shopModel, 'map', $inputOption); ?>
      </div>
      <div class="span3">
        <?php echo $form->textField($shopModel, 'map', $inputOption); ?>
      </div>
    </div>

    <div class="row">
      <div class="span2">
        <?php echo $form->labelEx($shopModel, 'detail', $inputOption); ?>
      </div>
      <div class="span3">
        <?php echo $form->textField($shopModel, 'detail', $inputOption); ?>
      </div>
    </div>

    <div class="row">
      <div class="span2">
        <?php echo $form->labelEx($shopModel, 'rule', $inputOption); ?>
      </div>
      <div class="span3">
        <?php echo $form->textField($shopModel, 'rule', $inputOption); ?>
      </div>
    </div>

    <div class="row">
      <div class="span2">
        <?php echo $form->labelEx($shopModel, 'referrance', $inputOption); ?>
      </div>
      <div class="span3">
        <?php echo $form->textField($shopModel, 'referrance', $inputOption); ?>
      </div>
    </div>

    <div class="row">
      <div class="span2">
        <?php echo $form->labelEx($shopModel, 'referrance_phonenumber', $inputOption); ?>
      </div>
      <div class="span3">
        <?php echo $form->textField($shopModel, 'referrance_phonenumber', $inputOption); ?>
      </div>
    </div>

    <div class="row">
      <h4>ข้อมูลบัญชีธนาคาร</h4>
    </div>
    <div class="row">
      <div class="span2">
        <?php echo $form->labelEx($bankModel, 'bank_name', $inputOption); ?>
      </div>
      <div class="span3">
        <?php  echo $form->DropDownList($bankModel, 'bank_name', Banks::getBank(), $inputOption); ?>
      </div>
    </div>
    <div class="row">
      <div class="span2">
        <?php echo $form->labelEx($bankModel, 'bank_type', $inputOption); ?>
      </div>
      <div class="span3">
        <?php  echo $form->DropDownList($bankModel, 'bank_type', Banks::getBankType(), $inputOption); ?>
      </div>
    </div>
    <div class="row">
      <div class="span2">
        <?php echo $form->labelEx($bankModel, 'bank_Branch', $inputOption); ?>
      </div>
      <div class="span3">
        <?php echo $form->textField($bankModel, 'bank_Branch', $inputOption); ?>
      </div>
    </div>
    <div class="row">
      <div class="span2">
        <?php echo $form->labelEx($bankModel, 'account_name', $inputOption); ?>
      </div>
      <div class="span3">
        <?php echo $form->textField($bankModel, 'account_name', $inputOption); ?>
      </div>
    </div>
    <div class="row">
      <div class="span2">
        <?php echo $form->labelEx($bankModel, 'account_number', $inputOption); ?>
      </div>
      <div class="span3">
        <?php echo $form->textField($bankModel, 'account_number', $inputOption); ?>
      </div>
    </div>

    <div class="row">
      <h4>การจ่ายเงิน</h4>
    </div>
    <div class="row">
      <div class="span2">
        <?php echo $form->labelEx($PaymentModel, 'pay_type', $inputOption); ?>
      </div>
      <div class="span3">
        <?php  echo $form->DropDownList($PaymentModel, 'pay_type', Payment::getPaymentType(), $inputOption); ?>
      </div>
    </div>

    <div class="row">
      <h4>ส่งรูปเพื่อให้ชมรมพิจารณา</h4>
    </div>
    <div class="row" style="margin-bottom: 20px;">
      <sub>( ภาพพระที่ส่งจะต้องมีรูปด้านหน้า, ด้านหลัง, ด้านข้างหรือก้้น )</sub>
    </div>
    <div class="row">
      <div class="span2">
        <label>พระองค์ที่ 1 : </label>
      </div>
      <div class="span3">
        <?php echo $form->FileField($shopModel, 'sample_pic[1][picName]', $inputOption); ?>
        <?php echo $form->TextArea($shopModel, 'sample_pic[1][detail]', $DetailOption); ?>
      </div>
      <div class="span2">
        <label>พระองค์ที่ 2 : </label>
      </div>
      <div class="span3">
        <?php echo $form->FileField($shopModel, 'sample_pic[2][picName]', $inputOption); ?>
        <?php echo $form->TextArea($shopModel, 'sample_pic[2][detail]', $DetailOption); ?>
      </div>
    </div>

    <div class="row">
      <div class="span2">
        <label>พระองค์ที่ 3 : </label>
      </div>
      <div class="span3">
        <?php echo $form->FileField($shopModel, 'sample_pic[3][picName]', $inputOption); ?>
        <?php echo $form->TextArea($shopModel, 'sample_pic[3][detail]', $DetailOption); ?>
      </div>
      <div class="span2">
        <label>พระองค์ที่ 4 : </label>
      </div>
      <div class="span3">
        <?php echo $form->FileField($shopModel, 'sample_pic[4][picName]', $inputOption); ?>
        <?php echo $form->TextArea($shopModel, 'sample_pic[4][detail]', $DetailOption); ?>
      </div>
    </div>

    <div class="row" style="width: 880px;">
      <label>
        ผู้สมัครจะต้องปฏิบัติตาม ระเบียบการใช้งาน ของทางเว็บอย่างเคร่งครัด ทางเว็บสามารถที่จะตัดสิทธิการเปิดร้านค้าของท่านได้ทันทีหากพบเห็นการฝ่าฝืนระเบียบ และในกรณที่ีเกิดการร้องเรียนจากลูกค้าหรือผู้ที่เข้ามาเยี่ยมชม คณะกรรมการฯจะทำการตรวจสอบและหากพบว่าร้านค้ามีพฤติกรรมที่ไม่เหมาะสม หรือมีพฤติกรรมที่ส่อไปในทางทุจริต ไม่ว่าด้วยกรณีใด ทางเว็บสามารถที่จะตัดสิทธิการเปิดร้านค้าของท่านได้ทันที และไม่คืนเงินค่าสมาชิก ทั้งนี้สมาชิกจะต้องพึงปฏิบัติตามระเบียบต่อไปนี้ด้วย (หากมีข้อสงสัยประการใด กรุณาติดต่อเว็บก่อนสมัคร)
        <br /><br />
        <ol>
          <li>
            ต้องชำระค่าสมาชิกตามที่กำหนด
          </li>
          <li>
            พระที่นำมาลงในร้านค้า จะต้องเป็นพระแท้ตามมาตรฐานสากล
          </li>
          <li>
            ถ้าพระที่นำมาลง มีการอุด ซ่อม แต่ง จะต้องแจ้งให้ลูกค้าทราบก่อนทำการตกลง ซื้อ-ขาย
          </li>
          <li>
            ต้องมีความรับผิดชอบในกรณีเกิดปัญหา หากลูกค้าเกิดความไม่พอใจในพระที่เช่าไป จะต้องไกล่เกลี่ยตกลงกับลูกค้าด้วยเหตุผลอันสมควร
          </li>
          <li>
            ต้องมีพระที่เสนอขายตามรูปจริง
          </li>
          <li>
            ลงพระหมุนเวียนได้ไม่เกิน 150 รายการต่อร้านค้า
          </li>
        </ol>
      </label>
      
    </div>

    <div class="row" style="text-align: center;">
      <label class="checkbox" style="width: 310px;margin: 0 auto;">
        <?php echo CHtml::checkBox('chkRegis',false); ?>
        ข้าพเจ้าได้อ่านและยอมรับในเงื่อนไขข้างต้นทุกประการ
      </label>
      <br />
      <?php 
        $str = Shops::randomStr();
        echo CHtml::image(Yii::app()->request->baseUrl . '/plugin/imagetext/pic_text.php?str=' . $str,null,array('style'=>'margin-bottom:10px;margin-right:10px;'));
        echo $form->textField($shopModel, 'code_input', $inputOption+array('placeholder'=>'กรุณากรอกข้อความตามรูปภาพ'));
        echo $form->hiddenField($shopModel, 'code_hidden', $inputOption+array('value'=>$str)); 
      ?>
      <br />
      <?php echo CHtml::submitbutton('สมัครสมาชิก', array('class'=>'btn btn-danger')); ?>
    </div>

  </div>

<?php
  $this->endWidget();

  Dialog::alertMessage();

?>
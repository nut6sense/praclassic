<?php
  $form = $this->beginWidget('CActiveForm', array(
      'id' => 'all-shop-form',
      // 'action' => Yii::app()->request->baseUrl . '/shop/delShop'
          ));

?>

<div class="container-fluid">
  <div class="row-fluid headerForm sizeContent">
    <div class="span12">
      <h1>ประวัติการชำระเงิน (ทั้งหมด)</h1>
    </div>
  </div>
  <div class="row-fluid bodyForm">
    <div class="span12">
      <table class="table" id="category-detail-table">
        <thead>
          <tr>
            <th>ชื่อร้าน</th>
            <th>ชื่อ - สกุล</th>
            <th>วันที่จ่าย</th>
            <th style="text-align:center;">เป็นจำนวนเงิน</th>
          </tr>
        </thead>
        <tbody>
          <?php
            echo Payment::tblAllPaymentData(); 
          ?>
        </tbody>
      </table>
    </div>
  </div>
</div>

<?php
  $this->endWidget();
?>
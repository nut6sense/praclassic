<?php
  $form = $this->beginWidget('CActiveForm', array(
      'id' => 'block-other-shop-form',
      'action' => Yii::app()->request->baseUrl . '/shop/delShop',
      'htmlOptions' => array('class' => 'form-horizontal')
          ));

  $model_user = new Users;
  $model_shop = new Shops;
  $model_bank = new Banks;
  $model_payment = new Payment;
?>

<div class="container-fluid">
  <div class="row-fluid headerForm sizeContent">
    <div class="span12">
      <h1>ร้านค้าที่ถูกระงับ (ระบุวัน)</h1>
    </div>
  </div>

  <div class="row-fluid bodyForm">
    <div class="span12">
      <table class="table" id="category-detail-table">
        <thead>
          <tr>
            <th>#</th>
            <th>ลำดับ</th>
            <th>ชื่อร้าน</th>
            <th>ล็อกอิน</th>
            <th>ส่งเมล์</th>
            <th>การถูกระงับ</th>
            <th>ตั้งแต่วันที่</th>
            <th>จนถึงวันที่</th>
            <th>ยกเลิก</th>
          </tr>
        </thead>
        <tbody>
          <?php 
            $banStatus = 3;
            echo Shops::tblBanShopsData($banStatus); 
          ?>
        </tbody>
      </table>
    </div>
  </div>

  <div class="row-fluid field-submit">
    <div class="span12">
      .<?php echo CHtml::hiddenField('shopType', 'banOtherShop'); ?>
      <?php echo CHtml::submitButton('ลบร้านค้าที่เลือก', array('id' => 'btn-delete', 'class' => 'btn btn-danger',  'confirm'=>'ต้องการลบข้อมูลร้านค้าที่เลือกใช้หรือไม่ ?')); ?>&nbsp;
    </div>
  </div>
</div>

<?php
  $this->endWidget();

  Dialog::alertMessage();
?>
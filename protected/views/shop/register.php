<?php
	$form = $this->beginWidget('CActiveForm', array(
    'id' => 'regis-form',
    'action' => Yii::app()->request->baseUrl . '/shop/addUser',
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
        ));

	$userModel = new Users;
	$shopModel = new Shops;
	$bankModel = new Banks;
	$PaymentModel = new Payment;

  $fileOption = array('class'=>'span10');
	$inputOption = array('class'=>'span12');
	$DetailOption = array('class'=>'span12','style'=>'height: 100px;','placeholder'=>'รายละเอียด');

?>
<div id="form-register">
	<div class="row-fluid">
    <div class="span12 form-header">
      <h1>ใบสมัครสมาชิก</h1><div class="description">( กรุณาทำตามขั้นตอนการสมัครและกรอกข้อมูลให้ครบทุกช่องตรงตามความเป็นจริง )</div>
    </div> 
  </div>
  <div class="container-fluid">
  	<div class="row-fluid form-content">
      <div id="user-account" class="fields row-fluid">
        <h3>ข้อมูลผู้ใช้งาน</h3>
    		<div class="row-fluid">
          <?php echo $form->labelEx($userModel, 'username', $inputOption); ?>
          <?php echo $form->textField($userModel, 'username', $inputOption); ?>
        </div>
        <div class="row-fluid">
          <?php echo $form->labelEx($userModel, 'password', $inputOption); ?>
          <?php echo $form->PasswordField($userModel, 'password', $inputOption); ?>
        </div>
        <div class="row-fluid">
          <?php echo $form->labelEx($userModel, 'confirmPassword', $inputOption); ?>
          <?php echo $form->PasswordField($userModel, 'confirmPassword', $inputOption); ?>
        </div>
      </div>
      <div id="user-information" class="fields row-fluid">
        <h3>ข้อมูลส่วนตัว</h3>
        <div class="row-fluid">
          <?php echo $form->labelEx($userModel, 'name', $inputOption); ?>
          <?php echo $form->textField($userModel, 'name', $inputOption); ?>
        </div>
        <div class="row-fluid">
          <?php echo $form->labelEx($userModel, 'surname', $inputOption); ?>
          <?php echo $form->textField($userModel, 'surname', $inputOption); ?>
        </div>
        <div class="row-fluid">
          <?php echo $form->labelEx($userModel, 'phonenumber', $inputOption); ?>
          <?php echo $form->textField($userModel, 'phonenumber', array('onkeyup'=>'autoFormat(this, "telephone")', 'onKeyPress'=>'chkNumber()')+$inputOption); ?>
        </div>
        <div class="row-fluid">
          <?php echo $form->labelEx($userModel, 'email', $inputOption); ?>
          <?php echo $form->textField($userModel, 'email', $inputOption); ?>
        </div>
        <div class="row-fluid">
          <?php echo $form->labelEx($userModel, 'iden_id', $inputOption); ?>
          <?php echo $form->textField($userModel, 'iden_id', array('onkeyup'=>'autoFormat(this, "iden")', 'onKeyPress'=>'chkNumber()')+$inputOption); ?>
        </div>
        <div class="row-fluid">
          <?php echo $form->labelEx($userModel, 'iden_picture', $inputOption); ?>
          <?php echo $form->FileField($userModel, 'iden_picture', $inputOption); ?>
        </div>
      </div>
      <div id="shop-information" class="fields row-fluid">
        <h3>รายละเอียดร้านค้า</h3>
        <div class="row-fluid">
          <?php echo $form->labelEx($shopModel, 'shotName', $inputOption); ?>
          <?php echo $form->textField($shopModel, 'shotName', $inputOption+array('maxlength'=>25,'placeholder'=>'กรอกตัวอักษรได้ไม่เกิน 25 ตัว')); ?>
        </div>
        <div class="row-fluid">
          <?php echo $form->labelEx($shopModel, 'fullName', $inputOption); ?>
          <?php echo $form->textField($shopModel, 'fullName', $inputOption); ?>
        </div>
        <div class="row-fluid">
          <?php echo $form->labelEx($shopModel, 'map', $inputOption); ?>
          <?php echo $form->textField($shopModel, 'map', $inputOption); ?>
        </div>
        <div class="row-fluid">
          <?php echo $form->labelEx($shopModel, 'detail', $inputOption); ?>
          <?php echo $form->textField($shopModel, 'detail', $inputOption); ?>
        </div>
        <div class="row-fluid">
          <?php echo $form->labelEx($shopModel, 'rule', $inputOption); ?>
          <?php echo $form->textField($shopModel, 'rule', $inputOption); ?>
        </div>
        <div class="row-fluid">
          <?php echo $form->labelEx($shopModel, 'referrance', $inputOption); ?>
          <?php echo $form->textField($shopModel, 'referrance', $inputOption); ?>
        </div>
        <div class="row-fluid">
          <?php echo $form->labelEx($shopModel, 'referrance_phonenumber', $inputOption); ?>
          <?php echo $form->textField($shopModel, 'referrance_phonenumber', array('onkeyup'=>'autoFormat(this, "telephone")', 'onKeyPress'=>'chkNumber()')+$inputOption); ?>
        </div>
      </div>
      <div id="shop-bank-information" class="fields row-fluid">
        <h3>ข้อมูลบัญชีธนาคาร</h3>
        <div class="row-fluid">
          <?php echo $form->labelEx($bankModel, 'bank_name', $inputOption); ?>
          <?php  echo $form->DropDownList($bankModel, 'bank_name', Banks::getBank(), $inputOption); ?>
        </div>
        <div class="row-fluid">
          <?php echo $form->labelEx($bankModel, 'bank_type', $inputOption); ?>
          <?php  echo $form->DropDownList($bankModel, 'bank_type', Banks::getBankType(), $inputOption); ?>
        </div>
        <div class="row-fluid">
          <?php echo $form->labelEx($bankModel, 'bank_Branch', $inputOption); ?>
          <?php echo $form->textField($bankModel, 'bank_Branch', $inputOption); ?>
        </div>
        <div class="row-fluid">
          <?php echo $form->labelEx($bankModel, 'account_name', $inputOption); ?>
          <?php echo $form->textField($bankModel, 'account_name', $inputOption); ?>
        </div>
        <div class="row-fluid">
          <?php echo $form->labelEx($bankModel, 'account_number', $inputOption); ?>
          <?php echo $form->textField($bankModel, 'account_number', array('onkeyup'=>'autoFormat(this, "book_bank")', 'onKeyPress'=>'chkNumber()', 'maxlength'=>13)+$inputOption); ?>
        </div>
      </div>
      <div id="shop-payment-information" class="fields row-fluid">
        <h3>การจ่ายเงิน</h3>
        <div class="row-fluid">
          <?php echo $form->labelEx($PaymentModel, 'pay_type', array('class'=>'span4')); ?>
          <?php echo $form->DropDownList($PaymentModel, 'pay_type', Payment::getPaymentType(), array('class'=>'span8')); ?>
        </div>
      </div>
      <div id="product-reference-simple" class="fields row-fluid">
        <h3>ส่งรูปเพื่อให้ชมรมพิจารณา</h3>
        <div class="description">(ภาพพระที่ส่งจะต้องมีรูปด้านหน้า, ด้านหลัง, ด้านข้างหรือก้้น)</div>
        <div class="row-fluid">
          <label>พระองค์ที่ 1: </label>
          <label class="span2">ด้านหน้า : </label>
          <?php echo $form->FileField($shopModel, 'sample_pic[1][picName][front]', $fileOption); ?>
          <label class="span2">ด้านหลัง : </label>
          <?php echo $form->FileField($shopModel, 'sample_pic[1][picName][back]', $fileOption); ?>
          <label class="span2">ฐาน : </label>
          <?php echo $form->FileField($shopModel, 'sample_pic[1][picName][base]', $fileOption); ?>
          <?php echo $form->TextArea($shopModel, 'sample_pic[1][detail]', $DetailOption); ?>
        </div>
        <div class="row-fluid">
          <label>พระองค์ที่ 2: </label>
          <label class="span2">ด้านหน้า : </label>
          <?php echo $form->FileField($shopModel, 'sample_pic[2][picName][front]', $fileOption); ?>
          <label class="span2">ด้านหลัง : </label>
          <?php echo $form->FileField($shopModel, 'sample_pic[2][picName][back]', $fileOption); ?>
          <label class="span2">ฐาน : </label>
          <?php echo $form->FileField($shopModel, 'sample_pic[2][picName][base]', $fileOption); ?>
          <?php echo $form->TextArea($shopModel, 'sample_pic[2][detail]', $DetailOption); ?>
        </div>
        <div class="row-fluid">
          <label>พระองค์ที่ 3: </label>
          <label class="span2">ด้านหน้า : </label>
          <?php echo $form->FileField($shopModel, 'sample_pic[3][picName][front]', $fileOption); ?>
          <label class="span2">ด้านหลัง : </label>
          <?php echo $form->FileField($shopModel, 'sample_pic[3][picName][back]', $fileOption); ?>
          <label class="span2">ฐาน : </label>
          <?php echo $form->FileField($shopModel, 'sample_pic[3][picName][base]', $fileOption); ?>
          <?php echo $form->TextArea($shopModel, 'sample_pic[3][detail]', $DetailOption); ?>
        </div>
        <div class="row-fluid">
          <label>พระองค์ที่ 4: </label>
          <label class="span2">ด้านหน้า : </label>
          <?php echo $form->FileField($shopModel, 'sample_pic[4][picName][front]', $fileOption); ?>
          <label class="span2">ด้านหลัง : </label>
          <?php echo $form->FileField($shopModel, 'sample_pic[4][picName][back]', $fileOption); ?>
          <label class="span2">ฐาน : </label>
          <?php echo $form->FileField($shopModel, 'sample_pic[4][picName][base]', $fileOption); ?>
          <?php echo $form->TextArea($shopModel, 'sample_pic[4][detail]', $DetailOption); ?>
        </div>
        <div class="row-fluid">
          <label>พระองค์ที่ 5: </label>
          <label class="span2">ด้านหน้า : </label>
          <?php echo $form->FileField($shopModel, 'sample_pic[5][picName][front]', $fileOption); ?>
          <label class="span2">ด้านหลัง : </label>
          <?php echo $form->FileField($shopModel, 'sample_pic[5][picName][back]', $fileOption); ?>
          <label class="span2">ฐาน : </label>
          <?php echo $form->FileField($shopModel, 'sample_pic[5][picName][base]', $fileOption); ?>
          <?php echo $form->TextArea($shopModel, 'sample_pic[5][detail]', $DetailOption); ?>
        </div>
        <div class="row-fluid">
          <label>พระองค์ที่ 6: </label>
          <label class="span2">ด้านหน้า : </label>
          <?php echo $form->FileField($shopModel, 'sample_pic[6][picName][front]', $fileOption); ?>
          <label class="span2">ด้านหลัง : </label>
          <?php echo $form->FileField($shopModel, 'sample_pic[6][picName][back]', $fileOption); ?>
          <label class="span2">ฐาน : </label>
          <?php echo $form->FileField($shopModel, 'sample_pic[6][picName][base]', $fileOption); ?>
          <?php echo $form->TextArea($shopModel, 'sample_pic[6][detail]', $DetailOption); ?>
        </div>
      </div>
      <div class="rules-field row-fluid">
        <p>ผู้สมัครจะต้องปฏิบัติตาม ระเบียบการใช้งาน ของทางเว็บอย่างเคร่งครัด ทางเว็บสามารถที่จะตัดสิทธิการเปิดร้านค้าของท่านได้ทันทีหากพบเห็นการฝ่าฝืนระเบียบ และในกรณที่ีเกิดการร้องเรียนจากลูกค้าหรือผู้ที่เข้ามาเยี่ยมชม คณะกรรมการฯจะทำการตรวจสอบและหากพบว่าร้านค้ามีพฤติกรรมที่ไม่เหมาะสม หรือมีพฤติกรรมที่ส่อไปในทางทุจริต ไม่ว่าด้วยกรณีใด ทางเว็บสามารถที่จะตัดสิทธิการเปิดร้านค้าของท่านได้ทันที และไม่คืนเงินค่าสมาชิก ทั้งนี้สมาชิกจะต้องพึงปฏิบัติตามระเบียบต่อไปนี้ด้วย (หากมีข้อสงสัยประการใด กรุณาติดต่อเว็บก่อนสมัคร)</p>
        <ol>
          <li>ต้องชำระค่าสมาชิกตามที่กำหนด
          </li>
          <li>ต้องระบุหมายเลขที่บัญชีตามจริง 
          </li>
          <li>พระที่นำมาลงในร้านค้า จะต้องเป็นพระแท้ตามมาตรฐานสากล
          </li>
          <li>ถ้าพระที่นำมาลง มีการอุด ซ่อม แต่ง จะต้องแจ้งให้ลูกค้าทราบก่อนทำการตกลง ซื้อ-ขาย
          </li>
          <li>ต้องมีความรับผิดชอบในกรณีเกิดปัญหา หากลูกค้าเกิดความไม่พอใจในพระที่เช่าไป จะต้องไกล่เกลี่ยตกลงกับลูกค้าด้วยเหตุผลอันสมควร
          </li>
          <li>ต้องมีพระที่เสนอขายตามรูปจริง
          </li>
          <li>ลงพระหมุนเวียนได้ไม่เกิน 150 รายการต่อร้านค้า
          </li>
        </ol>
      </div>
      <div class="field-submit row-fluid">
        <label class="checkbox">
          <?php echo CHtml::checkBox('chkRegis',false); ?>
          ข้าพเจ้าได้อ่านและยอมรับในเงื่อนไขข้างต้นทุกประการ
        </label>
        <?php 
          $str = Shops::randomStr();
          echo CHtml::image(Yii::app()->request->baseUrl . '/plugin/imagetext/pic_text.php?str=' . $str,null,array('style'=>'margin-bottom:10px;margin-right:10px;'));
          echo $form->textField($shopModel, 'code_input', array('placeholder'=>'กรุณากรอกข้อความตามรูปภาพ','class'=>'span10'));
          echo $form->hiddenField($shopModel, 'code_hidden', $inputOption + array('value'=>$str)); 
        ?>
        <?php echo CHtml::submitbutton('สมัครสมาชิก', array('class'=>'btn btn-primary')); ?>
      </div>
  	</div>
  </div>
</div>
<?php
	$this->endWidget();
	Dialog::alertMessage();
?>
<?php
  $form = $this->beginWidget('CActiveForm', array(
      'id' => 'expire-shop-form',
      'action' => Yii::app()->request->baseUrl . '/shop/delShop',
      'htmlOptions' => array('class' => 'form-horizontal')
          ));

  $model_user = new Users;
  $model_shop = new Shops;
  $model_bank = new Banks;
  $model_payment = new Payment;
?>

<div class="container-fluid">
  <div class="row-fluid headerForm sizeContent">
    <div class="span12">
      <h1>ร้านค้าที่หมดอายุ</h1>
    </div>
  </div>

  <div class="row-fluid bodyForm">
    <div class="span11">
      <table class="table" id="category-detail-table">
        <thead>
          <tr>
            <th>#</th>
            <th>ลำดับ</th>
            <th>ชื่อร้าน</th>
            <th>ล็อกอิน</th>
            <th>ส่งเมล์</th>
            <th>ต่ออายุ</th>
            <th>การจ่ายเงิน</th>
            <th>หมดอายุ</th>
            <th>หมายเหตุ</th>
          </tr>
        </thead>
        <tbody>
          <?php 
            echo Shops::tblExpireShopsData(); 
          ?>
        </tbody>
      </table>
    </div>
  </div>

  <div class="row-fluid field-submit">
    <div class="span11">
      <?php echo CHtml::hiddenField('shopType', 'expireShop'); ?>
      <?php echo CHtml::submitButton('ลบร้านค้าที่เลือก', array('id' => 'btn-delete', 'class' => 'btn btn-danger',  'confirm'=>'ต้องการลบข้อมูลร้านค้าที่เลือกใช้หรือไม่ ?')); ?>&nbsp;
    </div>
  </div>
</div>

<?php
  $this->endWidget();
  //
  $dialog_id = 'renewShop';
  $dialog_heaher = 'ต่ออายุร้านค้า';

  $inputOption = array('class'=>'span3');
  $htmlOptions = array('class'=>'span3', 'style'=>'margin-left:65px;');

  $dialog_content = CHtml::openTag('div', array('class'=>'container-fluid'));

  $dialog_content .= CHtml::openTag('div', array('class'=>'row-fluid'));
  $dialog_content .= $form->hiddenField($model_user, 'user_id');
  $dialog_content .= CHtml::hiddenField('shopType', 'expireShop');
  $dialog_content .= CHtml::closeTag('div');

  $dialog_content .= CHtml::openTag('div', array('class'=>'row-fluid'));
  $dialog_content .= CHtml::openTag('div', array('class'=>'span'));
  $dialog_content .= CHtml::openTag('label', array('style'=>'font-weight:bold;'));
  $dialog_content .= 'รายละเอียดการชำระเงิน';
  $dialog_content .= CHtml::closeTag('label');
  $dialog_content .= CHtml::closeTag('div');
  $dialog_content .= CHtml::closeTag('div');

  $dialog_content .= CHtml::openTag('div', array('class'=>'row'));
  $dialog_content .= CHtml::openTag('div', $htmlOptions);
  $dialog_content .= $form->labelEx($model_payment, 'date_begin');
  $dialog_content .= CHtml::closeTag('div');
  $dialog_content .= CHtml::openTag('div');
  $dialog_content .= $form->textField($model_payment, 'date_begin', array('class'=>'span4 datepicker'));
  $dialog_content .= CHtml::closeTag('div');
  $dialog_content .= CHtml::closeTag('div');

  $dialog_content .= CHtml::openTag('div', array('class'=>'row'));
  $dialog_content .= CHtml::openTag('div', $htmlOptions);
  $dialog_content .= $form->labelEx($model_payment, 'date_end');
  $dialog_content .= CHtml::closeTag('div');
  $dialog_content .= CHtml::openTag('div');
  $dialog_content .= $form->textField($model_payment, 'date_end', array('class'=>'span4 datepicker'));
  $dialog_content .= CHtml::closeTag('div');
  $dialog_content .= CHtml::closeTag('div');

  $dialog_content .= CHtml::openTag('div', array('class'=>'row'));
  $dialog_content .= CHtml::openTag('div', $htmlOptions);
  $dialog_content .= $form->labelEx($model_payment, 'date_pay');
  $dialog_content .= CHtml::closeTag('div');
  $dialog_content .= CHtml::openTag('div');
  $dialog_content .= $form->textField($model_payment, 'date_pay', array('class'=>'span4 datepicker'));
  $dialog_content .= CHtml::closeTag('div');
  $dialog_content .= CHtml::closeTag('div');

  $dialog_content .= CHtml::openTag('div', array('class'=>'row'));
  $dialog_content .= CHtml::openTag('div', $htmlOptions);
  $dialog_content .= $form->labelEx($model_payment, 'pay_amount');
  $dialog_content .= CHtml::closeTag('div');
  $dialog_content .= CHtml::openTag('div', array('class'=>'span3', 'style'=>'margin-left:0px;'));
  $dialog_content .= $form->textField($model_payment, 'pay_amount', array('class'=>'span'));
  $dialog_content .= CHtml::closeTag('div');
  $dialog_content .= CHtml::openTag('div');
  $dialog_content .= CHtml::openTag('label');
  $dialog_content .= '&nbsp&nbspบาท';
  $dialog_content .= CHtml::closeTag('label');
  $dialog_content .= CHtml::closeTag('div');
  $dialog_content .= CHtml::closeTag('div');

  $dialog_content .= CHtml::openTag('div', array('class'=>'row'));
  $dialog_content .= CHtml::openTag('div', $htmlOptions);
  $dialog_content .= $form->labelEx($model_payment, 'title_pay_type');
  $dialog_content .= CHtml::closeTag('div');
  $dialog_content .= CHtml::openTag('div');
  $dialog_content .= $form->DropDownList($model_payment, 'pay_type', Payment::getPaymentType(), array('class'=>'span4'));
  $dialog_content .= CHtml::closeTag('div');
  $dialog_content .= CHtml::closeTag('div');

  $dialog_content .= CHtml::closeTag('div');

  $dialog_path = '/shop/renewShop';

  Dialog::LinkDialog($dialog_id, $dialog_heaher, $dialog_content, $dialog_path);
  //
?>

<?php
  Dialog::alertMessage();
?>
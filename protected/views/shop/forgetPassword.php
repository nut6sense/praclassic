<?php
  $form = $this->beginWidget('CActiveForm', array(
    'id' => 'shop-list-form',
    'action' => Yii::app()->request->baseUrl . '/shop/submitForgetPassword',
    ));

  $model_users = new Users;
?>

<div id="shoplist" class="container-fluid">
  <div class="row-fluid headerForm sizeContent">
    <div class="span12">
      <h1>ลืมรหัสผ่าน</h1>
    </div>
  </div>

  <div class="span12 bodyForm" style="margin-left:0px;">

    <div class="row-fluid">
      <div class="span2">
        <?php echo $form->labelEx($model_users, 'username'); ?>
      </div>
      <div class="span3">
        <?php echo $form->textField($model_users, 'username'); ?>
      </div>
      <div class="span3">
        <?php echo CHtml::submitButton('ยืนยัน', array('id' => 'btn-add', 'class' => 'btn btn-primary','style'=>'margin-left: 20px;')); ?>
      </div>
    </div>

  </div>
</div>
<?php
  $this->endWidget();
?>
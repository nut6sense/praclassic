<?php
  $form = $this->beginWidget('CActiveForm', array(
      'id' => 'view-private-massage-form',
      'action' => Yii::app()->request->baseUrl . '/shop/submitSendShopEmail'
          ));

  $user_id = Yii::app()->input->get('param1');
  $fromPage = Yii::app()->input->get('param2');

  $headerOption = 'class="span3"';
  $bodyOption = 'class="span7"';
?>
<div class="container-fluid">
  <div class="row-fluid">
    <div class="span12"><h1>ส่งเมล์ถึงร้านค้า</h1></div>
  </div>

  <div class="row-fluid">
    <div <?php echo $headerOption; ?> >
      <label>ชื่อร้าน</label>
    </div>
    <div <?php echo $bodyOption; ?> >
      <label>
        <?php 
          $explodeShopName = explode('&&', $dataShop['name']);
          echo $explodeShopName[1]; 
        ?>
      </label>
    </div>
  </div>

  <div class="row-fluid">
    <div <?php echo $headerOption; ?> >
      <label>ส่งถึง</label>
    </div>
    <div <?php echo $bodyOption; ?> >
      <label><?php echo $dataUser['email']; ?></label>
    </div>
  </div>

  <div class="row-fluid">
    <div <?php echo $headerOption; ?> >
      <label>หัวข้อ</label>
    </div>
    <div <?php echo $bodyOption; ?> >
      <?php echo CHtml::textField('subject', null, array('class'=>'span')); ?>
    </div>
  </div>  

  <div class="row-fluid">
    <div <?php echo $headerOption; ?> >
      <label>ข้อความ</label>
    </div>
    <div <?php echo $bodyOption; ?> >
      <?php echo CHtml::textArea('message', null, array('rows'=>'10', 'class'=>'span')); ?>
    </div>
  </div>  

  <div class="row-fluid">
    <div class="span12 row-fluid field-submit">
      <?php echo CHtml::hiddenField('user_id', $user_id); ?>
      <?php echo CHtml::hiddenField('fromPage', $fromPage); ?>
      <?php
        $disabled = array();
        if(empty($dataUser['email'])) {
          $disabled = array('disabled'=>true);
        }
        echo CHtml::submitbutton('ส่งข้อความ', array('class'=>'btn btn-primary')+$disabled); 
      ?>
    </div>
  </div>
</div>

<?php
  $this->endWidget();
?>
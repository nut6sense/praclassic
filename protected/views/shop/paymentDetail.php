<?php
  $form = $this->beginWidget('CActiveForm', array(
    'id' => 'payment-detail-form',
    'action' => Yii::app()->request->baseUrl . '/shop/confirmShop',
  ));
  $model_user = new Users;
  $model_payment = new Payment;

  $inputOption = 'class="span4"';
  $htmlOptions = 'class="span2" style="margin-left:60px;"';
?>

<div class="container-fluid">
  <div class="row-fluid">
    <div class="span12"><h1>ยืนยันข้อมูลร้านค้า</h1></div>
  </div>

  <div>
    <?php echo $form->hiddenField($model_user, 'user_id', array('value'=>$dataShop['user_id'])); ?>
    <?php echo CHtml::hiddenField('shopType', 'newShop'); ?>
  </div>

  <div class="row-fluid">
    <div <?php echo $htmlOptions; ?> >
      <?php echo $form->labelEx($model_payment, 'date_begin'); ?>
    </div>
    <div <?php echo $inputOption; ?> >
      <?php echo $form->textField($model_payment, 'date_begin', array('class'=>'span','value'=>$dataShop['date_begin'], 'disabled'=>true)); ?>
    </div>
  </div>

  <div class="row-fluid">
    <div <?php echo $htmlOptions;?> >
      <?php echo $form->labelEx($model_payment, 'date_pay'); ?>
    </div>
    <div <?php echo $inputOption; ?> >
      <?php echo $form->textField($model_payment, 'date_pay', array('class'=>'span datepicker','value'=>date('d/m/Y'))); ?>
    </div>
  </div>

  <div class="row-fluid">
    <div <?php echo $htmlOptions;?> >
      <?php echo $form->labelEx($model_payment, 'pay_amount'); ?>
    </div>
    <div <?php echo $inputOption; ?> >
      <?php echo $form->textField($model_payment, 'pay_amount', array('class'=>'span')); ?>
    </div>
    <div>
      <label><?php echo '&nbsp&nbspบาท'; ?></label>
    </div>
  </div>

  <div class="row-fluid">
    <div <?php echo $htmlOptions;?> >
      <?php echo $form->labelEx($model_payment, 'title_pay_type'); ?>
    </div>
    <div <?php echo $inputOption; ?> >
      <?php echo $form->DropDownList($model_payment, 'pay_type', Payment::getPaymentType(), array('options'=>array($dataShop['pay_type']=>array('selected'=>true)), 'class'=>'span')); ?>
    </div>
  </div>

  <div class="row-fluid field-submit">
    <div class="span12">
      <?php echo $form->hiddenField($model_user, 'user_id', array('value'=>Yii::app()->input->get('param1')));; ?>
      <?php echo CHtml::hiddenField('formPage', $formPage); ?>
      <?php echo CHtml::link('ย้อนกลับ', '../../'.$formPage, array('id' => 'btn-add', 'class' => 'btn btn-success btnLink')); ?>&nbsp;
      <?php echo CHtml::submitButton('บันทึกข้อมูล', array('id' => 'btn-add', 'class' => 'btn btn-info')); ?>
    </div>
  </div>

</div>
<?php
  $this->endWidget();

  Dialog::alertMessage();
?>
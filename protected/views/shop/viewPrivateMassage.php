<?php
  $form = $this->beginWidget('CActiveForm', array(
      'id' => 'view-private-massage-form',
      'action' => Yii::app()->request->baseUrl . '/shop/delPrivateMassage'
          ));
  $user_id = Yii::app()->input->get('param1');

  $contact_model = new Contact;
  $users_model = new Users;
?>

<div class="container-fluid">
  <div class="row-fluid headerForm sizeContent">
    <div class="span12">
      <h1>ข้อความส่วนตัว</h1>
    </div>
  </div>
  <div class="row-fluid bodyForm">
    <div class="span12">
      <table class="table" id="category-detail-table">
        <thead>
          <tr>
            <th>#</th>
            <th>ลำดับ</th>
            <th>วันที่</th>
            <th>หัวข้อ</th>
            <th>ข้อความจาก</th>
          </tr>
        </thead>
        <tbody>
          <?php 
            echo Contact::tblPrivateMassage($user_id); 
          ?>
        </tbody>
      </table>
    </div>
  </div>
  <div class="row-fluid field-submit">
    <div class="span12">
      <?php echo $form->hiddenField($users_model, 'user_id', array('value'=>$user_id)); ?>
      <?php echo CHtml::submitButton('ลบข้อความที่เลือก', array('id' => 'btn-delete', 'class' => 'btn btn-danger',  'confirm'=>'ต้องการลบข้อความที่เลือกใช้หรือไม่ ?')); ?>&nbsp;
    </div>
  </div>
</div>

<?php
  $this->endWidget();

  $headerOption = array('class'=>'span3');
  $bodyOption = array('class'=>'span9');

  $dialog_id = 'dialogPrivateMassage';
  $dialog_heaher = 'ข้อความส่วนตัว';

  $dialog_content = CHtml::openTag('div', array('class'=>'container-fluid'));

  $dialog_content .= CHtml::openTag('div', array('class'=>'row-fluid'));
  $dialog_content .= CHtml::openTag('div', array('style'=>'text-align:center;'));
  $dialog_content .= CHtml::openTag('div', array('id'=>'picture'));
  $dialog_content .= CHtml::image(null, null, array('class'=>'picShop','style'=>'margin-bottom: 20px;')); 
  $dialog_content .= CHtml::closeTag('div');
  $dialog_content .= CHtml::closeTag('div');
  $dialog_content .= CHtml::closeTag('div');

  $dialog_content .= CHtml::openTag('div', array('class'=>'row-fluid'));
  $dialog_content .= CHtml::openTag('div', $headerOption);
  $dialog_content .= $form->labelEx($contact_model, 'from');
  $dialog_content .= CHtml::closeTag('div');
  $dialog_content .= CHtml::openTag('div', $bodyOption);
  $dialog_content .= CHtml::openTag('label', array('id'=>'sendFrom'));
  $dialog_content .= CHtml::closeTag('label');
  $dialog_content .= CHtml::closeTag('div');
  $dialog_content .= CHtml::closeTag('div');

  $dialog_content .= CHtml::openTag('div', array('class'=>'row-fluid'));
  $dialog_content .= CHtml::openTag('div', $headerOption);
  $dialog_content .= $form->labelEx($contact_model, 'title_email');
  $dialog_content .= CHtml::closeTag('div');
  $dialog_content .= CHtml::openTag('div', $bodyOption);
  $dialog_content .= CHtml::openTag('label', array('id'=>'email'));
  $dialog_content .= CHtml::closeTag('label');
  $dialog_content .= CHtml::closeTag('div');
  $dialog_content .= CHtml::closeTag('div');

  $dialog_content .= CHtml::openTag('div', array('class'=>'row-fluid'));
  $dialog_content .= CHtml::openTag('div', $headerOption);
  $dialog_content .= $form->labelEx($contact_model, 'title_subject');
  $dialog_content .= CHtml::closeTag('div');
  $dialog_content .= CHtml::openTag('div', $bodyOption);
  $dialog_content .= CHtml::openTag('label', array('id'=>'subject'));
  $dialog_content .= CHtml::closeTag('label');
  $dialog_content .= CHtml::closeTag('div');
  $dialog_content .= CHtml::closeTag('div');

  $dialog_content .= CHtml::openTag('div', array('class'=>'row-fluid'));
  $dialog_content .= CHtml::openTag('div', $headerOption);
  $dialog_content .= $form->labelEx($contact_model, 'title_message');
  $dialog_content .= CHtml::closeTag('div');
  $dialog_content .= CHtml::openTag('div', $bodyOption);
  $dialog_content .= CHtml::openTag('label', array('id'=>'message'));
  $dialog_content .= CHtml::closeTag('label');
  $dialog_content .= CHtml::closeTag('div');
  $dialog_content .= CHtml::closeTag('div');

  $dialog_content .= CHtml::openTag('hr');

  $dialog_content .= CHtml::openTag('div', array('class'=>'row-fluid'));
  $dialog_content .= CHtml::openTag('div', $headerOption);
  $dialog_content .= $form->labelEx($contact_model, 'send_to_email');
  $dialog_content .= CHtml::closeTag('div');
  $dialog_content .= CHtml::openTag('div', $bodyOption);
  $dialog_content .= CHtml::openTag('label', array('id'=>'send_to_email'));
  $dialog_content .= CHtml::closeTag('label');
  $dialog_content .= CHtml::closeTag('div');
  $dialog_content .= CHtml::closeTag('div');

  $dialog_content .= CHtml::openTag('div', array('class'=>'row-fluid'));
  $dialog_content .= CHtml::openTag('div', $headerOption);
  $dialog_content .= $form->labelEx($contact_model, 'send_from_email');
  $dialog_content .= CHtml::closeTag('div');
  $dialog_content .= CHtml::openTag('div', $bodyOption);
  $dialog_content .= CHtml::openTag('label', array('id'=>'send_from_email'));
  $dialog_content .= CHtml::closeTag('label');
  $dialog_content .= CHtml::closeTag('div');
  $dialog_content .= CHtml::closeTag('div');

  $dialog_content .= CHtml::openTag('div', array('class'=>'row-fluid'));
  $dialog_content .= CHtml::openTag('div', $headerOption);
  $dialog_content .= $form->labelEx($contact_model, 'title_subject');
  $dialog_content .= CHtml::closeTag('div');
  $dialog_content .= CHtml::openTag('div', $bodyOption);
  $dialog_content .= $form->textField($contact_model, 'subject');
  $dialog_content .= CHtml::closeTag('div');
  $dialog_content .= CHtml::closeTag('div');

  $dialog_content .= CHtml::openTag('div', array('class'=>'row-fluid'));
  $dialog_content .= CHtml::openTag('div', $headerOption);
  $dialog_content .= $form->labelEx($contact_model, 'message');
  $dialog_content .= CHtml::closeTag('div');
  $dialog_content .= CHtml::openTag('div', $bodyOption);
  $dialog_content .= $form->textArea($contact_model, 'message', array('rows'=>'10', 'class'=>'span'));
  $dialog_content .= CHtml::closeTag('div');
  $dialog_content .= CHtml::closeTag('div');

  $dialog_content .= $form->hiddenField($contact_model, 'cid');

  $dialog_content .= CHtml::closeTag('div');

  $dialog_path = '/contact/resendPrivateMessage';
  $submit_text = 'ตอบ';

  Dialog::LinkDialog($dialog_id, $dialog_heaher, $dialog_content, $dialog_path, $submit_text);
?>

<?php
  Dialog::alertMessage();
?>
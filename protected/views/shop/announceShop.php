<?php
  $form = $this->beginWidget('CActiveForm', array(
      'id' => 'announce-shop-form',
      'action' => Yii::app()->request->baseUrl . '/shop/addAnnounceMassage',
      'htmlOptions' => array('class' => 'form-horizontal')
          ));
?>

<div class="container-fluid">
  <div class="row-fluid">
    <div class="span12"><h1>ประกาศร้านค้า</h1></div>
  </div>
  <div class="row-fluid">
    <div class="span12">
      <?php echo CHtml::textArea('announce-shop-massage', Shopsmassage::getMassage('announce', Yii::app()->input->get('param1')), array('cols'=>'80', 'id'=>'editor1', 'name'=>'editor1', 'rows'=>'10')); ?>
    </div>
  </div>
  <div class="row-fluid field-submit">
    <div class="span12">
      <?php echo CHtml::hiddenField('user_id', Yii::app()->input->get('param1')); ?>
      <?php echo CHtml::submitButton('เพิ่มข้อความ', array('id' => 'btn-add', 'class' => 'btn btn-info')); ?>
    </div>
  </div>
</div>

<?php
  $this->renderPartial('//layouts/plugin/scriptWebTextEditor');
  $this->endWidget();
  Dialog::alertMessage();
?>
<?php
  $form = $this->beginWidget('CActiveForm', array(
      'id' => 'admin-all-shop-form',
      'action' => Yii::app()->request->baseUrl . '/shop/delShop'
          ));

  $model_user = new Users;
  $model_shop = new Shops;
  $model_bank = new Banks;
  $model_payment = new Payment;
?>

<div class="container-fluid">
  <div class="row-fluid headerForm sizeContent">
    <div class="span12">
      <h1>ร้านค้าทั้งหมด</h1>
    </div>
  </div>
  <div class="row-fluid bodyForm">
    <div class="span12">
      <table class="table" id="category-detail-table">
        <thead>
          <tr>
            <th>#</th>
            <th>ลำดับ</th>
            <th>ชื่อร้าน</th>
            <th>ล็อกอิน</th>
            <th>ส่งเมล์</th>
            <th>สถิติ</th>
            <th>การจ่ายเงิน</th>
            <th>หมดอายุ</th>
            <th>แนบเอกสาร</th>
            <th>หมายเหตุ</th>
            <th>เปิด/ปิด</th>
            <th>การรับรอง</th>
          </tr>
        </thead>
        <tbody>
          <?php 
            echo Shops::tblAllShopsData(); 
          ?>
        </tbody>
      </table>
    </div>
  </div>
  <div class="row-fluid field-submit" style="text-align:center;">
    <div class="span12">
      <?php
        echo CHtml::hiddenField('shopType', 'allShop');
        $page = !is_null(Yii::app()->input->get('param1'))?Yii::app()->input->get('param1'):1;
        $shopQueryAll = Shops::model()->findAll();
        echo Paging::getPaging(count($shopQueryAll),50,$page);
      ?>
    </div>
  </div>
  <div class="row-fluid field-submit">
    <div class="span12">
      <?php echo CHtml::hiddenField('shopType', 'allShop'); ?>
      <?php echo CHtml::submitButton('ลบร้านค้าที่เลือก', array('id' => 'btn-delete', 'class' => 'btn btn-danger',  'confirm'=>'ต้องการลบข้อมูลร้านค้าที่เลือกใช้หรือไม่ ?')); ?>&nbsp;
    </div>
  </div>
</div>

<?php
  $this->endWidget();

  Dialog::alertMessage();
?>
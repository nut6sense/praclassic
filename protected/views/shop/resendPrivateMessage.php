<?php
  $form = $this->beginWidget('CActiveForm', array(
      'id' => 'view-private-massage-form',
      'action' => Yii::app()->request->baseUrl . '/contact/submitResendPrivateMessage'
          ));

  $user_id = Yii::app()->input->get('param1');
  $contact_id = Yii::app()->input->get('param2');

  $contact_model = new Contact;

  $headerOption = 'class="span3"';
  $bodyOption = 'class="span7"';
?>
<div class="container-fluid">

  <div class="row-fluid">
    <div style="text-align:center;">
      <div id="picture">
        <?php CHtml::image(null, null, array('class'=>'picShop','style'=>'margin-bottom: 20px;')); ?>
      </div>
    </div>
  </div>

  <div class="row-fluid">
    <div <?php echo $headerOption; ?> >
      <?php echo $form->labelEx($contact_model, 'from'); ?>
    </div>
    <div <?php echo $bodyOption; ?> >
      <label><?php echo $dataContact['name']; ?></label>
    </div>
  </div>

  <div class="row-fluid">
    <div <?php echo $headerOption; ?> >
      <?php echo $form->labelEx($contact_model, 'title_email'); ?>
    </div>
    <div <?php echo $bodyOption; ?> >
      <label><?php echo $dataContact['email']; ?></label>
    </div>
  </div>

  <div class="row-fluid">
    <div <?php echo $headerOption; ?> >
      <?php echo $form->labelEx($contact_model, 'title_subject'); ?>
    </div>
    <div <?php echo $bodyOption; ?> >
      <label><?php echo $dataContact['subject']; ?></label>
    </div>
  </div>

  <div class="row-fluid">
    <div <?php echo $headerOption; ?> >
      <?php echo $form->labelEx($contact_model, 'title_message'); ?>
    </div>
    <div <?php echo $bodyOption; ?> >
      <label><?php echo $dataContact['message']; ?></label>
    </div>
  </div>
  <hr/>
  <div class="row-fluid">
    <div <?php echo $headerOption; ?> >
      <?php echo $form->labelEx($contact_model, 'send_to_email'); ?>
    </div>
    <div <?php echo $bodyOption; ?> >
      <label><?php echo $dataContact['email']; ?></label>
    </div>
  </div>

  <div class="row-fluid">
    <div <?php echo $headerOption; ?> >
      <?php echo $form->labelEx($contact_model, 'send_from_email'); ?>
    </div>
    <div <?php echo $bodyOption; ?> >
      <label><?php echo $dataContact['shop_email']; ?></label>
    </div>
  </div>

  <div class="row-fluid">
    <div <?php echo $headerOption; ?> >
      <?php echo $form->labelEx($contact_model, 'title_subject'); ?>
    </div>
    <div <?php echo $bodyOption; ?> >
      <?php echo $form->textField($contact_model, 'subject', array('value'=>'RE:'.$dataContact['subject'], 'class'=>'span')); ?>
    </div>
  </div>  

  <div class="row-fluid">
    <div <?php echo $headerOption; ?> >
      <?php echo $form->labelEx($contact_model, 'message'); ?>
    </div>
    <div <?php echo $bodyOption; ?> >
      <?php echo $form->textArea($contact_model, 'message', array('rows'=>'10', 'class'=>'span')); ?>
    </div>
  </div>  

  <div class="row-fluid">
    <div class="span12 row-fluid field-submit">
      <?php echo $form->hiddenField($contact_model, 'cid', array('value'=>$contact_id)); ?>
      <?php echo CHtml::submitbutton('ยืนยัน', array('class'=>'btn btn-primary')); ?>
    </div>
  </div>

</div>

<?php
  $this->endWidget();
?>
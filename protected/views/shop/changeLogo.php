<?php
	$form = $this->beginWidget('CActiveForm', array(
    'id' => 'changeLogo-form',
    'action' => Yii::app()->request->baseUrl . '/shop/submitChangeLogo',
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
        ));
  $user_id = Yii::app()->input->get('param1');
	$shopModel = new Shops;

	$inputOption = array('class'=>'span12');
	$DetailOption = array('class'=>'span12','style'=>'height: 100px;','placeholder'=>'รายละเอียด');

?>
<div id="form-register">
	<div class="row-fluid">
    <div class="span12 form-header">
      <h1>แก้ไขรูปโลโก้ร้านค้า</h1>
    </div> 
  </div>
  <div class="container-fluid">
  	<div class="row-fluid form-content">
      <div class="fields row-fluid">
        <div class="row-fluid">
          <label>เลือกรูป</label>
          <?php echo $form->FileField($shopModel, 'cover', $inputOption); ?>
          <sub>ใช้รูปาพในสกุล *.jpg เท่านั้น ขนาดที่แนะนำคือ 1170x210 Pixels</sub>
          <?php echo $form->hiddenField($shopModel, 'user_id', array('value'=>$user_id)); ?>
        </div>
      </div>

      <div class="field-submit row-fluid">
        <?php echo CHtml::submitbutton('บันทึก', array('class'=>'btn btn-primary')); ?>
        <?php echo CHtml::link('Default', '../defaultLogo/'.$user_id, array('id' => 'btn-add', 'class' => 'btn btn-inverse btnLink')); ?>
      </div>
  	</div>
  </div>
</div>
<?php
	$this->endWidget();
	Dialog::alertMessage();
?>
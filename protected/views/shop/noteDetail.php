<?php
  $form = $this->beginWidget('CActiveForm', array(
    'id' => 'note-detail-form',
    'action' => Yii::app()->request->baseUrl . '/shop/addNoteShop',
  ));

  $model_user = new Users;
  $model_shop = new Shops;
?>

<div class="container-fluid">
  <div class="row-fluid">
    <div class="span12"><h1>หมายเหตุ</h1></div>
  </div>

  <div class="row-fluid">
    <div class="span12">
      <?php echo $form->hiddenField($model_user, 'user_id', array('value'=>Yii::app()->input->get('param1'))); ?>
      <?php echo CHtml::hiddenField('shopType', Yii::app()->input->get('param2')); ?>
      <?php echo $form->textArea($model_shop, 'note', array('value'=>$dataShop['note'], 'rows'=>'10', 'class'=>'span')); ?>
    </div>
  </div>

  <div class="row-fluid field-submit">
    <div class="span12">
      <?php echo CHtml::link('ย้อนกลับ', '../../'.$formPage, array('id' => 'btn-add', 'class' => 'btn btn-success btnLink')); ?>&nbsp;
      <?php echo CHtml::submitButton('บันทึกข้อมูล', array('id' => 'btn-add', 'class' => 'btn btn-info')); ?>
    </div>
  </div>
</div>

<?php
	$this->endWidget();

  // Dialog::alertMessage();
?>

<?php
  $form = $this->beginWidget('CActiveForm', array(
      'id' => 'ban-productsInShop-form',
      // 'action' => Yii::app()->request->baseUrl . '/products/delProducts',
      'htmlOptions' => array('class' => 'form-horizontal')
          ));
?>

<div class="container-fluid">
  <div class="row-fluid headerForm sizeContent">
    <div class="span12">
      <h1>สินค้าที่ถูกล๊อค</h1>
    </div>
  </div>
  <div class="row-fluid bodyForm">
    <div class="span12">
      <table class="table" id="category-detail-table">
        <thead>
          <tr>
            <th>ลำดับ</th>
            <th>รายการสินค้า</th>
            <th>ราคาขาย</th>
            <th>วันที่</th>
            <th>สถานะ</th>
            <th>ปลดล๊อค</th>
          </tr>
        </thead>
        <tbody>
          <?php 
            echo Products::tblDataProductBlock(Yii::app()->input->get('param1')); 
          ?>
        </tbody>
      </table>
    </div>
  </div>
</div>

<?php
  $this->endWidget();

  Dialog::alertMessage();
?>
<?php
  $form = $this->beginWidget('CActiveForm', array(
    'id' => 'payment-detail-form',
    // 'action' => Yii::app()->request->baseUrl . '/shop/confirmShop',
  ));
?>

<div class="container-fluid">
  <div class="row-fluid">
    <div class="span12"><h1>แนบเอกสาร</h1></div>
  </div>

  <div class="row-fluid">
    <div class="span12" style="text-align:center;">
      <?php
          $picPath = Yii::app()->request->baseUrl.'/img/iden/'.$dataShop->iden_picture.'.jpg';
          echo Highslide::PicHighslide($picPath,null,250);
      ?>
    </div>
  </div>
  <hr/>
  <div class="row-fluid">
    <div class="span12">
      <table class="table" id="category-detail-table">
        <thead>
          <tr>
            <th>ลำดับ</th>
            <th>ชื่อร้าน</th>
            <th>ธนาคาร</th>
            <th>จำนวนเงิน</th>
            <th>วันที่</th>
            <th>เวลา</th>
            <th>หมายเหตุ</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          <?php
            echo $tblPayment; 
          ?>
        </tbody>
      </table>
    </div>
  </div>

  <div class="row-fluid field-submit">
    <div class="span12">
      <?php echo CHtml::link('ย้อนกลับ', Yii::app()->request->baseUrl.'/shop/allShop', array('id' => 'btn-add', 'class' => 'btn btn-success btnLink')); ?>
    </div>
  </div>

</div>
<?php
  $this->endWidget();
?>
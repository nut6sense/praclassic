<?php
  $form = $this->beginWidget('CActiveForm', array(
    'id' => 'payment-history-form',
    'action' => Yii::app()->request->baseUrl . '/shop/renewShop',
  ));

  $model_user = new Users;
  $model_payment = new Payment;

  $inputOption = array('class'=>'span3');
  $htmlOptions = 'class="span3" style="margin-left:65px;"';
?>

<div class="container-fluid">
	<div class="row-fluid">
    <div class="span12"><h1>สถิติการชำระเงินของร้านค้า</h1></div>
  </div>

  <div class="row-fluid">
  	<?php echo $form->hiddenField($model_user, 'user_id', array('value'=>Yii::app()->input->get('param1'))); ?>
  	<?php echo CHtml::hiddenField('shopType', Yii::app()->input->get('param2')); ?>
  </div>

<?php
	if(sizeof($dataShop)!=0 && Yii::app()->input->get('param2')=='allShop') {
		echo CHtml::openTag('table', array('class'=>'table'));
		echo CHtml::openTag('thead');
		echo CHtml::openTag('tr');
    echo CHtml::openTag('th');
    echo CHtml::openTag('label');
    echo 'วันเริ่มต้น';
    echo CHtml::closeTag('label');
    echo CHtml::closeTag('th');
    echo CHtml::openTag('th');
    echo CHtml::openTag('label');
    echo 'วันหมดอายุ';
    echo CHtml::closeTag('label');
    echo CHtml::closeTag('th');
    echo CHtml::openTag('th');
    echo CHtml::openTag('label');
    echo 'วันที่จ่าย';
    echo CHtml::closeTag('label');
    echo CHtml::closeTag('th');
    echo CHtml::openTag('th');
    echo CHtml::openTag('label');
    echo 'เป็นจำนวนเงิน';
    echo CHtml::closeTag('label');
    echo CHtml::closeTag('th');
    echo CHtml::closeTag('tr');
		echo CHtml::closeTag('thead');
		echo CHtml::openTag('tbody');
		foreach ($dataShop as $key => $value) {
			echo CHtml::openTag('tr');
      echo CHtml::openTag('td');
      echo $value['date_begin'];
      echo CHtml::closeTag('td');
      echo CHtml::openTag('td');
      echo $value['date_end'];
      echo CHtml::closeTag('td');
      echo CHtml::openTag('td');
      echo $value['date_pay'];
      echo CHtml::closeTag('td');
      echo CHtml::openTag('td');
      echo number_format($value['pay_amount']);
      echo CHtml::closeTag('td');
      echo CHtml::closeTag('tr');
    }
    echo CHtml::closeTag('tbody');
   	echo CHtml::closeTag('table');

    echo '<hr/>';
	}
?>

  <div class="row-fluid">
  	<div class="span">
  		<label style="font-weight:bold;">รายละเอียดการชำระเงิน</label>
  	</div>
  </div>

  <div class="row-fluid">
  	<div <?php echo $htmlOptions; ?> >
  		<?php echo $form->labelEx($model_payment, 'date_pay'); ?>
  	</div>
  	<div>
  		<?php echo $form->textField($model_payment, 'date_pay', array('class'=>'span4 datepicker','value'=>date('d/m/Y'))); ?>
  	</div>
  </div>

  <div class="row-fluid">
  	<div <?php echo $htmlOptions; ?> >
  		<?php echo $form->labelEx($model_payment, 'pay_amount'); ?>
  	</div>
  	<div class="span4" style="margin-left:0px;">
  		<?php echo $form->textField($model_payment, 'pay_amount', array('class'=>'span')); ?>
  	</div>
  	<div>
  		<label><?php echo '&nbsp&nbspบาท'; ?></label>
  	</div>
  </div>

  <div class="row-fluid">
  	<div <?php echo $htmlOptions; ?> >
  		<?php echo $form->labelEx($model_payment, 'title_pay_type'); ?>
  	</div>
  	<div>
  		<?php echo $form->DropDownList($model_payment, 'pay_type', Payment::getPaymentType(), array('class'=>'span4')); ?>
  	</div>
  </div>

	<div class="row-fluid field-submit">
    <div class="span12">
    	<?php echo $form->hiddenField($model_user, 'user_id', array('value'=>Yii::app()->input->get('param1'))); ?>
  		<?php echo CHtml::hiddenField('shopType', Yii::app()->input->get('param2')); ?>
      <?php echo CHtml::link('ย้อนกลับ', '../../'.Yii::app()->input->get('param2'), array('id' => 'btn-add', 'class' => 'btn btn-success btnLink')); ?>&nbsp;
      <?php echo CHtml::submitButton('บันทึกข้อมูล', array('id' => 'btn-add', 'class' => 'btn btn-info')); ?>
    </div>
  </div>
</div>

<?php
	$this->endWidget();

  Dialog::alertMessage();
?>
<?php
  $form = $this->beginWidget('CActiveForm', array(
    'id' => 'data-shop-form',
    // 'action' => Yii::app()->request->baseUrl . '/shop/delShop',
    'htmlOptions' => array('class' => 'form-horizontal')
  ));
  $textAlign = 'style="text-align:center;"';
?>

<div class="container-fluid">
  <div class="row-fluid">
    <div class="span12"><h1>ข้อมูลร้านค้า</h1></div>
  </div>
  <div class="row-fluid">
    <div class="span4 datashop-picture">
      <div class="row-fluid">
        <div class="span" <?php echo $textAlign; ?> >
          <div class="picShop">
            <?php 
              $picPath = !empty($dataShop['shop_pic'])?Yii::app()->request->baseUrl.'/img/shopPic/'.$dataShop['shop_pic']:Yii::app()->request->baseUrl.'/img/shopPic/none.jpg';
              echo CHtml::image($picPath, null, array('id'=>'Shops_shop_pic'));
            ?>
          </div>
        
        </div>
      </div>
    </div>
    <div class="span8 datashop-detail">
      <div class="row-fluid">
        <div class="span5">
          <?php echo $form->labelEx($shopsModel, 'title_shopid'); ?>
        </div>
        <div class="span7">
          <?php echo $dataShop['sequence']; ?>
        </div>
      </div>

      <div class="row-fluid">
        <div class="span5">
          <?php echo $form->labelEx($shopsModel, 'title_name'); ?>
        </div>
        <div class="span7">
          <?php echo $dataShop['shopname']; ?>
        </div>
      </div>

      <div class="row-fluid">
        <div class="span5">
          <?php echo $form->labelEx($usersModel, 'title_phonenumber'); ?>
        </div>
        <div class="span7">
          <?php echo $dataShop['phonenumber']; ?>
        </div>
      </div>

      <div class="row-fluid">
        <div class="span5">
          <label>ข้อความส่วนตัว</label>
        </div>
        <div class="span7">
          <?php echo $dataShop['privateMassage']; ?>
        </div>
      </div>

      <div class="row-fluid">
        <div class="span5">
          <label>รายการทั้งหมด</label>
        </div>
        <div class="span7">
          <?php echo $dataShop['view']; ?>
        </div>
      </div>

      <div class="row-fluid">
        <div class="span5">
          <label>ผู้เข้าชมทั้งหมด</label>
        </div>
        <div class="span7">
          <?php echo '0'; ?>
        </div>
      </div>

      <div class="row-fluid">
        <div class="span5">
          <?php echo $form->labelEx($paymentModel, 'title_pay_type'); ?>
        </div>
        <div class="span7">
          <?php echo $dataShop['pay_type'].' เดือน'; ?>
        </div>
      </div>

      <div class="row-fluid">
        <div class="span5">
          <?php echo $form->labelEx($paymentModel, 'date_end'); ?>
        </div>
        <div class="span7">
          <?php 
            $exEndDate = explode('-', $dataShop['date_end']);
            echo $exEndDate[2].'/'.$exEndDate[1].'/'.$exEndDate[0]; 
          ?>
        </div>
      </div>

      <div class="row-fluid">
        <div class="span5">
          <label>จำนวนวันคงเหลือ</label>
        </div>
        <div class="span7">
          <?php echo $dataShop['dayBalance'].' วัน'; ?>
        </div>
      </div>

    </div>
  </div>
  <hr/>
  <div class="row-fluid">
    <div class="span12">
      <?php 
        $findMassage = Massage::model()->findByAttributes(array('massagetype'=>'ShopMassage')); 
        if(sizeof($findMassage)!=0) {
          echo $findMassage->massagename;
        }
      ?>
    </div>
  </div>
</div>
	
<?php
	$this->endWidget();
?>
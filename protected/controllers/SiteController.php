<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$this->render('index');
		// $this->redirect(Yii::app()->request->baseUrl.'/site/login');

	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	// public function actionContact()
	// {
	// 	$model=new ContactForm;
	// 	if(isset($_POST['ContactForm']))
	// 	{
	// 		$model->attributes=$_POST['ContactForm'];
	// 		if($model->validate())
	// 		{
	// 			$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
	// 			$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
	// 			$headers="From: $name <{$model->email}>\r\n".
	// 				"Reply-To: {$model->email}\r\n".
	// 				"MIME-Version: 1.0\r\n".
	// 				"Content-type: text/plain; charset=UTF-8";

	// 			mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
	// 			Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
	// 			$this->refresh();
	// 		}
	// 	}
	// 	$this->render('contact',array('model'=>$model));
	// }

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->request->baseUrl.'/shop/dataShop/'.Yii::app()->user->userKey);
				// $this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
	public function actionDiscipline()
	{
		$this->layout='//layouts/noLeftMenu';
		$this->render('disipline');
	}
	public function actionNews()
	{
		$this->layout='//layouts/noLeftMenu';
		$this->render('news');
	}
	public function actionShop_list()
	{
		$this->layout='//layouts/noLeftMenu';
		$this->render('shoplist');
	}
	public function actionShop_standard()
	{
		$this->layout='//layouts/noLeftMenu';
		$this->render('shopstandard');
	}
	public function actionAdviser()
	{
		$this->render('adviser');
	}
	public function actionContactus()
	{
		$this->layout='//layouts/noLeftMenu';
		$this->render('contactus');
	}
	public function actionAddContact()
	{
		$contactModel = new Contact;

		$contactModel->attributes = $_POST['Contact'];
		$contactModel->ipaddress = $_SERVER['REMOTE_ADDR'];
		$contactModel->status = 0;
		//
		$pic_pm = CUploadedFile::getInstance($contactModel, 'picture');
		
		$shopData = null;
		if (!is_null($pic_pm)) {
	    $shopData['picture'] = date('ymdhis').'.jpg';
		}

		$contactModel->created = date('Y-m-d H:i:s');
		$contactModel->param = json_encode($shopData);
		//
		if($contactModel->validate()){
			if($contactModel->save()) {
				if (!is_null($pic_pm)) {
					$pic_pm->saveAs(Yii::app()->basePath . '/../img/contactPic/' . $shopData['picture']);
				}
				Dialog::successMessage('บันทึกข้อมูลเรียบร้อยแล้ว');
			}
		}else{
			Dialog::errorMessage('ไม่สามารถส่งข้อมูลได้');
		}
		if ($_POST['Contact']['type']==1)  {
			$this->redirect('messageToAdmin');
		}else{
			$this->redirect('contactus');
		}
	}

	public function actionProductDetail(){
		$this->render('productDetail');
	}

	public function actionShopDetail(){
		$this->layout='//layouts/noLeftMenu';
		$user_id = Yii::app()->input->get('param1');

		$updateViewShop = Shops::model()->findByAttributes(array('user_id'=>$user_id));
		$updateViewShop->view = $updateViewShop->view + 1;
		$updateViewShop->update();

		$userQuery = Yii::app()->db->createCommand('SELECT *, users.name AS firstname, shops.name AS shopname FROM users INNER JOIN shops ON(users.user_id=shops.user_id) INNER JOIN banks ON(users.user_id=banks.user_id) INNER JOIN payment ON(users.user_id=payment.user_id) WHERE users.user_id = '.$user_id)->query();
		foreach ($userQuery as $uKey => $uValue) {
			$dataShop = $uValue;
		}

		//find amount of day
		$dataShop['dayBalance'] = Shops::findAmountOfDay($dataShop['date_end']);
		//end
		
		$shopName = explode('&&', $dataShop['shopname']);
		$dataShop['shopname'] = isset($shopName[1])?$shopName[1]:$shopName[0];

		$usersModel = new Users;
		$shopsModel = new Shops;
		$banksModel = new Banks;

		$this->render('shopDetail', array('dataShop'=>$dataShop, 'usersModel'=>$usersModel, 'shopsModel'=>$shopsModel, 'banksModel'=>$banksModel));
	}

	public function actionProductDescription() {
		$this->layout='//layouts/noLeftMenu';
		$prd_id = Yii::app()->input->get('param1');

		$updateViewProduct = Products::model()->findByAttributes(array('productid'=>$prd_id));
		$updateViewProduct->view = $updateViewProduct->view + 1;
		$updateViewProduct->update();

		$productQuery = Yii::app()->db->createCommand('SELECT *, users.name AS firstname, shops.name AS shopname FROM users INNER JOIN shops ON(users.user_id=shops.user_id) INNER JOIN products ON(users.user_id=products.user_id) WHERE products.productid='.$prd_id)->query();;
		foreach ($productQuery as $key => $value) {
			$itemsDetail = $value;
		}

		$proName = explode('&&', $itemsDetail['productname']);
    $proName = implode($proName, ' ');
		$itemsDetail['productname'] = $proName;

		$productModel = new Products;
		$shopsModel = new Shops;
		$usersModel = new Users;

		$this->render('productDescription', array('dataProduct'=>$itemsDetail, 'productModel'=>$productModel, 'usersModel'=>$usersModel, 'shopsModel'=>$shopsModel));
	}

	public function actionMessageToAdmin(){
		$this->render('messageToAdmin');
	}
	
	public function actionPrivateMassage() {
		$this->render('privateMassage');
	}

	public function actionAddPrivateMassage() {
		$postContact = $_POST['Contact'];
		$postUsers = $_POST['Users'];
		
		$contactModel = new Contact;
		$contactModel->attributes = $postContact;

		$pic_pm=CUploadedFile::getInstance($contactModel,'picture');

		$shopData = array();
		$shopData['user_id'] = $postUsers['user_id'];

		if (!is_null($pic_pm)) {
	    $shopData['picture'] = $postUsers['user_id'].'-'.date('ymdhis').'.jpg';
		}

		$contactModel->created = date('Y-m-d');
		$contactModel->param = json_encode($shopData);

		if($contactModel->validate()) {
			if($contactModel->save()) {
				if (!is_null($pic_pm)) {
					$pic_pm->saveAs(Yii::app()->basePath . '/../img/contactPic/' . $postUsers['user_id'].'-'.date('ymdhis').'.jpg');
				}
				Dialog::successMessage('ส่งข้อความถึงร้านค้าสำเร็จ');
				$this->redirect('shopDetail/'.$postUsers['user_id']);
			}else{
				Dialog::errorMessage('ไม่สามารถส่งข้อความถึงร้านค้าได้');
				$this->redirect('privateMassage/'.$postUsers['user_id']);
			}
		}else{
			Dialog::errorMessage('ไม่สามารถส่งข้อความถึงร้านค้าได้');
			$this->redirect('privateMassage/'.$postUsers['user_id']);
		}
	}

	public function actionPayConfirm() {
		$this->layout='//layouts/noLeftMenu';
		$this->render('payConfirm');
	}

	public function actionAddPayConfirm(){
		$model = new Payconfirm;

		$checkShop = Shops::model()->getShopIdByName($_POST['Payconfirm']['shop_name']);

		if ($checkShop['result'] == true) {

			$payConPicName = date('dmYHis') . '.jpg';
			$payConPic=CUploadedFile::getInstance($model,'payconfirm_pic');
			$_POST['Payconfirm']['payconfirm_pic'] = $payConPicName;

			$model->attributes=$_POST['Payconfirm'];
			// $model->dateTime = date('Y-m-d H:i:s');
			$model->dateTime = FormatToDate::FormatDateTimeForSave($_POST['Payconfirm']['date'],$_POST['Payconfirm']['time']);
			$model->shop_id = $checkShop['shop_id'];

			if ($model->validate() && $model->save()) {
				$payConPic->saveAs(Yii::app()->basePath . '/../img/payConPic/' . $payConPicName);
				Dialog::successMessage('ส่งข้อความสำเร็จ');
			}else{
				$error = $model->getErrors();
				$getError = null;
				foreach ($error as $key => $value) {
					foreach ($value as $k => $v) {
						$getError .= $v;
					}
				}
				Dialog::errorMessage('ไม่สามารถส่งข้อความได้ ('.$getError.')');
			}
		}else{
			Dialog::errorMessage('ชื่อร้านค้าไม่ถูกต้อง กรุณากรอกใหม่อีกครั้ง');
		}

		$this->redirect('payConfirm');
	}

	public function actionGallery(){
		$getGalleryText = Massage::model()->findByAttributes(array('massagetype'=>'GalleryMassage'));
		$result['text'] = (sizeof($getGalleryText)!=0?$getGalleryText->massagename:null);

		$this->render('gallery', array('galleryText'=>$result['text']));
	}

	public function actionGalleryDescription(){
		$gallery_id = Yii::app()->input->get('param1');
		$dataGallery = Gallery::model()->findByAttributes(array('galleryid'=>$gallery_id));
		$dataGallery->view = $dataGallery->view + 1;
		$dataGallery->update();
		$this->render('galleryDescription', array('dataGallery'=>$dataGallery));
	}

	public function actionPremium(){
		$getPremiumText = Massage::model()->findByAttributes(array('massagetype'=>'PremiumMassage'));
		$result['text'] = (sizeof($getPremiumText)!=0?$getPremiumText->massagename:null);

		$this->render('premium', array('PremiumText'=>$result['text']));
	}

	public function actionPremiumDescription(){
		$premium_id = Yii::app()->input->get('param1');
		$dataPremium = Premium::model()->findByAttributes(array('premiumid'=>$premium_id));
		$dataPremium->view = $dataPremium->view + 1;
		$dataPremium->update();
		$this->render('premiumDescription', array('dataPremium'=>$dataPremium));
	}

	public function actionBlockProduct(){
		if (Yii::app()->request->isAjaxRequest) {
			Products::model()->updateByPk($_POST['productId'],array('approve'=>3));
    }
	}

	public function actionUpdateShopEndDate(){
		if (Yii::app()->request->isAjaxRequest) {
			Payment::checkExpireShop();
			Block::checkBlockShop();

      header('Content-type: application/json');
      echo json_encode(array('result' => 'true'));
    }
	}

}
<?php

class GalleryController extends Controller {

	public $layout = '//layouts/admin';

	public function actionGallery() {
		$this->render('gallery');
	}

	public function actionGalleryForm() {
		$model_gallery = new Gallery;

		if(isset($_POST['gallery_id'])) {
			$dateQuery = Gallery::model()->findByAttributes(array('galleryid' => $_POST['gallery_id']));
			foreach ($dateQuery as $key => $value) {
				$model_gallery[$key] = $value;
			}
		}

		$this->render('galleryForm', array('model_gallery'=>$model_gallery));
	}

	public function actionSubmitGallery() {
		$postGallery = $_POST['Gallery'];
		
		if(empty($postGallery['galleryid'])) {
			$model_gallery = new Gallery;
			$model_gallery->galleryid = null;
			$model_gallery->galleryname = $postGallery['galleryname'];
			$model_gallery->gallerydetail = $_POST['gallerydetail'];
			$model_gallery->gallerydate = date('Y-m-d');
			$model_gallery->category_id = $postGallery['category_id'];
			$model_gallery->view = 0;

			$arrPicName = array();
			foreach ($postGallery['gallerypic'] as $key => $value) {
				$samPic[$key] = CUploadedFile::getInstance($model_gallery,'gallerypic['.$key.'][picName]');
				$arrPicName[$key] = !is_null($samPic[$key])?date('ymdhis').'-'.$key:null;
				$postGallery['gallerypic'][$key]['picName'] = !is_null($samPic[$key])?date('ymdhis').'-'.$key.'.jpg':null;
			}

			$model_gallery->gallerypic = json_encode($postGallery['gallerypic']);

			if($model_gallery->validate()) {
				if($model_gallery->save()){
					foreach ($arrPicName as $key => $value) {
						if (!is_null($samPic[$key])) {
							$samPic[$key]->saveAs(Yii::app()->basePath . '/../img/galleryPic/' . $value . '.jpg');
						}
					}
					Dialog::successMessage('บันทึก Gallery เสร็จสิ้น');
				}else{
					Dialog::errorMessage('ไม่สามารถเพิ่ม Gallery ใหม่ได้');
				}
			}else{
				Dialog::errorMessage('ไม่สามารถเพิ่ม Gallery ใหม่ได้');
				var_dump($model_gallery->getErrors());
			}
		}else{
			$dataQuery = Gallery::model()->findByAttributes(array('galleryid' => $postGallery['galleryid']));
			$dataQuery->galleryname = $postGallery['galleryname'];
			$dataQuery->gallerydetail = $_POST['gallerydetail'];
			$dataQuery->category_id = $postGallery['category_id'];

			$oldPic = array();
			$dbPicArray = json_decode($dataQuery->gallerypic, true);
			foreach ($postGallery['gallerypic'] as $key => $value) {
				$glyPic[$key] = CUploadedFile::getInstance($dataQuery,'gallerypic['.$key.'][picName]');
				if(!is_null($glyPic[$key])) {
					$arrPicName[$key] = !is_null($glyPic[$key])?date('ymdhis').'-'.$key:null;
					array_push($oldPic, $dbPicArray[$key]['picName']);
					$dbPicArray[$key]['picName'] = !is_null($glyPic[$key])?date('ymdhis').'-'.$key.'.jpg':null;
				}
			}
			$dataQuery->gallerypic = json_encode($dbPicArray);
			
			if($dataQuery->update()) {
				foreach ($oldPic as $delKey => $delValue) {
		    	$objScan = scandir('img/galleryPic');
					foreach ($objScan as $value) {
				    if($value == $delValue) {
				    	unlink(getcwd().DIRECTORY_SEPARATOR.'/img/galleryPic/'.$delValue);
				    }
					}
				}

				if(isset($arrPicName)) {
					foreach ($arrPicName as $key => $value) {
						if (!is_null($glyPic[$key])) {
							$glyPic[$key]->saveAs(Yii::app()->basePath . '/../img/galleryPic/' . $value . '.jpg');
						}
					}
				}

				Dialog::successMessage('แก้ไขข้อมูลเสร็จสิ้น');
			}else{
				Dialog::successMessage('ไม่สามารถแก้ไขข้อมูลได้');
			}
		}

		$this->redirect('gallery');
	}

	public function actionDelGallery() {
		if(isset($_POST['GLY_DELETE'])) {
			foreach ($_POST['GLY_DELETE'] as $key => $value) {
				$dataQuery = Gallery::model()->findByAttributes(array('galleryid' => $value));

				$sampleArray = json_decode($dataQuery->gallerypic, true);
				foreach ($sampleArray as $picKey => $picValue) {
					$filename = $picValue['picName'];
					$objScan = scandir('img/galleryPic');
					foreach ($objScan as $sValue) {
				    if($sValue == $filename) {
				    	unlink(getcwd().DIRECTORY_SEPARATOR.'/img/galleryPic/'.$filename);
				    }
					}
				}
				Gallery::model()->deleteAll('galleryid = :glyId' , array('glyId' => $value));

			}

			Dialog::successMessage('ลบ Gallery เสร็จสิ้น');
		}else{
	  	Dialog::errorMessage('ไม่สามารถลบ Gallery ได้ กรุณาเลือก Gallery ที่ต้องการลบ');
		}

		$this->redirect('gallery');
	}

}

?>
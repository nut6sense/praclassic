<?php

class MassageController extends Controller {

	public $layout = '//layouts/admin';

	public function actionShopMassage() {
		$this->render('shopMassage');
	}

	public function actionRuleMassage() {
		$this->render('ruleMassage');
	}

	public function actionGalleryMassage() {
		$this->render('galleryMassage');
	}

	public function actionPremiumMassage() {
		$this->render('premiumMassage');
	}

	public function actionAddMassage() {
		if($_POST['massageType']=='ShopMassage') {
			$massage = $_POST['shop-massage'];
		}elseif($_POST['massageType']=='RuleMassage') {
			$massage = $_POST['rule-massage'];
		}elseif($_POST['massageType']=='GalleryMassage') {
			$massage = $_POST['gallery-massage'];
		}elseif($_POST['massageType']=='PremiumMassage') {
			$massage = $_POST['premium-massage'];
		}

		$command = Yii::app()->db->createCommand();
		$massageModel = new Massage;
		$massageModel->massageid = null;
    $massageModel->massagename = $massage;
    $massageModel->massagetype = $_POST['massageType'];
    $massageModel->user_id = 'Admin';

    if($massageModel->validate()) {
			$massageQuery = Massage::model()->findByAttributes(array('massagetype' => $massageModel->massagetype));
			if(sizeof($massageQuery)!=0) {
				$massageModel->massageid = $massageQuery->massageid;
				Massage::model()->updateByPk($massageQuery->massageid, $massageModel);
			}else{
				$massageModel->save();
			}
		}

		Dialog::successMessage('บันทึกเสร็จสิ้น');
    $this->redirect($massageModel->massagetype);
	}

}

?>
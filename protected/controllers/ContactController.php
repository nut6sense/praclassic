<?php

class ContactController extends Controller {

	public $layout = '//layouts/admin';

	public function actionMessage() {
		$this->render('message');
	}

	public function actionIssue() {
		$this->render('issue');
	}

	public function actionMentor() {
		$this->render('mentor');
	}

	public function actionMessageDetail() {
		if (Yii::app()->request->isAjaxRequest) {
			header('Content-type: application/json');
			$findMessage = Contact::model()->findByAttributes(array('cid'=>$_POST['cid']));
			$result['subject'] = $findMessage['subject'];
			$result['name'] = $findMessage['name'];
			$result['message'] = $findMessage['message'];
			$result['email'] = $findMessage['email'];
			$result['telephone'] = $findMessage['telephone'];
			echo json_encode($result);
		}
	}

	public function actionDelMessage() {
		if(isset($_POST['MESSAGE_DELETE'])) {
			foreach ($_POST['MESSAGE_DELETE'] as $Key => $Value) {
				Contact::model()->deleteAll('cid = :cid' , array('cid' => $Value));
			}

   		Dialog::successMessage('ลบรายการเสร็จสิ้น');
		}else{
   		Dialog::errorMessage('ไม่สามารถลบรายการได้ กรุณาเลือกรายการที่ต้องการลบ');
		}
		
		$this->redirect('message');
	}

	// public function actionPrivateMassageDetail() {
	// 	if (Yii::app()->request->isAjaxRequest) {
	// 		header('Content-type: application/json');
	// 		$result = Contact::getPrivateMassageDetail($_POST);
	// 		echo json_encode($result);
	// 	}
	// }

	public function actionSubmitResendPrivateMessage() {
		$post = $_POST['Contact'];
		$findMessage = Contact::model()->findByAttributes(array('cid'=>$post['cid']));
		$decode = json_decode($findMessage->param, true);

		SendMail::mailTo($findMessage->email, $post['subject'], $post['message']);
		
		$this->redirect(Yii::app()->request->baseUrl.'/shop/viewPrivateMassage/'.$decode['user_id']);
	}

	public function actionContactMessage() {
		$idContact = Yii::app()->input->get('param1');

		$findMessage = Contact::model()->findByAttributes(array('cid'=>$idContact));
		$decode = json_decode($findMessage->param, true);

		switch ($findMessage->type) {
			case 1:
				$topic = array('สอบถามข้อมูล','mentor');
				break;
			case 4:
				$topic = array('แจ้งปัญหา หรือร้องเรียน','issue');
				break;
			case 5:
				$topic = array('ติชมแนะนำเว็บไซต์','message');
				break;

		}
		
		$this->render('contactMessage', array('dataContact'=>$findMessage, 'dataParam'=>$decode, 'topic'=>$topic));
	}

}
<?php

class NewsController extends Controller {

	public $layout = '//layouts/admin';

	public function actionNews() {
		$this->render('news');
	}

	public function actionDelNews() {
		if(isset($_POST['NEWS_DELETE'])) {
			foreach ($_POST['NEWS_DELETE'] as $newsKey => $newsValue) {
				News::model()->deleteAll('newsid = :newsId' , array('newsId' => $newsValue));
			}

			Dialog::successMessage('ลบข่าวประชาสัมพันธ์เสร็จสิ้น');
		}else{
	  	Dialog::errorMessage('ไม่สามารถลบข่าวประชาสัมพันธ์ได้ กรุณาเลือกข่าวประชาสัมพันธ์ต้องการลบ');
		}

		$this->redirect('news');
	}

	public function actionDataNews() {
		$model_news = new News;
		
		if(isset($_POST['news_id'])) {
			$newsQuery = News::model()->findByAttributes(array('newsid' => $_POST['news_id']));
			foreach ($newsQuery as $nKey => $nValue) {
				$model_news[$nKey] = $nValue;
			}
		}

		$this->render('dataNews', array('model_news'=>$model_news));
	}

	public function actionSaveNews() {
		$user_id = Yii::app()->user->userKey;

		if(empty($_POST['News']['newsid'])) {
			$command = Yii::app()->db->createCommand();
			$newsModel = new News;
			$newsModel->newsid = null;
	    $newsModel->user_id = $user_id;
	    $newsModel->newsname = $_POST['News']['newsname'];
	    $newsModel->newssubdetail = $_POST['News']['newssubdetail'];
	    $newsModel->newsdetail = $_POST['newsdetail'];

			$newspic=CUploadedFile::getInstance($newsModel,'newspic');
			$picName = date('YmdHis').'.jpg';
	    $newsModel->newspic = $picName;


	    if($newsModel->validate()) {
	    	$newsModel->save();
				$newspic->saveAs(Yii::app()->basePath . '/../img/newsPic/' . $picName);
				Dialog::successMessage('เพิ่มข้อมูลข่าวประชาสัมพันธ์เสร็จสิ้น');
	    }else{
	    	Dialog::errorMessage('ไม่สามารถเพิ่มข้อมูลข่าวประชาสัมพันธ์ได้');
	    }

	   
	  }elseif(!empty($_POST['News']['newsid'])) {
	  	$findNews = News::model()->findByAttributes(array('newsid'=>$_POST['News']['newsid']));

	  	$findNews->newsid = $_POST['News']['newsid'];
	    $findNews->user_id = $user_id;
	    $findNews->newsname = $_POST['News']['newsname'];
	    $findNews->newssubdetail = $_POST['News']['newssubdetail'];
	    $findNews->newsdetail = $_POST['newsdetail'];
			$newspic=CUploadedFile::getInstance($findNews,'newspic');

	    if($findNews->validate()) {
    		$findNews->update();
    		if (!is_null($newspic)) {
					$newspic->saveAs(Yii::app()->basePath . '/../img/newsPic/' . $findNews->newspic);
    		}
    		Dialog::successMessage('แก้ไขข้อมูลข่าวประชาสัมพันธ์เสร็จสิ้น');
    	}else{
	    	Dialog::errorMessage('ไม่สามารถแก้ไขข้อมูลข่าวประชาสัมพันธ์ได้');
	    }

	  }

    $this->redirect('news');
	}

	public function actionNewsDetail() {
		$this->layout='//layouts/main';
		$news_id = Yii::app()->input->get('param1');
		$newsModel = News::model()->findByAttributes(array('newsid' => $news_id));
		$this->render('newsDetail',array('newsModel'=>$newsModel));
	}

}

?>
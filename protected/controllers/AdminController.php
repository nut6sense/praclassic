<?php

class AdminController extends Controller {

  // public function filters() {
  //   return array(
  //       'accessControl', // perform access control for CRUD operations
  //   );
  // }

	public function actionIndex(){
		if (!isset(Yii::app()->user->userKey)) {
			$this->layout='//layouts/adminLogin';
			$this->render('login');
		}else{
			$this->layout='//layouts/admin';
			$this->render('index');
		}
		
	}

	public $layout = '//layouts/admin';

	public function actionMain(){
		$this->render('index');
	}

	// public function actionLogin(){
	// 	$this->redirect(Yii::app()->request->baseUrl.'/shopMassage/ShopMassage');
	// }

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$this->layout='//layouts/adminLogin';
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid

			$user = Users::model()->findByAttributes(array('username'=>$_POST['LoginForm']['username'],'password'=>md5($_POST['LoginForm']['password']), 'permission'=>'3'));
			if(count($user)==0){
				Dialog::errorMessage('No Data');
				$this->redirect(Yii::app()->request->baseUrl);
			}else{

				if($model->validate() && $model->login()){
					$this->redirect(Yii::app()->request->baseUrl.'/admin');
					// $this->redirect(Yii::app()->request->baseUrl.'/Massage/ShopMassage');
				}else{
					Dialog::errorMessage('No Data');
				}

			}
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */

	public function actionSendSms(){
		$this->render('sendSms');
	}

	public function actionSending(){
		$msisdn = $_POST['phone'];
		$msisdn = preg_replace('#[^0-9]#u', '', $msisdn);
		if (Strlen($msisdn)==10) {
			SMS::SendSMS($msisdn,$_POST['detail']);
			Dialog::successMessage('ส่งข้อความเรียบร้อยแล้ว');
		}else{
			Dialog::errorMessage('หมายเลขโทรศัพท์ไม่ถูกต้อง');
		}

		$this->redirect(Yii::app()->request->baseUrl.'/admin/sendSms');
	}

	public function actionSendEmail(){
		$this->render('sendEmail');
	}

	public function actionSendingMail(){

		$email = $_POST['email'];

		if (eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $email)){ 

			$strTo = $_POST['email'];
			$strSubject = $_POST['subject'];
			$strHeader = "From: Praclassic.com";
			$strMessage = $_POST['message'];
			$flgSend = @mail($strTo,$strSubject,$strMessage,$strHeader);  // @ = No Show Error //
			if($flgSend){
				Dialog::successMessage('ส่งข้อความเรียบร้อยแล้ว');
			}else{
				Dialog::errorMessage('ไม่สามารถส่งข้อความได้');
			}

		}else{
			Dialog::errorMessage('อีเมล์ไม่ถูกต้อง');
		}

		$this->redirect(Yii::app()->request->baseUrl.'/admin/sendEmail');
	}

	public function actionShowPayConfirm(){
		$this->render('payConfirm');
	}

	public function actionSettingWebsite(){
		$this->render('settingWebsite');
	}

	public function actionSettingMain(){
		$this->render('settingMain');
	}

	public function actionChangeNumberMenu(){
		$oldTopMenu = Topmenu::model()->findByAttributes(array('topmenu_number'=>$_POST['oldNumber']));
		$newTopMenu = Topmenu::model()->findByAttributes(array('topmenu_number'=>$_POST['newNumber']));

		$oldTopMenu->topmenu_number = $_POST['newNumber'];
		$newTopMenu->topmenu_number = $_POST['oldNumber'];

		$oldTopMenu->save();
		$newTopMenu->save();

	}

	public function actionChangeTheme(){
		$theme = Setting::model()->findByAttributes(array('setting_id'=>1));

		$setting = json_decode($theme->setting_parameter);
		$setting->theme = $_POST['themeSelect'];

		$theme->setting_parameter = json_encode($setting);
		$theme->save();

	}

	public function actionChangeLayoutNews(){
		$theme = Setting::model()->findByAttributes(array('setting_id'=>1));

		$setting = json_decode($theme->setting_parameter);
		$setting->layoutNews = $_POST['value'];

		$theme->setting_parameter = json_encode($setting);
		$theme->save();
	}

	public function actionSubmitSettingWebsite(){
		if ($_FILES['background_image']['name']!='') {
			$theme = Setting::model()->findByAttributes(array('setting_id'=>1));

			$picName = 'bg_' . date('YmdHis') . '.jpg';
			$uploadPic = CUploadedFile::getInstanceByName("background_image");
			$uploadPic->saveAs(Yii::app()->basePath . '/../img/' . $picName );

			$setting = json_decode($theme->setting_parameter);

			if (isset($setting->background)) {
				if ($setting->background != 'background.jpg') {
	  			unlink(getcwd().DIRECTORY_SEPARATOR.'img/'.$setting->background);
				}
			}

			$setting->background = $picName;

			$theme->setting_parameter = json_encode($setting);
			$theme->save();
		}

		$this->redirect(Yii::app()->request->baseUrl.'/admin/settingWebsite');

	}

	public function actionSettingWebsiteDefault(){
		$theme = Setting::model()->findByAttributes(array('setting_id'=>1));
		$setting = json_decode($theme->setting_parameter);
		
		if (isset($setting->background)) {
			if ($setting->background != 'background.jpg') {
  			unlink(getcwd().DIRECTORY_SEPARATOR.'img/'.$setting->background);
			}
		}

		$setting->background = 'background.jpg';

		$theme->setting_parameter = json_encode($setting);
		$theme->save();

		$this->redirect(Yii::app()->request->baseUrl.'/admin/settingWebsite');
	}

	public function actionChangeBgColor(){
		// var_dump($_POST);	
		$theme = Setting::model()->findByAttributes(array('setting_id'=>1));

		$setting = json_decode($theme->setting_parameter);
		$setting->colorBox1 = $_POST['colorBox1'];
		$setting->colorBox2 = $_POST['colorBox2'];
		$setting->colorBox3 = $_POST['colorBox3'];

		$theme->setting_parameter = json_encode($setting);
		$theme->save();

	}

	public function actionSettingWebsiteColorDefault(){
		$theme = Setting::model()->findByAttributes(array('setting_id'=>1));
		$setting = json_decode($theme->setting_parameter);
		
		$setting->colorBox1 = '#630E0E';
		$setting->colorBox2 = '#630E0E';
		$setting->colorBox3 = '#4a0d05';

		$theme->setting_parameter = json_encode($setting);
		$theme->save();

		$this->redirect(Yii::app()->request->baseUrl.'/admin/settingWebsite');
	}

	public function actionChangeFontColor(){
		// var_dump($_POST);	
		$theme = Setting::model()->findByAttributes(array('setting_id'=>1));

		$setting = json_decode($theme->setting_parameter);
		$setting->fontColorHeader = $_POST['fontColorHeader'];
		$setting->fontColorContent = $_POST['fontColorContent'];

		$theme->setting_parameter = json_encode($setting);
		$theme->save();

	}

	public function actionSettingWebsiteFontColorDefault(){
		$theme = Setting::model()->findByAttributes(array('setting_id'=>1));
		$setting = json_decode($theme->setting_parameter);
		
		$setting->fontColorHeader = '#d3b748';
		$setting->fontColorContent = '#ffffff';

		$theme->setting_parameter = json_encode($setting);
		$theme->save();

		$this->redirect(Yii::app()->request->baseUrl.'/admin/settingWebsite');
	}

	public function actionChangeThemeColor(){
		// var_dump($_POST);	exit();
		$theme = Setting::model()->findByAttributes(array('setting_id'=>1));
		$setting = json_decode($theme->setting_parameter);

		if ($_POST['themeColor']!='') {
			$colorCode = explode('); ', $_POST['themeColor']);
			$colorCode = substr($colorCode[0], 22);
		}else{
			$colorCode = null;
		}
		

		$setting->themeColor = $colorCode;
		$setting->themeColorCode = $_POST['themeColorCode'];

		$theme->setting_parameter = json_encode($setting);
		$theme->save();

	}

	public function actionSettingWebsiteThemeColorDefault(){
		$theme = Setting::model()->findByAttributes(array('setting_id'=>1));
		$setting = json_decode($theme->setting_parameter);
		
		$setting->themeColor = null;
		$setting->themeColorCode = null;

		$theme->setting_parameter = json_encode($setting);
		$theme->save();

		$this->redirect(Yii::app()->request->baseUrl.'/admin/settingWebsite');
	}

	public function actionEditProfile(){
		$user_id = 'Admin';
		// $user_id = Yii::app()->input->get('param1');

		$model_user = new Users;
		$userQuery = Users::model()->findByAttributes(array('user_id' => $user_id));
		foreach ($userQuery as $uKey => $uValue) {
			$model_user[$uKey] = $uValue;
		}
		$model_user['confirmPassword'] = null;
		$model_user['password'] = null;

		// $model_shop = new Shops;
		// $shopQuery = Shops::model()->findByAttributes(array('user_id' => $user_id));
		// foreach ($shopQuery as $sKey => $sValue) {
		// 	$model_shop[$sKey] = $sValue;
		// }
		// $shopName = explode('&&', $model_shop['name']);
		// $model_shop['shotName'] = isset($shopName[0])?$shopName[0]:null;
		// $model_shop['fullName'] = isset($shopName[1])?$shopName[1]:null;

		// $model_bank = new Banks;
		// $bankQuery = Banks::model()->findByAttributes(array('user_id' => $user_id));
		// foreach ($bankQuery as $bKey => $bValue) {
		// 	$model_bank[$bKey] = $bValue;
		// }

		$this->render('editProfile', array('model_user'=>$model_user));
		// $this->render('profileShop', array('model_user'=>$model_user, 'model_shop'=>$model_shop, 'model_bank'=>$model_bank));
	}

	public function actionSaveProfile() {
		$command = Yii::app()->db->createCommand();
		
		if($_POST['Users']['password']==$_POST['Users']['confirmPassword'] && !empty($_POST['Users']['password']) && !empty($_POST['Users']['confirmPassword'])) {
			$_POST['Users']['password'] = md5($_POST['Users']['password']);
		}elseif($_POST['Users']['password']!=$_POST['Users']['confirmPassword'] || !empty($_POST['Users']['password']) && !empty($_POST['Users']['confirmPassword'])) {
			Dialog::errorMessage('รหัสผ่านไม่ตรงกัน ไม่สามารถแก้ไขข้อมูลได้');
			$this->redirect('profileShop/'.$_POST['Users']['user_id']);
		}

		if(empty($_POST['Users']['password'])) {
			unset($_POST['Users']['password']);
		}
		
    $model_user = Users::model()->findByAttributes(array('user_id' => $_POST['Users']['user_id']));
  	$model_user->attributes = $_POST['Users'];

    if($model_user->validate()) {
    	Users::model()->updateAll($model_user, 'user_id = "Admin"');

			Dialog::successMessage('แก้ไขข้อมูลร้านค้าเสร็จสิ้น');
    }else{
    	Dialog::errorMessage('ไม่สามารถแก้ไขข้อมูลร้านค้าได้');
    }

		$this->redirect('editProfile');
	}

	public function actionSubmitSettingSMS(){
		$findSetting = Setting::model()->findByAttributes(array('setting_id'=>1));
		$setting = json_decode($findSetting->setting_parameter);

		$setting->smsUser = $_POST['sms_username'];
		$setting->smsPass = $_POST['sms_password'];
		$setting->smsType = $_POST['sms_type'];

		$findSetting->setting_parameter = json_encode($setting);
		$findSetting->save();

		$this->redirect(Yii::app()->request->baseUrl.'/admin/settingMain');

	}

	public function actionSettingSMSDefault(){
		$findSetting = Setting::model()->findByAttributes(array('setting_id'=>1));
		$setting = json_decode($findSetting->setting_parameter);
		
		$setting->smsUser = '08XXXXXXXX';
		$setting->smsPass = 'XXXX';
		$setting->smsType = 'standard';

		$findSetting->setting_parameter = json_encode($setting);
		$findSetting->save();

		$this->redirect(Yii::app()->request->baseUrl.'/admin/settingMain');
	}

	public function actionSubmitSettingEmail(){
		$findSetting = Setting::model()->findByAttributes(array('setting_id'=>1));
		$setting = json_decode($findSetting->setting_parameter);

		$setting->emailMain = $_POST['setting_email'];

		$findSetting->setting_parameter = json_encode($setting);
		$findSetting->save();

		$this->redirect(Yii::app()->request->baseUrl.'/admin/settingMain');

	}

	public function actionSettingEmailDefault(){
		$findSetting = Setting::model()->findByAttributes(array('setting_id'=>1));
		$setting = json_decode($findSetting->setting_parameter);
		
		$setting->emailMain = 'admin@praclassic.com';

		$findSetting->setting_parameter = json_encode($setting);
		$findSetting->save();

		$this->redirect(Yii::app()->request->baseUrl.'/admin/settingMain');
	}

	public function actionDelPayConfirm() {
		if(isset($_POST['PAY_DELETE'])) {

			$pathConfirmPicture = getcwd().DIRECTORY_SEPARATOR.'img/payConPic/';
			foreach ($_POST['PAY_DELETE'] as $payKey => $payValue) {
				$findPayConfirm = Payconfirm::model()->findByAttributes(array('payconfirm_id'=>$payValue));
				unlink($pathConfirmPicture.$findPayConfirm->payconfirm_pic);
				Payconfirm::model()->deleteAll('payconfirm_id = :confirmId' , array('confirmId' => $payValue));
			}
   		Dialog::successMessage('ลบร้านค้าเสร็จสิ้น');
		}else{
   		Dialog::errorMessage('ไม่สามารถลบร้านค้าได้ กรุณาเลือกร้านค้าที่ต้องการลบ');
		}
		
		$this->redirect(Yii::app()->request->baseUrl.'/admin/ShowPayConfirm');
	}

}

?>
<?php

class AnnounceController extends Controller {

	public $layout = '//layouts/admin';

	public function actionAnnounce() {
		$this->render('announce');
	}

	public function actionDelAnnounce() {
		if(isset($_POST['ANNOUNCE_DELETE'])) {
			foreach ($_POST['ANNOUNCE_DELETE'] as $announceKey => $announceValue) {
				Announce::model()->deleteAll('announceid = :announceId' , array('announceId' => $announceValue));
			}

			Dialog::successMessage('ลบประกาศเสร็จสิ้น');
		}else{
	  	Dialog::errorMessage('ไม่สามารถลบประกาศได้ กรุณาเลือกประกาศต้องการลบ');
		}

		$this->redirect('announce');
	}

	public function actionDataAnnounce() {
		$model_announce = new Announce;
		$findAnnounce = Announce::model()->findAll();
		if (count($findAnnounce)>0) {
			$_POST['announce_id'] = 1;
		}
		if(isset($_POST['announce_id'])) {
			$announceQuery = Announce::model()->findByAttributes(array('announceid' => $_POST['announce_id']));
			foreach ($announceQuery as $aKey => $aValue) {
				$model_announce[$aKey] = $aValue;
			}
		}

		$this->render('dataAnnounce', array('model_announce'=>$model_announce));
	}

	public function actionSaveAnnounce() {
		$user_id = Yii::app()->user->userKey;

		if(empty($_POST['Announce']['announceid'])) {
			$command = Yii::app()->db->createCommand();
			$dataModel = new Announce;
			$dataModel->announceid = null;
	    $dataModel->user_id = $user_id;
	    $dataModel->announcename = $_POST['Announce']['announcename'];
	    $dataModel->announcesubdetail = $_POST['Announce']['announcesubdetail'];
	    $dataModel->announcedetail = $_POST['announcedetail'];

	    if($dataModel->validate()) {
	    	$dataModel->save();
	    }

	    Dialog::successMessage('เพิ่มข้อมูลประกาศเสร็จสิ้น');
	  }elseif(!empty($_POST['Announce']['announceid'])) {
	  	$findAnnounce = Announce::model()->findByAttributes(array('announceid'=>$_POST['Announce']['announceid']));
	  	$findAnnounce->announceid = $_POST['Announce']['announceid'];
	    $findAnnounce->user_id = $user_id;
	    $findAnnounce->announcename = $_POST['Announce']['announcename'];
	    $findAnnounce->announcesubdetail = $_POST['Announce']['announcesubdetail'];
	    $findAnnounce->announcedetail = $_POST['announcedetail'];

	    if($findAnnounce->validate()) {
    		$findAnnounce->update();
	    }

    	Dialog::successMessage('แก้ไขข้อมูลประกาศเสร็จสิ้น');
	  }

    

    $this->redirect('dataAnnounce');
	}

}

?>
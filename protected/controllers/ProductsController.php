<?php

class ProductsController extends Controller {

	public $layout = '//layouts/shop';

	public function actionDataProduct() {
		$this->render('products');
	}

	public function actionAddProduct() {
		$postPrd = $_POST['Products'];
		$postCat = $_POST['Category'];
		$userId = $_POST['user_id'];

		if(!empty($postPrd['productid'])) {
			$dataQuery = Products::model()->findByAttributes(array('productid' => $postPrd['productid']));
			$dataQuery->productname = $postPrd['productname'].'&&'.$postPrd['productName2'];
			$dataQuery->category_id = $postCat['category_id'];
			$dataQuery->productdetail = $postPrd['productdetail'];
			$dataQuery->productstatus = $postPrd['productstatus'];
			if($dataQuery->productstatus==6) {
				$dataQuery->productprice = $postPrd['productprice'];
			}else{
				$dataQuery->productprice = null;
			}

			$oldPic = array();
			$dbPicArray = json_decode($dataQuery->productpic, true);
			foreach ($postPrd['productpic'] as $key => $value) {
				$prdPic[$key] = CUploadedFile::getInstance($dataQuery,'productpic['.$key.'][picName]');
				if(!is_null($prdPic[$key])) {
					$arrPicName[$key] = !is_null($prdPic[$key])?$userId.'-'.date('ymdhis').'-'.$key:null;
					array_push($oldPic, $dbPicArray[$key]['picName']);
					$dbPicArray[$key]['picName'] = !is_null($prdPic[$key])?$userId.'-'.date('ymdhis').'-'.$key.'.jpg':null;
				}
			}
			$dataQuery->productpic = json_encode($dbPicArray);
			
			if($dataQuery->update()) {
				foreach ($oldPic as $delKey => $delValue) {
		    	$objScan = scandir('img/productPic');
					foreach ($objScan as $value) {
				    if($value == $delValue) {
				    	unlink(getcwd().DIRECTORY_SEPARATOR.'/img/productPic/'.$delValue);
				    }
					}
					$objScan = scandir('img/productPicOriginal');
					foreach ($objScan as $value) {
				    if($value == $delValue) {
				    	unlink(getcwd().DIRECTORY_SEPARATOR.'/img/productPicOriginal/'.$delValue);
				    }
					}
					$objScan = scandir('img/productThumb');
					foreach ($objScan as $value) {
				    if($value == $delValue) {
				    	unlink(getcwd().DIRECTORY_SEPARATOR.'/img/productThumb/'.$delValue);
				    }
					}
				}

				if(isset($arrPicName)) {
					foreach ($arrPicName as $key => $value) {
						if (!is_null($prdPic[$key])) {
							$prdPic[$key]->saveAs(Yii::app()->basePath . '/../img/productPic/' . $value . '.jpg');

							$srcfile = getcwd().DIRECTORY_SEPARATOR.'img/productPic/' . $value . '.jpg';
							$dstfile = getcwd().DIRECTORY_SEPARATOR.'img/productPicOriginal/' . $value . '.jpg';
							copy($srcfile, $dstfile);

							if($key == 1){
								$thumbfile = getcwd().DIRECTORY_SEPARATOR.'img/productThumb/' . $value . '.jpg';
								copy($srcfile, $thumbfile);
								ResizeImage::resize($thumbfile);
							}

  						imageLicense::addLogoLicense(getcwd().DIRECTORY_SEPARATOR.'img/productPic/' . $value . '.jpg');
						}
					}
				}

				Dialog::successMessage('แก้ไขข้อมูลเสร็จสิ้น');
			}else{
				Dialog::successMessage('ไม่สามารถแก้ไขข้อมูลได้');
			}
		}else{
			$postPrd['productname'] = $postPrd['productname'].'&&'.$postPrd['productName2'];

			$model_product = new Products;
			$model_product->attributes = $postPrd;
			$model_product->user_id = $userId;
			$model_product->category_id = $postCat['category_id'];
			$model_product->productdate = date('Y-m-d');
			$model_product->productnoted = 0;
			$model_product->view = 0;
			
			$findShop = Shops::model()->findByAttributes(array('user_id'=>$userId));
			$model_product->approve = empty($findShop->referrance)?0:1;
			
			$arrPicName = array();
			foreach ($postPrd['productpic'] as $key => $value) {
				$samPic[$key] = CUploadedFile::getInstance($model_product,'productpic['.$key.'][picName]');
				$arrPicName[$key] = !is_null($samPic[$key])?$userId.'-'.date('ymdhis').'-'.$key:null;
				$postPrd['productpic'][$key]['picName'] = !is_null($samPic[$key])?$userId.'-'.date('ymdhis').'-'.$key.'.jpg':null;
			}	

			$model_product->productpic = json_encode($postPrd['productpic']);
			
			if($model_product->validate()) {
				if($model_product->save()){
					foreach ($arrPicName as $key => $value) {
						if (!is_null($samPic[$key])) {
							$samPic[$key]->saveAs(Yii::app()->basePath . '/../img/productPic/' . $value . '.jpg');

							$srcfile = getcwd().DIRECTORY_SEPARATOR.'img/productPic/' . $value . '.jpg';
							$dstfile = getcwd().DIRECTORY_SEPARATOR.'img/productPicOriginal/' . $value . '.jpg';
							copy($srcfile, $dstfile);

							if($key == 1){
								$thumbfile = getcwd().DIRECTORY_SEPARATOR.'img/productThumb/' . $value . '.jpg';
								copy($srcfile, $thumbfile);
								ResizeImage::resize($thumbfile);
							}

  						imageLicense::addLogoLicense(getcwd().DIRECTORY_SEPARATOR.'img/productPic/' . $value . '.jpg');
						}
					}
					Dialog::successMessage('บันทึกเสร็จสิ้น');
				}else{
					Dialog::errorMessage('ไม่สามารถเพิ่มพระใหม่ได้');
				}
			}else{
				Dialog::errorMessage('ไม่สามารถเพิ่มพระใหม่ได้');
				var_dump($model_product->getErrors());
			}
		}

		$this->redirect('dataProduct/'.$userId);
	}

	public function actionDelProducts() {
		$user_id = $_POST['user_id'];
		if(isset($_POST['PRD_DELETE'])) {
			foreach ($_POST['PRD_DELETE'] as $key => $value) {
				$dataQuery = Products::model()->findByAttributes(array('productid' => $value));

				$sampleArray = json_decode($dataQuery->productpic, true);
				foreach ($sampleArray as $picKey => $picValue) {
					$filename = $picValue['picName'];
					$objScan = scandir('img/productPic');
					foreach ($objScan as $sValue) {
				    if($sValue == $filename) {
				    	unlink(getcwd().DIRECTORY_SEPARATOR.'/img/productPic/'.$filename);
				    }
					}
				}
				Products::model()->deleteAll('productid = :prdId' , array('prdId' => $value));
			}

			Dialog::successMessage('ลบสินค้าเสร็จสิ้น');
		}else{
	  	Dialog::errorMessage('ไม่สามารถลบสินค้าได้ กรุณาเลือกสินค้าต้องการลบ');
		}

		$this->redirect('dataProduct/'.$user_id);
	}

	public function actionPopularProduct() {
		$this->render('popular');
	}

	public function actionEditProducts() {
		$user_id = $_POST['user_id'];
		if(isset($_POST['PRD_POPULAR'])) {
			$popular_id = array();
			foreach ($_POST['PRD_POPULAR'] as $key => $value) {
				$dataQuery = Products::model()->findByAttributes(array('productid' => $value));
				$dataQuery->productnoted = 1;
				$dataQuery->update();

				array_push($popular_id, $value);
			}
			$where = "WHERE productid NOT IN (".implode(',', $popular_id).") and user_id = '$user_id'";
		}else{
			$where = null;
		}
		$unChkQuery = Yii::app()->db->createCommand('SELECT productid FROM products '.$where)->query();
		foreach ($unChkQuery as $unChkKey => $unChkValue) {
			$unChkDataQuery = Products::model()->findByAttributes(array('productid' => $unChkValue['productid']));
			$unChkDataQuery->productnoted = 0;
			$unChkDataQuery->update();
		}

		Dialog::successMessage('แก้ไขข้อมูลพระเด่นเสร็จสิ้น');

		$this->redirect('popularProduct/'.$user_id);
	}

	public function actionProductReferences() {
		$this->layout='//layouts/admin';
		$this->render('productReferences');
	}

	public function actionApproveProduct(){
		if (Yii::app()->request->isAjaxRequest) {
			$dataQuery = Products::model()->findByAttributes(array('productid' => $_POST['productid']));
			$dataQuery->approve = 1;
			$dataQuery->update();
		}
	}

	public function actionEditProduct() {
		$user_id = Yii::app()->input->get('param1');
		$product_id = Yii::app()->input->get('param2');
		if(!is_null($product_id)) {
			$result = Products::getProdcutDetail($product_id);
		}else{
			$result = new Products;
		}
		$this->render('editProduct', array('dataProduct'=>$result, 'formPage'=>'dataProduct'));
	}

	public function actionReferencesProduct() {
		$this->layout='//layouts/admin';
		$product_id = Yii::app()->input->get('param1');
		if(!is_null($product_id)) {
			$result = Products::getProdcutDetail($product_id);
		}else{
			$result = new Products;
		}
		$this->render('editProduct', array('dataProduct'=>$result, 'formPage'=>'references'));
	}

	public function actionBanProduct(){
		$this->layout='//layouts/admin';
		$this->render('banProduct');
	}

	public function actionUnbanProduct(){
		if (Yii::app()->request->isAjaxRequest) {
			Products::model()->updateByPk($_POST['productId'],array('approve'=>1));
    }
	}

}

?>
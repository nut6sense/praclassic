<?php

class PremiumController extends Controller {

	public $layout = '//layouts/admin';

	public function actionPremium() {
		$this->render('premium');
	}

	public function actionPremiumForm() {
		$model_premium = new Premium;

		if(isset($_POST['premium_id'])) {
			$dateQuery = Premium::model()->findByAttributes(array('premiumid' => $_POST['premium_id']));
			foreach ($dateQuery as $key => $value) {
				$model_premium[$key] = $value;
			}
		}

		$this->render('premiumForm', array('model_premium'=>$model_premium));
	}

	public function actionSubmitPremium() {
		$postPremium = $_POST['Premium'];
		
		if(empty($postPremium['premiumid'])) {
			$model_premium = new Premium;
			$model_premium->premiumid = null;
			$model_premium->premiumname = $postPremium['premiumname'];
			$model_premium->premiumdetail = $_POST['premiumdetail'];
			$model_premium->premiumdate = date('Y-m-d');
			$model_premium->category_id = $postPremium['category_id'];
			$model_premium->view = 0;
			$model_premium->default = $postPremium['default'];

			if($postPremium['default'] == 1) {
				Premium::model()->updateAll(array('default'=>0));
			}

			$arrPicName = array();
			foreach ($postPremium['premiumpic'] as $key => $value) {
				$prmPic[$key] = CUploadedFile::getInstance($model_premium,'premiumpic['.$key.'][picName]');
				$arrPicName[$key] = !is_null($prmPic[$key])?date('ymdhis').'-'.$key:null;
				$postPremium['premiumpic'][$key]['picName'] = !is_null($prmPic[$key])?date('ymdhis').'-'.$key.'.jpg':null;
			}

			$model_premium->premiumpic = json_encode($postPremium['premiumpic']);

			if($model_premium->validate()) {
				if($model_premium->save()){
					foreach ($arrPicName as $key => $value) {
						if (!is_null($prmPic[$key])) {
							$prmPic[$key]->saveAs(Yii::app()->basePath . '/../img/premiumPic/' . $value . '.jpg');
						}
					}
					Dialog::successMessage('บันทึก Premium เสร็จสิ้น');
				}else{
					Dialog::errorMessage('ไม่สามารถเพิ่ม Premium ใหม่ได้');
				}
			}else{
				Dialog::errorMessage('ไม่สามารถเพิ่ม Premium ใหม่ได้');
				var_dump($model_premium->getErrors());
			}
		}else{
			$dataQuery = Premium::model()->findByAttributes(array('premiumid' => $postPremium['premiumid']));
			$dataQuery->premiumname = $postPremium['premiumname'];
			$dataQuery->premiumdetail = $_POST['premiumdetail'];
			$dataQuery->category_id = $postPremium['category_id'];
			$dataQuery->default = $postPremium['default'];

			if($postPremium['default'] == 1) {
				Premium::model()->updateAll(array('default'=>0));
			}

			$oldPic = array();
			$dbPicArray = json_decode($dataQuery->premiumpic, true);
			foreach ($postPremium['premiumpic'] as $key => $value) {
				$prmPic[$key] = CUploadedFile::getInstance($dataQuery,'premiumpic['.$key.'][picName]');
				if(!is_null($prmPic[$key])) {
					$arrPicName[$key] = !is_null($prmPic[$key])?date('ymdhis').'-'.$key:null;
					array_push($oldPic, $dbPicArray[$key]['picName']);
					$dbPicArray[$key]['picName'] = !is_null($prmPic[$key])?date('ymdhis').'-'.$key.'.jpg':null;
				}
			}
			$dataQuery->premiumpic = json_encode($dbPicArray);
			
			if($dataQuery->update()) {
				foreach ($oldPic as $delKey => $delValue) {
		    	$objScan = scandir('img/premiumPic');
					foreach ($objScan as $value) {
				    if($value == $delValue) {
				    	unlink(getcwd().DIRECTORY_SEPARATOR.'/img/premiumPic/'.$delValue);
				    }
					}
				}

				if(isset($arrPicName)) {
					foreach ($arrPicName as $key => $value) {
						if (!is_null($prmPic[$key])) {
							$prmPic[$key]->saveAs(Yii::app()->basePath . '/../img/premiumPic/' . $value . '.jpg');
						}
					}
				}

				Dialog::successMessage('แก้ไขข้อมูลเสร็จสิ้น');
			}else{
				Dialog::successMessage('ไม่สามารถแก้ไขข้อมูลได้');
			}
		}

		$this->redirect('premium');
	}

	public function actionDelPremium() {
		if(isset($_POST['PRM_DELETE'])) {
			foreach ($_POST['PRM_DELETE'] as $key => $value) {
				$dataQuery = Premium::model()->findByAttributes(array('premiumid' => $value));

				$sampleArray = json_decode($dataQuery->premiumpic, true);
				foreach ($sampleArray as $picKey => $picValue) {
					$filename = $picValue['picName'];
					$objScan = scandir('img/premiumPic');
					foreach ($objScan as $sValue) {
				    if($sValue == $filename) {
				    	unlink(getcwd().DIRECTORY_SEPARATOR.'/img/premiumPic/'.$filename);
				    }
					}
				}
				Premium::model()->deleteAll('premiumid = :prmId' , array('prmId' => $value));

			}

			Dialog::successMessage('ลบ Premium เสร็จสิ้น');
		}else{
	  	Dialog::errorMessage('ไม่สามารถลบ Premium ได้ กรุณาเลือก Premium ที่ต้องการลบ');
		}

		$this->redirect('premium');
	}

}

?>
<?php

class CategoryController extends Controller {

	public $layout = '//layouts/admin';

	public function actionCategory() {
		$this->render('category');
	}

	public function actionAddCategory()	{	
		if(empty($_POST['Category']['category_id'])) {
			if(!empty($_POST['Category']['category_name'])) {
				$command = Yii::app()->db->createCommand();
				$categoryModel = new Category;
				$categoryModel->category_id = null;
		    $categoryModel->category_name = $_POST['Category']['category_name'];

		    if($categoryModel->validate()) {
		    	$categoryModel->save();
		    }

	   		Dialog::successMessage('เพิ่มข้อมูลหมวดหมู่พระเสร็จสิ้น');
		  }else{
		  	Dialog::errorMessage('ไม่สามารถเพิ่มข้อมูลหมวดหมู่พระได้');
		  }
		}else{
			if(!empty($_POST['Category']['category_name'])) {
				$command = Yii::app()->db->createCommand();
				$categoryModel = new Category;
				$categoryModel->category_id = $_POST['Category']['category_id'];
		    $categoryModel->category_name = $_POST['Category']['category_name'];

		    if($categoryModel->validate()) {
		    	category::model()->updateByPk($_POST['Category']['category_id'], $categoryModel);
		    }

				Dialog::successMessage('แก้ไขหมวดหมู่พระเสร็จสิ้น');
		  }else{
		  	Dialog::errorMessage('ไม่สามารถแก้ไขหมวดหมู่พระได้');
		  }
		}

  	$this->redirect('category');
	}

	public function actionDelCategory() {
		if(isset($_POST['CAT_DELETE'])) {
			foreach ($_POST['CAT_DELETE'] as $catKey => $catValue) {
				Category::model()->deleteAll('category_id = :catId' , array('catId' => $catValue));
			}

			Dialog::successMessage('ลบหมวดหมู่พระเสร็จสิ้น');
		}else{
	  	Dialog::errorMessage('ไม่สามารถลบหมวดหมู่พระได้ กรุณาเลือกหมวดหมู่พระที่ต้องการลบ');
		}

		$this->redirect('category');
	}

	public function actionDataCategory() {
		$category_id = Yii::app()->input->get('param1');

		$result = new Category;
		if(!is_null($category_id)) {
			$result = category::getCategoryDetail($category_id);
		}
		$this->render('dataCategory', array('dataCategory'=>$result));
	}

}

?>
<?php

class ShopController extends Controller {

	public function actionLogin(){
		// $this->layout='//layouts/adminLogin';
		Yii::app()->request->baseUrl = Yii::app()->request->getBaseUrl(true);
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form'){
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
		// collect user input data
		if(isset($_POST['LoginForm'])){
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			$user = Users::model()->findByAttributes(array('username'=>$_POST['LoginForm']['username'],'password'=>md5($_POST['LoginForm']['password']), 'permission'=>'1'));
			if(count($user)==0){
				Dialog::errorMessage('No Data');
				$this->redirect(Yii::app()->request->baseUrl);
			}else{
				if($user->active==0) {
					$findBlockDate = block::model()->findByAttributes(array('user_id'=>$user->user_id));
					$blockExplode = explode('-', $findBlockDate->date_end);
					$thai_month_arr=array(0=>'', 1=>'มกราคม', 2=>'กุมภาพันธ์', 3=>'มีนาคม', 4=>'เมษายน', 5=>'พฤษภาคม', 6=>'มิถุนายน', 7=>'กรกฎาคม', 8=>'สิงหาคม', 9=>'กันยายน', 10=>'ตุลาคม', 11=>'พฤศจิกายน', 12=>'ธันวาคม');
					Dialog::errorMessage('ร้านค้าของคุณถูกระงับจนถึงวันที่ <br>'.$blockExplode[2].' '.$thai_month_arr[$blockExplode[1]].' '.($blockExplode[0]+543));
					$this->redirect(Yii::app()->request->baseUrl);
				}elseif($user->active==2) {
					Dialog::errorMessage('ร้านค้าของคุณหมดอายุ ไม่สามาระถเข้าสู่ระบบได้ครับ');
					$this->redirect(Yii::app()->request->baseUrl);
				}else{
					if($model->validate() && $model->login()){
						$this->redirect(Yii::app()->request->baseUrl.'/shop/dataShop/'.Yii::app()->user->userKey);
					}else{
						Dialog::errorMessage('No Data');
						$this->redirect(Yii::app()->request->baseUrl);
					}
				}

			}
		}
		// display the login form
		// $this->render('login',array('model'=>$model));
	}

	public function actionRegister(){
		// $this->layout='//layouts/main';
		$this->layout='//layouts/noLeftMenu';
		$this->render('register');
	}

	public function actionIndex(){
		$this->layout='//layouts/shop';
		$this->redirect('index');
	}

	public function actionAddUser(){

		if ($_POST['Shops']['code_input']!=$_POST['Shops']['code_hidden']) {
		  Dialog::errorMessage('ข้อความไม่ตรงกับรูปภาพ');
			$this->redirect('register');
		}

		$postUser = $_POST['Users'];
		$queryUser = Yii::app()->db->createCommand("SELECT `id` FROM `users` order by `id` DESC Limit 1")->query();

		if(count($queryUser)!=0){
			foreach ($queryUser as $key => $value) {
				$userId = date('dmY').sprintf('%03d',($value['id']+1));
			}
		}else{
			$userId = date('dmY').sprintf('%03d',1);
		}
		//
		// Create Users
		//
		$userModel = new Users;

    $chkUserName = Users::model()->findByAttributes(array('username' => $postUser['username']));

		if (is_null($chkUserName)) {

			if($postUser['password']==$postUser['confirmPassword']){

				$userModel['user_id'] = $userId;
				$userModel['username'] = $postUser['username'];
				$userModel['password'] = md5($postUser['password']);
				$userModel['name'] = $postUser['name'];
				$userModel['surname'] = $postUser['surname'];
				$userModel['email'] = $postUser['email'];
				$userModel['phonenumber'] = $postUser['phonenumber'];
				$userModel['iden_id'] = $postUser['iden_id'];
				
				$chkIdenId = !empty($userModel['iden_id'])?Users::checkIdenId($userModel['iden_id']):$userModel['iden_id'];

				$idenPic=CUploadedFile::getInstance($userModel,'iden_picture');

				$userModel['iden_picture'] = $userId;
				$userModel['permission'] = 0;
				$userModel['active'] = 1;
				$userModel['sequence'] = null;
				//
				//Create Shops
				//
				$postShop = $_POST['Shops'];
				$shopModel = new Shops;

				$shopModel['user_id'] = $userId;
				$shopModel['name'] = $postShop['shotName'].'&&'.$postShop['fullName'];
				$shopModel['detail'] = $postShop['detail'];
				$shopModel['rule'] = $postShop['rule'];
				$shopModel['map'] = $postShop['map'];
				// $shopModel['cover'] = $postShop['cover'];
				$shopModel['referrance'] = $postShop['referrance'];
				$shopModel['referrance_phonenumber'] = $postShop['referrance_phonenumber'];

				$shopModel['shotName'] = $postShop['shotName'];
				$shopModel['fullName'] = $postShop['fullName'];

				$arrPicName = array();
				foreach ($postShop['sample_pic'] as $key => $value) {
					foreach ($value['picName'] as $subKey => $subValue) {
						$samPic[$key][$subKey] = CUploadedFile::getInstance($shopModel,'sample_pic['.$key.'][picName]['.$subKey.']');
						$arrPicName[$key][$subKey] = $userId.'-'.$key.'-'.$subKey;
						$postShop['sample_pic'][$key]['picName'][$subKey] = is_null($samPic[$key][$subKey])?null:$userId.'-'.$key.'-'.$subKey.'.jpg';
					}
				}
				$shopModel['sample_pic'] = json_encode($postShop['sample_pic']);
				//
				//Create Banks
				//
				$postBanks = $_POST['Banks'];
				$bankModel = new Banks;

				$bankModel['user_id'] = $userId;
				$bankModel['bank_name'] = $postBanks['bank_name'];
				$bankModel['bank_type'] = $postBanks['bank_type'];
				$bankModel['bank_Branch'] = $postBanks['bank_Branch'];
				$bankModel['account_name'] = $postBanks['account_name'];
				$bankModel['account_number'] = $postBanks['account_number'];
				//
				//Create Payment
				//
				$postPayment = $_POST['Payment'];
				$paymentModel = new Payment;

				$paymentModel['user_id'] = $userId;
				$paymentModel['pay_type'] = $postPayment['pay_type'];
				$paymentModel['date_begin'] = date('Y-m-d');
				$paymentModel['pay_status'] = 0;
				//
				//SaveModel
				//
				if($userModel->validate() && $shopModel->validate() && $bankModel->validate() && $paymentModel->validate() && $chkIdenId==true){

					if($userModel->save() && $shopModel->save() && $bankModel->save() && $paymentModel->save()){
						if (!is_null($idenPic)) {
							$idenPic->saveAs(Yii::app()->basePath . '/../img/iden/' . $userId . '.jpg');
						}

						foreach ($arrPicName as $key => $value) {
							foreach ($value as $subKey => $subValue) {
								if (!is_null($samPic[$key][$subKey])) {
									$samPic[$key][$subKey]->saveAs(Yii::app()->basePath . '/../img/samplePic/' . $subValue . '.jpg');
								}
							}
						}

						Dialog::successMessage('บันทึกเสร็จสิ้น');

					}else{
			  		Dialog::errorMessage('ไม่สามารถบันทึกได้');
					}

				}else{
					$error = null;
					// foreach ($userModel->getErrors() as $key => $value) {
					// 	foreach ($value as $Ekey => $Evalue) {
					// 		$error .= $Evalue;
					// 		$error .= '<br>';
					// 	}
					// }
					// foreach ($shopModel->getErrors() as $key => $value) {
					// 	foreach ($value as $Ekey => $Evalue) {
					// 		$error .= $Evalue;
					// 		$error .= '<br>';
					// 	}
					// }
					// foreach ($bankModel->getErrors() as $key => $value) {
					// 	foreach ($value as $Ekey => $Evalue) {
					// 		$error .= $Evalue;
					// 		$error .= '<br>';
					// 	}
					// }
					// foreach ($paymentModel->getErrors() as $key => $value) {
					// 	foreach ($value as $Ekey => $Evalue) {
					// 		$error .= $Evalue;
					// 		$error .= '<br>';
					// 	}
					// }

					Dialog::errorMessage('ไม่สามารถบันทึกได้<br><br>'.$error);

					// var_dump($userModel->getErrors());
					// var_dump($shopModel->getErrors());
					// var_dump($bankModel->getErrors());
					// var_dump($paymentModel->getErrors());
					// exit();
				}

			}else{
		  	Dialog::errorMessage('รหัสผ่านไม่ตรงกัน');
			}

		}else{
		  Dialog::errorMessage('มีชื่อผู้ใช้นี้แล้ว');
		}

	  $this->redirect('register');

	}

	public function actionNewShop() {
		$this->layout='//layouts/admin';
		$this->render('newShop');
	}

	public function actionMailMember() {
		$this->layout='//layouts/admin';
		$this->render('mailMember');
	}

	public function actionDelShop() {
		if(isset($_POST['SHOP_DELETE'])) {
			foreach ($_POST['SHOP_DELETE'] as $usersKey => $usersValue) {
				Users::model()->deleteAll('user_id = :userId' , array('userId' => $usersValue));
				Shops::model()->deleteAll('user_id = :userId' , array('userId' => $usersValue));
				Banks::model()->deleteAll('user_id = :userId' , array('userId' => $usersValue));
				Payment::model()->deleteAll('user_id = :userId' , array('userId' => $usersValue));
			}

   		Dialog::successMessage('ลบร้านค้าเสร็จสิ้น');
		}else{
   		Dialog::errorMessage('ไม่สามารถลบร้านค้าได้ กรุณาเลือกร้านค้าที่ต้องการลบ');
		}
		
		$this->redirect($_POST['shopType']);
	}

	public function actionAllShop() {
		$this->layout='//layouts/admin';
		$this->render('allShop');
	}

	public function actionExpireShop() {
		$this->layout='//layouts/admin';
		$this->render('expireShop');
	}

	public function actionBanShop() {
		$this->layout='//layouts/admin';
		$this->render('banShop');
	}

	public function actionBanMonthShop() {
		$this->layout='//layouts/admin';
		$this->render('banMonthShop');
	}

	public function actionBanOtherShop() {
		$this->layout='//layouts/admin';
		$this->render('banOtherShop');
	}

	public $layout = '//layouts/shop';

	public function actionDataShop() {
		$user_id = Yii::app()->input->get('param1');
		
		$userQuery = Yii::app()->db->createCommand('SELECT *, users.name AS firstname, shops.name AS shopname FROM users INNER JOIN shops ON(users.user_id=shops.user_id) INNER JOIN banks ON(users.user_id=banks.user_id) INNER JOIN payment ON(users.user_id=payment.user_id) WHERE users.user_id = '.$user_id)->query();
		foreach ($userQuery as $uKey => $uValue) {
			$dataShop = $uValue;
		}

		//find amount of day
			$endExplode = explode('-', $dataShop['date_end']);
			$endDay = $endExplode[2];
			$endMonth = $endExplode[1];
			$endYear = $endExplode[0];
			$endDate = mktime(0, 0, 0, $endMonth, $endDay, $endYear);

			$nowDay = date('d');
			$nowMonth = date('m');
			$nowYear = date('Y');
			$nowDate = mktime(0, 0, 0, $nowMonth, $nowDay, $nowYear);

			$DateNum = ceil(($endDate - $nowDate)/86400);

			$dataShop['dayBalance'] = $DateNum;
		//end

		$pmCount = 0;
		$contactQuery = Contact::model()->findAll(array('order'=>'cid'));
		foreach ($contactQuery as $key => $value) {
			$decode = json_decode($value->param, true);
			if(isset($decode['user_id'])) {	
				if($decode['user_id']==$user_id) {
					$pmCount++;
				}
			}
		}
		$dataShop['privateMassage'] = $pmCount;

		$shopName = explode('&&', $dataShop['shopname']);
		$dataShop['shopname'] = isset($shopName[1])?$shopName[1]:$shopName[0];

		$usersModel = new Users;
		$shopsModel = new Shops;
		$paymentModel = new Payment;

		$this->render('dataShop', array('dataShop'=>$dataShop, 'usersModel'=>$usersModel, 'shopsModel'=>$shopsModel, 'paymentModel'=>$paymentModel));
	}

	public function actionAnnounceShop() {
		$this->render('announceShop');
	}

	public function actionNewsShop() {
		$this->render('newsShop');
	}

	public function actionEditShop()	{
    $data_user = $_POST['Users'];
    foreach ($data_user as $uKey => $uValue) {
        $model_user->$uKey = $uValue;
    }

    if(!empty($data_user['sequence'])) {
    	$model_user->sequence = $data_user['sequence'];
    }else{
    	$findOldSequence = Users::model()->findByAttributes(array('user_id'=>$_POST['Users']['user_id']));
    	$model_user->sequence = $findOldSequence->sequence;
    }

    $findDataUserModel = Users::model()->findByAttributes(array('user_id'=>$model_user->user_id));
    if($findDataUserModel->username != $model_user->username) {
    	$findUsername = Users::model()->findByAttributes(array('username'=>$model_user->username));
    	if(sizeof($findUsername)!=0) {
    		Dialog::successMessage('ไม่สามารถแก้ไขข้อมูลร้านค้าได้');
				$this->redirect('shopDetail/'.$_POST['Users']['user_id'].'/'.$_POST['formPage']);
    	}
    }

    if(!empty($model_user->password)) {
    	$model_user->password = md5($model_user->password);
    }else{
    	unset($model_user->password);
    }
    Users::model()->updateAll($model_user, 'user_id = "'.$_POST['Users']['user_id'].'"');

    $data_shop = $_POST['Shops'];
    foreach ($data_shop as $sKey => $sValue) {
        $model_shop->$sKey = $sValue;
    }
    $model_shop->name = $model_shop->shotName.'&&'.$model_shop->fullName;
    Shops::model()->updateAll($model_shop, 'user_id = "'.$_POST['Users']['user_id'].'"');

    $data_bank = $_POST['Banks'];
    foreach ($data_bank as $bKey => $bValue) {
        $model_bank->$bKey = $bValue;
    }
    Banks::model()->updateAll($model_bank, 'user_id = "'.$_POST['Users']['user_id'].'"');

    if(!empty($data_user['block_user'])) {
    	Block::blockUser($model_user);
    }

    Dialog::successMessage('แก้ไขข้อมูลร้านค้าเสร็จสิ้น');
		$this->redirect('shopDetail/'.$_POST['Users']['user_id'].'/'.$_POST['formPage']);
	}

	public function actionConfirmShop()	{
		$findUsers = Users::model()->findByAttributes(array('user_id'=>$_POST['Users']['user_id']));

		$findSequence = Users::model()->findByAttributes(array('permission'=>1, 'active'=>1), array('order'=>'sequence desc'));
		if(sizeof($findSequence)==0) {
			$findUsers->sequence = 1;
			$findUsers->update();
		}else{
			$findUsers->sequence = $findSequence->sequence+1;
			$findUsers->update();
		}

		$msisdn = $findUsers->phonenumber;
		$msisdn = preg_replace('#[^0-9]#u', '', $msisdn);
		
		$findUsers->permission = 1;
		$findUsers->update(array('permission'));

		if(!empty($_POST['Payment']['date_pay'])) {
			$exPay = explode('/', $_POST['Payment']['date_pay']);
			$_POST['Payment']['date_pay'] = $exPay[2].'-'.$exPay[1].'-'.$exPay[0];
		}

		if($_POST['Payment']['pay_type']==12) {
			$day_plus = 366*1;
		}elseif($_POST['Payment']['pay_type']==24) {
			$day_plus = 366*2;
		}elseif($_POST['Payment']['pay_type']==36) {
			$day_plus = 366*3;
		}

		$findPayment = Payment::model()->findByAttributes(array('user_id'=>$_POST['Users']['user_id'], 'pay_status'=>0));
		$explodeEndDate = explode("-", $findPayment->date_begin);
		$endDate = date('Y-m-d', mktime(0, 0, 0, $explodeEndDate[1], $explodeEndDate[2]+$day_plus, $explodeEndDate[0]));
		$findPayment->attributes = $_POST['Payment'];
		$findPayment->pay_status = 1;
		$findPayment->date_end = $endDate;
		$findPayment->update();

		//send_sms
		SMS::SendSMS($msisdn);
		
		Dialog::successMessage('ยืนยันข้อมูลร้านค้าเสร็จสิ้น');
		$this->redirect('newShop');
	}

	public function actionProfileShop() {
		$user_id = Yii::app()->input->get('param1');

		$model_user = new Users;
		$userQuery = Users::model()->findByAttributes(array('user_id' => $user_id));
		foreach ($userQuery as $uKey => $uValue) {
			$model_user[$uKey] = $uValue;
		}
		$model_user['confirmPassword'] = null;
		$model_user['password'] = null;

		$model_shop = new Shops;
		$shopQuery = Shops::model()->findByAttributes(array('user_id' => $user_id));
		foreach ($shopQuery as $sKey => $sValue) {
			$model_shop[$sKey] = $sValue;
		}
		$shopName = explode('&&', $model_shop['name']);
		$model_shop['shotName'] = isset($shopName[0])?$shopName[0]:null;
		$model_shop['fullName'] = isset($shopName[1])?$shopName[1]:null;

		$model_bank = new Banks;
		$bankQuery = Banks::model()->findByAttributes(array('user_id' => $user_id));
		foreach ($bankQuery as $bKey => $bValue) {
			$model_bank[$bKey] = $bValue;
		}

		$this->render('profileShop', array('model_user'=>$model_user, 'model_shop'=>$model_shop, 'model_bank'=>$model_bank));
	}

	public function actionSaveProfileShop() {
		$command = Yii::app()->db->createCommand();
		
		if($_POST['Users']['password']==$_POST['Users']['confirmPassword'] && !empty($_POST['Users']['password']) && !empty($_POST['Users']['confirmPassword'])) {
			$_POST['Users']['password'] = md5($_POST['Users']['password']);
		}elseif($_POST['Users']['password']!=$_POST['Users']['confirmPassword'] || !empty($_POST['Users']['password']) && !empty($_POST['Users']['confirmPassword'])) {
			Dialog::errorMessage('รหัสผ่านไม่ตรงกัน ไม่สามารถแก้ไขข้อมูลได้');
			$this->redirect('profileShop/'.$_POST['Users']['user_id']);
		}

		if(empty($_POST['Users']['password'])) {
			unset($_POST['Users']['password']);
		}
		
    $model_user = Users::model()->findByAttributes(array('user_id' => $_POST['Users']['user_id']));
  	$model_user->attributes = $_POST['Users'];

		$shopModel = new Shops;
		$shop_pic = CUploadedFile::getInstance($shopModel, 'shop_pic');
		$shopPicName = date('YmdHis').'.jpg';

		// if(!is_null($shop_pic)) {
		// 	$filename = $_POST['Users']['user_id'] . '.jpg';
  //   	$objScan = scandir('img/shopPic');
		// 	foreach ($objScan as $value) {
		//     if($value == $filename) {
		//     	unlink(getcwd().DIRECTORY_SEPARATOR.'/img/shopPic/'.$filename);
		//     }
		// 	}
  //   }

  	$model_shop = Shops::model()->findByAttributes(array('user_id' => $_POST['Users']['user_id']));
  	$oldPicName = $model_shop->shop_pic;

  	$_POST['Shops']['name'] = $_POST['Shops']['shotName'].'&&'.$_POST['Shops']['fullName'];
		$_POST['Shops']['shop_pic'] = !is_null($shop_pic)?$shopPicName:$oldPicName;
  	$model_shop->attributes = $_POST['Shops'];

    $model_bank = Banks::model()->findByAttributes(array('user_id' => $_POST['Users']['user_id']));
  	$model_bank->attributes = $_POST['Banks'];

    if($model_user->validate() && $model_shop->validate() && $model_bank->validate()) {
    	Users::model()->updateAll($model_user, 'user_id = "'.$_POST['Users']['user_id'].'"');
    	Shops::model()->updateAll($model_shop, 'user_id = "'.$_POST['Users']['user_id'].'"');
    	if(!is_null($shop_pic)) {

    		$objScan = scandir('img/shopPic');
				foreach ($objScan as $value) {
			    if($value == $oldPicName) {
	  				unlink(Yii::app()->basePath . '/../img/shopPic/' . $oldPicName);
			    }
				}
				$objScan = scandir('img/shopPicOriginal');
				foreach ($objScan as $value) {
			    if($value == $oldPicName) {
	  				unlink(Yii::app()->basePath . '/../img/shopPicOriginal/' . $oldPicName);
			    }
				}
				$objScan = scandir('img/shopThumb');
				foreach ($objScan as $value) {
			    if($value == $oldPicName) {
	  				unlink(Yii::app()->basePath . '/../img/shopThumb/' . $oldPicName);
			    }
				}
				
				// $shopPicName = date('YmdHis');
    		$shop_pic->saveAs(Yii::app()->basePath . '/../img/shopPic/' . $shopPicName);

    		$srcfile = getcwd().DIRECTORY_SEPARATOR.'img/shopPic/' . $shopPicName;
				$dstfile = getcwd().DIRECTORY_SEPARATOR.'img/shopPicOriginal/' . $shopPicName;
				copy($srcfile, $dstfile);

				$thumbfile = getcwd().DIRECTORY_SEPARATOR.'img/shopThumb/' . $shopPicName;
				copy($srcfile, $thumbfile);
				// ResizeImage::resize($thumbfile);
    	}
    	Banks::model()->updateAll($model_bank, 'user_id = "'.$_POST['Users']['user_id'].'"');

			Dialog::successMessage('แก้ไขข้อมูลร้านค้าเสร็จสิ้น');
    }else{
    	Dialog::errorMessage('ไม่สามารถแก้ไขข้อมูลร้านค้าได้');
    }

		$this->redirect('profileShop/'.$_POST['Users']['user_id']);
	}

	public function actionAddAnnounceMassage() {
		$user_id = $_POST['user_id'];

		$model_shopMassage = new Shopsmassage;
		$model_shopMassage->shopsmassageid = null;
		$model_shopMassage->user_id = $user_id;
		$model_shopMassage->shopsmassagedetail = $_POST['announce-shop-massage'];
		$model_shopMassage->shopsmassagetype = 'announce';

		if($model_shopMassage->validate()) {

			$findQuery = Shopsmassage::model()->findByAttributes(array('shopsmassagetype'=>'announce', 'user_id'=>$user_id));
			if(sizeof($findQuery)!=0) {
				$findQuery->shopsmassagedetail = $_POST['announce-shop-massage'];
      	$findQuery->update();

      	Dialog::successMessage('แก้ไขประกาศร้านค้าเสร็จสิ้น');
			}else{
				if($model_shopMassage->save()) {
					Dialog::successMessage('เพิ่มประกาศร้านค้าเสร็จสิ้น');
				}else{
					Dialog::errorMessage('ไม่สามารถเพิ่มประกาศร้านค้าได้');
				}
			}

		}else{
			Dialog::errorMessage('ไม่สามารถเพิ่มประกาศร้านค้าได้');
		}

		$this->redirect('announceShop/'.$user_id);
	}

	public function actionAddNewsMassage() {
		$user_id = $_POST['user_id'];

		$model_shopMassage = new Shopsmassage;
		$model_shopMassage->shopsmassageid = null;
		$model_shopMassage->user_id = $user_id;
		$model_shopMassage->shopsmassagedetail = $_POST['news-shop-massage'];
		$model_shopMassage->shopsmassagetype = 'news';

		if($model_shopMassage->validate()) {

			$findQuery = Shopsmassage::model()->findByAttributes(array('shopsmassagetype'=>'news', 'user_id'=>$user_id));
			if(sizeof($findQuery)!=0) {
				$findQuery->shopsmassagedetail = $_POST['news-shop-massage'];
      	$findQuery->update();

      	Dialog::successMessage('แก้ไขข่าวประชาสัมพันธ์ร้านค้าเสร็จสิ้น');
			}else{
				if($model_shopMassage->save()) {
					Dialog::successMessage('เพิ่มข่าวประชาสัมพันธ์ร้านค้าเสร็จสิ้น');
				}else{
					Dialog::errorMessage('ไม่สามารถเพิ่มข่าวประชาสัมพันธ์ร้านค้าได้');
				}
			}

		}else{
			Dialog::errorMessage('ไม่สามารถเพิ่มข่าวประชาสัมพันธ์ร้านค้าได้');
		}

		$this->redirect('newsShop/'.$user_id);
	}

	public function actionShowStoreMember() {
		$user_id = Yii::app()->input->get('param1');

		// $userQuery = Yii::app()->db->createCommand('SELECT *, users.name AS firstname, shops.name AS shopname FROM users INNER JOIN shops ON(users.user_id=shops.user_id) INNER JOIN banks ON(users.user_id=banks.user_id) INNER JOIN payment ON(users.user_id=payment.user_id) WHERE users.user_id = '.$user_id)->query();
		// foreach ($userQuery as $uKey => $uValue) {
		// 	$dataShop = $uValue;
		// }

		// $usersModel = new Users;
		// $shopsModel = new Shops;

		// $this->render('showStoreMember', array('dataShop'=>$dataShop, 'usersModel'=>$usersModel, 'shopsModel'=>$shopsModel));
	}

	public function actionCancelBlock() {
		$user_id = Yii::app()->input->get('param1');
		$fromPage = Yii::app()->input->get('param2');

		Users::renewSequence($user_id);
		Users::model()->updateAll(array('active'=>1), 'user_id = "'.$user_id.'"');
		Block::model()->deleteAll('user_id = :user_id' , array('user_id' => $user_id));

		Dialog::successMessage('ยกเลิกการระงับการใช้งานเสร็จสิ้น');

		$this->redirect('../../'.$fromPage);
	}

	public function actionRenewShop() {
		$postUsers = $_POST['Users'];

		$expireQuery = Payment::model()->findByAttributes(array('user_id' => $postUsers['user_id'], 'pay_status'=>'1'));
		if(!is_null($expireQuery)) {
			$expireQuery->pay_status = 2;
			$expireQuery->update();

			$endDateOld = $expireQuery->date_end;
		}else{
			$findEndDate = Payment::model()->findByAttributes(array('user_id' => $postUsers['user_id']), array('order'=>'pay_id desc'));
			$endDateOld = $findEndDate->date_end;
		}
		
		$_POST['Payment']['date_begin'] = $endDateOld;

		if($_POST['Payment']['pay_type']==12) {
			$day_plus = 366*1;
		}elseif($_POST['Payment']['pay_type']==24) {
			$day_plus = 366*2;
		}elseif($_POST['Payment']['pay_type']==36) {
			$day_plus = 366*3;
		}

		$explodeEndDate = explode("-", $endDateOld);
		$_POST['Payment']['date_end'] = date('Y-m-d', mktime(0, 0, 0, $explodeEndDate[1], $explodeEndDate[2]+$day_plus, $explodeEndDate[0]));

		if(!empty($_POST['Payment']['date_pay'])) {
			$exPay = explode('/', $_POST['Payment']['date_pay']);
			$_POST['Payment']['date_pay'] = $exPay[2].'-'.$exPay[1].'-'.$exPay[0];
		}
		$postPayment = $_POST['Payment'];
		
		$paymentModel = new Payment;
		$paymentModel->attributes = $postPayment;

		$paymentModel['user_id'] = $postUsers['user_id'];
		$paymentModel['pay_status'] = 1;

		if($paymentModel->validate()){
			$paymentModel->save();
			Users::model()->updateAll(array('active'=>1), 'user_id = "'.$postUsers['user_id'].'"');
		}

		Dialog::successMessage('ต่ออายุการใช้งานเสร็จสิ้น');

		$this->redirect($_POST['shopType']);
	}

	public function actionChangeLogo(){
		$this->render('changeLogo');
	}

	public function actionSubmitChangeLogo(){
		$shopModel = new Shops;
		$cover=CUploadedFile::getInstance($shopModel,'cover');

		if (!is_null($cover)) {

	    $findShop = Shops::model()->findByAttributes(array('user_id' => $_POST['Shops']['user_id']));
			$explodeShopname = explode('&&', $findShop['name']);
			$oldPicName = $findShop['cover'];
			$findShop['shotName'] = $explodeShopname[0];
			$findShop['fullName'] = $explodeShopname[1];
			$findShop['cover'] = date('YmdHis').'.jpg';

	  	if ($findShop->save()) {
	  		if(!is_null($oldPicName)) {
	  			file_exists(Yii::app()->basePath . '/../img/logo/' . $oldPicName)?unlink(Yii::app()->basePath . '/../img/logo/' . $oldPicName):null;
				}
				$cover->saveAs(Yii::app()->basePath . '/../img/logo/' . date('YmdHis') . '.jpg');
				Dialog::successMessage('เแก้ไขโลโก้ร้านสำเร็จ');
	  	}else{
				Dialog::errorMessage('ไม่สามารถเปลี่ยนโลโก้ร้านได้');
	  	}

		}else{
			Dialog::errorMessage('ไม่สามารถเปลี่ยนโลโก้ร้านได้');
		}

		$this->redirect('changeLogo/'.$_POST['Shops']['user_id']);
	}

	public function actionDefaultLogo(){
		$user_id = Yii::app()->input->get('param1');

    $findShop = Shops::model()->findByAttributes(array('user_id' => $user_id));
		$explodeShopname = explode('&&', $findShop['name']);

		$oldPicName = $findShop['cover'];
		// $findShop['shotName'] = $explodeShopname[0];
		// $findShop['fullName'] = $explodeShopname[1];
		$findShop['cover'] = null;

		if ($findShop->update()) {
  		file_exists(Yii::app()->basePath . '/../img/logo/' . $oldPicName)?unlink(Yii::app()->basePath . '/../img/logo/' . $oldPicName):null;
			Dialog::successMessage('เแก้ไขโลโก้ร้านสำเร็จ');
  	}else{
			Dialog::errorMessage('ไม่สามารถเปลี่ยนโลโก้ร้านได้');
  	}

		$this->redirect('../changeLogo/'.$user_id);
	}

	public function actionAddNoteShop(){
		$post_user = $_POST['Users'];
		$post_shop = $_POST['Shops'];
		$shopQuery = Shops::model()->findByAttributes(array('user_id' => $post_user['user_id']));
		$shopQuery->note = $post_shop['note'];

		if($shopQuery->update()) {
			Dialog::successMessage('บันทึกข้อมูลหมายเหตุเสร็จสิ้น');
		}else{
			Dialog::errorMessage('ไม่สามารถบันทึกข้อมูลหมายเหตุได้');
		}

		$this->redirect('noteDetail/'.$post_user['user_id'].'/'.$_POST['shopType']);
	}

	public function actionViewPrivateMassage() {
		$user_id = Yii::app()->input->get('param1');

		$this->render('viewPrivateMassage');
	}

	public function actionResendPrivateMessage() {
		$contact_id = Yii::app()->input->get('param2');

		$dataContact = Contact::getPrivateMassageDetail($contact_id);

		$this->render('resendPrivateMessage', array('dataContact'=>$dataContact));
	}

	public function actionDelPrivateMassage() {
		if(isset($_POST['PM_DELETE'])) {
			foreach ($_POST['PM_DELETE'] as $key => $value) {
				$contactQuery = Contact::model()->findByAttributes(array('cid' => $value));
				$json_param = $contactQuery->param;
				$json_decode = json_decode($json_param, true);
				if(isset($json_decode['picture'])) {
					unlink(getcwd().DIRECTORY_SEPARATOR.'img/contactPic/'.$json_decode['picture']);
				}

				Contact::model()->deleteAll('cid = :contact_id' , array('contact_id' => $value));
			}
			Dialog::successMessage('ลบข้อความส่วนตัวเสร็จสิ้น');
		}else{
	  	Dialog::errorMessage('ไม่สามารถลบข้อความส่วนตัวได้ กรุณาเลือกข้อความส่วนตัวที่ต้องการลบ');
		}

		$this->redirect('viewPrivateMassage/'.$_POST['Users']['user_id']);
	}

	public function actionForgetPassword() {
		$this->layout='//layouts/main';
		$this->render('forgetPassword');
	}

	public function actionSubmitForgetPassword() {
		$post = $_POST['Users'];
		if(!empty($post['username'])) {
			$usersQuery = Users::model()->findByAttributes(array('username' => $post['username']));
			if(sizeof($usersQuery)!=0) {
				$characters = '0123456789abcdefghijklmnopqrstuvwxyz';
		    $randstring = null;
		    for ($i=0;$i<6;$i++) {
	        $randstring .= $characters[rand(0, strlen($characters)-1)];
		    }
		    $usersQuery->password = md5($randstring);

		    $strTo = $usersQuery->email;
				$strSubject = 'รหัสผ่านใหม่ขอคุณ';
				$strHeader = "From: Praclassic.com";
				$strMessage = 'รหัสผ่านใหม่ของคุณ'.$usersQuery->name.' '.$usersQuery->surname.' คือ '.$randstring;
				$flgSend = @mail($strTo,$strSubject,$strMessage,$strHeader);
				if($flgSend){
					$usersQuery->save();
					Dialog::successMessage('ส่งข้อความเรียบร้อยแล้ว');
				}else{
					Dialog::errorMessage('ไม่สามารถส่งข้อความได้');
				}
			}else{
				Dialog::errorMessage('ไม่พบชื่อผู้ใช้งานนี้ในระบบ');
			}
		}else{
			Dialog::errorMessage('กรุณาระบุชื่อผู้ใช้งาน เพื่อใช้ในการข้อมูลรหัสผ่านใหม่ค่ะ');
		}
		$this->redirect('forgetPassword');
	}

	public function actionShopDetail() {
		$this->layout='//layouts/admin';
		$user_id = Yii::app()->input->get('param1');
		$formPage = Yii::app()->input->get('param2');
		$result = Shops::getShopDetail($user_id);
		$this->render('shopDetail', array('dataShop'=>$result, 'formPage'=>$formPage));
	}

	public function actionFormConfirmShop() {
		$this->layout='//layouts/admin';
		$user_id = Yii::app()->input->get('param1');
		$formPage = Yii::app()->input->get('param2');
		$result = Shops::getShopDetail($user_id);
		$this->render('shopDetail', array('dataShop'=>$result, 'formPage'=>$formPage));
	}

	public function actionPaymentDetail() {
		$this->layout='//layouts/admin';
		$user_id = Yii::app()->input->get('param1');
		$formPage = Yii::app()->input->get('param2');
		$result = Payment::getPaymentDetail($user_id);
		$this->render('paymentDetail', array('dataShop'=>$result, 'formPage'=>$formPage));
	}

	public function actionNoteDetail(){
		$this->layout='//layouts/admin';
		$user_id = Yii::app()->input->get('param1');
		$formPage = Yii::app()->input->get('param2');
		$result = Shops::getNoteDetail($user_id);
		$this->render('noteDetail', array('dataShop'=>$result, 'formPage'=>$formPage));
	}

	public function actionPaymentHistory() {
		$this->layout='//layouts/admin';
		$user_id = Yii::app()->input->get('param1');
		$formPage = Yii::app()->input->get('param2');
		$result = Payment::getPaymentHistory($user_id);
		$this->render('paymentHistory', array('dataShop'=>$result, 'formPage'=>$formPage));
	}

	public function actionAllPayment(){
		$this->layout='//layouts/admin';
		$this->render('allPayment');
	}

	public function actionDocument() {
		$this->layout='//layouts/admin';
		$user_id = Yii::app()->input->get('param1');
		$dataShop = Users::model()->findByAttributes(array('user_id' => $user_id));
		$findShopId = Shops::model()->findByAttributes(array('user_id' => $user_id));
		$tblPayment = Payconfirm::tblPayConfirmDataShop($findShopId->shopid);
		$this->render('document', array('tblPayment'=>$tblPayment, 'dataShop'=>$dataShop));
	}

	public function actionSendShopEmail() {
		$this->layout='//layouts/admin';
		$user_id = Yii::app()->input->get('param1');

		$dataShop = Shops::model()->findByAttributes(array('user_id' => $user_id));
		$dataUser = Users::model()->findByAttributes(array('user_id' => $user_id));

		$this->render('sendShopEmail', array('dataShop'=>$dataShop, 'dataUser'=>$dataUser));
	}

	public function actionSubmitSendShopEmail() {
		$post = $_POST;
		$userData = Users::model()->findByAttributes(array('user_id'=>$post['user_id']));

		SendMail::mailTo($userData->email, $post['subject'], $post['message']);

		$this->redirect(Yii::app()->request->baseUrl.'/shop/'.$post['fromPage']);
	}

}

?>
<?php

class FormatToDate {
  public function FormatDate($date) {
  	
  	$date = explode('-', $date);
  	$date = $date['2'].'/'.$date['1'].'/'.$date['0'];

  	return $date;
	}

	public function FormatDateForSave($date) {
  	
  	$date = explode('/', $date);
  	$date = $date['2'].'-'.$date['1'].'-'.$date['0'];

  	return $date;
	}

  public function FormatTime($time) {
    
    $time = explode(':', $time);
    $time = $time['0'].'.'.$time['1'].' น.';

    return $time;
  }

	public function FormatDateTimeForSave($date,$time) {
  	
  	$date = explode('/', $date);
  	$date = $date['2'].'-'.$date['1'].'-'.$date['0'];

  	$time = explode(' ', $time);
  	$time = explode('.', $time[0]);
  	preg_match("/[[:digit:]]+\.?[[:digit:]]*/", $time['1'] , $m );
  	$time = $time[0].':'.$m[0].':00';

  	$dateTime = $date.' '.$time;

  	return $dateTime;
	}

}

?>
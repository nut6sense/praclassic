<?php

class Highslide {

  public function PicHighslide($imgSrc, $width = null, $height = null) {
  	$width = !is_null($width)?'width:'.$width.'px;':null;
  	$height = !is_null($height)?'height:'.$height.'px;':null;

    $img = CHtml::image($imgSrc, null, array('style'=>$width.$height));
    return CHtml::link($img, $imgSrc, array('class'=>'highslide','onclick'=>'return hs.expand(this)'));
  }

  public function IdPicHighslide($imgSrc, $imgIcon = null, $width = null, $height = null) {
    $img = CHtml::image($imgIcon, null, array('style'=>'width:25px;'));
    return CHtml::link($img, $imgSrc, array('class'=>'highslide','onclick'=>'return hs.expand(this)'));
  }

  public function PicProduct($imgSrc, $width = null, $height = null) {
    // $width = !is_null($width)?'width:'.$width.'px;':null;
    // $height = !is_null($height)?'height:'.$height.'px;':null;
    // $img = CHtml::image($imgSrc, null, array('style'=>$width.$height));
    $img = CHtml::image($imgSrc, null, array('class'=>'picProductFix'));
    return CHtml::link($img, $imgSrc, array('class'=>'highslide','onclick'=>'return hs.expand(this)'));
  }

}
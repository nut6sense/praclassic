<?php
	class Statistics {

	  public function getCountShop() {
	  	$shopAll = Users::model()->findAllByAttributes(array('permission'=>'1','active'=>'1'));
	  	return number_format(count($shopAll));
	  }

	  public function getCountProduct() {
	  	// $proAll = Products::model()->findAll();
	  	$proAll = Yii::app()->db->createCommand()
	  		->select('COUNT(products.user_id)')
		  	->from('users')
		  	->leftjoin('products','users.user_id = products.user_id')
		  	->where('products.approve = 1')
		  	->queryScalar();

	  	return number_format($proAll);
	  }

	  public function getCountView() {
	  	$sum = Yii::app()->db->createCommand("SELECT COUNT(`ip`) as `sum` FROM `viewsite`")->queryScalar();
	  	// $sum = Yii::app()->db->createCommand("SELECT SUM(`unit`) as `sum` FROM `viewsite`")->queryScalar();
	  	return number_format($sum);
	  }

	  public function getCountViewToDay() {
	  	$date = date('Y-m-d');
	  	$sum = Yii::app()->db->createCommand("SELECT COUNT(`ip`) as `sum` FROM `viewsite` where `date` = '$date'")->queryScalar();
	  	// $sum = Yii::app()->db->createCommand("SELECT SUM(`unit`) as `sum` FROM `viewsite` where `date` = '$date'")->queryScalar();
	  	return number_format($sum);
	  }

	  public function getCountViewNow() {
	  	$date = date('Y-m-d');
	  	$time_start = date('H') - 1;
	  	$time_start = $time_start.':'.date('i:s');
	  	$time_now = date('H:i:s');
	  	$count = Yii::app()->db->createCommand("SELECT COUNT(`ip`) as `count` FROM `viewsite` where `date` = '$date' and `time` BETWEEN '$time_start' and '$time_now'")->queryScalar();
	  	return number_format($count);
	  }


	}

?>
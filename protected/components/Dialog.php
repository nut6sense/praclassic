<?php

class Dialog {

  public function LinkDialog($id, $header, $content, $path, $submit = null) {
    $this->beginWidget('CActiveForm', array('action' => Yii::app()->request->baseUrl . $path,'htmlOptions' => array('enctype' => 'multipart/form-data'),));
    $textSubmit = is_null($submit)?'บันทึก':$submit;
    echo CHtml::openTag('div', array('id'=>$id, 'class'=>'modal hide fade', 'tabindex'=>'-1', 'role'=>'dialog', 'aria-labelledby'=>'myModalLabel', 'aria-hidden'=>'myModalLabel'));
    echo CHtml::openTag('div', array('class'=>'modal-header '));
    echo CHtml::button('x', array('type'=>'button', 'class'=>'close', 'data-dismiss'=>'modal', 'aria-hidden'=>'true'));
    echo CHtml::openTag('h4', array('id'=>'myModalLabel'));
    echo $header;
    echo CHtml::closeTag('h4');
    echo CHtml::closeTag('div');
    echo CHtml::openTag('div', array('class'=>'modal-body'));
    echo $content;
    echo CHtml::closeTag('div');
    echo CHtml::openTag('div', array('class'=>'modal-footer'));
    echo CHtml::button('ปิด', array('class'=>'btn', 'data-dismiss'=>'modal', 'aria-hidden'=>'true', 'style'=>'margin-bottom: 0px;'));
    echo CHtml::submitbutton($textSubmit, array('class'=>'btn btn-primary'));
    echo CHtml::closeTag('div');
    echo CHtml::closeTag('div');
    
    $this->endWidget();
  }

  public function alertSuccess() {
    echo CHtml::openTag('div', array('style'=>'width: 100%'));
    echo CHtml::openTag('div', array('id'=>'success', 'class'=>'modal hide fade alertDialog', 'tabindex'=>'-1', 'role'=>'dialog', 'aria-labelledby'=>'myModalLabel', 'aria-hidden'=>'myModalLabel'));
    echo CHtml::openTag('div', array('class'=>'modal-header'));
    echo CHtml::button('x', array('type'=>'button', 'class'=>'close', 'data-dismiss'=>'modal', 'aria-hidden'=>'true'));
    echo CHtml::openTag('h4', array('id'=>'myModalLabel'));
    echo 'ข้อความจากระบบ';
    echo CHtml::closeTag('h4');
    echo CHtml::closeTag('div');
    echo CHtml::openTag('div', array('id'=>'SuccessMessage', 'class'=>'modal-body'));
    echo 'เสร็จสิ้น';
    echo CHtml::closeTag('div');
    echo CHtml::openTag('div', array('class'=>'modal-footer'));
    echo CHtml::button('ปิด', array('class'=>'btn', 'data-dismiss'=>'modal', 'aria-hidden'=>'true'));
    echo CHtml::closeTag('div');
    echo CHtml::closeTag('div');
    echo CHtml::closeTag('div');
  }

    public function alertError() {
    echo CHtml::openTag('div', array('style'=>'width: 100%'));
    echo CHtml::openTag('div', array('id'=>'error', 'class'=>'modal alertDialog hide fade', 'tabindex'=>'-1', 'role'=>'dialog', 'aria-labelledby'=>'myModalLabel', 'aria-hidden'=>'myModalLabel'));
    echo CHtml::openTag('div', array('class'=>'modal-header'));
    echo CHtml::button('x', array('type'=>'button', 'class'=>'close', 'data-dismiss'=>'modal', 'aria-hidden'=>'true'));
    echo CHtml::openTag('h4', array('id'=>'myModalLabel'));
    echo 'ข้อความจากระบบ';
    echo CHtml::closeTag('h4');
    echo CHtml::closeTag('div');
    echo CHtml::openTag('div', array('id'=>'ErrorMessage', 'class'=>'modal-body'));
    echo 'ล้มเหลว';
    echo CHtml::closeTag('div');
    echo CHtml::openTag('div', array('class'=>'modal-footer'));
    echo CHtml::button('ปิด', array('class'=>'btn', 'data-dismiss'=>'modal', 'aria-hidden'=>'true'));
    echo CHtml::closeTag('div');
    echo CHtml::closeTag('div');
    echo CHtml::closeTag('div');
  }

  public function successMessage($message) {
    Yii::app()->user->setFlash('status', 'success');
    Yii::app()->user->setFlash('message', $message);
  }

  public function errorMessage($message) {
    Yii::app()->user->setFlash('status', 'error');
    Yii::app()->user->setFlash('message', $message);
  }

  public function alertMessage() {
    $status = Yii::app()->user->getFlash('status');
    $message = Yii::app()->user->getFlash('message');

    switch ($status) {
      case 'success':
        Yii::app()->clientScript->registerScript('Success',"$('#success').find('#SuccessMessage').html('".$message."');$('#success').modal('show');");
        break;
      case 'error':
        Yii::app()->clientScript->registerScript('Error',"$('#error').find('#ErrorMessage').html('".$message."');$('#error').modal('show');");
        break;
    }
  }

  public function alertGetMessage($id, $header, $content) {
    echo CHtml::openTag('div', array('id'=>$id, 'class'=>'modal hide fade alertGetMessage', 'tabindex'=>'-1', 'role'=>'dialog', 'aria-labelledby'=>'myModalLabel', 'aria-hidden'=>'myModalLabel'));
    echo CHtml::openTag('div', array('class'=>'modal-header'));
    echo CHtml::button('x', array('type'=>'button', 'class'=>'close', 'data-dismiss'=>'modal', 'aria-hidden'=>'true'));
    echo CHtml::openTag('h4', array('id'=>'myModalLabel'));
    echo $header;
    echo CHtml::closeTag('h4');
    echo CHtml::closeTag('div');
    echo CHtml::openTag('div', array('id'=>'SuccessMessage', 'class'=>'modal-body'));
    echo $content;
    echo CHtml::closeTag('div');
    echo CHtml::openTag('div', array('class'=>'modal-footer'));
    echo CHtml::button('ปิด', array('class'=>'btn', 'data-dismiss'=>'modal', 'aria-hidden'=>'true'));
    echo CHtml::closeTag('div');
    echo CHtml::closeTag('div');
  }

}
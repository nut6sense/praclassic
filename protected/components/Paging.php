<?php
class Paging {

  public function getPaging($count,$limit,$page = 1) {
  	$data = null;
		$pageLimit = ($count!=0)?ceil($count/$limit):1;

		$data .= CHtml::openTag('i',array('class'=>'icon-chevron-left icon-white','style'=>'margin: 0;'));
    $data .= CHtml::closeTag('i');

    if ($page!=1) {
  		$data .= CHtml::link('ก่อนหน้า', '#', array('style'=>'font-weight: bold;','page'=>($page-1)));
    	$data .= ' | ';
    }

    for ($i=1; $i <= $pageLimit; $i++) { 

    	if ($i==$page) {
    		$selectPage = 'color: #f00;';
    	}else{
    		$selectPage = null;
    	}

  		$data .= CHtml::link($i, '#', array('style'=>'font-weight: bold;'.$selectPage,'page'=>$i));
    	
    	if($i!=$pageLimit){
    		$data .= ' | ';
    	}
    }

    if ($page!=$pageLimit) {
    	$data .= ' | ';
  		$data .= CHtml::link('ถัดไป', '#', array('style'=>'font-weight: bold;','page'=>($page+1)));
    }

    $data .= CHtml::openTag('i',array('class'=>'icon-chevron-right icon-white'));
    $data .= CHtml::closeTag('i');

    return $data;
	}

}

?>
<?php

class ResizeImage {

	public function resize($thumbfile, $width = 200, $height = 200, $quality = 75, $sharpen = 20) {

		Yii::import('application.extensions.image.Image');
		$image = new Image($thumbfile);
		$image->resize($width,$height)->quality($quality)->sharpen($sharpen);
		$image->save();
		
	}


}
<?php
class ColorCode {

  public function ColorChart() {
  	$table = null;
		$table .= CHtml::openTag('table',array('id'=>'colorchart','class'=>'colorchart span12'));
		$table .= CHtml::openTag('tbody');
		$table .= CHtml::openTag('tr');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#FBEFEF'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#FBF2EF'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#FBF5EF'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#FBF8EF'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#FBFBEF'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#F8FBEF'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#F5FBEF'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#F2FBEF'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#EFFBEF'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#EFFBF2'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#EFFBF5'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#EFFBF8'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#EFFBFB'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#EFF8FB'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#EFF5FB'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#EFF2FB'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#EFEFFB'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#F2EFFB'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#F5EFFB'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#F8EFFB'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#FBEFFB'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#FBEFF8'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#FBEFF5'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#FBEFF2'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#FFFFFF'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::closeTag('tr');
		$table .= CHtml::openTag('tr');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#F8E0E0'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#F8E6E0'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#F8ECE0'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#F7F2E0'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#F7F8E0'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#F1F8E0'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#ECF8E0'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#E6F8E0'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#E0F8E0'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#E0F8E6'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#E0F8EC'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#E0F8F1'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#E0F8F7'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#E0F2F7'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#E0ECF8'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#E0E6F8'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#E0E0F8'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#E6E0F8'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#ECE0F8'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#F2E0F7'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#F8E0F7'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#F8E0F1'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#F8E0EC'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#F8E0E6'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#FAFAFA'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::closeTag('tr');
		$table .= CHtml::openTag('tr');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#F6CECE'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#F6D8CE'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#F6E3CE'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#F5ECCE'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#F5F6CE'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#ECF6CE'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#E3F6CE'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#D8F6CE'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#CEF6CE'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#CEF6D8'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#CEF6E3'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#CEF6EC'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#CEF6F5'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#CEECF5'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#CEE3F6'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#CED8F6'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#CECEF6'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#D8CEF6'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#E3CEF6'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#ECCEF5'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#F6CEF5'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#F6CEEC'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#F6CEE3'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#F6CED8'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#F2F2F2'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::closeTag('tr');
		$table .= CHtml::openTag('tr');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#F5A9A9'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#F5BCA9'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#F5D0A9'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#F3E2A9'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#F2F5A9'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#E1F5A9'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#D0F5A9'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#BCF5A9'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#A9F5A9'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#A9F5BC'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#A9F5D0'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#A9F5E1'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#A9F5F2'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#A9E2F3'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#A9D0F5'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#A9BCF5'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#A9A9F5'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#BCA9F5'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#D0A9F5'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#E2A9F3'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#F5A9F2'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#F5A9E1'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#F5A9D0'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#F5A9BC'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#E6E6E6'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::closeTag('tr');
		$table .= CHtml::openTag('tr');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#F78181'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#F79F81'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#F7BE81'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#F5DA81'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#F3F781'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#D8F781'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#BEF781'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#9FF781'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#81F781'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#81F79F'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#81F7BE'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#81F7D8'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#81F7F3'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#81DAF5'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#81BEF7'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#819FF7'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#8181F7'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#9F81F7'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#BE81F7'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#DA81F5'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#F781F3'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#F781D8'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#F781BE'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#F7819F'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#D8D8D8'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::closeTag('tr');
		$table .= CHtml::openTag('tr');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#FA5858'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#FA8258'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#FAAC58'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#F7D358'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#F4FA58'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#D0FA58'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#ACFA58'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#82FA58'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#58FA58'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#58FA82'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#58FAAC'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#58FAD0'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#58FAF4'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#58D3F7'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#58ACFA'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#5882FA'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#5858FA'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#8258FA'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#AC58FA'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#D358F7'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#FA58F4'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#FA58D0'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#FA58AC'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#FA5882'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#BDBDBD'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::closeTag('tr');
		$table .= CHtml::openTag('tr');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#FE2E2E'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#FE642E'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#FE9A2E'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#FACC2E'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#F7FE2E'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#C8FE2E'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#9AFE2E'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#64FE2E'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#2EFE2E'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#2EFE64'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#2EFE9A'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#2EFEC8'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#2EFEF7'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#2ECCFA'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#2E9AFE'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#2E64FE'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#2E2EFE'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#642EFE'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#9A2EFE'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#CC2EFA'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#FE2EF7'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#FE2EC8'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#FE2E9A'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#FE2E64'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#A4A4A4'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::closeTag('tr');
		$table .= CHtml::openTag('tr');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#FF0000'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#FF4000'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#FF8000'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#FFBF00'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#FFFF00'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#BFFF00'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#80FF00'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#40FF00'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#00FF00'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#00FF40'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#00FF80'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#00FFBF'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#00FFFF'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#00BFFF'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#0080FF'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#0040FF'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#0000FF'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#4000FF'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#8000FF'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#BF00FF'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#FF00FF'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#FF00BF'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#FF0080'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#FF0040'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#848484'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::closeTag('tr');
		$table .= CHtml::openTag('tr');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#DF0101'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#DF3A01'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#DF7401'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#DBA901'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#D7DF01'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#A5DF00'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#74DF00'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#3ADF00'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#01DF01'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#01DF3A'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#01DF74'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#01DFA5'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#01DFD7'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#01A9DB'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#0174DF'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#013ADF'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#0101DF'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#3A01DF'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#7401DF'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#A901DB'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#DF01D7'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#DF01A5'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#DF0174'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#DF013A'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#6E6E6E'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::closeTag('tr');
		$table .= CHtml::openTag('tr');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#B40404'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#B43104'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#B45F04'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#B18904'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#AEB404'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#86B404'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#5FB404'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#31B404'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#04B404'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#04B431'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#04B45F'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#04B486'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#04B4AE'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#0489B1'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#045FB4'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#0431B4'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#0404B4'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#3104B4'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#5F04B4'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#8904B1'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#B404AE'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#B40486'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#B4045F'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#B40431'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#585858'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::closeTag('tr');
		$table .= CHtml::openTag('tr');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#8A0808'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#8A2908'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#8A4B08'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#886A08'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#868A08'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#688A08'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#4B8A08'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#298A08'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#088A08'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#088A29'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#088A4B'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#088A68'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#088A85'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#086A87'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#084B8A'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#08298A'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#08088A'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#29088A'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#4B088A'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#6A0888'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#8A0886'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#8A0868'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#8A084B'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#8A0829'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#424242'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::closeTag('tr');
		$table .= CHtml::openTag('tr');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#610B0B'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#61210B'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#61380B'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#5F4C0B'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#5E610B'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#4B610B'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#38610B'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#21610B'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#0B610B'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#0B6121'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#0B6138'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#0B614B'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#0B615E'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#0B4C5F'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#0B3861'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#0B2161'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#0B0B61'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#210B61'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#380B61'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#4C0B5F'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#610B5E'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#610B4B'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#610B38'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#610B21'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#2E2E2E'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::closeTag('tr');
		$table .= CHtml::openTag('tr');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#3B0B0B'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#3B170B'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#3B240B'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#3A2F0B'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#393B0B'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#2E3B0B'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#243B0B'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#173B0B'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#0B3B0B'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#0B3B17'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#0B3B24'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#0B3B2E'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#0B3B39'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#0B2F3A'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#0B243B'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#0B173B'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#0B0B3B'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#170B3B'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#240B3B'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#2F0B3A'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#3B0B39'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#3B0B2E'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#3B0B24'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#3B0B17'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#1C1C1C'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::closeTag('tr');
		$table .= CHtml::openTag('tr');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#2A0A0A'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#2A120A'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#2A1B0A'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#29220A'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#292A0A'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#222A0A'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#1B2A0A'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#122A0A'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#0A2A0A'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#0A2A12'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#0A2A1B'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#0A2A22'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#0A2A29'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#0A2229'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#0A1B2A'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#0A122A'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#0A0A2A'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#120A2A'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#1B0A2A'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#220A29'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#2A0A29'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#2A0A22'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#2A0A1B'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#2A0A12'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#151515'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::closeTag('tr');
		$table .= CHtml::openTag('tr');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#190707'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#190B07'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#191007'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#181407'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#181907'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#141907'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#101907'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#0B1907'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#071907'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#07190B'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#071910'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#071914'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#071918'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#071418'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#071019'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#070B19'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#070719'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#0B0719'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#100719'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#140718'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#190718'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#190714'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#190710'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#19070B'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::openTag('td',array('bgcolor'=>'#000000'));
		$table .= CHtml::closeTag('td');
		$table .= CHtml::closeTag('tr');
		$table .= CHtml::closeTag('tbody');
		$table .= CHtml::closeTag('table');

		echo $table;
	}

//////	
}
?>

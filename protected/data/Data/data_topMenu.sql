SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

TRUNCATE TABLE  `topmenu`;

INSERT INTO `topmenu` VALUES (null, 'หน้าแรก', '/site/index', 1);
INSERT INTO `topmenu` VALUES (null, 'รายการพระเครื่อง', '/site/shop_list', 2);
INSERT INTO `topmenu` VALUES (null, 'ร้านพระมาตรฐาน', '/site/shop_standard', 3);
INSERT INTO `topmenu` VALUES (null, 'ข่าวประชาสัมพันธ์', '/site/news', 4);
INSERT INTO `topmenu` VALUES (null, 'ระเบียบการใช้งาน', '/site/discipline', 5);
INSERT INTO `topmenu` VALUES (null, 'ยืนยันการชำระเงิน', '/site/payConfirm', 6);
INSERT INTO `topmenu` VALUES (null, 'สมัครสมาชิก', '/shop/register', 7);
INSERT INTO `topmenu` VALUES (null, 'ติดต่อเรา', '/site/contactus', 8);

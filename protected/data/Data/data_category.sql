SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

TRUNCATE TABLE  `category`;

INSERT INTO `category` VALUES (null, 'พระบูชา');
INSERT INTO `category` VALUES (null, 'พระกรุ');
INSERT INTO `category` VALUES (null, 'พระกริ่ง');
INSERT INTO `category` VALUES (null, 'พระชัยวัฒน์');
INSERT INTO `category` VALUES (null, 'รูปหล่อ');
INSERT INTO `category` VALUES (null, 'เหรียญหล่อ / ปั้ม');
INSERT INTO `category` VALUES (null, 'พระปิดตา');
INSERT INTO `category` VALUES (null, 'เครื่องราง');
INSERT INTO `category` VALUES (null, 'พระเนื้อผง / ดิน / ว่าน');
INSERT INTO `category` VALUES (null, 'อื่นๆ');

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

DROP TABLE IF EXISTS `banks`;
CREATE TABLE `banks` (
  `bank_id` int(11) NOT NULL AUTO_INCREMENT,
	`user_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
	`bank_name` int(11) NOT NULL,
	`bank_type` int(11) NOT NULL,
	`bank_Branch` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
	`account_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
	`account_number` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`bank_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

DROP TABLE IF EXISTS `massage`;
CREATE TABLE `massage` (
  `massageid` int(11) NOT NULL AUTO_INCREMENT,
  `massagename` text COLLATE utf8_unicode_ci NOT NULL,
  `massagetype` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`massageid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

DROP TABLE IF EXISTS `payment`;
CREATE TABLE `payment` (
  `pay_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `pay_type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `date_begin` date COLLATE utf8_unicode_ci NULL,
  `date_end` date COLLATE utf8_unicode_ci NULL,
  `date_pay` date COLLATE utf8_unicode_ci NULL,
  `pay_amount` int(11) COLLATE utf8_unicode_ci NULL,
  `pay_status` tinyint(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`pay_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

DROP TABLE IF EXISTS `shops`;
CREATE TABLE `shops` (
  `shopid` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `detail` text COLLATE utf8_unicode_ci NULL,
  `rule` text COLLATE utf8_unicode_ci NULL,
  `map` text COLLATE utf8_unicode_ci NULL,
  `cover` varchar(100) COLLATE utf8_unicode_ci NULL,
	`referrance` varchar(50) COLLATE utf8_unicode_ci NULL,
	`referrance_phonenumber` varchar(13) NULL,
  `sample_pic` text COLLATE utf8_unicode_ci NULL,
  `shop_pic` varchar(100) COLLATE utf8_unicode_ci NULL,
  `note` text COLLATE utf8_unicode_ci NULL,
  `view` int(11) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`shopid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(32) COLLATE utf8_unicode_ci NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NULL,
  `surname` varchar(50) COLLATE utf8_unicode_ci NULL,
  `phonenumber` varchar(13) COLLATE utf8_unicode_ci NULL,
  `iden_id` char(17) COLLATE utf8_unicode_ci NULL,
	`iden_picture` varchar(100) COLLATE utf8_unicode_ci NULL,
	`permission` tinyint(1) NULL,
	`active` tinyint(1) NULL,
  `sequence` tinyint(1) NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

DROP TABLE IF EXISTS `news`;
CREATE TABLE `news` (
  `newsid` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `newsname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `newssubdetail` text COLLATE utf8_unicode_ci NOT NULL,
  `newsdetail` text COLLATE utf8_unicode_ci NOT NULL,
  `newspic` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`newsid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

DROP TABLE IF EXISTS `announce`;
CREATE TABLE `announce` (
  `announceid` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `announcename` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `announcesubdetail` text COLLATE utf8_unicode_ci NOT NULL,
  `announcedetail` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`announceid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

DROP TABLE IF EXISTS `shopsmassage`;
CREATE TABLE `shopsmassage` (
  `shopsmassageid` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `shopsmassagedetail` text COLLATE utf8_unicode_ci NOT NULL,
  `shopsmassagetype` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`shopsmassageid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `productid` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `productname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `productprice` int(11) COLLATE utf8_unicode_ci NULL,
  `productdetail` text COLLATE utf8_unicode_ci NULL,
  `productpic` text COLLATE utf8_unicode_ci NULL,
  `productstatus` tinyint(1) COLLATE utf8_unicode_ci NULL,
  `productnoted` tinyint(1) COLLATE utf8_unicode_ci NULL,
  `productdate` date COLLATE utf8_unicode_ci NULL,
  `category_id` int(11) COLLATE utf8_unicode_ci NOT NULL,
  `approve` int(1) COLLATE utf8_unicode_ci NOT NULL,
  `view` int(11) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`productid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

DROP TABLE IF EXISTS `gallery`;
CREATE TABLE `gallery` (
  `galleryid` int(11) NOT NULL AUTO_INCREMENT,
  `galleryname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `gallerydetail` text COLLATE utf8_unicode_ci NOT NULL,
  `gallerypic` text COLLATE utf8_unicode_ci NOT NULL,
  `gallerydate` text COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(11) COLLATE utf8_unicode_ci NOT NULL,
  `view` int(11) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`galleryid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

DROP TABLE IF EXISTS `premium`;
CREATE TABLE `premium` (
  `premiumid` int(11) NOT NULL AUTO_INCREMENT,
  `premiumname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `premiumdetail` text COLLATE utf8_unicode_ci NOT NULL,
  `premiumpic` text COLLATE utf8_unicode_ci NOT NULL,
  `premiumdate` text COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(11) COLLATE utf8_unicode_ci NOT NULL,
  `view` int(11) COLLATE utf8_unicode_ci NOT NULL,
  `default` int(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`premiumid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

DROP TABLE IF EXISTS `reference`;
CREATE TABLE `reference` (
  `reference_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `reference_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `reference_phonenumber` varchar(50) COLLATE utf8_unicode_ci NULL,
  `reference_date` date COLLATE utf8_unicode_ci NULL,
  PRIMARY KEY (`reference_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

DROP TABLE IF EXISTS `contact`;
CREATE TABLE `contact` (
  `cid` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `subject` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `message` longtext COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `telephone` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created` timestamp COLLATE utf8_unicode_ci NULL,
  `ipaddress` varchar(50) COLLATE utf8_unicode_ci NULL,
  `status` int(2) COLLATE utf8_unicode_ci NULL,
  `param` text COLLATE utf8_unicode_ci NULL,
  PRIMARY KEY (`cid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

DROP TABLE IF EXISTS `block`;
CREATE TABLE `block` (
  `blockid` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(2) COLLATE utf8_unicode_ci NOT NULL,
  `date_start` date COLLATE utf8_unicode_ci NULL,
  `date_end` date COLLATE utf8_unicode_ci NULL,
  PRIMARY KEY (`blockid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

DROP TABLE IF EXISTS `payconfirm`;
CREATE TABLE `payconfirm` (
  `payconfirm_id` int(11) NOT NULL AUTO_INCREMENT,
  `shop_id` int(11) NOT NULL,
  `shop_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payconfirm_pic` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payconfirm_type` int(11) NOT NULL,
  `bank_name` int(11) NULL,
  `amount` int(11) NULL,
  `dateTime` dateTime NULL,
  `payconfirm_phone` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `note` text COLLATE utf8_unicode_ci NULL,
  PRIMARY KEY (`payconfirm_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

DROP TABLE IF EXISTS `topmenu`;
CREATE TABLE `topmenu` (
  `topmenu_id` int(11) NOT NULL AUTO_INCREMENT,
  `topmenu_topic` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `topmenu_link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `topmenu_number` int(11) NOT NULL,
  PRIMARY KEY (`topmenu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

DROP TABLE IF EXISTS `viewsite`;
CREATE TABLE `viewsite` (
  `ip` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `unit` int(11) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ip`,`date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

DROP TABLE IF EXISTS `statistics_view`;
CREATE TABLE `statistics_view` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `total` int(11) NOT NULL,
  `today` int(11) NOT NULL,
  `date` date COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

DROP TABLE IF EXISTS `news`;
CREATE TABLE `news` (
  `newsid` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `newsname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `newssubdetail` text COLLATE utf8_unicode_ci NOT NULL,
  `newsdetail` text COLLATE utf8_unicode_ci NOT NULL,
  `newspic` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`newsid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;
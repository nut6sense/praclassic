
SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

DROP TABLE IF EXISTS `topmenu`;
CREATE TABLE `topmenu` (
  `topmenu_id` int(11) NOT NULL AUTO_INCREMENT,
  `topmenu_topic` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `topmenu_link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `topmenu_number` int(11) NOT NULL,
  PRIMARY KEY (`topmenu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;
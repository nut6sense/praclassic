
SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `productid` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `productname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `productprice` int(11) COLLATE utf8_unicode_ci NULL,
  `productdetail` text COLLATE utf8_unicode_ci NULL,
  `productpic` text COLLATE utf8_unicode_ci NULL,
  `productstatus` tinyint(1) COLLATE utf8_unicode_ci NULL,
  `productnoted` tinyint(1) COLLATE utf8_unicode_ci NULL,
  `productdate` date COLLATE utf8_unicode_ci NULL,
  `category_id` int(11) COLLATE utf8_unicode_ci NOT NULL,
  `approve` int(1) COLLATE utf8_unicode_ci NOT NULL,
  `view` int(11) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`productid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;
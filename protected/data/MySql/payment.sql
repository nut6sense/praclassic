
SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

DROP TABLE IF EXISTS `payment`;
CREATE TABLE `payment` (
  `pay_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `pay_type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `date_begin` date COLLATE utf8_unicode_ci NULL,
  `date_end` date COLLATE utf8_unicode_ci NULL,
  `date_pay` date COLLATE utf8_unicode_ci NULL,
  `pay_amount` int(11) COLLATE utf8_unicode_ci NULL,
  `pay_status` tinyint(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`pay_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;



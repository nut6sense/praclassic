
SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(32) COLLATE utf8_unicode_ci NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NULL,
  `surname` varchar(50) COLLATE utf8_unicode_ci NULL,
  `phonenumber` varchar(13) COLLATE utf8_unicode_ci NULL,
  `iden_id` char(17) COLLATE utf8_unicode_ci NULL,
	`iden_picture` varchar(100) COLLATE utf8_unicode_ci NULL,
	`permission` tinyint(1) NULL,
	`active` tinyint(1) NULL,
  `sequence` tinyint(1) NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

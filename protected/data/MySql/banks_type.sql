
SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

DROP TABLE IF EXISTS `banks_type`;
CREATE TABLE `banks_type` (
  `banktypeid` int(11) NOT NULL AUTO_INCREMENT,
  `banktypename` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`banktypeid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

DROP TABLE IF EXISTS `shops`;
CREATE TABLE `shops` (
  `shopid` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `detail` text COLLATE utf8_unicode_ci NULL,
  `rule` text COLLATE utf8_unicode_ci NULL,
  `map` text COLLATE utf8_unicode_ci NULL,
  `cover` varchar(100) COLLATE utf8_unicode_ci NULL,
	`referrance` varchar(50) COLLATE utf8_unicode_ci NULL,
	`referrance_phonenumber` varchar(13) NULL,
  `sample_pic` text COLLATE utf8_unicode_ci NULL,
  `shop_pic` varchar(100) COLLATE utf8_unicode_ci NULL,
  `note` text COLLATE utf8_unicode_ci NULL,
  `view` int(11) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`shopid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

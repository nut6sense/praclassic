
SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

DROP TABLE IF EXISTS `massage`;
CREATE TABLE `massage` (
  `massageid` int(11) NOT NULL AUTO_INCREMENT,
  `massagename` text COLLATE utf8_unicode_ci NOT NULL,
  `massagetype` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`massageid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;
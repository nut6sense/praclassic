
SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

DROP TABLE IF EXISTS `block`;
CREATE TABLE `block` (
  `blockid` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(2) COLLATE utf8_unicode_ci NOT NULL,
  `date_start` date COLLATE utf8_unicode_ci NULL,
  `date_end` date COLLATE utf8_unicode_ci NULL,
  PRIMARY KEY (`blockid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;
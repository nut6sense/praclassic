
SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

DROP TABLE IF EXISTS `banks`;
CREATE TABLE `banks` (
  `bank_id` int(11) NOT NULL AUTO_INCREMENT,
	`user_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
	`bank_name` int(11) NOT NULL,
	`bank_type` int(11) NOT NULL,
	`bank_Branch` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
	`account_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
	`account_number` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`bank_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;


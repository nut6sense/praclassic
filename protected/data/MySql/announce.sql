
SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

DROP TABLE IF EXISTS `announce`;
CREATE TABLE `announce` (
  `announceid` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `announcename` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `announcesubdetail` text COLLATE utf8_unicode_ci NOT NULL,
  `announcedetail` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`announceid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;
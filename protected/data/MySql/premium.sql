
SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

DROP TABLE IF EXISTS `premium`;
CREATE TABLE `premium` (
  `premiumid` int(11) NOT NULL AUTO_INCREMENT,
  `premiumname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `premiumdetail` text COLLATE utf8_unicode_ci NOT NULL,
  `premiumpic` text COLLATE utf8_unicode_ci NOT NULL,
  `premiumdate` text COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(11) COLLATE utf8_unicode_ci NOT NULL,
  `view` int(11) COLLATE utf8_unicode_ci NOT NULL,
  `default` int(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`premiumid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

DROP TABLE IF EXISTS `reference`;
CREATE TABLE `reference` (
  `reference_id` int(11) NOT NULL AUTO_INCREMENT,
	`user_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
	`reference_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
	`reference_phonenumber` varchar(50) COLLATE utf8_unicode_ci NULL,
	`reference_date` date COLLATE utf8_unicode_ci NULL,
  PRIMARY KEY (`reference_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;



SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

DROP TABLE IF EXISTS `payconfirm`;
CREATE TABLE `payconfirm` (
  `payconfirm_id` int(11) NOT NULL AUTO_INCREMENT,
	`shop_id` int(11) NOT NULL,
	`shop_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
	`payconfirm_pic` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
	`payconfirm_type` int(11) NOT NULL,
	`bank_name` int(11) NULL,
	`amount` int(11) NULL,
	`dateTime` dateTime NULL,
	`payconfirm_phone` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
	`note` text COLLATE utf8_unicode_ci NULL,
  PRIMARY KEY (`payconfirm_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;


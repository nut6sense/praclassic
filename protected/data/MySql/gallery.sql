
SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

DROP TABLE IF EXISTS `gallery`;
CREATE TABLE `gallery` (
  `galleryid` int(11) NOT NULL AUTO_INCREMENT,
  `galleryname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `gallerydetail` text COLLATE utf8_unicode_ci NOT NULL,
  `gallerypic` text COLLATE utf8_unicode_ci NOT NULL,
  `gallerydate` text COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(11) COLLATE utf8_unicode_ci NOT NULL,
  `view` int(11) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`galleryid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;
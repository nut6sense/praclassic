
SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

DROP TABLE IF EXISTS `shopsmassage`;
CREATE TABLE `shopsmassage` (
  `shopsmassageid` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `shopsmassagedetail` text COLLATE utf8_unicode_ci NOT NULL,
  `shopsmassagetype` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`shopsmassageid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;
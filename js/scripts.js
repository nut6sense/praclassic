$(document).ready(function() {

  chkUpdate();
  
  function windowResize(){

    if ($(window).width() < 767) {
      $('#container-main').width('1170px');
      // $('#responsive').attr('href','');
    }else{
      $('#responsive').attr('href',window.baseUrl + '/plugin/bootstrap/css/bootstrap-responsive.min.css');
    };

    if ($(window).width() < 1200) {
      $('.topMenuLink > li').width('auto');
    }else{
      $('.topMenuLink > li').width('12.2%');
    };

    var boxPicHeight = $('.picProductSize').width()+10;
    $('.picProductSize').height(boxPicHeight).css({'padding-bottom':'initial'});

    if (($('#productPop > div').length%2)==0) {
      var a = ($('#productPop').width()-($('#productPop > div').width()*$('#productPop > div').length+(((($('#productPop > div').length*$('#productPop > div').length)/45)*$('#productPop > div').width()))))/2;
      $('#productPop > div:eq(0)').css({'margin-left':a+'px'});
    };
    

    // alert($(window).width());

  }


  setTimeout(windowResize,100);

  windowResize();

  $(window).resize(function(){
   windowResize();
  });

  

  $('.picLoupe').loupe({
    width: 250, // width of magnifier
    height: 250, // height of magnifier
    loupe: 'loupe' // css class for magnifier
  });
//
//Highslide
//
  hs.graphicsDir = window.baseUrl+'/plugin/highslide/graphics/';
  hs.wrapperClassName = 'wide-border';

//
//Datepicker
//
  var datepickerId = $('.datepicker');
  datepickerId.on("click", function(){
    $(this).datepicker('show');
  });
  datepickerId.on("click", function(){
    datepickerId.not(this).datepicker('hide');
  });
  datepickerId.datepicker({
    format: 'dd/mm/yyyy',
  });
//
//------------------------------------------------
//
	//LeftMenuAdmin
	// $('[id-menu]').on('click', function() {

 //      $('[id-menu]').removeAttr('class','active');
 //      $('#leftMenuAdmin').find('[id-menu="'+$(this).attr('id-menu')+'"]').attr('class','active');

 //    });

	//--------------------------

  if ($('#expire-shop-form').length > 0) {
    var edit_dialog = $('#editShop');
    var note_dialog = $('#noteShop');

    // $('#expire-shop-form').find('[name="editShop"]').on('click', function() {
    //   var data = {'USER_ID' : $(this).attr('user_id')};
    //   $.post(window.baseUrl + '/shop/shopDetail', data, function(html) {
    //   }).success(function(data) {
    //     $('#Users_user_id', edit_dialog).val(data.user_id);
    //     $('#Users_username', edit_dialog).val(data.username);
    //     $('#Users_password', edit_dialog).val(data.password);
    //     $('#Users_name', edit_dialog).val(data.firstname);
    //     $('#Users_surname', edit_dialog).val(data.surname);
    //     $('#Users_phonenumber', edit_dialog).val(data.phonenumber);
    //     $('#Users_email', edit_dialog).val(data.email);
    //     $('#Users_iden_id', edit_dialog).val(data.iden_id);
    //     $('#Users_iden_picture', edit_dialog).attr('src', '../img/iden/'+data.iden_picture+'.jpg');
    //     $('#Shops_name', edit_dialog).val(data.shopname);
    //     $('#Shops_detail', edit_dialog).val(data.detail);
    //     $('#Shops_referrance', edit_dialog).val(data.referrance);
    //     $('#Shops_referrance_phonenumber', edit_dialog).val(data.referrance_phonenumber);
    //     $('#Banks_bank_name', edit_dialog).val(data.bank_name);
    //     $('#Banks_bank_type', edit_dialog).val(data.bank_type);
    //     $('#Banks_bank_Branch', edit_dialog).val(data.bank_Branch);
    //     $('#Banks_account_name', edit_dialog).val(data.account_name);
    //     $('#Banks_account_number', edit_dialog).val(data.account_number);
    //     $('#Shops_sample_pic_1', edit_dialog).attr('src', '/praclassic/img/samplePic/'+data.sample_1_pic);
    //     $('#Shops_sample_detail_1', edit_dialog).html(data.sample_1_detail);
    //     $('#Shops_sample_pic_2', edit_dialog).attr('src', '/praclassic/img/samplePic/'+data.sample_2_pic);
    //     $('#Shops_sample_detail_2', edit_dialog).html(data.sample_2_detail);
    //     $('#Shops_sample_pic_3', edit_dialog).attr('src', '/praclassic/img/samplePic/'+data.sample_3_pic);
    //     $('#Shops_sample_detail_3', edit_dialog).html(data.sample_3_detail);
    //     $('#Shops_sample_pic_4', edit_dialog).attr('src', '/praclassic/img/samplePic/'+data.sample_4_pic);
    //     $('#Shops_sample_detail_4', edit_dialog).html(data.sample_4_detail);
    //     if(data.active=='1') {
    //       $('#block-detail').removeAttr('style');
    //       $('#user_active', edit_dialog).html('ปกติ');
    //     }else if(data.active=='2') {
    //       $('#block-detail').css('display', 'none');
    //       $('#user_active', edit_dialog).html('หมดอายุ');
    //     }else{
    //       $('#block-detail').css('display', 'none');
    //       $('#user_active', edit_dialog).html('ถูกระงับการใช้งาน');
    //     }
        
    //     $('#editShop').modal('show');
    //   });
    //   return false;
    // });

    $('#expire-shop-form').find('[name="renewShop"]').on('click', function() {
      var data = {'USER_ID' : $(this).attr('user_id')};
      $('#Users_user_id', $('#renewShop')).val(data.USER_ID);
    });

    $('#expire-shop-form').find('[name="noteShop"]').on('click', function() {
      var data = {'USER_ID' : $(this).attr('user_id')};
      $('#Users_user_id', note_dialog).val(data.USER_ID);
      $.post(window.baseUrl + '/shop/noteDetail', data, function(html) {
      }).success(function(data) {
        $('#Shops_note', note_dialog).val(data.note);
      });
    });

  }//End expire-shop-form

  if($('#shop-detail-form').length > 0) {

    $('#Users_block_user').on('change', function() {
      var ban_type = $('#Users_block_user').val();
      if(ban_type=='3') {
        $('#Users_block_day').attr('readonly', false);
      }else{
        $('#Users_block_day').attr('readonly', true);
      }
    });
  
  }//End shop-detail-form

  if($('#news-form').length > 0) {
    var news_form = $('#news-form');
    var url = window.baseUrl + '/news/dataNews';
  
    $('#news-form').find('[name="editNews"]').on('click', function() {
      var data = {'NEWS_ID': $(this).attr('value_id')};
      
      $('<form action="' + url + '" method="POST">' + 
        '<input type="hidden" name="news_id" value="' + data.NEWS_ID + '">' +
        '</form>').submit();
  
      return false;
    });
  
  }//End news-form

  if($('#announce-form').length > 0) {
    var news_form = $('#announce-form');
    var url = window.baseUrl + '/announce/dataAnnounce';
  
    $('#announce-form').find('[name="editAnnounce"]').on('click', function() {
      var data = {'ANNOUNCE_ID': $(this).attr('value_id')};
      
      $('<form action="' + url + '" method="POST">' + 
        '<input type="hidden" name="announce_id" value="' + data.ANNOUNCE_ID + '">' +
        '</form>').submit();
  
      return false;
    });
  
  }//End announce-form

  if ($('#products-form').length > 0) {
    var prd_dialog = $('#dialogProduct');

    $('#approve-product').on('click', function() {
      var data = {'productid' : $(this).attr('productid')};
      $.post(window.baseUrl + '/products/approveProduct', data, function(html) {
      }).success(function(data) {
        window.location=window.baseUrl + '/products/productReferences';
      });
      return false;
    });

  }

  if($('#edit-product-form').length > 0) {
    var formName = $('#edit-product-form');
    $('#Products_productstatus', formName).on('change', function() {
      var data = {'productstatus' : $(this).val()};
      if(data.productstatus==6) {
        $('#Products_productprice').removeAttr('readonly');
      }else{
        $('#Products_productprice').attr('readonly', true);
      }
    });
  }

  if($('#gallery-form').length > 0) {
    var url = window.baseUrl + '/gallery/galleryForm';
  
    $('#gallery-form').find('[name="editGallery"]').on('click', function() {
      var data = {'GALLERY_ID': $(this).attr('value_id')};
      
      $('<form action="' + url + '" method="POST">' + 
        '<input type="hidden" name="gallery_id" value="' + data.GALLERY_ID + '">' +
        '</form>').submit();
  
      return false;
    });
  
  }//End gallery-form

  if($('#premium-form').length > 0) {
    var url = window.baseUrl + '/premium/premiumForm';
  
    $('#premium-form').find('[name="editPremium"]').on('click', function() {
      var data = {'PREMIUM_ID': $(this).attr('value_id')};
      
      $('<form action="' + url + '" method="POST">' + 
        '<input type="hidden" name="premium_id" value="' + data.PREMIUM_ID + '">' +
        '</form>').submit();
  
      return false;
    });
  
  }//End premium-form

  if($('#regis-form').length > 0){
    $('#regis-form').find('[type="submit"]').attr('disabled',true);
    // $('#chkRegis').on('click', function() {
    //   if ($('#chkRegis').attr('checked')!='checked') {
    //     $('#chkRegis').attr('checked',true);
    //     $('#regis-form').find('[type="submit"]').removeAttr('disabled');
    //   }else{
    //     $('#chkRegis').removeAttr('checked');
    //     $('#regis-form').find('[type="submit"]').attr('disabled',true);
    //   }
    // });
  }//End regis-form

  if($('#contact-message-form').length > 0){
    $('#contact-message-form').find('[name="showMessage"]').on('click', function() {
      var data = {'cid' : $(this).attr('cid')};
      $.post(window.baseUrl + '/contact/messageDetail', data, function(html) {
      }).success(function(data) {
        $('#Contact_subject').val(data.subject);
        $('#Contact_message').val(data.message);
        $('#Contact_name').val(data.name);
        $('#Contact_telephone').val(data.telephone);
        $('#Contact_email').val(data.email);
      });
    });

  }//End contact-message-form

  if($('#shop-list-form').length > 0){
    var shopListPage = $('#shop-list-form');

    $('#shop-list-form').find('[name="letter-search"]').on('click', function() {
      $('#Products_productname', shopListPage).val($(this).text());
      $('#shop-list-form').submit();
    });

    $('#shop-list-form').find('[page]').on('click', function() {
      $('#Products_page', shopListPage).val($(this).attr('page'));
      $('#shop-list-form').submit();
    });

    $('#shop-list-form').find('#btn-search').on('click', function() {
      $('#Products_page', shopListPage).val(null);
    });
  }//End shop-list-form

  if($('#all-shop-form').length > 0){
    var shopListPage = $('#all-shop-form');
    $('#all-shop-form').find('[page]').on('click', function() {
      $('#Products_page', shopListPage).val($(this).attr('page'));
      $('#all-shop-form').submit();
    });

    $('#all-shop-form').find('[name="letter-search"]').on('click', function() {
      $('#Shops_name', shopListPage).val($(this).text());
      $('#all-shop-form').submit();
    });

  }//End all-shop-form

  if($('#admin-all-shop-form').length > 0){
    var shopListPage = $('#admin-all-shop-form');
    $('#admin-all-shop-form').find('[page]').on('click', function() {
      // $('#shop_page', shopListPage).val($(this).attr('page'));
      // $('#all-shop-form').submit();
      window.location=window.baseUrl + '/shop/allShop/' + $(this).attr('page');
    });

  }//End admin-all-shop-form

  // if($('#view-private-massage-form').length > 0){
  //   var privateMassagePage = $('#view-private-massage-form');
  //   var privateMassageDialog = $('#dialogPrivateMassage');

  //   $('#view-private-massage-form').find('[name="dialogPrivateMassage"]').on('click', function() {
  //     var data = {'PM_ID' : $(this).attr('privatemassageid')};
  //     $.post(window.baseUrl + '/contact/privateMassageDetail', data, function(html) {
  //     }).success(function(data) {
  //       $('#sendFrom', privateMassageDialog).html(data.name);
  //       $('#email', privateMassageDialog).html(data.email);
  //       $('#subject', privateMassageDialog).html(data.subject);
  //       $('#message', privateMassageDialog).html(data.message);
  //       $('#send_to_email', privateMassageDialog).html(data.email);
  //       $('#send_from_email', privateMassageDialog).html(data.shop_email);
  //       $('#Contact_subject', privateMassageDialog).val("RE:"+data.subject);
  //       $('#Contact_cid', privateMassageDialog).val(data.cid);

  //       if (data.picture == null) {
  //         $('#picture', privateMassageDialog).html('<img>');
  //       }else{
  //         $('#picture > img', privateMassageDialog).attr('src', '/praclassic/img/contactPic/'+data.picture);
  //       }

  //       $('#dialogPrivateMassage').modal('show');
  //     });
  //     return false;
  //   });
  // }//End view-private-massage-form

  if($('#setting-web-form').length > 0){

    $('#btn-themeColorUpdate').on('click', function(){
      var data = {'themeColorCode' : $('#themeColor').val(),'themeColor' : $('#themeColor').attr('style')};
      $.post(window.baseUrl + '/admin/changeThemeColor', data, function(html) {
      }).success(function(data) {
        window.location.reload(true);
      });
    });

    $('#btn-themeColorDefault').on('click', function(){
      window.location="settingWebsiteThemeColorDefault";
    });
    
    $('#setting-web-form').find('[name^="number"]').on('change', function() {
      if (confirm('คุณต้องการกำหนดค่าเมนูนี้ใช้หรือไม่')) {
        var data = {'oldNumber' : $(this).attr('no-menu'),'newNumber' : $(this).val()};
        $.post(window.baseUrl + '/admin/changeNumberMenu', data, function(html) {
        }).success(function(data) {
          window.location.reload(true);
        });
      } else {
          window.location.reload(true);
      }

    });

    $('#setting-web-form').find('[name="theme"]').on('change', function() {
      if (confirm('คุณต้องการกำหนดค่าเมนูนี้ใช้หรือไม่')) {
        var data = {'themeId' : $(this).attr('theme-id'),'themeSelect' : $(this).val()};
        $.post(window.baseUrl + '/admin/changeTheme', data, function(html) {
        }).success(function(data) {
          window.location.reload(true);
        });
      } else {
          window.location.reload(true);
      }

    });

    $('#setting-web-form').find('[name="layoutNews"]').on('click', function(){
      if (confirm('คุณต้องการกำหนดค่าเมนูนี้ใช้หรือไม่')) {
        var data = {'value' : $(this).val()};
        $.post(window.baseUrl + '/admin/changeLayoutNews', data, function(html) {
        }).success(function(data) {
          window.location.reload(true);
        });
      } else {
          window.location.reload(true);
      }
    });

    $('#btn-bgUpdate','#setting-web-form').on('click', function(){
      $('#setting-web-form').submit();
    });

    $('#btn-bgDefault','#setting-web-form').on('click', function(){
      window.location="settingWebsiteDefault";
    });

    // $('#customColor3Layout').find('td').on('click', function(){
    //   // $('.tblColorBox').find('.activeColorBox').html($(this).attr('bgcolor'));
    //   $('.tblColorBox').find('.activeColorBox').css( "background-color", $(this).attr('bgcolor'));
    //   $('.tblColorBox').find('.activeColorBox').attr('colorCode',$(this).attr('bgcolor'));
    // });

    // $('.tblColorBox').find('.colorBox').on('click', function(){
    //   var idBox = '#' + $(this).attr('id');
    //   $('.tblColorBox').find('.activeColorBox').removeClass('activeColorBox');
    //   $(idBox).addClass('activeColorBox');
    // });

    $('#btn-colorUpdate').on('click', function(){
      var data = {'colorBox1' : $('#colorBox1').val(),'colorBox2' : $('#colorBox2').val(),'colorBox3' : $('#colorBox3').val()};
      $.post(window.baseUrl + '/admin/changeBgColor', data, function(html) {
      }).success(function(data) {
        window.location.reload(true);
      });
    });

    $('#btn-colorDefault').on('click', function(){
      window.location="settingWebsiteColorDefault";
    });

    $('#btn-fontColorUpdate').on('click', function(){
      var data = {'fontColorHeader' : $('#fontColorHeader').val(),'fontColorContent' : $('#fontColorContent').val()};
      $.post(window.baseUrl + '/admin/changeFontColor', data, function(html) {
      }).success(function(data) {
        window.location.reload(true);
      });
    });

    $('#btn-fontColorDefault').on('click', function(){
      window.location="settingWebsiteFontColorDefault";
    });

    $('#btn-smsUpdate').on('click', function(){
      var data = {'sms_username' : $('#sms_username').val(),'sms_password' : $('#sms_password').val(), 'sms_type' : $('#sms_type').val()};
      $.post(window.baseUrl + '/admin/submitSettingSMS', data, function(html) {
      }).success(function(data) {
        window.location.reload(true);
      });
    });

    $('#btn-smsUpdateDefault').on('click', function(){
      window.location="settingSMSDefault";
    });

    $('#btn-emailUpdate').on('click', function(){
      var data = {'sms_email' : $('#sms_email').val()};
      $.post(window.baseUrl + '/admin/submitSettingEmail', data, function(html) {
      }).success(function(data) {
        window.location.reload(true);
      });
    });

    $('#btn-emailDefault').on('click', function(){
      window.location="settingEmailDefault";
    });

    

  }//End setting-web-form

  if($('#product-discription-form').length > 0){
    $('#blockProduct').on('click', function(){
      if (confirm('คุณต้องการล๊อคพระใช่หรือไม่ ?')) {
        var data = {'productId' : $(this).attr('prd-id')};
        $.post(window.baseUrl + '/site/blockProduct', data, function(html) {
        }).success(function(data) {
          window.location=window.baseUrl + '/site/shop_list';
        });
      } else {
        window.location=window.baseUrl + '/site/shop_list';
      }
    });

  }//End product-discription-form

  if($('#ban-products-form').length > 0){
    $('#unban-product').on('click', function(){
      if (confirm('คุณต้องการปลดล๊อคพระใช่หรือไม่ ?')) {
        var data = {'productId' : $(this).attr('productid')};
        $.post(window.baseUrl + '/products/unbanProduct', data, function(html) {
        }).success(function(data) {
          window.location=window.baseUrl + '/products/banProduct';
        });
      } else {
        window.location=window.baseUrl + '/products/banProduct';
      }
    });

  }//End product-products-form

  if($('#ban-productsInShop-form').length > 0){
    $('#unban-product').on('click', function(){
      if (confirm('คุณต้องการปลดล๊อคพระใช่หรือไม่ ?')) {
        var data = {'productId' : $(this).attr('productid')};
        var shopId = $(this).attr('shopid');
        $.post(window.baseUrl + '/products/unbanProduct', data, function(html) {
        }).success(function(data) {
          window.location=window.baseUrl + '/shop/noteDetail/' + shopId;
        });
      } else {
        window.location=window.baseUrl + '/shop/noteDetail/' + shopId;
      }
    });

  }//End product-productsInShop-form

  if($('#popular-form').length > 0){

    var checkChecked = $('#category-detail-table').find('[type="checkbox"]:checked').length;
    if (checkChecked == 5) {
      $('#category-detail-table').find('[type="checkbox"]:not(:checked)').attr('disabled',true);
    }else{
      $('#category-detail-table').find('[type="checkbox"]').removeAttr('disabled');
    };

    $('#category-detail-table').find('[type="checkbox"]').on('click', function(){
      var countChecked = $('#category-detail-table').find('[type="checkbox"]:checked').length;
      if (countChecked == 5) {
        $('#category-detail-table').find('[type="checkbox"]:not(:checked)').attr('disabled',true);
      }else{
        $('#category-detail-table').find('[type="checkbox"]').removeAttr('disabled');
      };
    });
    // $('#category-detail-table').find('[type="checkbox"]:checked').length;
  }//End popular-form

  

});//End Script


// edition by tan
$(function(){
  $btn_submit = $('#regis-form').find("[type='submit']")
  $('#chkRegis').bind("click", function(){
    if ( $(this).is(':checked') ) { $btn_submit.attr('disabled', false) }
    else { $btn_submit.attr('disabled', true) }
  });
});

function autoFormat(obj,typeCheck){  
  if(typeCheck=='iden'){  
    var pattern=new String("_-____-_____-__-_");
    var pattern_ex=new String("-");
  }else if(typeCheck=='telephone'){  
    var pattern=new String("___-___-____");
    var pattern_ex=new String("-");
  }else{
    var pattern=new String("___-_-_____-_");
    var pattern_ex=new String("-");
  }
  var returnText=new String("");  
  var obj_l=obj.value.length;  
  var obj_l2=obj_l-1;  
  for(i=0;i<pattern.length;i++){             
    if(obj_l2==i && pattern.charAt(i+1)==pattern_ex){  
        returnText+=obj.value+pattern_ex;  
        obj.value=returnText;  
    }  
  }  
  if(obj_l>=pattern.length){  
    obj.value=obj.value.substr(0,pattern.length);             
  }  
} 

function chkNumber(obj,typeCheck){  
  if (event.keyCode < 48 || event.keyCode > 57){
    event.returnValue = false;
  }
} 


function chkUpdate(){  
  $.post(window.baseUrl + '/site/updateShopEndDate', {}, function(html) {
  }).success(function(data) {
    console.log(data.result);
  });
} 
